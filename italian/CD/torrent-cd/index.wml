#use wml::debian::cdimage title="Scaricare le immagini per USB/CD/DVD Debian via BitTorrent" BARETITLE=true
#use wml::debian::toc
#include "$(ENGLISHDIR)/releases/info"
#use wml::debian::installer
#use wml::debian::translation-check translation="67bd189030c9c649cfb9f6581aafc637c6af3b8e" maintainer="Luca Monducci"
#include "$(ENGLISHDIR)/releases/images.data"
#include "$(ENGLISHDIR)/devel/debian-installer/images.data"

<p><a href="https://it.wikipedia.org/wiki/BitTorrent">BitTorrent</a> è un sistema
peer-to-peer per scaricare ottimizzato per un numero elevato di utilizzatori.
Applica un carico minimale sui nostri server perché i client BitTorrent mentre
scaricano inviano parti dei file agli altri client spalmando il carico sulla
rete e rendendo il trasferimento il più rapido possibile.</p>

<div class="tip">
<p>La <strong>prima</strong> immagine per USB/CD/DVD contiene tutti i file necessari
all'installazione di un sistema Debian standard.</p>
</div>

<p>È necessario un client BitTorrent per scaricare le immagini per
USB/CD/DVD Debian con questo sistema. La distribuzione Debian include
<a href="https://packages.debian.org/aria2">aria2</a>,
<a href="https://packages.debian.org/transmission">transmission</a> e
<a href="https://packages.debian.org/ktorrent">KTorrent</a>.
Su altri sistemi operativi (come Windows e MacOS) sono disponibili
<a href="https://www.qbittorrent.org/download">qBittorrent</a> e
<a href="https://www.bittorrent.com/download">BitTorrent</a>.</p>


<h3>Torrent ufficiali per la distribuzione <q>stable</q></h3>

<div class="line">
<div class="item col50">
<p><strong>CD/USB</strong></p>
  <stable-full-cd-torrent>
</div>
<div class="item col50 lastcol">
<p><strong>DVD/USB</strong></p>
  <stable-full-dvd-torrent>
</div>
<div class="clear"></div>
</div>

<p>Prima di procedere con l'installazione si dovrebbe consultare
la documentazione. <strong>Volendo leggere un solo documento</strong> per
l'installazione, leggere l'<a href="$(HOME)/releases/stable/amd64/apa">\
Installation Howto</a>, una <q>passeggiata</q> di tutto il processo di
installazione. Altri documenti utili sono:</p>

<ul>

  <li>La <a href="$(HOME)/releases/stable/installmanual">Guida
  all'installazione</a>, con istruzioni dettagliate per l'installazione</li>

  <li>La <a href="https://wiki.debian.org/DebianInstaller">documentazione del
  Debian-Installer</a>, comprese le FAQ con le domande comuni e relative
  risposte</li>

  <li>L'<a href="$(HOME)/releases/stable/debian-installer/#errata">Errata del
  Debian-Installer</a>, l'elenco dei problemi conosciuti dell'installatore</li>

</ul>

# <h3>Torrent ufficiali per la distribuzione <q>testing</q></h3>
# 
# <ul>
#   <li><strong>CD</strong>:<br />
#   <full-cd-torrent>
#   </li>
# 
#   <li><strong>DVD</strong>:<br />
#   <full-dvd-torrent>
#   </li>
# </ul>

<p>Se è possibile, lasciare il proprio client in esecuzione dopo aver finito
di scaricare in modo da aiutare gli altri utenti a scaricare le immagini più
velocemente!</p>
