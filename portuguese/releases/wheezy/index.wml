#use wml::debian::template title="Informações de lançamento do Debian &ldquo;wheezy&rdquo;"
#include "$(ENGLISHDIR)/releases/info"
#use wml::debian::translation-check translation="fc0a147ef1585beaa5ef80938ca7e595d27fa365"

<p>O Debian <current_release_wheezy> foi
lançado <a href="$(HOME)/News/<current_release_newsurl_wheezy/>"><current_release_date_wheezy></a>
<ifneq "7.0" "<current_release>"
  "O Debian 7.0 foi inicialmente lançado em <:=spokendate('2013-05-04'):>."
/>
O lançamento incluiu muitas
mudanças importantes, descritas no
nosso <a href="$(HOME)/News/2013/20130504">comunicado à imprensa</a> e
nas <a href="releasenotes">notas de lançamento</a>.</p>

<p><strong>O Debian 7 foi substituído pelo
<a href="../jessie/">Debian 8 (<q>jessie</q>)</a>.
# Atualizações de segurança foram descontinuadas em
<:=spokendate('XXXXXXXXXXX'):>.
</strong></p>

<p><strong>O Wheezy também se beneficiou do suporte de longo prazo (Long
Term Support – LTS) até o final de maio de 2018. O LTS era limitado a i386,
amd64, armel e armhf. Para mais informações, por favor, consulte a
<a href="https://wiki.debian.org/LTS">seção LTS da Wiki do Debian</a>.
</strong></p>

<p>Para obter e instalar o Debian, veja a página de informações de instalação e
o guia de instalação. Para atualizar a partir de uma versão mais antiga do
Debian, veja as instruções nas <a href="releasenotes">notas de lançamento</a>.</p>

<p>As seguintes arquiteturas de computadores são suportadas nesta versão:</p>

<ul>
<li><a href="../../ports/amd64/">64-bit PC (amd64)</a>
<li><a href="../../ports/i386/">32-bit PC (i386)</a>
<li><a href="../../ports/armel/">EABI ARM (armel)</a>
<li><a href="../../ports/powerpc/">PowerPC</a>
<li><a href="../../ports/armhf/">Hard Float ABI ARM (armhf)</a>
<li><a href="../../ports/sparc/">SPARC</a>
<li><a href="../../ports/kfreebsd-amd64/">kFreeBSD 64-bit PC (amd64)</a>
<li><a href="../../ports/ia64/">Intel Itanium IA-64</a>
<li><a href="../../ports/mipsel/">MIPS (little endian)</a>
<li><a href="../../ports/kfreebsd-i386/">kFreeBSD 32-bit PC (i386)</a>
<li><a href="../../ports/s390/">IBM S/390</a>
<li><a href="../../ports/mips/">MIPS (big endian)</a>
<li><a href="../../ports/s390x/">IBM System z</a>
</ul>

<p>Apesar dos nossos desejos, podem existir alguns problemas nesta versão,
embora ela tenha sido declarada <em>estável (stable)</em>. Fizemos
<a href="errata">uma lista dos problemas conhecidos mais importantes</a>,
e você sempre pode relatar outros problemas para nós.</p>
