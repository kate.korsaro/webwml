#use wml::debian::translation-check translation="1f51232f1964140bbf71ecfd1fc7bdcbd252e744"
<define-tag pagetitle>Debian 12 is bijgewerkt: 12.1 werd uitgebracht</define-tag>
<define-tag release_date>2023-07-22</define-tag>

#use wml::debian::news

<define-tag release>12</define-tag>
<define-tag codename>bookworm</define-tag>
<define-tag revision>12.1</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>Het Debian-project kondigt met genoegen de eerste update aan van zijn
stabiele distributie Debian <release> (codenaam <q><codename></q>).
Deze tussenrelease voegt voornamelijk correcties voor beveiligingsproblemen toe,
samen met een paar aanpassingen voor ernstige problemen. Beveiligingsadviezen
werden reeds afzonderlijk gepubliceerd en, waar beschikbaar, wordt hiernaar
verwezen.</p>

<p>Merk op dat de tussenrelease geen nieuwe versie van Debian <release> is,
maar slechts een update van enkele van de meegeleverde pakketten. Het is niet
nodig om oude media met <q><codename></q> weg te gooien. Na de installatie
kunnen pakketten worden opgewaardeerd naar de huidige versie door een
bijgewerkte Debian-spiegelserver te gebruiken.</p>

<p>Wie regelmatig updates installeert vanuit security.debian.org zal niet veel
pakketten moeten updaten, en de meeste van dergelijke updates zijn opgenomen in
de tussenrelease.</p>

<p>Nieuwe installatie-images zullen binnenkort beschikbaar zijn op de gewone
plaatsen.</p>

<p>Het upgraden van een bestaande installatie naar deze revisie kan worden
bereikt door het pakketbeheersysteem naar een van de vele HTTP-spiegelservers
van Debian te verwijzen. Een uitgebreide lijst met spiegelservers is
beschikbaar op:</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>




<h2>Oplossingen voor diverse problemen</h2>

<p>Met deze update van stable, de stabiele distributie, worden een paar
belangrijke correcties aangebracht aan de volgende pakketten:</p>

<table border=0>
<tr><th>Pakket</th>               <th>Reden</th></tr>
<correction aide "Correct omgaan met het aanmaken van de systeemgebruiker; verwerking van onderliggende mappen bij gelijke overeenkomst herstellen">
<correction autofs "Verhelpen van vastlopen bij gebruik van LDAP met Kerberos-authenticatie">
<correction ayatana-indicator-datetime "Afspelen van aangepaste alarmgeluiden repareren">
<correction base-files "Update voor de tussenrelease 12.1">
<correction bepasty "Weergave van tekstuploads repareren">
<correction boost1.81 "Bij libboost-json1.81-dev toevoegen van ontbrekende vereiste libboost-json1.81.0">
<correction bup "POSIX ACL's op de juiste manier herstellen">
<correction context "Socket inschakelen in ConTeXt mtxrun">
<correction cpdb-libs "Een kwetsbaarheid voor bufferoverloop repareren [CVE-2023-34095]">
<correction cpp-httplib "Een probleem van CRLF-injectie repareren [CVE-2023-26130]">
<correction crowdsec "Het standaard acquis.yaml aanpassen om ook de journalctl-gegevensbron op te nemen, beperkt tot de unit ssh.service, zodat acquisitie ook zonder het traditionele bestand auth.log werkt; ervoor zorgen dat een ongeldige gegevensbron het mechanisme niet doet uitvallen">
<correction cups "Beveiligingsreparaties: gebruik-na-vrijgave [CVE-2023-34241]; heap bufferoverloop [CVE-2023-32324]">
<correction cvs "Volledig pad naar ssh configureren">
<correction dbus "Nieuwe bovenstroomse stabiele release; probleem van denial of service repareren [CVE-2023-34969]; stoppen met het proberen rekening te houden met DPKG_ROOT, waarbij het kopiëren van systemd's /etc/machine-id wordt hersteld in plaats van het aanmaken van een geheel nieuwe machine-ID">
<correction debian-installer "Linux kernel ABI verhogen naar 6.1.0-10; opnieuw bouwen tegen proposed-updates">
<correction debian-installer-netboot-images "Opnieuw bouwen tegen proposed-updates">
<correction desktop-base "Emerald-alternatieven verwijderen bij het verwijderen van het pakket">
<correction dh-python "Herinvoeren van Breaks+Replaces bij python2 wat nodig is om apt te helpen bij sommige opwaarderingsscenario's">
<correction dkms "Breaks toevoegen tegen verouderde, incompatibele *-dkms pakketten">
<correction dnf "Standaard DNF-const PYTHON_INSTALL_DIR repareren">
<correction dpdk "Nieuwe bovenstroomse stabiele release">
<correction exim4 "Ontleding van argument voor ${run }-uitbreiding repareren; reparatie voor ${srs_encode ..} dat om de 1024 dagen een incorrect resultaat oplevert">
<correction fai "Levensduur van IP-adres herstellen">
<correction glibc "Reparatie voor een bufferoverloop in gmon; oplossing voor een systeemblokkade in getaddrinfo (__check_pf) met uitgestelde annulering; reparatie voor y2038-ondersteuning in strftime op 32-bits architecturen; reparatie voor het ontleden van /etc/gshadow in uitzonderlijke omstandigheden, hetgeen slechte pointers kan opleveren, waardoor segmentatiefouten kunnen optreden in toepassingen; een systeemblokkade in system() oplossen wanneer deze gelijktijdig wordt aangeroepen vanuit meerdere threads; cdefs: de definitie van veiligheidsmacro's beperken tot __FORTIFY_LEVEL &gt; 0 om oude C90-compilers te ondersteunen">
<correction gnome-control-center "Nieuwe bovenstroomse release met bugreparaties">
<correction gnome-maps "Nieuwe bovenstroomse release met bugreparaties">
<correction gnome-shell "Nieuwe bovenstroomse release met bugreparaties">
<correction gnome-software "Nieuwe bovenstroomse release; oplossingen voor gejeugenlekken">
<correction gosa "Waarschuwingen over het verouderd zijn van PHP 8.2 uitschakelen; ontbrekend sjabloon in standaardthema repareren; tabelstijl repareren; gebruik van debugLevel &gt; 0 repareren">
<correction groonga "Documentatiekoppelingen repareren">
<correction guestfs-tools "Beveiligingsupdate [CVE-2022-2211]">
<correction indent "De macro ROUND_UP herstellen en de initiële buffergrootte aanpassen">
<correction installation-guide "Indonesische vertaling aanzetten">
<correction kanboard "Kwaadaardige injectie van HTML-tags in DOM verhelpen [CVE-2023-32685]; reparatie voor op parameters gebaseerde indirecte objectverwijzingen die leiden tot blootstellen van privébestanden [CVE-2023-33956]; oplossing voor ontbrekende toegangscontroles [CVE-2023-33968, CVE-2023-33970]; reparatie voor opgeslagen XSS in functionaliteit Task External Link [CVE-2023-33969]">
<correction kf5-messagelib "Ook zoeken naar subsleutels">
<correction libmatekbd "Geheugenlekken verhelpen">
<correction libnginx-mod-http-modsecurity "Binair bestand herbouwen met pcre2">
<correction libreoffice "Nieuwe bovenstroomse release met bugreparaties">
<correction libreswan "Mogelijk probleem van denial-of-service oplossen [CVE-2023-30570]">
<correction libxml2 "Oplossen van probleem van NULL pointer dereference [CVE-2022-2309]">
<correction linux "Nieuwe bovenstroomse stabiele release; netfilter: nf_tables: genmask niet negeren bij het opzoeken van keten op id [CVE-2023-31248], OOB-toegang voorkomen in nft_byteorder_eval [CVE-2023-35001]">
<correction linux-signed-amd64 "Nieuwe bovenstroomse stabiele release; netfilter: nf_tables: genmask niet negeren bij het opzoeken van keten op id [CVE-2023-31248], OOB-toegang voorkomen in nft_byteorder_eval [CVE-2023-35001]">
<correction linux-signed-arm64 "Nieuwe bovenstroomse stabiele release; netfilter: nf_tables: genmask niet negeren bij het opzoeken van keten op id [CVE-2023-31248], OOB-toegang voorkomen in nft_byteorder_eval [CVE-2023-35001]">
<correction linux-signed-i386 "Nieuwe bovenstroomse stabiele release; netfilter: nf_tables: genmask niet negeren bij het opzoeken van keten op id [CVE-2023-31248], OOB-toegang voorkomen in nft_byteorder_eval [CVE-2023-35001]">
<correction mailman3 "Overbodige crontaak laten vallen; het ordenen van diensten gebruiken wanneer MariaDB aanwezig is">
<correction marco "De juiste venstertitel tonen wanneer dit eigendom is van de systeembeheerder">
<correction mate-control-center "Diverse geheugenlekken verhelpen">
<correction mate-power-manager "Diverse geheugenlekken verhelpen">
<correction mate-session-manager "Diverse geheugenlekken verhelpen; andere clutter-backends dan x11 toestaan">
<correction multipath-tools "Onderliggende paden verbergen voor LVM; voorkomen van een initieel falen van de dienst bij nieuwe installaties">
<correction mutter "Nieuwe bovenstroomse release met bugreparaties">
<correction network-manager-strongswan "Editorcomponent bouwen met GTK 4-ondersteuning">
<correction nfdump "Bij het starten succes teruggeven; reparatie voor segmentatiefout bij optie-ontleding">
<correction nftables "Herstellen van regressie in indeling van set-lijst">
<correction node-openpgp-seek-bzip "Corrigeren van installatie van bestanden in pakket seek-bzip">
<correction node-tough-cookie "Probleem met prototypepollutie oplossen [CVE-2023-26136]">
<correction node-undici "Beveiligingsoplossingen: de HTTP-header <q>Host</q> beschermen tegen CLRF-injectie [CVE-2023-23936]; mogelijke ReDoS (reguliere expressie denial of service) bij Headers.set en Headers.append [CVE-2023-24807]">
<correction node-webpack "Beveiligingsoplossing (cross-realm objecten) [CVE-2023-28154]">
<correction nvidia-cuda-toolkit "Gebundelde openjdk-8-jre bijwerken">
<correction nvidia-graphics-drivers "Nieuwe bovenstroomse stabiele release; beveiligingsoplossingen [CVE-2023-25515 CVE-2023-25516]">
<correction nvidia-graphics-drivers-tesla "Nieuwe bovenstroomse stabiele release; beveiligingsoplossingen [CVE-2023-25515 CVE-2023-25516]">
<correction nvidia-graphics-drivers-tesla-470 "Nieuwe bovenstroomse stabiele release; beveiligingsoplossingen [CVE-2023-25515 CVE-2023-25516]">
<correction nvidia-modprobe "Nieuwe bovenstroomse release met bugreparaties">
<correction nvidia-open-gpu-kernel-modules "Nieuwe bovenstroomse stabiele release; beveiligingsoplossingen [CVE-2023-25515 CVE-2023-25516]">
<correction nvidia-support "Breaks toevoegen tegen incompatibele pakketten van bullseye">
<correction onionshare "Installatie van bureaubladdecoratie repareren">
<correction openvpn "Reparatie voor geheugenlek en loze pointer (mogelijke crashvector)">
<correction pacemaker "Regressie verhelpen in de hulpbronnenplanner">
<correction postfix "Nieuwe bovenstroomse release met bugreparaties; repareren van <q>postfix set-permissions</q>">
<correction proftpd-dfsg "Socket in inetd-stijl niet inschakelen bij installatie">
<correction qemu "Nieuwe bovenstroomse stabiele release; oplossing voor USB-apparaten die niet beschikbaar zijn voor XEN HVM domUs; 9pfs: het openen van speciale bestanden voorkomen [CVE-2023-2861]; problemen in verband met reentrancy-aanval in de LSI-controller oplossen [CVE-2023-0330]">
<correction request-tracker5 "Documentatiekoppelingen repareren">
<correction rime-cantonese "Woorden en tekens sorteren op frequentie">
<correction rime-luna-pinyin "Ontbrekende pinyin-schemagegevens installeren">
<correction samba "Nieuwe bovenstroomse stabiele release; ervoor zorgen dat manpagina's worden gegenereerd tijdens het bouwen; mogelijk maken om Kerberos-tickets op te slaan in de sleutelbos van de kernel; bouwproblemen bij armel en mipsel oplossen; oplossen van windows aanmeldings-/vertrouwensproblemen met de updates van 07-2023 van windows">
<correction schleuder-cli "Beveiligingsprobleem (waardemaskering)">
<correction smarty4 "Oplossing voor willekeurige code-uitvoering [CVE-2023-28447]">
<correction spip "Diverse beveiligingsproblemen; beveiligingsoplossing (filteren van authenticatiegegevens)">
<correction sra-sdk "Oplossen van installatie van bestanden in libngs-java">
<correction sudo "Inbdeling gebeurtenissenlogboek verbeteren">
<correction systemd "Nieuwe bovenstroomse release met bugreparaties">
<correction tang "Raceconditie verhelpen bij het maken/roteren van sleutels [CVE-2023-1672]">
<correction texlive-bin "Socket in luatex standaard uitschakelen [CVE-2023-32668]; installatie op i386 mogelijk maken">
<correction unixodbc "Breaks+Replaces toevoegen tegen odbcinst1debian1">
<correction usb.ids "Inbegrepen gegevens bijwerken">
<correction vm "Byte-compilatie uitschakelen">
<correction vte2.91 "Nieuwe bovenstroomse release met bugreparaties">
<correction xerial-sqlite-jdbc "Een UUID gebruiken voor verbindings-ID [CVE-2023-32697]">
<correction yajl "Beveiligingsoplossing voor geheugenlek; oplossen van probleem van denial of service [CVE-2017-16516], probleem met overloop van gehele getallen [CVE-2022-24795]">
</table>


<h2>Beveiligingsupdates</h2>


<p>Met deze revisie worden de volgende beveiligingsupdates toegevoegd aan de
stabiele release. Het beveiligingsteam heeft voor elk van deze updates
al een advies uitgebracht:</p>

<table border=0>
<tr><th>Advies ID</th>  <th>Pakket</th></tr>
<dsa 2023 5423 thunderbird>
<dsa 2023 5425 php8.2>
<dsa 2023 5427 webkit2gtk>
<dsa 2023 5428 chromium>
<dsa 2023 5429 wireshark>
<dsa 2023 5430 openjdk-17>
<dsa 2023 5432 xmltooling>
<dsa 2023 5433 libx11>
<dsa 2023 5434 minidlna>
<dsa 2023 5435 trafficserver>
<dsa 2023 5436 hsqldb1.8.0>
<dsa 2023 5437 hsqldb>
<dsa 2023 5439 bind9>
<dsa 2023 5440 chromium>
<dsa 2023 5443 gst-plugins-base1.0>
<dsa 2023 5444 gst-plugins-bad1.0>
<dsa 2023 5445 gst-plugins-good1.0>
<dsa 2023 5446 ghostscript>
<dsa 2023 5447 mediawiki>
<dsa 2023 5448 linux-signed-amd64>
<dsa 2023 5448 linux-signed-arm64>
<dsa 2023 5448 linux-signed-i386>
<dsa 2023 5448 linux>
<dsa 2023 5449 webkit2gtk>
<dsa 2023 5450 firefox-esr>
<dsa 2023 5451 thunderbird>
</table>



<h2>Het Debian-installatiesysteem</h2>
<p>Het installatiesysteem werd bijgewerkt om de reparaties die met deze tussenrelease in stable, de stabiele release, opgenomen werden, toe te voegen.</p>

<h2>URL's</h2>

<p>De volledige lijsten met pakketten die met deze revisie gewijzigd werden:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>De huidige stabiele distributie:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/stable/">
</div>

<p>Voorgestelde updates voor de stabiele distributie:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/proposed-updates">
</div>

<p>informatie over de stabiele distributie (notities bij de release, errata, enz.):</p>

<div class="center">
  <a
  href="$(HOME)/releases/stable/">https://www.debian.org/releases/stable/</a>
</div>

<p>Beveiligingsaankondigingen en -informatie:</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>Over Debian</h2>

<p>Het Debian-project is een samenwerkingsverband van ontwikkelaars van vrije
software die vrijwillig tijd en moeite steken in het produceren van het
volledig vrije besturingssysteem Debian.</p>

<h2>Contactinformatie</h2>

<p>Ga voor verdere informatie naar de webpagina's van Debian op
<a href="$(HOME)/">https://www.debian.org/</a>, stuur een e-mail naar
&lt;press@debian.org&gt;, of neem contact met het release-team voor de stabiele
release op &lt;debian-release@lists.debian.org&gt;.</p>


