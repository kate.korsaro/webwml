msgid ""
msgstr ""
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../english/index.def:8
msgid "The Universal Operating System"
msgstr ""

#: ../../english/index.def:12
msgid "DebConf is underway!"
msgstr ""

#: ../../english/index.def:15
msgid "DebConf Logo"
msgstr ""

#: ../../english/index.def:19
msgid "DC22 Group Photo"
msgstr ""

#: ../../english/index.def:22
msgid "DebConf22 Group Photo"
msgstr ""

#: ../../english/index.def:26
msgid "DC23 Group Photo"
msgstr ""

#: ../../english/index.def:29
msgid "DebConf23 Group Photo"
msgstr ""

#: ../../english/index.def:33
msgid "DC24 Group Photo"
msgstr ""

#: ../../english/index.def:36
msgid "DebConf24 Group Photo"
msgstr ""

#: ../../english/index.def:40
msgid "MiniDebConf Berlin 2024"
msgstr ""

#: ../../english/index.def:43
msgid "Group photo of the MiniDebConf Berlin 2024"
msgstr ""

#: ../../english/index.def:47
msgid "MiniDebConf Brasília 2023"
msgstr ""

#: ../../english/index.def:50
msgid "Group photo of the MiniDebConf Brasília 2023"
msgstr ""

#: ../../english/index.def:54
msgid "Mini DebConf Regensburg 2021"
msgstr ""

#: ../../english/index.def:57
msgid "Group photo of the MiniDebConf in Regensburg 2021"
msgstr ""

#: ../../english/index.def:61
msgid "Screenshot Calamares Installer"
msgstr ""

#: ../../english/index.def:64
msgid "Screenshot from the Calamares installer"
msgstr ""

#: ../../english/index.def:68
#: ../../english/index.def:71
msgid "Debian is like a Swiss Army Knife"
msgstr ""

#: ../../english/index.def:75
msgid "People have fun with Debian"
msgstr ""

#: ../../english/index.def:78
msgid "Debian people at Debconf18 in Hsinchu really having fun"
msgstr ""

