#use wml::debian::translation-check translation="08d95547b797f8dd3e3b1575b759cba78e627b32" maintainer="Jean-Pierre Giraud"
<define-tag pagetitle>Le projet Debian déplore la perte de Peter De Schrijver</define-tag>
<define-tag release_date>2024-07-25</define-tag>
#use wml::debian::news
# $Id$

<p>
Le projet Debian déplore la brutale disparition de notre compagnon développeur
et ami Peter De Schrijver.
</p>

<p>
Beaucoup d'entre nous reconnaissaient en Peter une personne serviable et
dévouée, et nous appréciions ses contributions à notre projet et à la
communauté Linux.
</p>

<p>
Peter était un visage habituel et familier de nombreuses conférences et
rencontres à travers le monde.
</p>

<p>
Peter était très apprécié pour son expertise technique dans la résolution de
problèmes et pour sa volonté de partager ce savoir. Quand on lui demandait
<q>sur quoi travaillez-vous ?</q>, Peter prenait souvent le temps
d'expliquer de manière intelligible des choses que l'on trouvait extrêmement
compliquées, ou vous montrait lui-même ses compétences techniques de haut
niveau en réalisant des tâches comme traduire un binaire désassemblé en code
source C.
</p>

<p>
Le travail de Peter, ses idéaux et son souvenir laissent un héritage
remarquable et une perte ressentie dans le monde entier, non seulement dans
les communautés avec lesquelles il interagissait mais aussi dans celles qu'il
a inspirées et touchées.
</p>

<p>
Nos pensées vont à sa famille.
</p>

<h2>À propos de Debian</h2>

<p>
Le projet Debian est une organisation de développeurs de logiciels libres qui
offrent volontairement leur temps et leurs efforts afin de produire le système
complètement libre Debian.</p>

<h2>Contact</h2>
<p>Pour de plus amples informations, veuillez consulter le site internet
de Debian <a href="$(HOME)/">https://www.debian.org/</a> ou envoyez un
courrier électronique à &lt;<a href="mailto:press@debian.org">press@debian.org</a>&gt;.</p>
