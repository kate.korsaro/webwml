#use wml::debian::template title="Réplicas de Debian en todo el mundo" BARETITLE=true
#use wml::debian::translation-check translation="d83882f2dd9b20c81d0cb83b32c028d9765578c7" maintainer="Laura Arjona Reina"

<p>Esta es una lista <strong>completa</strong> de réplicas de Debian.
Para cada sitio, se listan los distintos tipos de material disponible,
junto con el método de acceso para cada tipo.</p>

<p>Se replica lo siguiente:</p>

<dl>
<dt><strong>Paquetes</strong></dt>
	<dd>El conjunto de paquetes de Debian.</dd>
<dt><strong>Imágenes de CD</strong></dt>
	<dd>Imágenes de CD oficiales de Debian. Vea
	<url "https://www.debian.org/CD/"> para más detalles.</dd>
<dt><strong>Versiones antiguas</strong></dt>
	<dd>El archivo de versiones antiguas de Debian.
	<br />
	Algunas de las versiones antiguas también incluían el archivo llamado
	«debian-non-US», con secciones para paquetes de Debian que no podían
	distribuirse en EE.UU. debido a patentes de software o al uso de
	criptografía.
	Las actualizaciones de debian-non-US se discontinuaron con Debian 3.1.</dd>
</dl>

<p>Puede haber los siguientes métodos de acceso:</p>

<dl>
<dt><strong>HTTP</strong></dt>
	<dd>Acceso estándar web, pero puede usarse para descarga de ficheros.</dd>
<dt><strong>rsync</strong></dt>
	<dd>Una manera eficiente de hacer réplicas.</dd>
</dl>

<p>La copia de referencia de la siguiente lista siempre se puede
consultar en: <url "https://www.debian.org/mirror/list">.

<br /> Consulte la página <url "https://www.debian.org/mirror/">
   para todo lo demás que quiera saber sobre réplicas.</p>

<hr style="height:1">

<p>Saltar directamente a un país de la lista:
<br />

#include "$(ENGLISHDIR)/mirror/list-full.inc"
