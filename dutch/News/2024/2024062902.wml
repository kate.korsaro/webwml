#use wml::debian::translation-check translation="67e7c0f3759a5673d880c47304c13ca117ee5d0c"
<define-tag pagetitle>Debian 11 is bijgewerkt:: 11.10 werd uitgebracht</define-tag>
<define-tag release_date>2024-06-29</define-tag>
#use wml::debian::news

<define-tag release>11</define-tag>
<define-tag codename>bullseye</define-tag>
<define-tag revision>11.10</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>Het Debian-project kondigt met genoegen de tiende update aan van oldstable,
zijn voorgaande stabiele distributie, Debian <release> (codenaam <q><codename></q>).
Deze tussenrelease voegt voornamelijk correcties voor beveiligingsproblemen toe,
samen met een paar aanpassingen voor ernstige problemen. Beveiligingsadviezen
werden reeds afzonderlijk gepubliceerd en, waar beschikbaar, wordt hiernaar
verwezen.</p>

<p>Merk op dat de tussenrelease geen nieuwe versie van Debian <release> is,
maar slechts een update van enkele van de meegeleverde pakketten. Het is niet
nodig om oude media met <q><codename></q> weg te gooien. Na de installatie
kunnen pakketten worden opgewaardeerd naar de huidige versie door een
bijgewerkte Debian-spiegelserver te gebruiken.</p>

<p>Wie regelmatig updates installeert vanuit security.debian.org zal niet veel
pakketten moeten updaten, en de meeste van dergelijke updates zijn opgenomen in
de tussenrelease.</p>

<p>Nieuwe installatie-images zullen binnenkort beschikbaar zijn op de gewone
plaatsen.</p>

<p>Het upgraden van een bestaande installatie naar deze revisie kan worden
bereikt door het pakketbeheersysteem naar een van de vele HTTP-spiegelservers
van Debian te verwijzen. Een uitgebreide lijst met spiegelservers is
beschikbaar op:</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>




<h2>Oplossingen voor diverse problemen</h2>

<p>Met deze update van oldstable, de voorgaande stabiele distributie, worden een paar
belangrijke correcties aangebracht aan de volgende pakketten:</p>

<table border=0>
<tr><th>Pakket</th>               <th>Reden</th></tr>
<correction allegro5 "Problemen met bufferoverloop verhelpen [CVE-2021-36489]">
<correction amavisd-new "Velerlei grensparameters die conflicterende waarden bevatten verwerken [CVE-2024-28054]">
<correction bart "Oplossing voor bouwtestfouten door een zwevendekommavergelijking te versoepelen">
<correction bart-cuda "Oplossing voor bouwtestfouten door een zwevendekommavergelijking te versoepelen">
<correction base-files "Update voor de tussenrelease">
<correction cloud-init-22.4.2 "Vervanging voor cloud-init-pakket met latere versie introduceren">
<correction cpu "Precies één definitie van globalLdap opgeven in de ldap-plug-in">
<correction curl "Geheugenlek repareren wanneer HTTP/2-serverpush wordt afgebroken [CVE-2024-2398]">
<correction debian-installer "Linux kernel ABI verhogen naar 5.10.0-30; opnieuw opbouwen tegen proposed-updates">
<correction debian-installer-netboot-images "Opnieuw opbouwen tegen proposed-updates">
<correction debsig-verify "Herbouwen voor verouderd Built-Using">
<correction deets "Herbouwen voor verouderd Built-Using">
<correction distro-info-data "Intenties kenbaar maken voor bullseye/bookworm; gegevens uit het verleden corrigeren; Ubuntu 24.10 toevoegen">
<correction django-mailman3 "Berichten voor het archiveren afschrobben">
<correction dns-root-data "Root hints bijwerken; verlopen beveiligingsinformatie bijwerken">
<correction emacs "Beschermen tegen onveilige externe bronnen [CVE-2024-30203 CVE-2024-30204 CVE-2024-30205]; oplossing voor geheugenlek in patch voor CVE-2022-48337">
<correction galera-4 "Nieuwe bovenstroomse release met probleemoplossingen; bijwerken van de bovenstroomse sleutel voor het ondertekenen van de release; datumgerelateerde testfouten voorkomen">
<correction gdk-pixbuf "ANI: Bestanden met meerdere anih-stukken verwerpen [CVE-2022-48622]; ANI: Bestanden met meerdere INAM- of IART-stukken verwerpen; ANI: De grootte van anih-stukken valideren">
<correction glib2.0 "Een (zeldzaam) geheugenlek repareren">
<correction gnutls28 "Oplossing voor bevestigingsfout bij het verifiëren van een certificaatketen met een cyclus van kruiselingse handtekeningen [CVE-2024-0567]; timingaanval via nevenkanalen binnen RSA-PSK-sleuteluitwisseling repareren [CVE-2024-0553]">
<correction gross "stack-bufferoverloop repareren [CVE-2023-52159]">
<correction hovercraft "python3-setuptools vereisen">
<correction imlib2 "Kwetsbaarheid voor heap-bufferoverloop bij het gebruik van de tgaflip-functie in loader_tga.c oplossen [CVE-2024-25447 CVE-2024-25448 CVE-2024-25450]">
<correction intel-microcode "Oplossingen voor INTEL-SA-INTEL-SA-00972 [CVE-2023-39368], INTEL-SA-INTEL-SA-00982 [CVE-2023-38575], INTEL-SA-INTEL-SA-00898 [CVE-2023-28746], INTEL-SA-INTEL-SA-00960 [CVE-2023-22655] en INTEL-SA-INTEL-SA-01045 [CVE-2023-43490]; lenigen van INTEL-SA-01051 [CVE-2023-45733], INTEL-SA-01052 [CVE-2023-46103], INTEL-SA-01036 [CVE-2023-45745,  CVE-2023-47855] en niet-gespecificeerde functionele problemen op verschillende Intel-processors">
<correction jose "Oplossing voor een mogelijk probleem van denial-of-service [CVE-2023-50967]">
<correction json-smart "oplossing voor buitensporige recursie die tot stack-overloop leidt [CVE-2023-1370]; oplossing voor denial-of-service via een bewerkt verzoek [CVE-2021-31684]">
<correction lacme "Validatielogica na uitgifte verbeteren">
<correction libapache2-mod-auth-openidc "Reparatie voor ontbrekende invoervalidatie die tot DoS leidt [CVE-2024-24814]">
<correction libjwt "Oplossing voor timingaanval over nevenkanaal via strcmp() [CVE-2024-25189]">
<correction libkf5ksieve "Het lekken van wachtwoorden in logboeken op de server voorkomen">
<correction libmicrohttpd "Oplossing voor lezen buiten het bereik bij bewerkte POST-verzoeken [CVE-2023-27371]">
<correction libssh2 "Probleem met geheugencontrole buiten het bereik opgelost in _libssh2_packet_add [CVE-2020-22218]">
<correction links2 "Herbouwen voor verouderd Built-Using">
<correction nano "Probleem met kwaadaardige symbolische koppeling oplossen [CVE-2024-5742]">
<correction ngircd "De optie <q>SSLConnect</q> voor binnenkomende verbindingen respecteren; validering van servercertificaat op serverlinks (S2S-TLS); METADATA: oplossing voor het uitschakelen van <q>cloakhost</q>">
<correction nvidia-graphics-drivers "Einde van ondersteuning voor Tesla 450 stuurprogramma's; bouwen van libnvidia-fbc1 voor arm64; bovenstroomse beveiligingsoplossingen [CVE-2022-42265 CVE-2024-0074 CVE-2024-0078]; nieuwe bovenstroomse stabiele release; beveiligingsoplossingen [CVE-2024-0090 CVE-2024-0092]; reparatie voor het bouwen op ppc64el">
<correction nvidia-graphics-drivers-tesla-450 "Ombouwen naar overgangspakketten">
<correction nvidia-graphics-drivers-tesla-470 "Nieuwe bovenstroomse LTS-release [CVE-2024-0074 CVE-2024-0078 CVE-2022-42265 CVE-2024-0090 CVE-2024-0092]; reparatie voor het bouwen op ppc64el">
<correction nvidia-settings "Nieuwe bovenstroomse release met probleemoplossingen; bouwen voor ppc64el">
<correction org-mode "Beschermen tegen onveilige externe bronnen [CVE-2024-30203 CVE-2024-30204 CVE-2024-30205]">
<correction php-composer-xdebug-handler "Laden van systeemvereisten afdwingen">
<correction php-doctrine-annotations "Laden van systeemvereisten afdwingen">
<correction php-phpseclib "Laden van systeemvereisten afdwingen; isPrime() en randomPrime() afschermen voor BigInteger [CVE-2024-27354]; lengte van OID in ASN1 beperken [CVE-2024-27355]; reparatie voor BigInteger getLength()">
<correction php-proxy-manager "Laden van systeemvereisten afdwingen">
<correction php-symfony-contracts "Laden van systeemvereisten afdwingen">
<correction php-zend-code "Laden van systeemvereisten afdwingen">
<correction phpseclib "Laden van systeemvereisten afdwingen; isPrime() en randomPrime() afschermen voor BigInteger [CVE-2024-27354]; lengte van OID in ASN1 beperken [CVE-2024-27355]; reparatie voor BigInteger getLength()">
<correction postfix "Bovenstroomse release met probleemoplossingen">
<correction postgresql-13 "Nieuwe bovenstroomse stabiele release">
<correction pypdf2 "Reparatie van kwadratische runtime waarbij in slecht opgemaakte PDF de xref-markering ontbreekt [CVE-2023-36810]; reparatie voor oneindige lus met bewerkte invoer [CVE-2022-24859]">
<correction python-aiosmtpd "Probleem met SMTP-smokkel oplossen [CVE-2024-27305]; oplossing voor probleem met niet-versleutelde STARTTLS-commando-injectie [CVE-2024-34083]">
<correction python-dnslib "Transactie-ID valideren in client.py">
<correction python-idna "Oplossing voor een probleem van denial-of-service [CVE-2024-3651]">
<correction python-stdnum "Oplossen van een mislukking om op te bouwen vanuit de broncode wanneer de testdatum onvoldoende ver in de toekomst ligt">
<correction qtbase-opensource-src "Beveiligingsoplossingen [CVE-2022-25255 CVE-2023-24607 CVE-2023-32762 CVE-2023-32763 CVE-2023-33285 CVE-2023-34410 CVE-2023-37369 CVE-2023-38197 CVE-2023-51714 CVE-2024-25580]">
<correction reportbug "De toewijzing van de suitenaam aan de codenaam repareren om de bookworm-release weer te geven">
<correction rust-cbindgen-web "Nieuw bronpakket ter ondersteuning van het opbouwen van nieuwere Firefox ESR-versies">
<correction rustc-web "Ondersteuning voor firefox-esr en thunderbird in bullseye voor LTS">
<correction sendmail "Probleem met SMTP-smokkel oplossen [CVE-2023-51765]; een vergeten configuratie toevoegen voor het standaard weigeren van NUL">
<correction symfony "Laden van systeemvereisten afdwingen; DateTypeTest: verzekeren dat het ingediende jaar aanvaardbaar is">
<correction systemd "Meson: architectuurfiltering in syscall-lijst weglaten; tijdzone uitschakelen voordat tijdzonegevoelige unit-tests worden uitgevoerd">
<correction wpa "Probleem met authenticatieomzeiling oplossen [CVE-2023-52160]">
</table>


<h2>Beveiligingsupdates</h2>


<p>Met deze revisie worden de volgende beveiligingsupdates toegevoegd aan de release oldstable. Het beveiligingsteam heeft voor elk van deze updates
al een advies uitgebracht:</p>

<table border=0>
<tr><th>Advies-ID</th>  <th>Pakket</th></tr>
<dsa 2022 5146 puma>
<dsa 2023 5360 emacs>
<dsa 2023 5575 webkit2gtk>
<dsa 2023 5580 webkit2gtk>
<dsa 2024 5596 asterisk>
<dsa 2024 5616 ruby-sanitize>
<dsa 2024 5618 webkit2gtk>
<dsa 2024 5619 libgit2>
<dsa 2024 5620 unbound>
<dsa 2024 5621 bind9>
<dsa 2024 5622 postgresql-13>
<dsa 2024 5624 edk2>
<dsa 2024 5625 engrampa>
<dsa 2024 5627 firefox-esr>
<dsa 2024 5628 imagemagick>
<dsa 2024 5630 thunderbird>
<dsa 2024 5631 iwd>
<dsa 2024 5632 composer>
<dsa 2024 5635 yard>
<dsa 2024 5637 squid>
<dsa 2024 5638 libuv1>
<dsa 2024 5640 openvswitch>
<dsa 2024 5641 fontforge>
<dsa 2024 5643 firefox-esr>
<dsa 2024 5644 thunderbird>
<dsa 2024 5645 firefox-esr>
<dsa 2024 5646 cacti>
<dsa 2024 5647 samba>
<dsa 2024 5650 util-linux>
<dsa 2024 5651 mediawiki>
<dsa 2024 5652 py7zr>
<dsa 2024 5653 gtkwave>
<dsa 2024 5657 xorg-server>
<dsa 2024 5659 trafficserver>
<dsa 2024 5660 php7.4>
<dsa 2024 5662 apache2>
<dsa 2024 5663 firefox-esr>
<dsa 2024 5664 jetty9>
<dsa 2024 5666 flatpak>
<dsa 2024 5667 tomcat9>
<dsa 2024 5669 guix>
<dsa 2024 5670 thunderbird>
<dsa 2024 5671 openjdk-11>
<dsa 2024 5672 openjdk-17>
<dsa 2024 5673 glibc>
<dsa 2024 5678 glibc>
<dsa 2024 5679 less>
<dsa 2024 5681 linux-signed-amd64>
<dsa 2024 5681 linux-signed-arm64>
<dsa 2024 5681 linux-signed-i386>
<dsa 2024 5681 linux>
<dsa 2024 5682 glib2.0>
<dsa 2024 5682 gnome-shell>
<dsa 2024 5684 webkit2gtk>
<dsa 2024 5685 wordpress>
<dsa 2024 5686 dav1d>
<dsa 2024 5688 atril>
<dsa 2024 5690 libreoffice>
<dsa 2024 5691 firefox-esr>
<dsa 2024 5692 ghostscript>
<dsa 2024 5693 thunderbird>
<dsa 2024 5695 webkit2gtk>
<dsa 2024 5698 ruby-rack>
<dsa 2024 5700 python-pymysql>
<dsa 2024 5702 gst-plugins-base1.0>
<dsa 2024 5703 linux-signed-amd64>
<dsa 2024 5703 linux-signed-arm64>
<dsa 2024 5703 linux-signed-i386>
<dsa 2024 5703 linux>
<dsa 2024 5704 pillow>
<dsa 2024 5707 vlc>
<dsa 2024 5709 firefox-esr>
<dsa 2024 5711 thunderbird>
<dsa 2024 5713 libndp>
<dsa 2024 5714 roundcube>
<dsa 2024 5715 composer>
</table>


<h2>Verwijderde pakketten</h2>

<p>De volgende pakketten zijn verwijderd door omstandigheden waar wij geen invloed op hadden:</p>

<table border=0>
<tr><th>Pakket</th>               <th>Reden</th></tr>
<correction phppgadmin "Veiligheidsproblemen">
<correction pytest-salt-factories "Enkel nodig voor het 'salten' van 'te-verwijderen'">
<correction pytest-testinfra "Enkel nodig voor het 'salten' van 'te-verwijderen'">
<correction salt "Onhoudbaar, niet onderhouden">
<correction snort "Beveiligingsproblemen, niet onderhouden">

</table>

<h2>Het Debian-installatiesysteem</h2>
<p>Het installatiesysteem werd bijgewerkt om de reparaties die met deze tussenrelease in oldstable, de vorige stabiele release, opgenomen werden, toe te voegen.</p>

<h2>URL's</h2>

<p>De volledige lijsten met pakketten die met deze revisie gewijzigd werden:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>De huidige distributie oldstable:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/oldstable/">
</div>

<p>Voorgestelde updates voor de distributie oldstable:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/oldstable-proposed-updates">
</div>

<p>informatie over de distributie oldstable (notities bij de release, errata, enz.):</p>

<div class="center">
  <a
  href="$(HOME)/releases/oldstable/">https://www.debian.org/releases/oldstable/</a>
</div>

<p>Beveiligingsaankondigingen en -informatie:</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>Over Debian</h2>

<p>Het Debian-project is een samenwerkingsverband van ontwikkelaars van vrije
software die vrijwillig tijd en moeite steken in het produceren van het
volledig vrije besturingssysteem Debian.</p>

<h2>Contactinformatie</h2>

<p>Ga voor verdere informatie naar de webpagina's van Debian op
<a href="$(HOME)/">https://www.debian.org/</a>, stuur een e-mail naar
&lt;press@debian.org&gt;, of neem contact met het release-team voor de stabiele
release op &lt;debian-release@lists.debian.org&gt;.</p>


