#use wml::debian::translation-check translation="175886d0881f4c17bea683840199ffa0e055b466"
<define-tag pagetitle>DebConf24 beëindigd in Busan en DebConf25 data aangekondigd</define-tag>
<define-tag release_date>2024-08-11</define-tag>
<define-tag frontpage>yes</define-tag>

#use wml::debian::news

<p>
Op zaterdag 3 augustus 2024 werd de jaarlijkse conferentie van ontwikkelaars en
medewerkers van Debian afgesloten.
</p>

<p>
Ruim 339 deelnemers uit 48 landen van over de hele wereld kwamen bijeen voor in
totaal 108 evenementen, bestaande uit meer dan 50 lezingen en discussies, 37
Birds of a Feather-sessies (BoF - informele ontmoeting tussen ontwikkelaars en
gebruikers), 12 workshops en activiteiten ter ondersteuning van het verder
ontwikkelen van onze distributie en vrije software (25 patches ingediend bij de
Linux-kernel). Het was een gelegenheid om te leren van onze mentoren en
collega's, onze community op te bouwen en een beetje plezier te hebben.
</p>

<p>
De conferentie werd voorafgegaan door het jaarlijkse
<a href="https://wiki.debian.org/DebCamp">DebCamp</a>, een hackingsessie die
van 21 tot en met 27 juli werd gehouden. Tijdens deze sessie kwamen
Debian-ontwikkelaars en -medewerkers bijeen om zich te richten op hun eigen
Debian-gerelateerde projecten of om te werken in teamsprints gericht op
persoonlijke samenwerking bij de ontwikkeling van Debian.

Dit jaar werd er een BootCamp gehouden voor nieuwkomers met een GPG-workshop en
een focus op een inleiding tot het maken van .deb-bestanden (Debian pakketten),
georganiseerd door een team van toegewijde mentoren die praktische ervaring met
Debian deelden en een dieper inzicht boden in hoe men in die gemeenschap kan
werken en eraan kan bijdragen.
</p>

<p>
De daadwerkelijke conferentie van Debian-ontwikkelaars begon op zondag 28 juli 2024.

Naast de traditionele lezing van de projectleider, 'Bits from the DPL', het
doorlopend ondertekenen van sleutels, korte presentaties in de vorm van
lightning talks en de aankondiging van de DebConf25 van volgend jaar, vonden er
verschillende sessies plaats waarin interne projecten en teams een update gaven.

Veel van de vergadersessies die plaats vonden werden geleid door onze
technische kernteams met het gebruikelijke en nuttige Kennismaking met het
Technisch Comité en het Ftp-team en een aantal BoF's over verpakkingsbeleid en
Debian-infrastructuur, inclusief een lezing over APT en het Debian
Installatiesysteem, en een overzicht van de eerste elf jaar van het project
Reproducible Builds (deterministische compilatie). Er werden verschillende
lezingen gehouden over internationalisering en lokalisatie. De teams van de
programmeertalen Python, Perl, Ruby en Gol evenals het Med-team gaven updates
over hun werk en hun doelstellingen.

Meer dan vijftien BoF's en lezingen over gemeenschap, diversiteit en lokale
mobiliseringsactiviteiten zetten het werk van verschillende teams die betrokken
zijn bij het sociale aspect van onze gemeenschap in het licht. Ook dit jaar
deelde Debian Brazilië met ons hun strategie en acties om nieuwe medewerkers en
leden aan te trekken en te behouden en welke mogelijkheden er zowel in Debian
als in F/OSS liggen.
</p>

<p>
Het <a href="https://debconf24.debconf.org/schedule/">programma</a>
werd elke dag bijgewerkt met geplande en ad-hocactiviteiten die door deelnemers
tijdens de conferentie werden aangebracht. Er vonden verschillende traditionele
activiteiten plaats: een jobbeurs, een poëzievoorstelling, de traditionele
kaas- en wijnavond, de groepsfoto's en de daguitstappem.
</p>

<p>
Voor wie er niet bij kon zijn, werden de meeste lezingen en sessies live
uitgezonden en opgenomen en werden de video's beschikbaar gesteld via een link
bij de samenvatting in het <a href="https://debconf24.debconf.org/schedule/">programmaschema</a>.
Bijna alle sessies maakten deelname op afstand mogelijk via IRC-berichtenapps
of online collaboratieve tekstdocumenten. Hierdoor konden deelnemers op afstand
'in de zaal' zijn en vragen stellen of opmerkingen delen met de spreker of het
aanwezige publiek.
</p>

<p>
Tijdens DebConf24 werd er meer dan 6,8 TiB (4,3 TiB in 2023) data gestreamd,
stonden er 91,25 uur (55 in 2023) lezingen gepland, waren er 20
netwerktoegangspunten, werden er 1,6 km glasvezelkabels (1 kapotte
glasvezelkabel...) en 2,2 km UTP-kabels aangelegd, waren er geogelokaliseerde
kijkers uit meer dan 20 landen, werden er 354 T-shirts verspreid, vonden er 3
dagtochten plaats en werden er tot wel 200 maaltijden per dag gepland.

Al deze evenementen, activiteiten, gesprekken en streams in combinatie met onze
liefde voor, interesse in en deelname aan Debian en F/OSS hebben deze
conferentie zeker tot een algemeen succes gemaakt, zowel hier in
Busan, Zuid-Korea, als online over de hele wereld.
</p>

<p>
De <a href="https://debconf24.debconf.org/">DebConf24 website</a>
blijft actief voor archiveringsdoeleinden en zal links blijven aanbieden naar
de presentaties en video's van lezingen en evenementen.
</p>

<p>
Volgend jaar zal <a href="https://wiki.debian.org/DebConf/25">Debconf25</a>
plaats vinden in Brest, Frankrijk, van maandag 7 juli tot en met zondag 20
juli 2025. Zoals de traditie het wil, zullen de lokale organisatoren uit
Frankrijk vóór de volgende DebConf de conferentie-activiteiten beginnen met
DebCamp met bijzondere aandacht voor individueel en teamwerk om de distributie
te verbeteren.
</p>

<p>
DebConf streeft naar een veilige en gastvrije omgeving voor alle deelnemers.
Zie de <a href="https://debconf24.debconf.org/about/coc/">webpagina over de
gedragscode op de DebConf24-website</a> voor meer informatie hierover.
</p>

<p>
Debian dankt de inzet van tal van <a href="https://debconf24.debconf.org/sponsors/">sponsors</a> ter ondersteuning van DebConf24, met name onze
Platinum Sponsors:
<a href="https://www.infomaniak.com">Infomaniak</a>,
<a href="https://www.proxmox.com/">Proxmox</a>,
en <a href="https://www.river.com/">Wind River</a>.
</p>

<p>
We willen ook graag dank zeggen aan ons video- en infrastructuurteam, het
DebConf24- en DebConf-comité, ons gastland Zuid-Korea en iedereen die een
bijdrage heeft geleverd aan dit evenement en aan Debian in het algemeen.

Dank aan allen voor jullie inzet om Debian "het universele besturingssysteem"
te laten blijven.

Tot volgend jaar!
</p>

<h2>Over Debian</h2>

<p>
Het Debian-project werd in 1993 opgericht door Ian Murdock om een echt vrij
gemeenschapsproject te zijn. Sindsdien is het project uitgegroeid tot een van
de grootste en meest invloedrijke opensourceprojecten. Duizenden vrijwilligers
van over de hele wereld werken samen om Debian-software te creëren en te
onderhouden. Beschikbaar in 70 talen en een groot aantal computertypes
ondersteunend, noemt Debian zichzelf het <q>universele besturingssysteem</q>.
</p>

<h2>Over DebConf</h2>

<p>
DebConf is de ontwikkelaarsconferentie van het Debian-project. Naast een
volledig programma van technische, sociale en beleidslezingen biedt DebConf de
mogelijkheid voor ontwikkelaars, medewerkers en andere geïnteresseerden om
elkaar persoonlijk te ontmoeten en nauwer samen te werken. Sinds 2000 vindt het
jaarlijks plaats op verschillende locaties als Schotland, Argentinië, Bosnië en
Herzegovina en India. Meer informatie over DebConf is beschikbaar op
<a href="https://debconf.org/">https://debconf.org</a>.
</p>

<h2>Over Infomaniak</h2>
<p>
<a href="https://www.infomaniak.com">Infomaniak</a> is een onafhankelijke
cloudserviceprovider die in heel Europa erkend wordt om zijn inzet voor
privacy, de lokale economie en het milieu. Het bedrijf registreerde een groei
van 18% in 2023 en ontwikkelt een reeks online samenwerkingstools en
oplossingen voor cloudhosting, streaming, marketing en evenementen. Infomaniak
gebruikt uitsluitend hernieuwbare energie, bouwt zijn eigen datacenters en
ontwikkelt zijn oplossingen in Zwitserland zonder delokalisatie. Het bedrijf
voedt de website van de Belgische radio- en tv-dienst (RTBF) en verzorgt
streaming voor meer dan 3.000 tv- en radiostations in Europa.
</p>

<h2>Over Proxmox</h2>
<p>
<a href="https://www.proxmox.com/">Proxmox</a> biedt krachtige en
gebruiksvriendelijke opensourceserversoftware. Bedrijven van alle groottes en
industrietakken gebruiken Proxmox-oplossingen om efficiënte en vereenvoudigde
IT-infrastructuren te implementeren, de totale eigendomskosten te minimaliseren
en leveranciersafhankelijkheid te vermijden. Proxmox biedt ook commerciële
ondersteuning, trainingsdiensten en een uitgebreid partnerecosysteem om de
bedrijfscontinuïteit voor haar klanten te garanderen. Proxmox Server Solutions
GmbH is opgericht in 2005 en heeft zijn hoofdkantoor in Wenen, Oostenrijk.
Proxmox bouwt zijn productaanbod bovenop het Debian besturingssysteem.
</p>

<h2>Over Wind River</h2>
<p>
<a href="https://www.windriver.com/">Wind River</a>. Wind River is al bijna 20
jaar toonaangevend in commerciële Open Source Linux-oplossingen voor
hoogwaardige bedrijfskritische rekenkracht voor ondernemingen. Met expertise in
de lucht- en ruimtevaart, de automobielsector, de industrie, de telecomsector,
enzovoort, zet het bedrijf zich in voor Open Source via initiatieven als eLxr,
Yocto, Zephyr en StarlingX.
</p>

<h2>Contactinformatie</h2>

<p>Voor meer informatie kunt u terecht op de DebConf24-webpagina op
<a href="https://debconf24.debconf.org/">https://debconf24.debconf.org/</a>
of kunt u een e-mail sturen naar &lt;press@debian.org&gt;.</p>
