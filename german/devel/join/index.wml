#use wml::debian::template title="Wie Sie Teil von Debian werden" MAINPAGE="true"
#use wml::debian::recent_list
#use wml::debian::translation-check translation="5eef343480ec39f5bb8db5e4e69bbb9e8b926c9b"
# $Id$
# Translator: Noèl Köthe, noel@koethe.net, 2001-05-16
# Updated: Holger Wansing <linux@wansing-online.de>, 2011, 2015, 2020, 2023.

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> Wir suchen immer nach Freiwilligen, die etwas zu Debian beitragen möchten.
   Wir begrüßen ausdrücklich die <a href="$(HOME)/intro/diversity">Teilnahme durch jeden</a>. Einzige Anforderungen: Interesse
   an Freier Software und ein bisschen freie Zeit.</p>
</aside>

<ul class="toc">
<li><a href="#reading">Lesen</a></li>
<li><a href="#contributing">Beitragen</a></li>
<li><a href="#joining">Beitreten</a></li>
</ul>


<h2><a id="reading">Lesen</a></h2>

<p>
Falls noch nicht geschehen, sollten Sie die Seiten lesen, die auf der <a
href="$(HOME)">Debian-Startseite</a> verlinkt sind.
Das wird Ihnen helfen zu verstehen, wer wir sind und was wir zu erreichen versuchen.
Wenn Sie potentiell beabsichtigen, etwas zu Debian beizutragen, richten Sie bitte Ihre
besondere Aufmerksamkeit auf die folgenden beiden Seiten:
</p>

<ul>
  <li><a href="$(HOME)/social_contract#guidelines">Debian-Richtlinien für Freie Software</a></li>
  <li><a href="$(HOME)/social_contract">Debians Gesellschaftsvertrag</a></li>
</ul>

<p>
Sehr viel Kommunikation innerhalb des Projekts läuft über unsere <a
href="$(HOME)/MailingLists/">Mailinglisten</a>.
Um ein Gefühl für die internen Arbeitsabläufe des Debian-Projekts
zu bekommen, sollten Sie zumindest die beiden Listen <a
href="https://lists.debian.org/debian-devel-announce/">debian-devel-announce</a>
und <a href="https://lists.debian.org/debian-news/">debian-news</a> abonnieren.
Beide verursachen nur wenig Mail-Verkehr und dokumentieren, was in der
Gemeinschaft passiert.
Die <a href="https://www.debian.org/News/weekly/">Debian Project News</a>
(auch veröffentlicht auf der debian-news-Liste) fasst die neuesten Diskussionen von 
Debian-bezogenen Mailinglisten und Blogs zusammen und stellt Links dazu
bereit.
</p>

<p>
Als angehender Entwickler sollten Sie auch <a
href="https://lists.debian.org/debian-mentors/">debian-mentors</a> abonnieren.
Sie können dort Fragen über das Paketieren und über Infrastrukturprojekte
stellen sowie zu anderen Entwickler-spezifischen Themen.
Bitte beachten Sie, dass diese Liste für neue Beitragende gedacht ist,
nicht für Benutzer. Andere interessante Listen sind <a
href="https://lists.debian.org/debian-devel/">debian-devel</a>
(technische Entwicklerthemen), <a
href="https://lists.debian.org/debian-project/">debian-project</a>
(Diskussionen über nicht-technische Themen im Projekt), <a
href="https://lists.debian.org/debian-release/">debian-release</a>
(Koordination von Debian-Veröffentlichungen) und <a
href="https://lists.debian.org/debian-qa/">debian-qa</a> (Qualitätssicherung).
</p>

<p style="text-align:center"><button type="button"><span class="fas fa-envelope fa-2x"></span> <a href="$(HOME)/MailingLists/subscribe">Mailinglisten-Abonnement</a></button></p>

<p>
<strong>Tipp:</strong> Wenn Sie die Anzahl der E-Mails reduzieren möchten,
die Sie bekommen - speziell auf Mailinglisten mit hohem E-Mail-Aufkommen:
wir bieten <q>-digest</q>-Listen, über die Sie komprimierte Zusammenfassungen
bekommen können statt der vielen einzelnen Mails.
Sie können auch die <a href="https://lists.debian.org/">Mailinglisten-Archive</a>
nutzen, um die Nachrichten der Mailinglisten in einem Webbrowser zu lesen.
</p>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> Sie müssen kein offizieller Debian-Entwickler (DD) sein, um etwas beizutragen.
   Stattdessen kann ein DD als <a href="newmaint#Sponsor">Sponsor</a> agieren und Ihnen helfen, Ihre Arbeit in das Projekt
   zu integrieren.</p>
</aside>

<h2><a id="contributing">Beitragen</a></h2>

<p>
Haben Sie Interesse daran, Pakete zu betreuen? Dann schauen Sie bitte in
unsere Liste <a href="$(DEVEL)/wnpp/">Arbeit-bedürfender und
voraussichtlicher Pakete</a> (Work-Needing and Prospective Packages, WNPP).
Sie finden dort Pakete, die einen (neuen) Betreuer brauchen. Ein verwaistes
Paket zu übernehmen ist ein toller Weg, als Betreuer anzufangen &ndash; es
hilft nicht nur unserer Distribution, sondern gibt Ihnen auch die Möglichkeit,
vom vorherigen Betreuer zu lernen.
</p>

<p>
Hier einige weitere Ideen, wie Sie etwas zu Debian beitragen können:
</p>

<ul>
  <li>Helfen Sie uns beim Schreiben unserer <a href="$(HOME)/doc/">Dokumentation</a>.</li>
  <li>Helfen Sie bei der Betreuung der <a href="$(HOME)/devel/website/">Debian-Website</a>; erstellen Sie Inhalte, überarbeiten oder übersetzen Sie vorhandene Texte.</li>
  <li>Treten Sie unserem <a href="$(HOME)/international/">Übersetzer-Team</a> bei.</li>
  <li>Verbessern Sie Debian und unterstützen Sie <a href="https://qa.debian.org/">Debians Qualitätssicherungs-Team (QA)</a>.</li>
</ul>

<p>
Natürlich gibt es noch eine Vielzahl von anderen Dingen, die Sie tun können und
wir suchen auch immer Leute, die rechtliche Unterstützung anbieten können oder unserem
<a href="https://wiki.debian.org/Teams/Publicity">Team für Öffentlichkeitsarbeit (Debian Publicity Team)</a>
beitreten. Mitglied eines Debian-Teams zu werden ist ein exzellenter
Weg, um Erfahrungen zu sammeln, bevor Sie in den <a href="newmaint">New Member</a>-Prozess
einsteigen (um sich für eine offizielle Mitgliedschaft im Debian-Projekt zu bewerben).
Es ist auch ein guter Anfang, wenn Sie einen Paketsponsor suchen.
Also: suchen Sie sich ein Team und treten Sie diesem bei!
</p>

<p style="text-align:center"><button type="button"><span class="fa fa-users fa-2x"></span> <a href="https://wiki.debian.org/Teams">Liste von Debian-Teams</a></button></p>


<h2><a id="joining">Beitreten</a></h2>

<p>
Nun, Sie haben jetzt einige Zeit Arbeiten zum Projekt beigetragen und möchten
dem Debian-Projekt in offiziellerem Rahmen beitreten?
Es gibt dazu grundsätzlich zwei Optionen:
</p>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> Die Rolle des Debian-Betreuers (Debian Maintainer, DM) ist 2007 eingeführt worden.
   Bis dahin gab es nur einen einzigen offiziellen Status, den des Debian-Entwicklers (Debian Developer, DD). Wir haben zwei
   eigenständige Prozesse für die Bewerbung auf eine dieser Rollen.</p>
</aside>

<ul>
  <li><strong>Debian-Betreuer (Debian Maintainer, DM):</strong> Der erste Schritt;
      Sie können Ihre eigenen Pakete hierbei selbst in das Debian-Archiv hochladen
      (mit einigen Einschränkungen). Debian-Betreuer können Pakete ohne einen Sponsor
      betreuen, außer sie haben den Status eines gesponsorten Betreuers (sponsored Maintainer).
  <br>Weitere Informationen:
  <a href="https://wiki.debian.org/DebianMaintainer">Debian Maintainer Wiki</a></li>
  <li><strong>Debian-Entwickler (Debian Developer, DD):</strong> Die traditionelle
      vollwertige Mitgliedschaft in Debian. Als DD können Sie an Debian-Wahlen teilnehmen.
      DDs mit Upload-Rechten (Uploading DD) können jedes Paket in das Archiv hochladen.
      Bevor Sie Uploading DD werden, sollten für Sie über mindestens sechs Monate
      Aufzeichnungen von Paketbetreuungsarbeiten existieren (wie das Hochladen von
      Paketen als DM, Arbeiten in einem Team oder Betreuen von Paketen, die durch
      Sponsoren hochgeladen wurden).
      DDs ohne Upload-Rechte (Non-uploading DDs) haben die gleichen Rechte betreffend
      Paketierungsarbeiten wie Debian-Betreuer. Bevor Sie sich als Non-Uploading DD
      bewerben, sollten Sie signifikante Aufzeichnungen von Arbeiten im Projekt
      nachweisen können.
  <br>Weitere Informationen: <a href="newmaint">Ecke für neue Mitglieder</a></li>
</ul>

<p>
Unabhängig davon, für welche Rolle Sie sich bewerben, sollten Sie mit Debians Prozessen
vertraut sein. Deshalb empfehlen wir dringend, dass Sie die 
<a href="$(DOC)/debian-policy/">Debian Policy</a> lesen sowie die <a
href="$(DOC)/developers-reference/">Entwicklerreferenz</a>.
</p>
