<define-tag pagetitle>Updated Debian 12: 12.8 released</define-tag>
<define-tag release_date>2024-11-09</define-tag>
#use wml::debian::news
# $Id:

<define-tag release>12</define-tag>
<define-tag codename>bookworm</define-tag>
<define-tag revision>12.8</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>The Debian project is pleased to announce the eighth update of its
stable distribution Debian <release> (codename <q><codename></q>). 
This point release mainly adds corrections for security issues,
along with a few adjustments for serious problems.  Security advisories
have already been published separately and are referenced where available.</p>

<p>Please note that the point release does not constitute a new version of Debian
<release> but only updates some of the packages included.  There is
no need to throw away old <q><codename></q> media. After installation,
packages can be upgraded to the current versions using an up-to-date Debian
mirror.</p>

<p>Those who frequently install updates from security.debian.org won't have
to update many packages, and most such updates are
included in the point release.</p>

<p>New installation images will be available soon at the regular locations.</p>

<p>Upgrading an existing installation to this revision can be achieved by
pointing the package management system at one of Debian's many HTTP mirrors.
A comprehensive list of mirrors is available at:</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>




<h2>Miscellaneous Bugfixes</h2>

<p>This stable update adds a few important corrections to the following packages:</p>

<table border=0>
<tr><th>Package</th>               <th>Reason</th></tr>
<correction 7zip "Fix heap buffer overflow in NTFS handler [CVE-2023-52168]; fix out-of-bounds read in NTFS handler [CVE-2023-52169]">
<correction amanda "Update incomplete fix for CVE-2022-37704, restoring operation with xfsdump">
<correction apr "Use 0600 perms for named shared mem consistently [CVE-2023-49582]">
<correction base-files "Update for the point release">
<correction btrfs-progs "Fix checksum calculation errors during volume conversion in btrfs-convert">
<correction calamares-settings-debian "Fix missing launcher on KDE desktops; fix btrfs mounts">
<correction cjson "Fix segmentation violation issue [CVE-2024-31755]">
<correction clamav "New upstream stable release; fix denial of service issue [CVE-2024-20505], file corruption issue [CVE-2024-20506]">
<correction cloud-init "Add support for multiple networkd Route sections">
<correction cloud-initramfs-tools "Add missing dependencies in the initramfs">
<correction curl "Fix incorrect handling of some OCSP responses [CVE-2024-8096]">
<correction debian-installer "Reinstate some armel netboot targets (openrd); increase Linux kernel ABI to 6.1.0-27; rebuild against proposed-updates">
<correction debian-installer-netboot-images "Rebuild against proposed-updates">
<correction devscripts "bts: always upgrade to STARTTLS on 587/tcp; build-rdeps: add support for non-free-firmware; chdist: update sources.list examples with non-free-firmware; build-rdeps: use all available distros by default">
<correction diffoscope "Fix build failure when processing a deliberately overlapping zip file in tests">
<correction distro-info-data "Add Ubuntu 25.04">
<correction docker.io "Fix bypassing of AuthZ plugins in some circumstances [CVE-2024-41110]">
<correction dpdk "New upstream stable release">
<correction exim4 "Fix crash in dbmnz when looking up keys with no content">
<correction fcgiwrap "Set proper ownership on repositories in git backend">
<correction galera-4 "New upstream stable release">
<correction glib2.0 "Provide libgio-2.0-dev from libglib2.0-dev, and libgio-2.0-dev-bin from libglib2.0-dev-bin">
<correction glibc "Change Croatian locale to use Euro as currency; revert upstream commit that modified the GLIBC_PRIVATE ABI, causing crashes with some static binaries on arm64; vfscanf(): fix matches longer than INT_MAX; ungetc(): fix uninitialized read when putting into unused streams, backup buffer leak on program exit; mremap(): fix support for the MREMAP_DONTUNMAP option; resolv: fix timeouts caused by short error responses or when single-request mode is enabled in resolv.conf">
<correction gtk+3.0 "Fix letting Orca announce initial focus">
<correction ikiwiki-hosting "Allow reading of all user repositories">
<correction intel-microcode "New upstream release; security fixes [CVE-2024-23984 CVE-2024-24968]">
<correction ipmitool "Fix a buffer overrun in <q>open</q> interface; fix <q>lan print fails on unsupported parameters</q>; fix reading of temperature sensors; fix using hex values when sending raw data">
<correction iputils "Fix incorrect handling of ICMP responses intended for other processes">
<correction kexec-tools "Mask kexec.service to prevent the init.d script handling kexec process on a systemd enabled system">
<correction lemonldap-ng "Fix cross-site scripting vulnerability on login page [CVE-2024-48933]">
<correction lgogdownloader "Fix parsing of Galaxy URLs">
<correction libskk "Prevent crash on invalid JSON escape">
<correction libvirt "Fix running i686 VMs with AppArmor on the host; prevent certain guests from becoming unbootable or disappearing during upgrade">
<correction linux "New upstream release; bump ABI to 27">
<correction linux-signed-amd64 "New upstream release; bump ABI to 27">
<correction linux-signed-arm64 "New upstream release; bump ABI to 27">
<correction linux-signed-i386 "New upstream release; bump ABI to 27">
<correction llvm-toolchain-15 "Architecture-specific rebuild on mips64el to sync version with other architectures">
<correction nghttp2 "Fix denial of service issue [CVE-2024-28182]">
<correction ninja-build "Support large inode numbers on 32-bit systems">
<correction node-dompurify "Fix prototype pollution issues [CVE-2024-45801 CVE-2024-48910]">
<correction node-es-module-lexer "Fix build failure">
<correction node-globby "Fix build failure">
<correction node-mdn-browser-compat-data "Fix build failure">
<correction node-rollup-plugin-node-polyfills "Fix build failure">
<correction node-tap "Fix build failure">
<correction node-xterm "Fix TypeScript declarations">
<correction node-y-protocols "Fix build failure">
<correction node-y-websocket "Fix build failure">
<correction node-ytdl-core "Fix build failure">
<correction notify-osd "Correct executable path in desktop launcher file">
<correction ntfs-3g "Fix use-after-free in <q>ntfs-uppercase-mbs</q>; re-classify fuse as Depends, not Pre-Depends">
<correction openssl "New upstream stable release; fix buffer overread issue [CVE-2024-5535], out of bounds memory access [CVE-2024-9143]">
<correction ostree "Prevent crashing libflatpak when using curl 8.10">
<correction puppetserver "Reinstate scheduled job to clean reports after 30 days, avoiding disk space exhaustion">
<correction puredata "Fix privilege escalation issue [CVE-2023-47480]">
<correction python-cryptography "Fix NULL dereference when loading PKCS7 certificates [CVE-2023-49083]; fix NULL dereference when PKCS#12 key and cert don't match [CVE-2024-26130]">
<correction python3.11 "Fix regression in zipfile.Path; prevent ReDoS vulnerability with crafted tar archives">
<correction reprepro "Prevent hangs when running unzstd">
<correction sqlite3 "Fix a buffer overread issue [CVE-2023-7104], a stack overflow issue and an integer overflow issue">
<correction sumo "Fix a race condition when building documentation">
<correction systemd "New upstream stable release">
<correction tgt "chap: Use proper entropy source [CVE-2024-45751]">
<correction timeshift "Add missing dependency on pkexec">
<correction util-linux "Allow lscpu to identify new Arm cores">
<correction vmdb2 "Set locale to UTF-8">
<correction wireshark "New upstream security release [CVE-2024-0208, CVE-2024-0209, CVE-2024-2955, CVE-2024-4853, CVE-2024-4854, CVE-2024-4855, CVE-2024-8250, CVE-2024-8645]">
<correction xfpt "Fix buffer overflow issue [CVE-2024-43700]">
</table>


<h2>Security Updates</h2>


<p>This revision adds the following security updates to the stable release.
The Security Team has already released an advisory for each of these
updates:</p>

<table border=0>
<tr><th>Advisory ID</th>  <th>Package</th></tr>
<dsa 2024 5729 apache2>
<dsa 2024 5733 thunderbird>
<dsa 2024 5744 thunderbird>
<dsa 2024 5758 trafficserver>
<dsa 2024 5759 python3.11>
<dsa 2024 5760 ghostscript>
<dsa 2024 5761 chromium>
<dsa 2024 5762 webkit2gtk>
<dsa 2024 5763 pymatgen>
<dsa 2024 5764 openssl>
<dsa 2024 5765 firefox-esr>
<dsa 2024 5766 chromium>
<dsa 2024 5767 thunderbird>
<dsa 2024 5768 chromium>
<dsa 2024 5769 git>
<dsa 2024 5770 expat>
<dsa 2024 5771 php-twig>
<dsa 2024 5772 libreoffice>
<dsa 2024 5773 chromium>
<dsa 2024 5774 ruby-saml>
<dsa 2024 5775 chromium>
<dsa 2024 5776 tryton-server>
<dsa 2024 5777 booth>
<dsa 2024 5778 cups-filters>
<dsa 2024 5779 cups>
<dsa 2024 5780 php8.2>
<dsa 2024 5781 chromium>
<dsa 2024 5782 linux-signed-amd64>
<dsa 2024 5782 linux-signed-arm64>
<dsa 2024 5782 linux-signed-i386>
<dsa 2024 5782 linux>
<dsa 2024 5783 firefox-esr>
<dsa 2024 5784 oath-toolkit>
<dsa 2024 5785 mediawiki>
<dsa 2024 5786 libgsf>
<dsa 2024 5787 chromium>
<dsa 2024 5788 firefox-esr>
<dsa 2024 5789 thunderbird>
<dsa 2024 5790 node-dompurify>
<dsa 2024 5791 python-reportlab>
<dsa 2024 5792 webkit2gtk>
<dsa 2024 5793 chromium>
<dsa 2024 5794 openjdk-17>
<dsa 2024 5795 python-sql>
<dsa 2024 5796 libheif>
<dsa 2024 5797 twisted>
<dsa 2024 5798 activemq>
<dsa 2024 5799 chromium>
<dsa 2024 5800 xorg-server>
<dsa 2024 5802 chromium>
</table>



<h2>Debian Installer</h2>
<p>The installer has been updated to include the fixes incorporated
into stable by the point release.</p>

<h2>URLs</h2>

<p>The complete lists of packages that have changed with this revision:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>The current stable distribution:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/stable/">
</div>

<p>Proposed updates to the stable distribution:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/proposed-updates">
</div>

<p>stable distribution information (release notes, errata etc.):</p>

<div class="center">
  <a
  href="$(HOME)/releases/stable/">https://www.debian.org/releases/stable/</a>
</div>

<p>Security announcements and information:</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>About Debian</h2>

<p>The Debian Project is an association of Free Software developers who
volunteer their time and effort in order to produce the completely
free operating system Debian.</p>

<h2>Contact Information</h2>

<p>For further information, please visit the Debian web pages at
<a href="$(HOME)/">https://www.debian.org/</a>, send mail to
&lt;press@debian.org&gt;, or contact the stable release team at
&lt;debian-release@lists.debian.org&gt;.</p>


