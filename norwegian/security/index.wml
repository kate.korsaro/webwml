#use wml::debian::template title="Sikkerhetsinformasjon" GEN_TIME="yes" MAINPAGE="true"
#use wml::debian::toc
#include "$(ENGLISHDIR)/releases/info"
#use wml::debian::translation-check translation="452896bf2af61f852728d258a34c0609dd632373" maintainer="Hans F. Nordhaug"

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<ul class="toc">
<li><a href="#keeping-secure">Holde Debian-systemet ditt sikkert</a></li>
<li><a href="#DSAS">De nyeste sikkerhetsmeldingene</a></li>
<li><a href="#infos">Kilder til sikkerhetsinformasjon</a></li>
</ul>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span>
Debian tar sikkerhet meget alvorlig. Vi behandler alle sikkerhetsproblemer
som vi blir informert om, og forsikrer oss om at de blir rettet innen
rimelig tid.</p>
</aside>

<p>Erfaring har vist at <q>security through obscurity</q> (sikkerhet gjennom
skjuling) ikke virker. Offentliggjørelse gir raskere og bedre løsninger på
sikkerhetsproblemer. Av den grunn viser denne siden status for Debian når
det gjelder forskjellige kjente sikkerhetshull som potensielt kan påvirke
Debian.</p>

<p>Mange sikkerhetsmeldinger er koordinert med andre leverandører av fri
programvare og som et resultat blir disse publisert på samme dag som en
sårbarhet blir offentliggjort. For å motta de siste sikkerhetsmeldingene
fra Debian, meld deg på e-postslisten
<a href="https://lists.debian.org/debian-security-announce/">debian-security-announce</a>.
</p>

<p>Debian deltar også i arbeidet med sikkerhetsstandardisering:</p>

<ul>
  <li><a href="#DSAS">Debian sikkerhetsmeldinger (DSA)</a> er
    <a href="cve-compatibility">CVE-kompatible</a> 
    (se <a href="crossreferences">kryssreferansene</a>).</li>
  <li>Debian <a href="oval/">publiserer</a> sin sikkerhetsinformasjon
    ved hjelp av <a href="https://github.com/CISecurity/OVALRepo">OVAL</a>
    (Open Vulnerability Assessment Language).</li>
</ul>

<h2><a id="keeping-secure">Holde Debian-systemet ditt sikkert</a></h2>

<p>
Pakken <a href="https://packages.debian.org/stable/admin/unattended-upgrades">unattended-upgrades</a>
kan installeres for å holde datamaskinen automatisk oppdatert. 
<a href="https://wiki.debian.org/UnattendedUpgrades">Debian wikien</a> har
detaljert informasjon om hvordan du kan sette opp <tt>unattended-upgrades</tt>.

<p>For mer informasjon om sikkerhetssaker i Debian, se vår FAQ og dokumentasjon:<p>

<p style="text-align:center">
<button type="button"><span class="fas fa-book-open fa-2x"></span> <a href="faq">FAQ for sikkerhet</a></button> 
<button type="button"><span class="fas fa-book-open fa-2x"></span> <a href="../doc/user-manuals#securing">Sikring av Debian</a></button>
</p>

<aside class="light">
  <span class="fa fa-rss fa-5x"></span>
</aside>

<h2><a id="DSAS">De nyeste sikkerhetsmeldingene</a><a class="rss_logo" style="float: none;" href="dsa">RSS</a></h2>

<p>Nedenfor er de nyeste Debian sikkerhetsmeldingene (Debian Security Advisories - DSA) sendt på e-postlisten
<a href="https://lists.debian.org/debian-security-announce/">debian-security-announce</a>.
<br><b>T</b> er en lenke til informasjon på 
<a href="https://security-tracker.debian.org/tracker">Debian Security Tracker</a>,
mens DSA-nummeret lenker til selve e-posten.

<p>
#include "$(ENGLISHDIR)/security/dsa.list"
</p>

{#rss#:
<link rel="alternate" type="application/rss+xml"
 title="Debians sikkerhetsmeldinger (kun titler)" href="dsa">
<link rel="alternate" type="application/rss+xml"
 title="Debians sikkerhetsmeldinger (sammendrag)" href="dsa-long">
:#rss#}

<h2><a id="infos">Kilder til sikkerhetsinformasjon</a></h2>
#include "security-sources.inc"
