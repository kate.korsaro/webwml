#use wml::debian::cdimage title="Médias d’installation pour USB, CD et DVD" BARETITLE=true
#use wml::debian::release_info
#use wml::debian::translation-check translation="67bd189030c9c649cfb9f6581aafc637c6af3b8e" maintainer="Jean-Paul Guillonneau"

# Translators:
# Willy Picard 2002-2004.
# Thomas Huriaux 2004-2006.
# Frédéric Bothamy
# Simon Paillard 2007, 2009, 2010.
# David Kremer 2008.
# David Prévot 2011.
# Jean-Paul Guillonneau 2015-2024.


<div class="tip">
<p>
Si vous voulez juste installer Debian et que l'ordinateur cible dispose d'une
connexion à Internet, nous recommandons le média d’<a href="netinst/">\
installation par le réseau</a> qui est moins lourd à télécharger.
</p>
</div>

<div class="tip">
<p>Sur les architectures i386 et amd64, toutes les images d’USB, CD ou DVD
peuvent être aussi
<a href="https://www.debian.org/CD/faq/#write-usb">utilisées sur des clés USB</a>.</div>


<ul>
  <li><a href="http-ftp/">Télécharger les images de USB/CD/DVD par HTTP.</a>

  <li><a href="torrent-cd/">Télécharger les images de USB/CD/DVD avec BitTorrent.</a>
  Le système pair à pair BitTorrent permet à de nombreux utilisateurs de
  télécharger les images de manière coopérative et simultanée, ce qui peut permettre
  d’accélérer le téléchargement.</li>
<li>
  <a href="live/">Télécharger les images autonomes (<q>live</q>) par HTTP, FTP
  ou BitTorrent.</a>
  Les images autonomes servent à amorcer un système autonome sans installation.
  Elles peuvent être utilisées pour essayer Debian dans un premier temps, puis installer le
  contenu de l'image.</li>

<li><a href="vendors/">Acheter un média complet de Debian.</a> </li>

<li><a href="jigdo-cd/">Télécharger les images de USB/CD/DVD avec jigdo.</a>
  Réservé aux utilisateurs experts. Le schéma « jigdo » permet de
  sélectionner le miroir Debian le plus rapide parmi les 300 existants dans le
  monde. Il permet de choisir facilement un miroir et de mettre à niveau
  aisément de vieilles images vers la dernière publication. Il s’agit également de la
  seule manière de télécharger toutes les images des DVD de Debian.</li>

<li>En cas de problème, veuillez consulter la <a href="faq/">FAQ à propos des
  CD et DVD de Debian</a>.</li>

</ul>

<p>
Les publications de USB, CD et DVD officielles sont signées afin de pouvoir
 <a href="verify">vérifier leur authenticité</a>.
</p>

<p>Debian est disponible pour diverses architectures d’ordinateur (la plupart
des gens ont besoin des images pour « amd64 », c'est-à-dire les systèmes
compatibles avec les ordinateurs personnels 64 bits).</p>

      <div class="cdflash" id="latest">Dernière version officielle
      des images de USB/CD/DVD de la distribution «&nbsp;stable&nbsp;»&nbsp;:
        <strong><current-cd-release></strong>.
      </div>

<p>Des informations à propos de problèmes d'installation connus peuvent être
trouvées à la page d'information concernant
<a href="$(HOME)/releases/stable/debian-installer/">
l'installation de Debian</a>.<br>
</p>

