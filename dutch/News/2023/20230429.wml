#use wml::debian::translation-check translation="b6a020c494e8e886f401ab63178faa0db93e39da"
<define-tag pagetitle>Debian 11 is bijgewerkt: 11.7 werd uitgebracht</define-tag>
<define-tag release_date>2023-04-29</define-tag>
#use wml::debian::news

<define-tag release>11</define-tag>
<define-tag codename>bullseye</define-tag>
<define-tag revision>11.7</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>Het Debian-project kondigt met genoegen de zevende update aan van zijn
stabiele distributie Debian <release> (codenaam <q><codename></q>).
Deze tussenrelease voegt voornamelijk correcties voor beveiligingsproblemen toe,
samen met een paar aanpassingen voor ernstige problemen. Beveiligingsadviezen
werden reeds afzonderlijk gepubliceerd en, waar beschikbaar, wordt hiernaar
verwezen.</p>

<p>Merk op dat de tussenrelease geen nieuwe versie van Debian <release> is,
maar slechts een update van enkele van de meegeleverde pakketten. Het is niet
nodig om oude media met <q><codename></q> weg te gooien. Na de installatie
kunnen pakketten worden opgewaardeerd naar de huidige versie door een
bijgewerkte Debian-spiegelserver te gebruiken.</p>

<p>Wie regelmatig updates installeert vanuit security.debian.org zal niet veel
pakketten moeten updaten, en de meeste van dergelijke updates zijn opgenomen in
de tussenrelease.</p>

<p>Nieuwe installatie-images zullen binnenkort beschikbaar zijn op de gewone
plaatsen.</p>

<p>Het upgraden van een bestaande installatie naar deze revisie kan worden
bereikt door het pakketbeheersysteem naar een van de vele HTTP-spiegelservers
van Debian te verwijzen. Een uitgebreide lijst met spiegelservers is
beschikbaar op:</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>




<h2>Oplossingen voor diverse problemen</h2>

<p>Met deze update van stable, de stabiele distributie, worden een paar
belangrijke correcties aangebracht aan de volgende pakketten:</p>

<table border=0>
<tr><th>Pakket</th>               <th>Reden</th></tr>
<correction akregator "Repareren van geldigheidscontroles, inclusief reparatie voor verwijderen van feeds en mappen">
<correction apache2 "apache2-doc.conf niet automatisch activeren; repareren van in 2.4.56 geïntroduceerde regressies in http2 en mod_rewrite">
<correction at-spi2-core "De stop-pauzetijd instellen op 5 seconden, om het afsluiten van het systeem niet onnodig te blokkeren.">
<correction avahi "Oplossen van een lokaal probleem van denial of service [CVE-2021-3468]">
<correction base-files "Update voor de tussenrelease 11.7">
<correction c-ares "Vermijden van stackoverloop en denial of service [CVE-2022-4904]">
<correction clamav "Nieuwe bovenstroomse stabiele release; oplossen van mogelijk probleem van code-uitvoering op afstand in de HFS+ bestandsontleder, van mogelijk informatielek in de DMG bestandsontleder [CVE-2023-20052]">
<correction command-not-found "Toevoegen van nieuwe component non-free-firmware, om opwaarderingen naar bookworm te regelen">
<correction containerd "Oplossen van probleem van denial of service [CVE-2023-25153]; oplossen van mogelijk probleem van rechtenuitbreiding door onjuiste instelling van extra groepen [CVE-2023-25173]">
<correction crun "Oplossen van probleem met bevoegdheidsuitbreiding doordat containers ten onrechte werden gestart met niet-lege standaardmachtigingen [CVE-2022-27650]">
<correction cwltool "Ontbrekende vereiste van python3-distutils toevoegen">
<correction debian-archive-keyring "bookwormsleutels toevoegen; stretchsleutels verplaatsen naar de sleutelbos met verwijderde sleutels">
<correction debian-installer "Linux kernel ABI verhogen naar 5.10.0-22; opnieuw bouwen tegen proposed-updates">
<correction debian-installer-netboot-images "Opnieuw bouwen tegen proposed-updates">
<correction debian-ports-archive-keyring "De vervaldatum van de ondertekeningssleutel voor 2023 met een jaar verlengen; de ondertekeningssleutel voor 2024 toevoegen; de ondertekeningssleutel voor 2022 verplaatsen naar de sleutelbos met verwijderde sleutels">
<correction dpdk "Nieuwe bovenstroomse stabiele release">
<correction duktape "Crashprobleem verhelpen [CVE-2021-46322]">
<correction e2tools "Bouwfout oplossen door bouwvereiste van e2fsprogs toe te voegen">
<correction erlang "Probleem met het omzeilen van cliëntauthenticatie verhelpen [CVE-2022-37026]; gebruiken van -O1 optimalisatie voor armel omdat -O2 erl op sommige platforms, bijv. Marvell, tot een segmentatiefout leidt">
<correction exiv2 "Oplossingen voor veiligheidsproblemen [CVE-2021-29458 CVE-2021-29463 CVE-2021-29464 CVE-2021-29470 CVE-2021-29473 CVE-2021-29623 CVE-2021-32815 CVE-2021-34334 CVE-2021-34335 CVE-2021-3482 CVE-2021-37615 CVE-2021-37616 CVE-2021-37618 CVE-2021-37619 CVE-2021-37620 CVE-2021-37621 CVE-2021-37622 CVE-2021-37623]">
<correction flask-security "Oplossen van open omleidingskwetsbaarheid [CVE-2021-23385]">
<correction flatpak "Nieuwe bovenstroomse stabiele release; speciale tekens bij de weergave van machtigingen en metagegevens maskeren [CVE-2023-28101]; geen kopiëren/plakken toestaan via de TIOCLINUX ioctl wanneer deze wordt uitgevoerd in een virtuele Linux-console [CVE-2023-28100]">
<correction galera-3 "Nieuwe bovenstroomse stabiele release">
<correction ghostscript "Pad voor PostScript-helperbestand in ps2epsi repareren">
<correction glibc "Geheugenlek oplossen in functies van de printf-familie met lange multibyte tekenreeksen; reparatie voor crash in printf-familie vanwege breedte/precisie-afhankelijke toewijzingen; segmentatiefout repareren in de verwerking door printf van scheidingsteken voor duizendtallen; reparatie voor overloop in de AVX2-implementatie van wcsnlen bij paginaovergang">
<correction golang-github-containers-common "reparatie voor ontleden van DBUS_SESSION_BUS_ADDRESS">
<correction golang-github-containers-psgo "De gebruikersnaamruimte van het proces niet binnengaam [CVE-2022-1227]">
<correction golang-github-containers-storage "Voorheen interne functies publiek toegankelijk maken, nodig om CVE-2022-1227 in andere pakketten te kunnen repareren">
<correction golang-github-prometheus-exporter-toolkit "Patchtesten om racecondities te voorkomen; probleem met authenticatiecache-vergiftiging oplossen [CVE-2022-46146]">
<correction grep "Correctie van onjuiste overeenkomsten wanneer het laatste van meerdere patronen een backreference-constructie bevat">
<correction gtk+3.0 "Reparatie voor Wayland + EGL op alleen GLES platformen">
<correction guix "Bouwfout door verlopen sleutels in testsuite verhelpen">
<correction intel-microcode "Nieuwe bovenstroomse release met bugreparaties">
<correction isc-dhcp "Verbeterde behandeling van levensduur van IPv6-adressen">
<correction jersey1 "Oplossing voor bouwfout met libjettison-java 1.5.3">
<correction joblib "Probleem van willekeurige code-uitvoering oplossen [CVE-2022-21797]">
<correction lemonldap-ng "Oplossing voor probleem van omzeilen van URL-validatie; oplossing voor 2FA-probleem bij het gebruik van de AuthBasic verwerker [CVE-2023-28862]">
<correction libapache2-mod-auth-openidc "Probleem met open omleidingen oplossen [CVE-2022-23527]">
<correction libapreq2 "Oplossen van probleem van bufferoverloop [CVE-2022-22728]">
<correction libdatetime-timezone-perl "Bijwerken van ingesloten gegevens">
<correction libexplain "Verbeterde compatibiliteit met nieuwere kernelversies - Linux 5.11 heeft niet langer if_frad.h, termiox verwijderd sinds kernel 5.12">
<correction libgit2 "SSH-sleutelverificatie standaard inschakelen [CVE-2023-22742]">
<correction libpod "Probleem van rechtenuitbreiding oplossen [CVE-2022-1227]; Oplossen van probleem met bevoegdheidsuitbreiding doordat containers ten onrechte werden gestart met niet-lege standaardmachtigingen [CVE-2022-27649]; ontleden van DBUS_SESSION_BUS_ADDRESS verbeteren">11111111
<correction libreoffice "De standaardmunt van Kroatië wijzigen in euro; leeg -Djava.class.path= vermijden [CVE-2022-38745]">
<correction libvirt "Problemen met het opnieuw opstarten van containers oplossen; testfouten in combinatie met nieuwere Xen-versies oplossen">
<correction libxpm "Problemen van oneindige lussen oplossen [CVE-2022-44617 CVE-2022-46285]; probleem met dubbele vrijgave in code voor foutverwerking oplossen; oplossing voor <q>compressiecommando's zijn afhankelijk van PATH</q> [CVE-2022-4883]">
<correction libzen "Oplossen van probleem van null pointer dereference [CVE-2020-36646]">
<correction linux "Nieuwe bovenstroomse stabiele release; ABI verhogen naar 22; [rt] opwaarderen naar 5.10.176-rt86">
<correction linux-signed-amd64 "Nieuwe bovenstroomse stabiele release; ABI verhogen naar 22; [rt] opwaarderen naar 5.10.176-rt86">
<correction linux-signed-arm64 "Nieuwe bovenstroomse stabiele release; ABI verhogen naar 22; [rt] opwaarderen naar 5.10.176-rt86">
<correction linux-signed-i386 "Nieuwe bovenstroomse stabiele release; ABI verhogen naar 22; [rt] opwaarderen naar 5.10.176-rt86">
<correction lxc "Oplossing voor orakel over het bestaan van een bestand [CVE-2022-47952]">
<correction macromoleculebuilder "Bouwfout oplossen door docbook-xsl als bouwvereiste toe te voegen">
<correction mariadb-10.5 "Nieuwe bovenstroomse stabiele release; bovenstroomse libmariadb API-wijziging terugdraaien">
<correction mono "Desktop-bestand verwijderen">
<correction ncurses "Beschermen tegen beschadigde terminfo-gegevens [CVE-2022-29458]; reparatie voor tic-crash bij zeer lange tc/use-clausules">
<correction needrestart "Oplossing voor waarschuwingen bij het gebruik van de optie <q>-b</q>">
<correction node-cookiejar "Bescherming tegen kwaadwillig manipuleren van grootte van cookies [CVE-2022-25901]">
<correction node-webpack "cross-realm objectbenadering vermijden [CVE-2023-28154]">
<correction nvidia-graphics-drivers "Nieuwe bovenstroomse release; beveiligingsoplossingen [CVE-2023-0180 CVE-2023-0184 CVE-2023-0185 CVE-2023-0187 CVE-2023-0188 CVE-2023-0189 CVE-2023-0190 CVE-2023-0191 CVE-2023-0194 CVE-2023-0195 CVE-2023-0198 CVE-2023-0199]">
<correction nvidia-graphics-drivers-tesla-450 "Nieuwe bovenstroomse release; beveiligingsoplossingen [CVE-2023-0180 CVE-2023-0184 CVE-2023-0185 CVE-2023-0188 CVE-2023-0189 CVE-2023-0190 CVE-2023-0191 CVE-2023-0194 CVE-2023-0195 CVE-2023-0198 CVE-2023-0199]">
<correction nvidia-graphics-drivers-tesla-470 "Nieuwe bovenstroomse release; beveiligingsoplossingen [CVE-2023-0180 CVE-2023-0184 CVE-2023-0185 CVE-2023-0187 CVE-2023-0188 CVE-2023-0189 CVE-2023-0190 CVE-2023-0191 CVE-2023-0194 CVE-2023-0195 CVE-2023-0198 CVE-2023-0199]">
<correction nvidia-modprobe "Nieuwe bovenstroomse release">
<correction openvswitch "Oplossing voor <q>update van openvswitch-switch laat interfaces uitgeschakeld</q>">
<correction passenger "Compatibiliteit met recentere versies van NodeJS oplossen">
<correction phyx "libatlas-cpp als bouwvereiste verwijderen, want onnodig">
<correction postfix "Nieuwe bovenstroomse stabiele release">
<correction postgis "Foutieve polaire stereografische asvolgorde herstellen">
<correction postgresql-13 "Nieuwe bovenstroomse stabiele release; probleem met openbaarmaking van clientgeheugen oplossen [CVE-2022-41862]">
<correction python-acme "Verbetering van de versie van aangemaakte CSR's om problemen met strikt RFC-conforme implementaties van de ACME API te voorkomen.">
<correction ruby-aws-sdk-core "Herstelling voor aanmaken van versiebestand">
<correction ruby-cfpropertylist "Een aantal functies herstellen door de compatibiliteit met Ruby 1.8 te laten vallen">
<correction shim "Nieuwe bovenstroomse release; nieuwe bovenstroomse stabiele release; NX-ondersteuning inschakelen tijdens het bouwen; binaire bestanden van grub in Debian blokkeren bij sbat &lt; 4">
<correction shim-helpers-amd64-signed "Nieuwe bovenstroomse stabiele release; NX-ondersteuning inschakelen tijdens het bouwen; binaire bestanden van grub in Debian blokkeren bij sbat &lt; 4">
<correction shim-helpers-arm64-signed "Nieuwe bovenstroomse stabiele release; NX-ondersteuning inschakelen tijdens het bouwen; binaire bestanden van grub in Debian blokkeren bij sbat &lt; 4">
<correction shim-helpers-i386-signed "Nieuwe bovenstroomse stabiele release; NX-ondersteuning inschakelen tijdens het bouwen; binaire bestanden van grub in Debian blokkeren bij sbat &lt; 4">
<correction shim-signed "Nieuwe bovenstroomse stabiele release; NX-ondersteuning inschakelen tijdens het bouwen; binaire bestanden van grub in Debian blokkeren bij sbat &lt; 4">
<correction snakeyaml "Oplossen van problemen van denial of service [CVE-2022-25857 CVE-2022-38749 CVE-2022-38750 CVE-2022-38751]; documentatie over beveiligingsondersteuning / -problemen toevoegen">
<correction spyder "Codeduplicatie bij het opslaan verhelpen">
<correction symfony "Private kopregels verwijderen alvorens antwoorden op te slaan met HttpCache [CVE-2022-24894]; CSRF-tokens uit de opslag verwijderen bij succesvol inloggen [CVE-2022-24895]">
<correction systemd "Verhelpen van probleem van weglekken van informatie [CVE-2022-4415], van probleem van denial of service [CVE-2022-3821]; ata_id: oplossing voor verkrijgen van Response Code van SCSI Sense Data; logind: oplossing voor het verkrijgen van de eigenschap OnExternalPower via D-Bus; crash in systemd-machined verhelpen">
<correction tomcat9 "Ondersteuning voor OpenJDK 17 toevoegen aan JDK-detectie">
<correction traceroute "v4mapped-IPv6-adressen interpreteren als IPv4">
<correction tzdata "Ingesloten gegevens bijwerken">
<correction unbound "Verhelpen van Non-Responsive Delegation Attack (niet-reagerende delegatieaanval) [CVE-2022-3204]; probleem met <q>spookdomeinnamen</q> verhelpen [CVE-2022-30698 CVE-2022-30699]">
<correction usb.ids "Ingesloten gegevens bijwerken">
<correction vagrant "Ondersteuning voor VirtualBox 7.0 toevoegen">
<correction voms-api-java "Bouwfouten oplossen door enkele niet-werkende tests uit te schakelen">
<correction w3m "Oplossen van probleem van schrijven buiten de grenzen [CVE-2022-38223]">
<correction x4d-icons "Bouwfout met nieuwere imagemagick-versies verhelpen">
<correction xapian-core "Databasebeschadiging bij uitputting van de schijf voorkomen">
<correction zfs-linux "Verscheidene stabiliteitsverbeteringen toevoegen">
</table>


<h2>Beveiligingsupdates</h2>


<p>Met deze revisie worden de volgende beveiligingsupdates toegevoegd aan de
stabiele release. Het beveiligingsteam heeft voor elk van deze updates
al een advies uitgebracht:</p>

<table border=0>
<tr><th>Advies ID</th>  <th>Pakket</th></tr>
<dsa 2022 5170 nodejs>
<dsa 2022 5237 firefox-esr>
<dsa 2022 5238 thunderbird>
<dsa 2022 5259 firefox-esr>
<dsa 2022 5262 thunderbird>
<dsa 2022 5282 firefox-esr>
<dsa 2022 5284 thunderbird>
<dsa 2022 5300 pngcheck>
<dsa 2022 5301 firefox-esr>
<dsa 2022 5302 chromium>
<dsa 2022 5303 thunderbird>
<dsa 2022 5304 xorg-server>
<dsa 2022 5305 libksba>
<dsa 2022 5306 gerbv>
<dsa 2022 5307 libcommons-net-java>
<dsa 2022 5308 webkit2gtk>
<dsa 2022 5309 wpewebkit>
<dsa 2022 5310 ruby-image-processing>
<dsa 2023 5311 trafficserver>
<dsa 2023 5312 libjettison-java>
<dsa 2023 5313 hsqldb>
<dsa 2023 5314 emacs>
<dsa 2023 5315 libxstream-java>
<dsa 2023 5316 netty>
<dsa 2023 5317 chromium>
<dsa 2023 5318 lava>
<dsa 2023 5319 openvswitch>
<dsa 2023 5320 tor>
<dsa 2023 5321 sudo>
<dsa 2023 5322 firefox-esr>
<dsa 2023 5323 libitext5-java>
<dsa 2023 5324 linux-signed-amd64>
<dsa 2023 5324 linux-signed-arm64>
<dsa 2023 5324 linux-signed-i386>
<dsa 2023 5324 linux>
<dsa 2023 5325 spip>
<dsa 2023 5326 nodejs>
<dsa 2023 5327 swift>
<dsa 2023 5328 chromium>
<dsa 2023 5329 bind9>
<dsa 2023 5330 curl>
<dsa 2023 5331 openjdk-11>
<dsa 2023 5332 git>
<dsa 2023 5333 tiff>
<dsa 2023 5334 varnish>
<dsa 2023 5335 openjdk-17>
<dsa 2023 5336 glance>
<dsa 2023 5337 nova>
<dsa 2023 5338 cinder>
<dsa 2023 5339 libhtml-stripscripts-perl>
<dsa 2023 5340 webkit2gtk>
<dsa 2023 5341 wpewebkit>
<dsa 2023 5342 xorg-server>
<dsa 2023 5343 openssl>
<dsa 2023 5344 heimdal>
<dsa 2023 5345 chromium>
<dsa 2023 5346 libde265>
<dsa 2023 5347 imagemagick>
<dsa 2023 5348 haproxy>
<dsa 2023 5349 gnutls28>
<dsa 2023 5350 firefox-esr>
<dsa 2023 5351 webkit2gtk>
<dsa 2023 5352 wpewebkit>
<dsa 2023 5353 nss>
<dsa 2023 5355 thunderbird>
<dsa 2023 5356 sox>
<dsa 2023 5357 git>
<dsa 2023 5358 asterisk>
<dsa 2023 5359 chromium>
<dsa 2023 5361 tiff>
<dsa 2023 5362 frr>
<dsa 2023 5363 php7.4>
<dsa 2023 5364 apr-util>
<dsa 2023 5365 curl>
<dsa 2023 5366 multipath-tools>
<dsa 2023 5367 spip>
<dsa 2023 5368 libreswan>
<dsa 2023 5369 syslog-ng>
<dsa 2023 5370 apr>
<dsa 2023 5371 chromium>
<dsa 2023 5372 rails>
<dsa 2023 5373 node-sqlite3>
<dsa 2023 5374 firefox-esr>
<dsa 2023 5375 thunderbird>
<dsa 2023 5376 apache2>
<dsa 2023 5377 chromium>
<dsa 2023 5378 xen>
<dsa 2023 5379 dino-im>
<dsa 2023 5380 xorg-server>
<dsa 2023 5381 tomcat9>
<dsa 2023 5382 cairosvg>
<dsa 2023 5383 ghostscript>
<dsa 2023 5384 openimageio>
<dsa 2023 5385 firefox-esr>
<dsa 2023 5386 chromium>
<dsa 2023 5387 openvswitch>
<dsa 2023 5388 haproxy>
<dsa 2023 5389 rails>
<dsa 2023 5390 chromium>
<dsa 2023 5391 libxml2>
<dsa 2023 5392 thunderbird>
<dsa 2023 5393 chromium>
</table>


<h2>Verwijderde pakketten</h2>

<p>De volgende pakketten werden verwijderd wegens omstandigheden waarover wij geen controle hebben:</p>

<table border=0>
<tr><th>Pakket</th>               <th>Reden</th></tr>
<correction bind-dyndb-ldap "Werkt niet bij recentere bind9-versies; kan in de stabiele release niet ondersteund worden">
<correction matrix-mirage "Vereist python-matrix-nio dat zal verwijderd worden">
<correction pantalaimon "Vereist python-matrix-nio dat zal verwijderd worden">
<correction python-matrix-nio "Veiligheidsproblemen; werkt niet met de huidige Matrix-servers">
<correction weechat-matrix "Vereist python-matrix-nio dat zal verwijderd worden">

</table>

<h2>Het Debian-installatiesysteem</h2>
<p>Het installatiesysteem werd bijgewerkt om de reparaties die met deze tussenrelease in stable, de stabiele release, opgenomen werden, toe te voegen.</p>

<h2>URL's</h2>

<p>De volledige lijsten met pakketten die met deze revisie gewijzigd werden:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>De huidige stabiele distributie:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/stable/">
</div>

<p>Voorgestelde updates voor de stabiele distributie:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/proposed-updates">
</div>

<p>informatie over de stabiele distributie (notities bij de release, errata, enz.):</p>

<div class="center">
  <a
  href="$(HOME)/releases/stable/">https://www.debian.org/releases/stable/</a>
</div>

<p>Beveiligingsaankondigingen en -informatie:</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>Over Debian</h2>

<p>Het Debian-project is een samenwerkingsverband van ontwikkelaars van vrije
software die vrijwillig tijd en moeite steken in het produceren van het
volledig vrije besturingssysteem Debian.</p>

<h2>Contactinformatie</h2>

<p>Ga voor verdere informatie naar de webpagina's van Debian op
<a href="$(HOME)/">https://www.debian.org/</a>, stuur een e-mail naar
&lt;press@debian.org&gt;, of neem contact met het release-team voor de stabiele
release op &lt;debian-release@lists.debian.org&gt;.</p>


