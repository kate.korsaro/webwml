#use wml::debian::cdimage title="使用 BitTorrent 下载 Debian USB/CD/DVD 映像" BARETITLE=true
#use wml::debian::toc
#include "$(ENGLISHDIR)/releases/info"
#use wml::debian::installer
#include "$(ENGLISHDIR)/releases/images.data"
#include "$(ENGLISHDIR)/devel/debian-installer/images.data"
#use wml::debian::translation-check translation="67bd189030c9c649cfb9f6581aafc637c6af3b8e"

<p><a href="https://zh.wikipedia.org/wiki/BitTorrent_(%E5%8D%8F%E8%AE%AE)">BitTorrent</a>
是一个点对点下载系统，专为大量的下载人数优化。\
它仅给我们的服务器增加很小的负担，因为 BitTorrent 客户端\
在下载的同时也将文件的一部分上传给其他人，因此可以将负载\
分散到整个网络，并使闪电般的高速下载成为可能。
</p>
<div class="tip">
<p><strong>第一个</strong> USB/CD/DVD 映像包含了安装标准 Debian 系统\
需要的全部[CN:文件:][HKTW:档案:]。<br />
</p>
</div>
<p>
要用这种方式下载 Debian USB/CD/DVD 映像，您需要一个 BitTorrent 客户端。\
Debian 发行版包含了 \
<a href="https://packages.debian.org/aria2">aria2</a>、\
<a href="https://packages.debian.org/transmission">transmission</a> 以及 \ 
<a href="https://packages.debian.org/ktorrent">KTorrent</a> 工具。\
其他操作系统（例如 Windows 和 macOS）也可以使用 <a
href="https://www.qbittorrent.org/download">qBittorrent</a> 和 <a
href="https://www.bittorrent.com/download">BitTorrent</a>。
</p>
<h3><q>稳定（stable）</q>版本的官方种子[CN:文件:][HKTW:档案:]</h3>

<div class="line">
<div class="item col50">
<p><strong>CD/USB</strong></p>
  <stable-full-cd-torrent>
</div>
<div class="item col50 lastcol">
<p><strong>DVD/USB</strong></p>
  <stable-full-dvd-torrent>
</div>
<div class="clear"></div>
</div>

<p>请您务必在安装前阅读文档。\
<strong>如果您在安装前只想阅读一份文档</strong>，请阅读我们的\
<a href="$(HOME)/releases/stable/amd64/apa">安装指南</a>，这是一份\
安装过程的简要介绍。其他有用的文档包括：
</p>
<ul>
<li><a href="$(HOME)/releases/stable/installmanual">安装手册</a>，
详细的安装步骤</li>
<li><a href="https://wiki.debian.org/DebianInstaller">Debian 安装[CN:程序:][HKTW:程式:]\
文档</a>，包括常见问题及解答（FAQ）</li>
<li><a href="$(HOME)/releases/stable/debian-installer/#errata">Debian 安装\
[CN:程序:][HKTW:程式:]勘误</a>，安装[CN:程序:][HKTW:程式:]的已知问题列表</li>
</ul>

# <h3>Official torrents for the <q>testing</q> distribution</h3>
# 
# <ul>
# 
#   <li><strong>CD</strong>:<br />
#   <full-cd-torrent>
#   </li>
# 
#   <li><strong>DVD</strong>:<br />
#   <full-dvd-torrent>
#   </li>
# 
# </ul>

<p>
如果可以的话，请在下载完成后让客户端保持运行，以帮助其他人下载得更快！
</p>
