#use wml::debian::template title="Debian GNU/Linux 3.0 &ldquo;woody&rdquo; Release Information" BARETITLE="true"
#include "$(ENGLISHDIR)/releases/info"

<p>Debian GNU/Linux 3.0 (a.k.a. <em>woody</em>) was released on 19th of
July, 2002. The new release includes many major changes, described in
our <a href="$(HOME)/News/2002/20020719">press release</a> and the
<a href="releasenotes">Release Notes</a>.</p>

<p><strong>Debian GNU/Linux 3.0 has been obsoleted by
<a href="../sarge/">Debian GNU/Linux 3.1 (<q>sarge</q>)</a>.
Security updates have been discontinued as of the end of June 2006.</strong></p>

<p>Debian GNU/Linux 3.0 is available <a href="$(DISTRIB)/">from
the Internet</a> and <a href="$(HOME)/CD/vendors/">from CD vendors</a>.</p>

<p>Before installing Debian, please read the
Installation Manual. The Installation Manual for your target
architecture contains instructions and links for all the files you need to
install.</p>

<p>The following computer architectures were supported in this release:</p>

<ul>
<li><a href="../../ports/alpha/">Alpha</a>
<li><a href="../../ports/arm/">ARM</a>
<li><a href="../../ports/hppa/">HP PA-RISC</a>
<li><a href="../../ports/i386/">32-bit PC (i386)</a>
<li><a href="../../ports/ia64/">Intel Itanium IA-64</a>
<li><a href="../../ports/m68k/">Motorola 680x0</a>
<li><a href="../../ports/mips/">MIPS (big endian)</a>
<li><a href="../../ports/mips/">MIPS (little endian)</a>
<li><a href="../../ports/powerpc/">PowerPC</a>
<li><a href="../../ports/s390/">IBM S/390</a>
<li><a href="../../ports/sparc/">SPARC</a>
</ul>

<p>Contrary to our wishes, there may be some problems that exist in the woody
release, even though it is declared <em>stable</em>. We've made
<a href="errata">a list of the major known problems</a>, and you can always
report other issues to us.</p>
