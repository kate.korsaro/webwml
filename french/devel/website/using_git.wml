#use wml::debian::template title="Utilisation de Git pour travailler sur le site web de Debian"
#use wml::debian::translation-check translation="68ac2b55da5ec0c0222f324e928679e27e5de908" maintainer="Jean-Pierre Giraud"

# Translator:
# Jean-Pierre Giraud, 2018-2022.

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<ul class="toc">
<li><a href="#work-on-repository">Travailler sur le dépôt Git</a></li>
<li><a href="#write-access">Accès en écriture au dépôt Git</a></li>
<li><a href="#notifications">Recevoir des notifications</a></li>
</ul>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> <a href="https://git-scm.com/">Git</a>
est un <a href="https://fr.wikipedia.org/wiki/Version_control">système de
gestion de versions</a> qui aide à coordonner le travail entre plusieurs
développeurs. Chaque utilisateur peut détenir une copie locale du dépôt
principal. Ces copies locales peuvent être sur la même machine ou n'importe où
dans le monde. Les développeurs peuvent alors modifier leur copie locale et,
quand la version modifiée est prête, enregistrer leurs changements (« commit »)
et les envoyer en retour vers le dépôt principal.</p>
</aside>

<h2><a id="work-on-repository">Travailler sur le dépôt Git</a></h2>

<p>
Entrons dans le vif du sujet — dans cette partie, vous apprendrez comment créer
une copie locale du dépôt principal, comment garder votre dépôt local à jour et
comment soumettre votre travail. Nous vous expliquerons aussi comment
travailler sur les traductions.
</p>

<h3><a name="get-local-repo-copy">Obtenir une copie locale</a></h3>

<p>
D'abord, installez Git. Ensuite, configurez Git et entrez votre nom et votre
adresse courriel. Si vous êtes un nouvel utilisateur de Git, c'est sans doute
une bonne idée de lire la documentation générale de Git en premier.
</p>

<p style="text-align:center"><button type="button"><span class="fas fa-book-open fa-2x"></span> <a href="https://git-scm.com/doc">Documentation de Git</a></button></p>

<p>
La prochaine étape est de cloner le dépôt (autrement dit, créer une copie
locale du dépôt). Il y a deux manières de le faire :
</p>

<ul>
  <li>ouvrir un compte sur <url https://salsa.debian.org/> et autoriser un
  accès SSH à git en envoyant une clé publique SSH dans votre compte salsa.
  Voir les <a href="https://salsa.debian.org/help/user/ssh.md">pages d'aide
  de Salsa</a> pour obtenir plus de détails. Vous pouvez ensuite cloner le
  dépôt <code>webwml</code> avec la commande suivante :
<pre>
  git clone git@salsa.debian.org:webmaster-team/webwml.git
</pre>
  </li>
  <li>autrement, vous pouvez cloner le dépôt avec le protocole HTTPS. Gardez à
  l'esprit que cela créera le dépôt local, mais que vous ne serez pas capable
  d'envoyer directement vos modifications de cette manière :
<pre>
  git clone https://salsa.debian.org/webmaster-team/webwml.git
</pre>
  </li>
</ul>

<p>
<strong>Astuce :</strong> Cloner la totalité du dépôt <code>webwml</code>
demandera le téléchargement d'environ 1,3 Go de données, ce qui pourrait être
trop si vous avez une liaison Internet lente ou instable. Donc, il est possible
de définir une profondeur minimale pour un téléchargement initial plus court :
</p>

<pre>
  git clone git@salsa.debian.org:webmaster-team/webwml.git --depth 1
</pre>

<p>Une fois que vous avez obtenu un dépôt (superficiel) utilisable, vous pouvez
approfondir la copie locale et, finalement, la convertir en un
dépôt local complet :</p>

  git fetch --deepen=1000 # approfondit le dépôt de 1000 commits supplémentaires
  git fetch --unshallow   # obtient tous les commits manquants, convertit le dépôt en dépôt complet

<p>Vous pouvez procéder à la récupération de seulement un sous-ensemble des
pages ainsi :</p>

<ol>
  <li><code>git clone --no-checkout git@salsa.debian.org:webmaster-team/webwml.git</code></li>
  <li><code>cd webwml</code></li>
  <li><code>git config core.sparseCheckout true</code></li>
  <li>Créez le fichier <code>.git/info/sparse-checkout</code> dans le répertoire
  <code>webwml</code> pour définir le contenu que vous voulez récupérer. Par
  exemple, si vous ne voulez que les fichiers de base, l'anglais et les
  traductions en français et en espagnol, le ficher devra ressembler à cela :
    <pre>
      /*
      !/[a-z]*/
      /english/
      /french/
      /spanish/
    </pre></li>
  <li>Enfin, vous pouvez récupérer le dépôt : <code>git checkout --</code></li>
</ol>

<h3><a name="submit-changes">Soumettre des modifications locales</a></h3>

<h4><a name="keep-local-repo-up-to-date">Garder à jour le dépôt local</a></h4>

<p>De temps en temps (et sans aucun doute avant de débuter un travail
d'édition !) il faudra lancer la commande</p>

<pre>
  git pull
</pre>

<p>pour récupérer tous les fichiers du dépôt qui ont changé.</p>

<p>
Il est fortement recommandé de garder propre son répertoire local de travail de
Git avant d'exécuter la commande <code>git pull</code> et de poursuivre le
travail d'édition. Si vous avez des modifications qui n'ont pas fait l'objet de
commits ou des commits locaux qui ne sont pas dans le dépôt distant dans la
branche courante, exécuter <code>git pull</code> créera immédiatement des
demandes de fusion ou même échouera dû à des conflits. Vous devriez envisager
de garder votre travail inachevé dans une autre branche ou d'utiliser des
commandes telles que <code>git stash</code>.
</p>

<p>Attention : Git est un système de contrôle de versions distribué (non
centralisé). Cela signifie que quand vous préparez l'envoi de vos modifications,
elles seront seulement stockées dans votre dépôt local. Pour les partager avec
d'autres, vous devrez aussi envoyer vos modifications vers le dépôt central de
Salsa.</p>

<h4><a name="example-edit-english-file">Exemple de modifications de fichiers anglais</a></h4>

<p>
Regardons un exemple pratique et une session de travail typique. Considérons
que vous avez obtenu une <a href="#get-local-repo-copy">copie locale</a> du
dépôt avec la commande <code>git clone</code>. Les étapes suivantes sont :
</p>

<ol>
  <li><code>git pull</code></li>
  <li>Maintenant vous pouvez commencer à éditer et modifier les fichiers.</li>
  <li>Quand c'est fait, préparez l'envoi de vos modifications dans votre dépôt
  local avec les commandes :
    <pre>
    git add chemin/vers/fichier(s)
    git commit -m "Votre message d'envoi"
    </pre></li>
  <li>Si vous avez un <a href="#write-access">accès en écriture illimité</a> au
  dépôt <code>webwml</code> distant, vous pouvez maintenant envoyer directement
  vos modifications dans le dépôt de Salsa : <code>git push</code></li>
  <li>Si vous n'avez pas un accès en écriture direct au dépôt <code>webwml</code>,
  soumettez vos modifications en utilisant la fonction
  <a href="#write-access-via-merge-request">merge request</a> ou sollicitez
  l'aide d'autres développeurs.</li>
</ol>

<p style="text-align:center"><button type="button"><span class="fas fa-book-open fa-2x"></span> <a href="https://git-scm.com/docs/gittutorial">Documentation Git</a></button></p>

<h4><a name="closing-debian-bug-in-git-commits">Fermer des bogues de Debian dans les envois de Git</a></h4>

<p>
En incluant <code>Closes: #</code><var>nnnnnn</var> dans votre entrée de
journal d'envoi, le bogue <code>nº</code><var>nnnnnn</var> sera
fermé automatiquement quand vous enverrez vos modifications. La forme exacte
est la même que celle décrite dans
<a href="$(DOC)/debian-policy/ch-source.html#id24">la Charte Debian</a>.</p>

<h4><a name="links-using-http-https">Connexion en utilisant HTTP/HTTPS</a></h4>

<p>
Beaucoup de sites web de Debian prennent en charge SSL/TLS, veuillez donc
utiliser des liens en HTTPS lorsque c'est possible et pertinent.
<strong>Néanmoins</strong>, certains sites web de Debian, DebConf, SPI, etc.,
ne prennent pas en charge HTTPS ou n'ont leur certificat SSL signé que par SPI,
(et pas par une autorité SSL considérée de confiance par tous les navigateurs).
Veuillez éviter les liens en HTTPS vers ces sites web pour que les utilisateurs
en dehors de Debian ne reçoivent pas de messages d'erreur.</p>

<p>Le dépôt Git rejettera les envois contenant des liens en HTTP pur vers les
sites web Debian qui prennent en charge HTTPS, ou contenant des liens en HTTPS
vers les sites web Debian, DebConf et SPI qui sont connus pour ne pas prendre en
charge HTTPS ou avoir un certificat signé uniquement par SPI.</p>

<h3><a name="translation-work">Travailler sur des traductions</a></h3>

<p>
Les traductions doivent être toujours tenues à jour avec les fichiers anglais
correspondants. L'en-tête <q>translation-check</q> des fichiers traduits
est utilisé pour suivre la version des fichiers anglais sur laquelle elles
reposent. Si vous modifiez des fichiers traduits, il est nécessaire de mettre à
jour l'en-tête <q>translation-check</q> pour qu'il corresponde à l'empreinte de
la modification correspondante du fichier anglais. Vous pouvez trouver
cette empreinte avec la commande suivante :</p>

<pre>
  git log chemin/vers/fichier/en_anglais/
</pre>

<p>
Si vous faites une nouvelle traduction de fichier, veuillez utiliser le
script <code>copypage.pl</code> et il créera un modèle pour votre langue, y
compris l'en-tête de traduction correct.</p>

<h4><a name="translation-smart-change">Modifications de traductions avec smart_change.pl</a></h4>

<p><code>smart_change.pl</code> est un script conçu pour faciliter la mise à
jour à la fois des fichiers originaux et de leurs traductions. Il y a deux
façons de l'utiliser, selon les modifications que vous faites.</p>

<p>
Voici comment utiliser <code>smart_change.pl</code> et comment mettre à jour
seulement les en-têtes <code>translation-check</code> quand vous travaillez
manuellement sur les fichiers :
</p>

<ol>
  <li>modifiez le ou les fichiers originaux et envoyez les modifications ;</li>
  <li>mettez à jour les traductions ;</li>
  <li>exécutez <code>smart_change.pl -c COMMIT_HASH</code> (utiliser
  l'empreinte des modifications dans les fichiers originaux). Le script
  récupérera les modifications et mettra à jour les en-têtes dans les fichiers
  de traduction ;</li>
  <li>vérifiez les modifications (par exemple avec <code>git diff</code>) ;</li>
  <li>envoyez les modifications de traduction.</li>
</ol>

<p>
Autrement, vous pouvez travailler avec une expression rationnelle pour faire
plusieurs modifications dans plusieurs fichiers en une seule passe :
</p>

<ol>
  <li>exécutez <code>smart_change.pl -s s/toto/titi/ fichier_origine1
    fichier_origine2 ...</code> ;</li>
  <li>vérifiez les modifications (par exemple avec « git diff ») ;</li>
  <li>envoyez les fichiers originaux ;</li>
  <li>exécutez <code>smart_change.pl fichier_origine1 fichier_origine2</code>
    (c'est-à-dire <strong>sans l'expression rationnelle</strong> cette
    fois-ci) ; cela mettra à jour uniquement les en-têtes dans les fichiers
    traduits ;</li>
  <li>finalement, envoyez les modifications de traduction.</li>
</ol>

<p>
Il faut reconnaître que c'est plus compliqué qu'avec le premier exemple
(deux envois sont nécessaires), mais inévitable vue la manière dont
fonctionnent les empreintes d'envoi de Git.
</p>

<h2><a id="write-access">Accès en écriture au dépôt Git</a></h2>

<p>
Le code source du site web de Debian est géré avec Git et situé à l'adresse
<url https://salsa.debian.org/webmaster-team/webwml/>. Par défaut, les invités
(guest) ne sont pas autorisés à envoyer de « commits » sur le dépôt du code
source. Si vous souhaitez contribuer au site web de Debian, vous aurez besoin
de certaines permissions pour obtenir un accès en écriture au dépôt.

<h3><a name="write-access-unlimited">Accès en écriture illimité</a></h3>

<p>
Si vous avez besoin d'un accès en écriture illimité au dépôt (par exemple,
vous êtes sur le point de devenir un contributeur régulier), veuillez en
faire la demande à l'aide de l'interface web
<url https://salsa.debian.org/webmaster-team/webwml/> après vous être
connecté à la plateforme Salsa de Debian.
</p>

<p>
Si vous débutez dans le développement du site web de Debian et si vous
n'avez pas d'expérience antérieure, veuillez aussi envoyer un courriel à <a
href="mailto:debian-www@lists.debian.org">debian-www@lists.debian.org</a>
en vous présentant avant de demander l'accès en écriture illimité.
Pourriez-vous avoir la gentillesse d'en dire plus sur vous comme la partie du
site web à laquelle vous voudriez contribuer ou quelles langues vous parlez, et
aussi s'il y a un autre membre de l'équipe Debian qui pourrait se porter garant
de vous.
</p>

<h3><a name="write-access-via-merge-request">Demandes de fusion</a></h3>

<p>
Il n'est pas indispensable d'obtenir un accès en écriture illimité au dépôt
— vous pouvez toujours soumettre une demande de fusion (« merge request ») et
laisser d'autres développeurs revoir et accepter votre travail. Veuillez
soumettre les « merge requests » en utilisant la procédure standard fournie par
la plateforme GitLab de Salsa à travers son interface web et consulter les
deux documents suivants :
</p>

<ul>
  <li><a href="https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html">Project forking workflow</a></li>
  <li><a href="https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html#when-you-work-in-a-fork">When you work in a fork</a></li>
</ul>

<p>
Veuillez noter que les demandes de fusion ne sont pas suivies par tous les
développeurs du site web. Il se pourrait donc que cela prenne quelque temps
avant que vous receviez un commentaire. Si vous n'êtes pas sûr que votre
contribution sera acceptée, envoyez un message à la liste de diffusion <a
href="https://lists.debian.org/debian-www/">debian-www</a> et demandez une
relecture.
</p>

<h2><a name="notifications">Recevoir des notifications</a></h2>

<p>
Si vous travaillez sur le site web de Debian, vous voulez sans doute savoir ce
qui se passe dans le dépôt <code>webwml</code>. Il y a deux manières de se
tenir au courant : les notifications de modification et les notifications de
requête de fusion.</p>


<h3><a name="commit-notifications">Recevoir des notifications de modification</a></h3>
<p>
Nous avons configuré le projet webwml dans Salsa, de telle manière que les
modifications apparaissent sur le canal IRC #debian-www.</p>

<p>
Si vous voulez recevoir des notifications par courriel lorsqu'il y a des
modifications dans le dépôt <code>webwml</code>, veuillez vous abonner au
pseudo-paquet <q>www.debian.org</q> au moyen de
<a href="https://tracker.debian.org/">tracker.debian.org</a> et activez-y
le mot clé <code>vcs</code>, en suivant ces étapes (une seule fois) :</p>

<ol>
  <li>ouvrez un navigateur web et allez à l'adresse
      <url https://tracker.debian.org/pkg/www.debian.org> ;</li>
  <li>abonnez-vous au pseudo-paquet <code>www.debian.org</code> (vous pouvez
      vous authentifier par SSO ou enregistrer une adresse courriel et un mot 
      de passe, si vous n'utilisez pas déjà tracker.debian.org avec un autre
      objectif) ;</li>
  <li>allez à la page <url https://tracker.debian.org/accounts/subscriptions/>,
      puis à <code>modify keywords</code>, cochez <code>vcs</code> (si ce n'est
      pas déjà fait) et sauvegardez ;</li>
  <li>à partir de ce moment, vous recevrez des courriels lorsque quelqu'un
      enverra une modification dans le dépôt <code>webwml</code>.</li>
</ol>

<h3><a name="merge-request-notifications">Recevoir des notifications de requête de fusion</a></h3>

<p>
Si vous voulez recevoir des notifications par courriel lorsqu'il y a de
nouvelles requêtes de fusion soumises sur le dépôt <code>webwml</code> dans
Salsa, vous pouvez configurer les réglages de notification du dépôt webwml sur
l'interface web, en suivant ces étapes :
</p>

<ol>
  <li>connectez-vous à votre compte Salsa et allez sur la page du projet ;</li>
  <li>cliquez sur l'icône cloche en haut de la page d'accueil du projet ;</li>
  <li>choisissez le niveau de notification que vous préférez.</li>
</ol>
