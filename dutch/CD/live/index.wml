#use wml::debian::cdimage title="Live installatie-images"
#use wml::debian::release_info
#use wml::debian::installer
#include "$(ENGLISHDIR)/releases/images.data"
#use wml::debian::translation-check translation="7791836a42fa0b102a9f31cfc78e2c4345504388"

<p>Een <q>live installatie</q>-image bevat een Debian-systeem dat kan
opstarten zonder een bestand op de harde schijf te wijzigen. Ook maakt de
inhoud van het image de installatie van Debian mogelijk.
</p>

<p><a name="choose_live"><strong>Is een live-image geschikt voor mij?</strong></a>
Hierna volgen een aantal in aanmerking te nemen zaken die u kunnen helpen een beslissing te nemen.
<ul>
<li><b>Varianten:</b> De live-images bestaan in verschillende "varianten"
die een keuze bieden uit grafische werkomgevingen (GNOME, KDE, LXDE, Xfce,
Cinnamon en MATE).
<li><b>Architectuur:</b> Momenteel worden enkel images voor 64-bits pc (amd64)
aangeboden.
<li><b>Installatieprogramma:</b> de live-images bevatten
het gebruikersvriendelijke <a href="https://calamares.io">Calamares
installatieprogramma</a>, een distributie-onafhankelijk installatieraamwerk.
<li><b>Taal:</b> De images bevatten niet de volledige verzameling pakketten
voor taalondersteuning. Als u invoermethoden, lettertypes of bijkomende
pakketten nodig heeft voor uw taal, moet u deze nadien installeren.
</ul>

<div class="line">
<div class="item col50">
<h2 id="live-install-stable">Officiële live installatie-images voor de <q>stabiele</q> release</h2>

<p>Deze images zijn geschikt om een ​​Debian-systeem te proberen en het vervolgens vanaf datzelfde medium te installeren.
Ze kunnen worden geschreven naar USB-sticks en DVD-R(W)-media.</p>
    <ul class="quicklist downlist">
      <li><a title="Gnome live-ISO voor 64-bits Intel en AMD pc downloaden"
          href="<live-images-url/>/amd64/iso-hybrid/debian-live-<current-cd-release/>-amd64-gnome.iso">Live Gnome</a></li>
      <li><a title="Xfce live-ISO voor 64-bits Intel en AMD pc downloaden"
          href="<live-images-url/>/amd64/iso-hybrid/debian-live-<current-cd-release/>-amd64-xfce.iso">Live Xfce</a></li>
      <li><a title="KDE live-ISO voor 64-bits Intel en AMD pc downloaden"
          href="<live-images-url/>/amd64/iso-hybrid/debian-live-<current-cd-release/>-amd64-kde.iso">Live KDE</a></li>
      <li><a title="Ander live-ISO voor 64-bits Intel en AMD pc downloaden"
            href="<live-images-url/>/amd64/iso-hybrid/">Ander live-ISO</a></li>
      <li><a title="Live torrents voor 64-bits Intel en AMD pc downloaden"
          href="<live-images-url/>/amd64/bt-hybrid/">Live torrents</a></li>
    </ul>

</div>

<div class="item col50 lastcol">
<h2 id="live-install-testing">Officiële live installatie-images voor de <q>testing</q> release</h2>

<p>Deze images zijn geschikt om een ​​Debian-systeem te proberen en het vervolgens vanaf datzelfde medium te installeren.
Ze kunnen worden geschreven naar USB-sticks en DVD-R(W)-media.</p>
    <ul class="quicklist downlist">
      <li>TESTING IMAGES:</li>
      <li><a title="Gnome live-ISO voor 64-bits Intel en AMD pc downloaden"
          href="<live-images-testing-url/>/amd64/iso-hybrid/debian-live-testing-amd64-gnome.iso">Live Gnome</a></li>
      <li><a title="Xfce live-ISO voor 64-bits Intel en AMD pc downloaden"
          href="<live-images-testing-url/>/amd64/iso-hybrid/debian-live-testing-amd64-xfce.iso">Live Xfce</a></li>
      <li><a title="KDE live-ISO voor 64-bits Intel en AMD pc downloaden"
          href="<live-images-testing-url/>/amd64/iso-hybrid/debian-live-testing-amd64-kde.iso">Live KDE</a></li>
      <li><a title="Ander live-ISO voor 64-bits Intel en AMD pc downloaden"
            href="<live-images-testing-url/>/amd64/iso-hybrid/">Ander live-ISO</a></li>
    </ul>

</div>

</div>

<p>Raadpleeg de <a href="../faq/">FAQ</a> voor informatie over wat deze
bestanden zijn en hoe u ze kunt gebruiken.</p>

<p>Raadpleeg <a href="$(HOME)/devel/debian-live"> de projectpagina van Debian Live</a> voor
extra informatie over de Debian Live-systemen die op deze images aangeboden worden.</p>

