#use wml::debian::template title="Installare Debian via Internet" BARETITLE=true
#use wml::debian::toc
#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/releases/images.data"
#use wml::debian::translation-check translation="75e33068da4f64cce68c5fda8f15b7d6cd7179ea" maintainer="Luca Monducci"

<p>Questo metodo di installazione richiede una connessione ad Internet
funzionante <em>durante</em> l'installazione stessa. Rispetto agli altri
metodi, permette di scaricare una minore quantità di dati poiché il processo
di installazione viene fatto su misura in base alle proprie necessità.
Sono supportate sia le connessioni mediante Ethernet che wireless.
</p>

<div class="line">
<div class="item col50">

<h2>CD ridotti o per chiavette USB</h2>

<p>Sono disponibili i seguenti file immagine.
Per ulteriori informazioni consultare <a href="../CD/netinst/">Installazione
via rete da un CD minimale</a>.
</p>

<stable-netinst-images />
</div>
<div class="clear"></div>
</div>

<div class="line">
<div class="item col50">

<br/>
<h2>CD ridottissimi, per chiavette USB flessibili, ecc.</h2>

<p><strong>Per utenti esperti</strong>:
È possibile scaricare un paio di file immagine di piccole 
dimensioni, adatti alle chiavette USB o ad altri supporti rimovibili,
scriverli sul supporto
ed effettuare l'installazione facendo il boot da esso.</p>

<p>Nell'installazione mediante le varie immagini molto piccole ci sono alcune
differenze tra le architetture.
</p>

<p>Per istruzioni più dettagliate si faccia riferimento al
<a href="$(HOME)/releases/stable/installmanual">manuale di installazione per
la propria architettura</a> ed in particolare al capitolo
<q>Obtaining System Installation Media</q> (nel manuale in inglese) o
<q>Recupero dei supporti per l'installazione</q> (nel manuale in italiano).</p>

<p>
Questi sono i link ai file
immagine disponibili (si veda il file MANIFEST per ulteriori informazioni):
</p>

<stable-verysmall-images />
</div>
<div class="item col50 lastcol">

<br/>
<h2>Avvio da rete</h2>

<p><strong>Per utenti esperti</strong>:
Chi dispone di un server TFTP ed uno DHCP (o BOOTP o RARP) può
configurarli per fornire
i supporti per l'installazione alla propria rete locale e
se il BIOS delle proprie macchine client lo consente, è possibile avviare il
sistema di installazione Debian mediante rete (usando PXE e TFTP) e proseguire
con il resto dell'installazione via rete.</p>

<p>Non tutte le macchine supportano l'avvio dalla rete. A causa del
lavoro addizionale richiesto, questo metodo di installazione Debian non
è raccomandato agli utenti inesperti.</p>

<p>Per istruzioni più dettagliate si faccia riferimento al
<a href="$(HOME)/releases/stable/installmanual">manuale di installazione per
la propria architettura</a> ed in particolare al capitolo
<q>Preparing Files for TFTP Net Booting</q> (nel manuale in inglese) o
<q>Preparazione dei file per l'avvio TFTP da rete</q> (nel manuale in italiano).</p>
<p>Questi sono i link ai file immagine (si veda il file MANIFEST per ulteriori
informazioni):</p>

<stable-netboot-images />
</div>
</div>
