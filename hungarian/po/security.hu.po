msgid ""
msgstr ""
"Project-Id-Version: Debian webwml organization\n"
"PO-Revision-Date: 2011-01-10 22:48+0100\n"
"Last-Translator: unknown\n"
"Language-Team: unknown\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../english/security/dsa.rdf.in:16
msgid "Debian Security"
msgstr "Debian Security"

#: ../../english/security/dsa.rdf.in:19
msgid "Debian Security Advisories"
msgstr "Debian Biztonsági Figyelmeztetések"

#: ../../english/security/faq.inc:6
msgid "Q"
msgstr "K"

#: ../../english/security/index.include:17
msgid ""
"<a href=\\\"undated/\\\">undated</a> security advisories, included for "
"posterity"
msgstr ""
"<a href=\\\"undated/\\\">dátum nélküli</a> biztonsági figyelmeztetések, az "
"utókor számára"

#: ../../english/security/make-ref-table.pl:81
msgid "Mitre CVE dictionary"
msgstr "Mitre CVE szótár"

#: ../../english/security/make-ref-table.pl:84
msgid "Securityfocus Bugtraq database"
msgstr "Securityfocus Bugtraq adatbázis"

#: ../../english/security/make-ref-table.pl:88
msgid "CERT Advisories"
msgstr "CERT Figyelmeztetések"

#: ../../english/security/make-ref-table.pl:92
msgid "US-CERT vulnerabilities Notes"
msgstr "US-CERT Sebezhetőségi Feljegyzések"

#: ../../english/template/debian/security.wml:11
msgid "Source:"
msgstr "Forrás:"

#: ../../english/template/debian/security.wml:15
msgid "Architecture-independent component:"
msgstr "Architektúrafüggetlen komponens:"

#. don't translate `<get-var url />'
#: ../../english/template/debian/security.wml:22
msgid ""
"MD5 checksums of the listed files are available in the <a href=\"<get-var "
"url />\">original advisory</a>."
msgstr ""
"A listázott fájlokhoz MD5 ellenőrző összegek elérhetőek az <a href=\"<get-"
"var url />\">eredeti tanácsban</a>."

#. don't translate `<get-var url />'
#: ../../english/template/debian/security.wml:30
msgid ""
"MD5 checksums of the listed files are available in the <a href=\"<get-var "
"url />\">revised advisory</a>."
msgstr ""
"A listázott fájlokhoz MD5 ellenőrző összegek elérhetőek az <a href=\"<get-"
"var url />\">felülvizsgált figyelmeztetésben</a>."

#: ../../english/template/debian/security.wml:44
msgid "Debian Security Advisory"
msgstr "Debian Biztonsági Figyelmeztetésben"

#: ../../english/template/debian/security.wml:49
msgid "Date Reported"
msgstr "Bejelentés dátuma"

#: ../../english/template/debian/security.wml:52
msgid "Affected Packages"
msgstr "Érintett csomagok"

#: ../../english/template/debian/security.wml:74
msgid "Vulnerable"
msgstr "Sebezhető"

#: ../../english/template/debian/security.wml:77
msgid "Security database references"
msgstr "Biztonsági adatbázis hivatkozások"

#: ../../english/template/debian/security.wml:80
msgid "More information"
msgstr "Bővebb információ"

#: ../../english/template/debian/security.wml:86
msgid "Fixed in"
msgstr "Kijavítása"

#: ../../english/template/debian/securityreferences.wml:16
msgid "BugTraq ID"
msgstr "BugTraq ID"

#: ../../english/template/debian/securityreferences.wml:60
msgid "Bug"
msgstr "Hiba"

#: ../../english/template/debian/securityreferences.wml:76
msgid "In the Debian bugtracking system:"
msgstr "A Debian hibakövető rendszerben:"

#: ../../english/template/debian/securityreferences.wml:79
msgid "In the Bugtraq database (at SecurityFocus):"
msgstr "A Bugtraq adatbázisban (a SecurityFocus-on):"

#: ../../english/template/debian/securityreferences.wml:82
msgid "In Mitre's CVE dictionary:"
msgstr "A Mitre CVE szótárában:"

#: ../../english/template/debian/securityreferences.wml:85
msgid "CERT's vulnerabilities, advisories and incident notes:"
msgstr "A Cert sebezhetőségei, figyelmeztetései és feljegyzései:"

#: ../../english/template/debian/securityreferences.wml:88
msgid "No other external database security references currently available."
msgstr "Más, külső adatbázis hivatkozásai jelenleg nem elérhetőek."
