#use wml::debian::countries
#use wml::debian::url
#use wml::debian::common_tags
 
<bind-gettext-domain domain="partners" /> 
 
 <define-tag partnerofthemonth whitespace=delete>
 <gettext domain="partners">Partner of the Month</gettext>
 </define-tag>
 
 <define-tag alphabeticallist whitespace=delete>
 <gettext domain="partners">Alphabetical List of Partners</gettext>
 </define-tag>
 
 <define-tag developmentpartner whitespace=delete>
 <gettext domain="partners">Development Partners</gettext>
 </define-tag>
 
 <define-tag financialpartner whitespace=delete>
 <gettext domain="partners">Financial Partners</gettext>
 </define-tag>
 
 <define-tag servicepartner whitespace=delete>
 <gettext domain="partners">Service Partners</gettext>
 </define-tag>
 
## 1and1

 <define-tag oneandone1 whitespace=delete> 
 <p><gettext domain="partners"><a href="http://www.1and1.info/">1&amp;1</a> supports us by providing several servers in their Karlsruhe high-performance datacenter.</gettext></p> 
 </define-tag> 
 <define-tag oneandone2 whitespace=delete> 
 <p><gettext domain="partners">1&amp;1 is one of the biggest Domain Registrars and Web hosting companies worldwide, with offices in Germany, France, Great Britain, Spain and the USA. Most of the over 5 million websites and other services managed by 1&amp;1 are running on a Debian-based environment using several thousand servers.</gettext></p> 
</define-tag> 

## Ampere
## last update: 2020/06/16 (new partner)

<define-tag ampere1 whitespace=delete> 
 <p><gettext domain="partners"><a href="https://amperecomputing.com">Ampere®</a> provides hardware for arm64/armhf/armel build daemons and other Debian services (current Ampere hardware donations are listed in the <a href="https://db.debian.org/machines.cgi">Debian machines</a> page).</gettext></p> 
 </define-tag> 
 <define-tag ampere2 whitespace=delete> 
 <p><gettext domain="partners">Ampere is designing the future of hyperscale cloud and edge computing with the world’s first cloud native processor. Built for the cloud with a modern 64-bit Arm server-based architecture, Ampere gives customers the freedom to accelerate the delivery of all cloud computing applications. With industry-leading cloud performance, power efficiency and scalability, Ampere processors are tailored for the continued growth of cloud and edge computing.</gettext></p> 
</define-tag> 

## Bytemark
# last updates: 2019/02/19 (new logo) 
#               2017/06/13 (new logo, update description)
 
 <define-tag bytemark1 whitespace=delete> 
<p><gettext domain="partners"><a href="https://www.bytemark.co.uk/r/deb-partners">Bytemark</a> support us by providing hosting services and sponsorship for the Debian Conference.</gettext></p> 
</define-tag> 
 
 <define-tag bytemark2 whitespace=delete> 
 <p><gettext domain="partners">They are a leading Internet Service Provider (ISP) in the United Kingdom and provide scalable, powerful and affordable hosting with lots of <q>geek friendly</q> extras as standard. Debian GNU/Linux is deployed extensively within their network, and comes recommended as the <q>Distribution of Choice</q> to any new customer who's not 100% sure on what to pick. Bytemark are also well known for their expert and friendly support.</gettext></p> 
</define-tag> 
 
 ## conova
 # date added: 2017/05/02
 # last update: 2017/05/02
  
 <define-tag conova1 whitespace=delete>
 <p><gettext domain="partners"><a href="https://www.conova.com">conova communications GmbH</a> has been supporting the Debian project as a hosting partner for several years. In addition, the IT specialists also host the Debian Bug Squashing Parties in Salzburg/Austria. Debian is also used on many systems used on a daily basis at conova.</gettext></p>
 </define-tag> 
 <define-tag conova2 whitespace=delete> 
 <p><gettext domain="partners">conova operates one of the most modern data centers in all of Europe in Salzburg, Austria. Their services: customized solutions for housing, hosting, managed &amp; cloud services, outsourcing as well as network and security. The company offers space for more than 10,000 servers at the highest level of supply and security on their 2,000 m² of technical area.</gettext></p>
 </define-tag> 

 
## credativ

 <define-tag credativ1 whitespace=delete> 
 <p><gettext domain="partners"><a href="https://www.credativ.com/">credativ</a> is an independent consulting and services company and since 1999 been offering comprehensive services and technical support for the implementation and operation of open source software in business applications. Our <q>Open Source Support Center</q> is always available, 365 days a year, around the clock. </gettext></p> 
 </define-tag> 
 <define-tag credativ2 whitespace=delete> 
 <p><gettext domain="partners">From the very beginning credativ has actively supported the Debian project and will continue to do so in the future. Furthermore many of our consultants are also Debian developers and actively involved in contributing to free software projects worldwide. Further information can be found at <a href="https://www.credativ.com/">https://www.credativ.com/</a>.</gettext></p> 
 </define-tag> 
 

## Freexian
# last update: 2024/09/16
 <define-tag freexian1 whitespace=delete>
 <p><gettext domain="partners"><a href="https://www.freexian.com/">Freexian</a> is a services company specialized in Free Software and in particular Debian GNU/Linux. Most of Freexian's employees are well-known contributors in the free software community, a choice that is integral to Freexian's business model.</gettext></p>
 </define-tag> 
 <define-tag freexian2 whitespace=delete>
 <p><gettext domain="partners"> Freexian is a major contributor to the Debian LTS team. Freexian assist in extending the security support of stable releases to 5 years and extends security support for old Debian releases up to 10 years for Debian releases beyond the normal support period advertised by Debian.</gettext></p> 
 </define-tag> 

## Fastly
# last update: 2020/11/26
 
 <define-tag fastly1 whitespace=delete>
 <p><gettext domain="partners"><a href="https://www.fastly.com/">Fastly</a> provides Debian with content delivery network (CDN) services and is helping us deliver packages to users through <a href="https://deb.debian.org/">deb.debian.org</a> and <a href="http://security.debian.org">security.debian.org</a>.</gettext></p>
 </define-tag> 
 <define-tag fastly2 whitespace=delete> 
 <p><gettext domain="partners">Fastly’s edge cloud platform provides advanced application delivery and cloud security for the world’s most popular online destinations. Fastly works with the best of the Internet, serving 14 trillion requests each month, more than 10 percent of all internet requests.</gettext></p> 
 </define-tag> 

## Gandi
# last update: 2023/06/07

 <define-tag gandi1 whitespace=delete>
 <p><gettext domain="partners"><a href="https://www.gandi.net">Gandi</a> is Debian's DNS registrar and provides hosting and discounts to Debian developers and maintainers, supporting Debian France since 2014 and Debian Worldwide via SPI since 2022.</gettext></p>
 </define-tag> 
 <define-tag gandi2 whitespace=delete> 
 <p><gettext domain="partners">Gandi is a French registrar which is deeply engaged in supporting Free Open Source Software and other ethic projects since 2006.</gettext></p> 
 </define-tag> 
 

## Google
# last update: 2019/03/20 (News addition)

 <define-tag google1 whitespace=delete> 
 <p><gettext domain="partners"><a href="https://www.google.com">Google</a> sponsors parts of Salsa's continuous integration infrastructure within Google Cloud Platform.</gettext></p> 
 </define-tag> 
 <define-tag google2 whitespace=delete> 
 <p><gettext domain="partners">Google is one of the largest technology companies in the world, providing a wide range of Internet-related services and products as online advertising technologies, search, cloud computing, software, and hardware.</gettext></p> 
 </define-tag> 

## Hetzner
# date added: 2020/11/26
 <define-tag hetzner1 whitespace=delete>
 <p><gettext domain="partners"><a href="https://www.hetzner.com">Hetzner</a> provides the Debian project with hosting services.</gettext></p>
 </define-tag>
 <define-tag hetzner2 whitespace=delete>
 <p><gettext domain="partners">Hetzner Online is a professional web hosting provider and experienced data center operator. Since 1997 the company has provided private and business clients with high-performance hosting products as well as the necessary infrastructure for the efficient operation of websites. A combination of stable technology, attractive pricing and flexible support and services has enabled Hetzner Online to continuously strengthen its market position both nationally and internationally. The company owns several data centers in Germany and Finland.</gettext></p>
 </define-tag>

## HPE 
# last update: 2017/03/25 (new logo, update description)

 <define-tag hpe1 whitespace=delete> 
 <p><gettext domain="partners"><a href="http://www.hpe.com/engage/opensource">Hewlett Packard Enterprise (HPE)</a> provides hardware for port development, Debian mirrors, and other Debian services (current HPE hardware donations are listed in the <a href="https://db.debian.org/machines.cgi">Debian machines</a> page).</gettext></p> 
 </define-tag> 
 <define-tag hpe2 whitespace=delete>
 <p><gettext domain="partners">HPE is one of the largest computer companies in the world, providing a wide range of products and services, such as servers, storage, networking, consulting and support, software, and financial services.</gettext></p> 
 </define-tag> 
 
## Leaseweb
# date added: 2017/05/20
# last update: 2020/11/26
 
 <define-tag leaseweb1 whitespace=delete>
 <p><gettext domain="partners"><a href="https://www.leaseweb.com/">LeaseWeb</a> has been one of two partners that provide the infrastructure for the <a href="https://snapshot.debian.org">Debian OS Snapshot Archive</a> since <a href="https://www.debian.org/News/2014/20141014">October 2014</a>, providing 300 Terabytes (TB) of capacity. In 2020 they renewed their support by provisioning new dedicated servers with bigger disk drives, enough to accommodate anticipated growth for years to come.</gettext></p> \
 </define-tag> 
 <define-tag leaseweb2 whitespace=delete> 
 <p><gettext domain="partners">LeaseWeb is a global Infrastructure-as-a-Service (IaaS) provider – offering customers on-demand, world-class hosting solutions, from dedicated servers to cloud solutions. You can learn more about LeaseWeb visiting their <a href="https://www.leaseweb.com/">website</a>.</gettext></p> 
 </define-tag> 

## Lenovo
# last update: 2024/X/X
 <define-tag lenovo1 whitespace=delete>
 <p><gettext domain="partners"><a href="https://www.lenovo.com/">Lenovo</a> Some Text </gettext></p> 
 </define-tag> 
 <define-tag lenovo2 whitespace=delete>
 <p><gettext domain="partners">Lenovo is a </gettext></p> 
 </define-tag> 


## Loongson

  <define-tag loongson1 whitespace=delete> 
 <p><gettext domain="partners"><a href="http://www.loongson.cn">Loongson</a> and Lemote have provided several Loongson-based machines to Debian.</gettext></p>
 </define-tag> 
 <define-tag loongson2 whitespace=delete> 
 <p><gettext domain="partners">Loongson processors are a series of MIPS-compatible processors. They have been widely used in many areas, such as desktop, server, embedded application, high-performance computing etc. For more information, contact <a href="mailto:info@loongson.cn">info@loongson.cn</a>.</gettext></p> 
 </define-tag> 

## Man-Da 
# last update: 2020/11/26
 
  <define-tag man-da1 whitespace=delete> 
<p><gettext domain="partners"><a href="http://www.man-da.de/">man-da.de GmbH</a> is the backbone provider of the Metropolitan Area Network Darmstadt. It is supporting Debian by hosting several debian.org and debian.net servers.</gettext></p> 
</define-tag> 
 
 <define-tag man-da2 whitespace=delete> 
 <p><gettext domain="partners">man-da.de GmbH is operating MANDA, a wide area network in the South Hessen region connecting educational and research organisations to a high speed redundant network ring and providing internet access. The company is owned by TU Darmstadt and University of Applied Sciences Darmstadt and in addition to operating MANDA it is also providing IT consulting and IT services to both universities.</gettext></p> 
</define-tag> 

## OSUOSL
# last update: 2017/06/23 (new logo)

<define-tag osuosl1 whitespace=delete> 
 <p><gettext domain="partners"><a href="http://osuosl.org/">The Oregon State University Open Source Lab</a> provides hosting and administration services to the Debian project.</gettext></p> 
</define-tag> 
 
 <define-tag osuosl2 whitespace=delete> 
 <p><gettext domain="partners">The Open Source Lab is a focal point for open source development at Oregon State University and beyond. The OSL provides development, hosting and assorted other services to the Open Source community.</gettext></p> 
</define-tag> 

## Raptor Computing Systems
# last update: 2023/06/07 (new addition)

<define-tag raptor1 whitespace=delete> 
 <p><gettext domain="partners"><a href="https://www.raptorcs.com/">Raptor Computing Systems</a> provides Debian with bare metal access to fully open firmware POWER ISA servers, along with discounted pricing for fully open firmware POWER ISA desktop machines to Debian developers and maintainers.</gettext></p> 
</define-tag> 
 
 <define-tag raptor2 whitespace=delete> 
 <p><gettext domain="partners">Raptor Computing Systems is a hardware ODM specializing in fully owner-controllable computing hardware with 100&percnt; open-source firmware.  Their extensive experience with IBM POWER and OpenPOWER allows them to produce secure desktop and server class systems for general purpose computing applications, as well as custom hardware and FPGA / ASIC designs built around the open POWER ISA.</gettext></p> 
</define-tag> 

## RcodeZeroDNS
# last update: 2018/12/05 (added)

<define-tag rcodezerodns1 whitespace=delete> 
<p><gettext domain="partners">nic.at is sponsoring the anycast service <a href="https://www.rcodezero.at/">RcodeZero DNS</a> for Debian as it meets the needs to have geographically disperse locations and support DNSSEC.</gettext></p> 
</define-tag>

<define-tag rcodezerodns2 whitespace=delete> 
<p><gettext domain="partners">ipcom is a subsidiary of nic.at, the Austrian domain registry. nic.at has been managing the .at-zone since 1998 on a highly professional and reliable level. The RcodeZero Anycast network has been developed by nic.at’s R&amp;D department and has been successfully in use for the .at zone.</gettext></p> 
</define-tag> 


 ## Wellcome Sanger
 # date added: 2018/12/29

 <define-tag wellcome1 whitespace=delete> 
 <p><gettext domain="partners">The <a href="https://www.sanger.ac.uk/">Wellcome Sanger Institute</a> provides infrastructure for the <a href="https://snapshot.debian.org/">Debian OS Snapshot Archive</a> since the creation of the service in <a href="https://www.debian.org/News/2010/20100412">April 2010</a>. In 2018 they renewed their support by provisioning a new frontend server and increasing the amount of snapshot storage provided.</gettext></p> 
 </define-tag> 
 <define-tag wellcome2 whitespace=delete> 
 <p><gettext domain="partners">The Wellcome Sanger Institute is one of the world's leading genome centres. Through its ability to conduct research at scale, it is able to engage in bold and long-term exploratory projects that are designed to influence and empower medical science globally. Institute research findings, generated through its own research programmes and through its leading role in international consortia, are being used to develop new diagnostics and treatments for human disease.</gettext></p> 
 </define-tag> 

# UBC
# date added: 2024/09/13

 <define-tag ubc1 whitespace=delete> 
 <p><gettext domain="partners"><a href="https://arc.ubc.ca">The University of British Columbia (UBC)</a> provides Debian with infrastructure hosting since 2005.</gettext></p> 
</define-tag> 
 <define-tag ubc2 whitespace=delete> 
 <p><gettext domain="partners">UBC Advanced Research Computing (ARC) supports the high-performance computing, data management and infrastructure needs of UBC researchers. Working closely with national, regional, and institutional partners, UBC ARC provides consultation, expertise, and access to digital research infrastructure.</gettext></p> 
</define-tag> 
