#use wml::debian::translation-check translation="9d41ab1625a3bbe9bf95b782d91e91b766a3f664" maintainer="Grzegorz Szymaszek"
<define-tag pagetitle>Aktualizacja Debiana&nbsp;12: wersja 12.5 wydana</define-tag>
<define-tag release_date>2024-02-10</define-tag>
#use wml::debian::news
# $Id:

<define-tag release>12</define-tag>
<define-tag codename>bookworm</define-tag>
<define-tag revision>12.5</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>Projekt Debian ma zaszczyt ogłosić piątą aktualizację swojej stabilnej
dystrybucji Debian&nbsp;<release> (nazwa kodowa <q lang="en"><codename></q>).
To wydanie głównie uzupełnia poprawki problemów z&nbsp;bezpieczeństwem,
wraz z&nbsp;paroma ulepszeniami dla poważniejszych problemów.
Porady dotyczące bezpieczeństwa zostały już osobno opublikowane i&nbsp;będą
podlinkowane w&nbsp;miarę możliwości.</p>

<p>Proszę zauważyć, że to wydanie nie stanowi nowej wersji
Debiana&nbsp;<release>, a&nbsp;jedynie odświeża część dołączonych pakietów.
Nie ma potrzeby wyrzucania starych nośników instalacyjnych
<q lang="en"><codename></q>.
Po instalacji pakiety można zaktualizować do bieżących wersji używając
aktualnego serwera lustrzanego Debiana.</p>

<p>Osoby na bieżąco instalujące aktualizacje z&nbsp;security.debian.org nie będą
musiały aktualizować wielu pakietów, jednocześnie większość tych aktualizacji
znajduje się w&nbsp;nowym wydaniu.</p>

<p>Nowe nośniki instalacyjne zostaną udostępnione wkrótce w&nbsp;zwykłych
lokalizacjach.</p>

<p>Istniejącą instalację można zaktualizować do tego wydania wskazując systemowi
zarządzania pakietami jeden z&nbsp;wielu serwerów lustrzanych HTTP Debiana.
Kompletna lista serwerów jest dostępna na:</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>




<h2>Różne poprawki błędów</h2>

<p>To stabilne wydanie dodaje kilka ważnych poprawek do następujących pakietów:</p>

<table border=0>
<tr><th>Pakiet</th>               <th>Powód</th></tr>
<correction apktool "Uniemożliwienie pisania do dowolnych plików za pomocą złośliwych nazw zasobów [CVE-2024-21633]">
<correction atril "Naprawa awarii podczas otwierania niektórych plików epub; naprawa wczytywania indeksu dla niektórych dokumentów epub; dodanie awaryjnej obsługi źle zbudowanych plików epub w check_mime_type; użycie libarchive zamiast zewnętrznego polecenia od rozpakowywania dokumentów [CVE-2023-51698]">
<correction base-files "Aktualizacja dla wersji 12.5">
<correction caja "Naprawa artefaktów renderowania pulpitu po zmianie rozdzielczości; naprawa użycia <q>nieformalnego</q> formatu daty">
<correction calibre "Domyślnie uniemożliwenie dodawania zasobów umieszczonych poza hierarchią katalogów zakorzenioną w katalogu nadrzędnym wejściowego pliku HTML [CVE-2023-46303]">
<correction compton "Usunięcie rekomendacji picom">
<correction cryptsetup "cryptsetup-initramfs: dodanie obsługi kompresowanych modułów jądra; cryptsetup-suspend-wrapper: usunięcie błędu w przypadku brakującego katalogu /lib/systemd/system-sleep; add_modules(): dostosowanie logiki usuwania sufiksu do initramfs-tools">
<correction debian-edu-artwork "Dodanie oprawy graficznej opartej o motyw Emerald dla Debiana Edu 12">
<correction debian-edu-config "Nowe wydanie oryginalnego projektu">
<correction debian-edu-doc "Aktualizacja dołączonej dokumentacji i tłumaczeń">
<correction debian-edu-fai "Nowe wydanie oryginalnego projektu">
<correction debian-edu-install "Nowe wydanie oryginalnego projektu; naprawa wpisu security w sources.list">
<correction debian-installer "Podniesienie ABI jądra Linuksa do 6.1.0-18; przebudowanie dla proposed-updates">
<correction debian-installer-netboot-images "Przebudowanie dla proposed-updates">
<correction debian-ports-archive-keyring "Dodanie klucza Debian Ports Archive Automatic Signing Key (2025)">
<correction dpdk "Nowe wydanie stabilne oryginalnego projektu">
<correction dropbear "Poprawka na atak Terrapin [CVE-2023-48795]">
<correction engrampa "Naprawa kilku wycieków pamięci; naprawa funkcjonalności <q>zapisz jako</q> archiwum">
<correction espeak-ng "Naprawa błędów przepełnienia bufora [CVE-2023-49990 CVE-2023-49992 CVE-2023-49993], błędu niedomiaru bufora [CVE-2023-49991], problemu wyjątku liczb zmiennoprzecinkowych [CVE-2023-49994]">
<correction filezilla "Ochrona przed wykorzystaniem błędu w Terrapin [CVE-2023-48795]">
<correction fish "Bezpieczna obsługa niedrukowalnych znaków Unikodu gdy podane w podstawianiu wyników poleceń [CVE-2023-49284]">
<correction fssync "Wyłączenie zawodnych testów">
<correction gnutls28 "Naprawa błędu asercji podczas weryfikacji łańcucha certyfikatów z cykliczną zależnością między podpisami [CVE-2024-0567]; naprawa problemu czasowego kanału bocznego [CVE-2024-0553]">
<correction indent "Naprawa problemu odczytu bufora [CVE-2024-0911]">
<correction isl "Naprawa działania na starszych procesorach">
<correction jtreg7 "Nowy pakiet źródłowy do obsługi openjdk-17">
<correction libdatetime-timezone-perl "Aktualizacja dołączonych danych stref czasowych">
<correction libde265 "Naprawa błędów przepełnienia bufora [CVE-2023-49465 CVE-2023-49467 CVE-2023-49468]">
<correction libfirefox-marionette-perl "Naprawa kompatybilności z nowszymi wersjami firefox-esr">
<correction libmateweather "Naprawa adresu URL aviationweather.gov">
<correction libspreadsheet-parsexlsx-perl "Naprawa potencjalnej bomby pamięciowej [CVE-2024-22368]; naprawa problemu z XML External Entity [CVE-2024-23525]">
<correction linux "Nowe wydanie stabilne oryginalnego projektu; podniesienie ABI do 18">
<correction linux-signed-amd64 "Nowe wydanie stabilne oryginalnego projektu; podniesienie ABI do 18">
<correction linux-signed-arm64 "Nowe wydanie stabilne oryginalnego projektu; podniesienie ABI do 18">
<correction linux-signed-i386 "Nowe wydanie stabilne oryginalnego projektu; podniesienie ABI do 18">
<correction localslackirc "Wysyłanie nagłówków uwierzytelniania i ciasteczek do gniazda websocket">
<correction mariadb "Nowe wydanie stabilne oryginalnego projektu; naprawa problemu blokady usług [CVE-2023-22084]">
<correction mate-screensaver "Naprawa wycieków pamięci">
<correction mate-settings-daemon "Naprawa wycieków pamięci; złagodzenie ograniczeń wysokich rozdzielczości; naprawa obsługi wielu zdarzeń rfkill">
<correction mate-utils "Naprawa różnych wycieków pamięci">
<correction monitoring-plugins "Naprawa wtyczki check_http gdy jest użyty argument <q>--no-body</q> oraz oryginalna odpowiedź jest dzielona na kawałki">
<correction needrestart "Naprawa regresji sprawdzania mikrokodu na procesorach AMD">
<correction netplan.io "Naprawa autopkgtest przy nowszych wersjach systemd">
<correction nextcloud-desktop "Naprawa synchronizacji plików ze znakami specjalnymi jak ‘:’; naprawa powiadomień dwuskładnikowego uwierzytelniania">
<correction node-yarnpkg "Naprawa użycia z Commander 8">
<correction onionprobe "Naprawa inicjalizacji Tora przy użyciu haszowanych haseł">
<correction pipewire "Używanie malloc_trim() gdy dostępne do zwalniania pamięci">
<correction pluma "Naprawa wycieków pamięci; naprawa dwukrotnej aktywacji rozszerzeń">
<correction postfix "Nowe wydanie stabilne oryginalnego projektu; obsługa problemu SMTP smuggling [CVE-2023-51764]">
<correction proftpd-dfsg "Implementacja poprawki na atak Terrapin [CVE-2023-48795]; naprawa odczytu poza zakresem [CVE-2023-51713]">
<correction proftpd-mod-proxy "Implementacja poprawki na atak Terrapin [CVE-2023-48795]">
<correction pypdf "Naprawa problemu nieskończonej pętli [CVE-2023-36464]">
<correction pypdf2 "Naprawa problemu nieskończonej pętli [CVE-2023-36464]">
<correction pypy3 "Unikanie błędu asercji rpython w JIT gdy zakresy liczb nie nachodzą na siebie w pętli">
<correction qemu "Nowe wydanie stabilne oryginalnego projektu; virtio-net: prawidłowe kopiowanie nagłówka vnet podczas wypróżniania TX [CVE-2023-6693]; naprawa problemu dereferencji pustego wskaźnika [CVE-2023-6683]; wycofanie łatki powodującej regresje w funkcjonalności usypiania/wybudzania">
<correction rpm "Włączenie zaplecza tylko do odczytu BerkeleyDB">
<correction rss-glx "Instalacja wygaszaczy ekranu do /usr/libexec/xscreensaver; wywołanie GLFinish() przed glXSwapBuffers()">
<correction spip "Naprawa dwóch problemów cross-site scripting">
<correction swupdate "Ochrona przed uzyskaniem praw administratora poprzez nieodpowiedni tryb gniazda">
<correction systemd "Nowe wydanie stabilne oryginalnego projektu; naprawa brakującej weryfikacji w systemd-resolved [CVE-2023-7008]">
<correction tar "Naprawa sprawdzania granic w dekoderze base-256 [CVE-2022-48303], obsługa rozszerzonych prefiksów nagłówka [CVE-2023-39804]">
<correction tinyxml "Naprawa problemu asercji [CVE-2023-34194]">
<correction tzdata "Nowe wydanie stabilne oryginalnego projektu">
<correction usb.ids "Aktualizacja dołączonych danych">
<correction usbutils "Naprawa wyświetlania wszystkich urządzeń przez usb-devices">
<correction usrmerge "Czyszczenie wieloarchitekturowych katalogów gdy nie są potrzebne; pomijanie ponownego uruchamiania convert-etc-shells na skonwertowanych systemach; obsługa zamontowanego /lib/modules na systemach Xen; poprawione zgłaszanie błędów; dodanie wersjonowanych konfliktów z libc-bin, dhcpcd, libparted1.8-10 i lustre-utils">
<correction wolfssl "Naprawa błędu bezpieczeństwa gdy klient nie wysłał rozszerzeń PSK ani KSE [CVE-2023-3724]">
<correction xen "Nowe wydanie stabilne oryginalnego projektu; security fixes [CVE-2023-46837 CVE-2023-46839 CVE-2023-46840]">
</table>


<h2>Aktualizacje bezpieczeństwa</h2>


<p>To wydanie dodaje następujące aktualizacje bezpieczeństwa do wersji
stabilnej.
Zespół Bezpieczeństwa opublikował już porady do każdej z&nbsp;tych
aktualizacji:</p>

<table border=0>
<tr><th>Identyfikator porady</th>  <th>Pakiet</th></tr>
<dsa 2023 5572 roundcube>
<dsa 2023 5573 chromium>
<dsa 2023 5574 libreoffice>
<dsa 2023 5576 xorg-server>
<dsa 2023 5577 chromium>
<dsa 2023 5578 ghostscript>
<dsa 2023 5579 freeimage>
<dsa 2023 5581 firefox-esr>
<dsa 2023 5582 thunderbird>
<dsa 2023 5583 gst-plugins-bad1.0>
<dsa 2023 5584 bluez>
<dsa 2023 5585 chromium>
<dsa 2023 5586 openssh>
<dsa 2023 5587 curl>
<dsa 2023 5588 putty>
<dsa 2023 5589 node-undici>
<dsa 2023 5590 haproxy>
<dsa 2023 5591 libssh>
<dsa 2023 5592 libspreadsheet-parseexcel-perl>
<dsa 2024 5593 linux-signed-amd64>
<dsa 2024 5593 linux-signed-arm64>
<dsa 2024 5593 linux-signed-i386>
<dsa 2024 5593 linux>
<dsa 2024 5595 chromium>
<dsa 2024 5597 exim4>
<dsa 2024 5598 chromium>
<dsa 2024 5599 phpseclib>
<dsa 2024 5600 php-phpseclib>
<dsa 2024 5601 php-phpseclib3>
<dsa 2024 5602 chromium>
<dsa 2024 5603 xorg-server>
<dsa 2024 5605 thunderbird>
<dsa 2024 5606 firefox-esr>
<dsa 2024 5607 chromium>
<dsa 2024 5608 gst-plugins-bad1.0>
<dsa 2024 5609 slurm-wlm>
<dsa 2024 5610 redis>
<dsa 2024 5611 glibc>
<dsa 2024 5612 chromium>
<dsa 2024 5613 openjdk-17>
<dsa 2024 5614 zbar>
<dsa 2024 5615 runc>
</table>



<h2>Instalator Debiana</h2>
<p>Instalator został zaktualizowany i&nbsp;uwzględnia poprawki dołączone do
dystrybucji stabilnej w&nbsp;nowej wersji.</p>

<h2>Adresy <abbr lang="en">URL</abbr></h2>

<p>Kompletna lista pakietów, które zmieniły się w&nbsp;tej wersji:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>Aktualna dystrybucja stabilna:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/stable/">
</div>

<p>Proponowane aktualizacje do dystrybucji stabilnej:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/proposed-updates">
</div>

<p>Informacje o&nbsp;dystrybucji stabilnej (uwagi do wydania, erraty
<abbr title="i&nbsp;tak dalej">itd.</abbr>):</p>

<div class="center">
  <a
  href="$(HOME)/releases/stable/">https://www.debian.org/releases/stable/</a>
</div>

<p>Ogłoszenia i&nbsp;informacje dotyczące bezpieczeństwa:</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>O&nbsp;Debianie</h2>

<p>Projekt Debian jest zrzeszeniem twórców Wolnego Oprogramowania, którzy
poświęcają swój czas i&nbsp;wysiłek w&nbsp;celu stworzenia całkowicie wolnego
systemu operacyjnego, Debiana.</p>

<h2>Informacje kontaktowe</h2>

<p>Więcej informacji można znaleźć na stronach internetowych Debiana pod adresem
<a href="$(HOME)/">https://www.debian.org/</a>, wysyłając wiadomość na adres
&lt;press@debian.org&gt; lub kontaktując się z&nbsp;zespołem wydania stabilnego
pod adresem &lt;debian-release@lists.debian.org&gt;.</p>
