#use wml::debian::template title="Documentation"
#use wml::debian::recent_list
#use wml::debian::translation-check translation="019351f156c8f8ffc0f7922929f7062fb4b732db" maintainer="Jean-Paul Guillonneau"

# Premier traducteur: Frédéric Bothamy, 2004-2007
# Previous translator, Jean-Paul Guillonneau, 2021

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<ul class="toc">
  <li><a href="#quick_start">Démarrage rapide</a></li>
  <li><a href="#manuals">Manuels</a></li>
  <li><a href="#other">Autres documents plus concis</a></li>
</ul>

<p>
La création d'un système d'exploitation libre de haute qualité englobe les
manuels techniques qui décrivent le fonctionnement et l'utilisation des
programmes. Le «&nbsp;Projet Debian&nbsp;» s'efforce de fournir à tous ses
utilisateurs une documentation facilement accessible. Cette page fournit un
ensemble de liens vers des guides d'installation, des guides pratiques, des FAQ,
les notes de publication et bien plus encore.
</p>

<h2><a id="quick_start">Démarrage rapide</a></h2>

<p>
Si vous ne connaissez pas Debian, nous vous recommandons de
commencer par lire ces deux guides&nbsp;:
</p>

<aside class="light">
  <span class="fas fa-fast-forward fa-5x"></span>
</aside>

<ul>
  <li><a href="$(HOME)/releases/stable/installmanual">le manuel d'installation</a>,</li>
  <li><a href="manuals/debian-faq/">la FAQ Debian GNU/Linux</a>.</li>
</ul>

<p>
Conservez ces documents à portée de main lors de votre première
installation de Debian, ils répondront probablement à un grand nombre de
questions et ils vous aideront à utiliser votre nouveau système
Debian.
</p>

<p>
Par la suite, vous pouvez consulter ces documents&nbsp;:
</p>

<ul>
  <li><a href="manuals/debian-reference/">la référence Debian</a> : le
  manuel concis pour l’utilisateur axé sur la ligne de commande ;</li>
  <li><a href="$(HOME)/releases/stable/releasenotes">les notes de publication</a> :
  publiées à chaque mise à jour de Debian, pour les personnes effectuant une
  mise à niveau de la distribution ;</li>
  <li><a href="https://wiki.debian.org/">le wiki Debian </a> : le wiki officiel
  de Debian.</li>
</ul>

<p style="text-align:center"><button type="button"><span class="fas fa-print fa-2x"></span> <a href="https://www.debian.org/doc/manuals/refcard/refcard">Imprimez la carte de référence de Debian GNU/Linux</a></button></p>

<h2><a id="manuals">Manuels</a></h2>

<p>
La plupart de la documentation disponible avec Debian est issue de la
documentation écrite pour le système GNU/Linux. Il existe, toutefois, des
documents spécifiques à Debian. En gros, ils sont classés selon les catégories
suivantes&nbsp;:
</p>

<ul>
  <li>les manuels : ces guides sont comparables à des livres parce qu'ils
  décrivent des sujets majeurs de manière très détaillée. Beaucoup des
  manuels listés ici sont disponibles à la fois en ligne et sous la forme
  de paquets Debian. En fait, la plupart des manuels sur le site web sont
  extraits de leurs paquets respectifs. Vous pouvez choisir un des manuels
  ci-dessous par son nom de paquet ou par sa version en ligne ;</li>
  <li>les guides d'emploi : comme leurs noms l'indiquent, les
  <a href="https://tldp.org/HOWTO/HOWTO-INDEX/categories.html">guides d'emploi</a>
  (« HOWTO ») décrivent <em>comment faire</em> une action particulière
  c'est-à-dire qu'ils offrent des conseils pratiques détaillés sur la manière
  de faire quelque chose ;</li>
  <li>les FAQ : nous avons rassemblé plusieurs documents répondant à des
  <em>questions souvent posées</em>. Les questions spécifiques à Debian sont
  traitées dans la <a href="manuals/debian-faq/">FAQ Debian</a>. Il existe
  également une <a href="../CD/faq/">FAQ sur les images de USB/CD/DVD Debian</a>,
  répondant à toutes sortes de questions sur les média d'installation ;</li>
  <li>autres documents plus concis : consultez aussi <a href="#other">liste</a>
  de modes d'emploi concis.</li>
</ul>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> Pour disposer d'une liste complète des manuels Debian et des autres documentations, allez consulter la page du <a href="ddp">Debian Documentation Project</a>. En outre, plusieurs manuels destinés à l'utilisateur, écrits pour Debian GNU/Linux, sont disponibles sous la forme d'<a href="books">ouvrages imprimés</a>.</p>
</aside>

<p>Beaucoup des manuels listés ici sont disponibles à la fois en ligne et sous
la forme de paquets Debian. En fait, la plupart des manuels sur le site web
sont extraits de leurs paquets respectifs. Vous pouvez choisir un des manuels
ci-dessous par son nom de paquet ou par sa version en ligne.</p>

<h3>Manuels spécifiques à Debian</h3>

<div class="line">
  <div class="item col50">
    
    <h4><a href="user-manuals">Manuels pour les utilisateurs</a></h4>
    <ul>
      <li><a href="https://debian-beginners-handbook.tuxfamily.org/index-fr.html">Les cahiers du débutant sur Debian Bookworm</a></li>
      <li><a href="user-manuals#faq">FAQ Debian GNU/Linux</a></li>
      <li><a href="user-manuals#install">Manuel d'installation Debian</a></li>
      <li><a href="user-manuals#relnotes">Notes de publication Debian</a></li>
      <li><a href="user-manuals#refcard">Carte de référence Debian</a></li>
      <li><a href="user-manuals#debian-handbook">Manuel de l’administrateur Debian</a></li>
      <li><a href="user-manuals#quick-reference">Guide de référence pour Debian</a></li>
      <li><a href="user-manuals#securing">Manuel de sécurisation</a></li>
      <li><a href="user-manuals#aptitude">Manuel de l’utilisateur d’aptitude</a></li>
      <li><a href="user-manuals#apt-guide">Guide pour l’utilisateur d’APT</a></li>
      <li><a href="user-manuals#java-faq">FAQ Debian GNU/Linux et Java</a></li>
      <li><a href="user-manuals#hamradio-maintguide">Guide pour le responsable du radioamateurisme dans Debian</a></li>
    </ul>

  </div>

  <div class="item col50 lastcol">

    <h4><a href="devel-manuals">Manuels pour les développeurs</a></h4>
    <ul>
      <li><a href="devel-manuals#policy">Charte Debian</a></li>
      <li><a href="devel-manuals#devref">Référence du développeur Debian</a></li>
      <li><a href="devel-manuals#debmake-doc">Guide pour les responsables de
      paquet Debian</a></li>
      <li><a href="devel-manuals#packaging-tutorial">Introduction à
      l'empaquetage Debian</a></li>
      <li><a href="devel-manuals#menu">Le système de menu Debian</a></li>
      <li><a href="devel-manuals#d-i-internals">À l’intérieur de l’installateur Debian</a></li>
      <li><a href="devel-manuals#dbconfig-common">Guide pour les responsables de paquet utilisant une base de données</a></li>
      <li><a href="devel-manuals#dbapp-policy">Politique pour les paquets utilisant une base de données</a></li>
    </ul>

  </div>
</div>

<p style="text-align:center"><button type="button"><span class="fas fa-book-open fa-2x"></span> <a href="misc-manuals#history">Lisez l'histoire du projet Debian</a></button></p>

<h2><a id="other">Autres documents plus concis</a></h2>

<p>Les documents suivants fournissent des instructions plus concises et
rapidement consultables&nbsp;:</p>
<p>
<dl>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> Si vous avez consulté les
ressources précédentes et que vous n'avez toujours pas trouvé de réponses à
vos questions ou de solutions à vos problèmes concernant Debian, jetez un
coup d'&oelig;il à notre<a href="../support">page dédiée à l'assistance</a>.</p>
</aside>

  <dt><strong>Pages de manuels</strong></dt>
    <dd>Traditionnellement, tous les programmes Unix sont documentés par des
        <em>pages de manuel</em>, accessibles avec la commande
        <tt>man</tt>. Elles ne s'adressent habituellement pas aux
        débutants, mais documentent toutes les caractéristiques et fonctions
        d'une commande. Le dépôt complet de l'ensemble des pages de manuel
        disponibles dans Debian est en ligne à l'adresse <a
        href="https://manpages.debian.org/cgi-bin/man.cgi">https://manpages.debian.org/</a>
    </dd>

  <dt><strong><a href="https://www.gnu.org/software/texinfo/manual/texinfo/html_node/index.html">Fichiers info</a></strong></dt>
    <dd>Un grand nombre de logiciels GNU sont documentés par des
        <em>fichiers info</em> au lieu de pages de manuel. Ces fichiers
        incluent des informations détaillées sur le programme lui-même,
        ses options et des exemples d'utilisation. Les pages info
        sont disponibles grâce à la commande <tt>info</tt>.
    </dd>

  <dt><strong>Fichiers README</strong></dt>
    <dd>Les fichiers README sont de simples fichiers textes décrivant un
        sujet unique, d'habitude un paquet. Vous en trouverez beaucoup dans
        les sous-répertoires <tt>/usr/share/doc/</tt> du système Debian. En
        plus du fichier README, certains de ces répertoires contiennent des
        exemples de configuration. Veuillez noter que la documentation des
        plus gros programmes est habituellement fournie dans un paquet
        séparé (avec un nom similaire au paquet d'origine, mais se
        terminant par <em>-doc</em>).
    </dd>
</dl>
</p>
