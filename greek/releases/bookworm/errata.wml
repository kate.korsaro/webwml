#use wml::debian::template title="Debian 12 -- Παροράματα" BARETITLE=true
#use wml::debian::toc
#use wml::debian::translation-check translation="a5d0ace4d5a31020a8e5aa86a3b29c8089f7574b" maintainer="galaxico"

#include "$(ENGLISHDIR)/releases/info"

<toc-display/>


# <toc-add-entry name="known_probs">Known problems</toc-add-entry>
<toc-add-entry name="security">Ζητήματα ασφαλείας</toc-add-entry>

<p>Η ομάδα ασφαλείας του Debian εκδίδει επικαιροποιήσεις πακέτων στην σταθερή διανομή
στα οποία έχουν εντοπιστεί προβλήματα που σχετίζονται με την ασφάλεια. Παρακαλούμε, συμβουλευθείτε
τις
<a href="$(HOME)/security/">σελίδες ασφαλείας</a> για πληροφορίες σχετικές με οποιαδήποτε
προβλήματα ασφαλείας εντοπίζονται για την έκδοση <q>bookworm</q>.</p>

<p>Αν χρησιμοποιείτε το APT, προσθέστε την ακόλουθη γραμμή στο αρχείο <tt>/etc/apt/sources.list</tt>
για να μπορείτε να έχετε πρόσβαση στις πιο πρόσφατες επικαιροποιήσεις ασφαλείας:</p>

<pre>
  deb http://security.debian.org/ bookworm-security main contrib non-free non-free-firmware
</pre>

<p>Μετά από αυτό, τρέξτε την εντολή< kbd>apt update</kbd> ακολουθούμενη από την εντολή
<kbd>apt upgrade</kbd>.</p>


<toc-add-entry name="pointrelease">Σημειακές εκδόσεις</toc-add-entry>

<p>Μερικές φορές, στην περίπτωση αρκετών κρίσιμων προβλημάτων ή επικαιροποιήσεων ασφαλείας, η διανομή που κυκλοφορεί
επικαιροποιείται. Γενικά, αυτές οι επικαιροποιήσεις υποδεικνύονται ως σημειακές εκδόσεις.</p>


<ifeq <current_release_bookworm> 12.0 "

<p>Δεν υπάρχουν ακόμα σημειακές εκδόσεις για το Debian 12.</p>" "

<p>Δείτε το αρχείο <a
href="http://http.us.debian.org/debian/dists/bookworm/ChangeLog">\
ChangeLog</a>
για λεπτομέρειες σχετικά με τις αλλαγές μεταξύ της έκδοσης 12 και <current_release_bookworm/>.</p>"/>


<p>Διορθώσεις την σταθερή διανομή σε κυκλοφορία περνάνε συχνά μέσα από εκτενή δοκιμαστική περίοδο
πριν γίνουν δεκτές στην αρχειοθήκη, Παρ' όλα αυτά, αυτές οι διορθώσεις είναι διαθέσιμες
ατον κατάλογο
<a href="http://ftp.debian.org/debian/dists/bookworm-proposed-updates/">\
dists/bookworm-proposed-updates</a> οποιουδήποτε καθρέφτη της αρχειοθήκης του Debian.</p>

<p>Αν χρησιμοποιείτε το APT για να αναβαθμίσετε τα πακέτα σας, μπορείτε να εγκαταστήσετε τις
προτεινόμενες αναβαθμίσεις προσθέτοντας την ακόλουθη γραμμή στο αρχείο
<tt>/etc/apt/sources.list</tt>:</p>

<pre>
  \# proposed additions for a 12 point release
  deb https://deb.debian.org/debian bookworm-proposed-updates main contrib non-free-firmware non-free
</pre>

<p>Μετά από αυτό, τρέξτε την εντολή <kbd>apt update</kbd> ακολουθούμενη από την
εντολή <kbd>apt upgrade</kbd>.</p>


<toc-add-entry name="installer">Σύστημα εγκατάστασης</toc-add-entry>

<p>
Για πληροφορίες σχετικά με παροράματα και επικαιροποιήσεις για το σύστημα εγκατάστασης, δείτε την σελίδα 
 <a href="debian-installer/">πληροφορίες εγκατάστασης</a>.
</p>
