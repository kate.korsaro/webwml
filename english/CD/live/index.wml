#use wml::debian::cdimage title="Live install images"
#use wml::debian::release_info
#use wml::debian::installer
#include "$(ENGLISHDIR)/releases/images.data"

<p>A <q>live install</q> image contains a Debian system that can boot without
modifying any files on the hard drive and also allows installation of Debian
from the contents of the image.
</p>

<p><a name="choose_live"><strong>Is a live image suitable for me?</strong></a> Here are some things
to consider that will help you decide.
<ul>
<li><b>Flavors:</b> The live images come in several "flavors"
providing a choice of desktop environments (GNOME, KDE, LXDE, Xfce,
Cinnamon and MATE).
<li><b>Architecture:</b> Currently only images for 64-bit PC (amd64) are provided.
<li><b>Installer:</b> The live images contain
the end-user-friendly <a href="https://calamares.io">Calamares Installer</a>, a
distribution-independent installer framework
<li><b>Languages:</b> The images do not contain a complete set of language
support packages. If you need input methods, fonts and supplemental language
packages for your language, you'll need to install these afterwards.
</ul>

</div>

<div class="line">
<div class="item col50">
<h2 id="live-install-stable">Official live install images for the <q>stable</q> release</h2>

<p>These
images are suitable for trying a Debian system and then install it from the same media.
They can be written to USB keys and DVD-R(W) media.</p>
    <ul class="quicklist downlist">
      <li><a title="Download Gnome live ISO for 64-bit Intel and AMD PC"
          href="<live-images-url/>/amd64/iso-hybrid/debian-live-<current-cd-release/>-amd64-gnome.iso">Live Gnome</a></li>
      <li><a title="Download Xfce live ISO for 64-bit Intel and AMD PC"
          href="<live-images-url/>/amd64/iso-hybrid/debian-live-<current-cd-release/>-amd64-xfce.iso">Live Xfce</a></li>
      <li><a title="Download KDE live ISO for 64-bit Intel and AMD PC"
          href="<live-images-url/>/amd64/iso-hybrid/debian-live-<current-cd-release/>-amd64-kde.iso">Live KDE</a></li>
      <li><a title="Download other live ISO for 64-bit Intel and AMD PC"
            href="<live-images-url/>/amd64/iso-hybrid/">Other live ISO</a></li>
      <li><a title="Download live torrents for 64-bit Intel and AMD PC"
          href="<live-images-url/>/amd64/bt-hybrid/">live torrents</a></li>
    </ul>

</div>

<div class="item col50 lsatcol">
<h2 id="live-install-testing">Official live install images for the <q>testing</q> release</h2>

<p>These
images are suitable for trying a Debian system and then install it from the same media.
They can be written to USB keys and DVD-R(W) media.</p>
    <ul class="quicklist downlist">
      <li>TESTING IMAGES:</li>
      <li><a title="Download Gnome live ISO for 64-bit Intel and AMD PC"
          href="<live-images-testing-url/>/amd64/iso-hybrid/debian-live-testing-amd64-gnome.iso">Live Gnome</a></li>
      <li><a title="Download Xfce live ISO for 64-bit Intel and AMD PC"
          href="<live-images-testing-url/>/amd64/iso-hybrid/debian-live-testing-amd64-xfce.iso">Live Xfce</a></li>
      <li><a title="Download KDE live ISO for 64-bit Intel and AMD PC"
          href="<live-images-testing-url/>/amd64/iso-hybrid/debian-live-testing-amd64-kde.iso">Live KDE</a></li>
      <li><a title="Download other live ISO for 64-bit Intel and AMD PC"
            href="<live-images-testing-url/>/amd64/iso-hybrid/">Other live ISO</a></li>
    </ul>

</div>

</div>

<p>For information about what these files are and how to use them, please see
the <a href="../faq/">FAQ</a>.</p>

<p>See the <a href="$(HOME)/devel/debian-live">Debian Live Project page</a> for
more information about the Debian Live systems provided by these images.</p>
