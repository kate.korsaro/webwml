# vendors webwml Catalan template.
# Copyright (C) 2002, 2003 Free Software Foundation, Inc.
# Jordi Mallach <jordi@debian.org>, 2002, 2003.
# Guillem Jover <guillem@debian.org>, 2007-2011, 2017.
#
msgid ""
msgstr ""
"Project-Id-Version: webwml\n"
"PO-Revision-Date: 2017-06-19 00:13+0200\n"
"Last-Translator: Guillem Jover <guillem@debian.org>\n"
"Language-Team: Catalan <debian-l10n-catalan@lists.debian.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../english/CD/vendors/vendors.CD.def:14
msgid "Vendor"
msgstr "Proveïdor"

#: ../../english/CD/vendors/vendors.CD.def:15
msgid "Allows Contributions"
msgstr "Permet contribucions a Debian"

#: ../../english/CD/vendors/vendors.CD.def:16
msgid "CD/DVD/BD/USB"
msgstr "CD/DVD/BD/USB"

#: ../../english/CD/vendors/vendors.CD.def:17
msgid "Architectures"
msgstr "Arquitectures"

#: ../../english/CD/vendors/vendors.CD.def:18
msgid "Ship International"
msgstr "Enviaments internacionals"

#: ../../english/CD/vendors/vendors.CD.def:19
msgid "Contact"
msgstr "Contacte"

#. ###################
#. Vendor home page link, Debian page link
#: ../../english/CD/vendors/vendors.CD.def:44
msgid "Vendor Home"
msgstr "Pàgina del proveïdor"

#: ../../english/CD/vendors/vendors.CD.def:70
msgid "page"
msgstr "pàgina"

#: ../../english/CD/vendors/vendors.CD.def:71
msgid "email"
msgstr "correu-e"

#: ../../english/CD/vendors/vendors.CD.def:114
msgid "within Europe"
msgstr "dins d'Europa"

#: ../../english/CD/vendors/vendors.CD.def:118
msgid "To some areas"
msgstr "A algunes àrees"

#: ../../english/CD/vendors/vendors.CD.def:122
msgid "source"
msgstr "font"

#: ../../english/CD/vendors/vendors.CD.def:126
msgid "and"
msgstr "i"

#~ msgid "Allows Contribution to Debian:"
#~ msgstr "Permet contribucions a Debian:"

#~ msgid "Architectures:"
#~ msgstr "Arquitectures:"

#~ msgid "CD Type:"
#~ msgstr "Tipus de CD:"

#~ msgid "Country:"
#~ msgstr "Estat:"

#~ msgid "Custom Release"
#~ msgstr "Versió personalitzada"

#~ msgid "DVD Type:"
#~ msgstr "Tipus de DVD:"

#~ msgid "Development Snapshot"
#~ msgstr "Instantània de desenvolupament"

#~ msgid "Multiple Distribution"
#~ msgstr "Mùltiples distribucions"

#~ msgid "Official CD"
#~ msgstr "CD Oficial"

#~ msgid "Official DVD"
#~ msgstr "DVD Oficial"

#~ msgid "Ship International:"
#~ msgstr "Enviaments internacionals:"

#~ msgid "URL for Debian Page:"
#~ msgstr "URL de la pàgina sobre Debian:"

#~ msgid "Vendor Release"
#~ msgstr "Versió del venedor"

#~ msgid "Vendor:"
#~ msgstr "Venedor:"

#~ msgid "contrib included"
#~ msgstr "contrib inclòs"

#~ msgid "email:"
#~ msgstr "correu-e:"

#~ msgid "non-US included"
#~ msgstr "non-US inclòs"

#~ msgid "non-free included"
#~ msgstr "non-free inclòs"

#~ msgid "reseller"
#~ msgstr "revenedor"

#~ msgid "reseller of $var"
#~ msgstr "revenedor de $var"

#~ msgid "updated monthly"
#~ msgstr "actualitzat mensualment"

#~ msgid "updated twice weekly"
#~ msgstr "actualitzat dos vegades a la setmana"

#~ msgid "updated weekly"
#~ msgstr "actualitzat semanalment"

#~ msgid "vendor additions"
#~ msgstr "afegiments del venedor"
