#use wml::debian::template title="Instalando o Debian via Internet" BARETITLE=true
#use wml::debian::toc
#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/releases/images.data"
#use wml::debian::translation-check translation="75e33068da4f64cce68c5fda8f15b7d6cd7179ea"

<p>Este método de instalação do Debian requer uma conexão permanente
à Internet <em>durante</em> a instalação. Comparado com outros métodos
você acaba baixando menos dados já que o processo será adaptado às suas
necessidades. Conexões de Ethernet e sem fio são suportadas.</p>

<div class="line">
<div class="item col50">

<h2>CDs pequenos ou pendrives USB</h2>

<p>A seguir estão os arquivos de imagens. Para mais detalhes, por favor veja:
<a href="../CD/netinst/">instalação pela rede com um CD mínimo</a>


<stable-netinst-images />
</div>
<div class="clear"></div>
</div>

<div class="line">
<div class="item col50">

<br/>
<h2>CDs minúsculos, pendrives USB, etc.</h2>

<p><strong>Para usuários(as) avançados(as)</strong>:Você pode baixar um par de
arquivos de imagens de tamanho pequeno, ideais
para pendrives USB e dispositivos similares, gravá-los na mídia e então começar
a inicialização a partir dela.</p>

<p>Há alguma diversidade no suporte à instalação a partir de várias imagens
muito pequenas entre as arquiteturas.
</p>

<p>Para mais detalhes, por favor consulte o
<a href="$(HOME)/releases/stable/installmanual">manual de instalação para a
sua arquitetura </a>, especialmente o capítulo
<q>Obtendo a mídia de instalação do sistema</q>.</p>

<p>
Aqui estão os links para os arquivos de imagens disponíveis (veja o arquivo
MANIFEST para informações):
</p>

<stable-verysmall-images />
</div>
<div class="item col50 lastcol">

<br/>
<h2>Inicialização pela rede</h2>

<p><strong>Para usuários(as) avançados(as)</strong>: você configura um servidor
TFTP e um DHCP (ou BOOTP, ou RARP) que irá
fornecer a mídia de instalação para as máquinas da sua rede local.
Se a BIOS da sua máquina cliente suportar, você pode então inicializar o
sistema de instalação Debian a partir da rede (usando PXE e TFTP) e
prosseguir com a instalação do restante do Debian a partir da rede.</p>

<p>Nem todas as máquinas suportam inicialização a partir da rede. Por causa
do trabalho adicional requerido, este método de instalação do Debian não
é recomendado para usuários(as) sem experiência.</p>

<p>Para detalhes, por favor consulte o
<a href="$(HOME)/releases/stable/installmanual">manual de instalação para
sua arquitetura</a>, especialmente o capítulo
<q>Preparando arquivos para inicialização TFTP pela rede</q>.</p>
<p>Aqui estão os links para os arquivos de imagens (veja o arquivo MANIFEST
para informações):</p>

<stable-netboot-images />
</div>
</div>
