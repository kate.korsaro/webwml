#use wml::debian::template title="Posizione di Debian sui brevetti software" BARETITLE="true"
#use wml::debian::translation-check translation="088c528e5eb9d95504ef91aa449795178ac06be1" maintainer="Ceppo"

<pre>
  <code>
  Versione: 1.0
  Data di pubblicazione: 19 febbraio 2012
  </code>
</pre>

<h1>Introduzione</h1>

<p>
Questo documento descrive la posizione di Debian sui brevetti. Debian riconosce
la minaccia al Free Software costituita dai brevetti e continua a lavorare con
altri nella comunità del Free Software per la difesa da essi.
</p>

<p>
La politica seguente vuole fornire indicazioni chiare e precise ai membri della
comunità che affrontano problemi legati ai brevetti, in modo che la comunità
Debian possa coordinare la propria difesa da essi in modo libero da timori,
incertezze e dubbi.
</p>

<h1>Dichiarazione della politica</h1>

<ol>
  <li><p>
    Debian non distribuisce coscientemente software gravato da brevetti; i
    collaboratori di Debian non devono impacchettare o distribuire software del
    quale sanno che sanno viola un brevetto.
  </p></li>
  <li><p>
    Debian non accetta licenze relative a brevetti che non rispettino il <a
    href="$(HOME)/social_contract">Contratto Sociale Debian</a> o le <a
    href="$(HOME)/social_contract#guidelines">Linee Guida Debian per il
    Software Libero</a>.
  </p></li>
  <li><p>
    A meno che siano soggette al segreto professionale tra avvocato e cliente, i
    membri della comunità possono essere costretti a fornire in un caso
    giudiziario le comunicazioni legate a brevetti. Inoltre, preoccupazioni
    legate ai brevetti espresse in pubblico potrebbero rivelarsi infondate ma
    nel frattempo creare comunque notevole paura, incertezza e dubbio. Quindi,
    si prega di astenersi dallo scrivere pubblicamente di preoccupazioni legate
    ai brevetti o discutere di brevetti al di fuori delle comunicazioni con la
    comitato legale, dove sono soggette al segreto professionale del rapporto
    tra avvocato e cliente.
  </p></li>
  <li><p>
    I rischi dei brevetti colpiscono l'intera comunità. Chi fosse preoccupato
    per un brevetto specifico non deve tenerlo per sé: informi il comitato
    legale.
  </p></li>
  <li><p>
    Tutte le comunicazioni relative ad uno specifico specifico rischio legato
    ai brevetti devono esere indirizzate all'indirizzo <a
    href="mailto:patents@debian.org">patents@debian.org</a>, le comunicazioni
    con il quale sono coperte dal segreto professionale del rapporto tra
    avvocato e cliente. Il rischio sarà valutato e qualunque misura necessaria
    sarà intrapresa direttamente nei confronti delle parti coinvolte.
  </p></li>
</ol>

<h1>Informazioni di contatto</h1>

<p>
Chi voglia contattarci riguardo specifici rischi legati ai brevetti
nell'archivio Debian deve scrivere a <a
href="mailto:patents@debian.org">patents@debian.org</a> specificando:
</p>

<ul>
  <li>il proprio nome,</li>
  <li>il proprio ruolo in Debian,</li>
  <li>il nome dei pacchetti o dei progetti coinvolti,</li>
  <li>titolo, numero e giurisdizione dei brevetti coinvolti,</li>
  <li>una descrizione delle proprie preoccupazioni.</li>
</ul>

<h1>Ulteriori informazioni</h1>

<p>
Per altre informazioni sui brevetti e su come interagiscono con distribuzioni
comunitarie di Free Software raccomandiamo la lettura delle nostre <a
href="$(HOME)/reports/patent-faq">domande frequenti sulle politiche delle
distribuzioni comunitarie riguardo brevetti</a>, un documento inteso a istruire
gli sviluppatori di Free Software, e specialmente gli editor delle
distribuzioni, sui rischi dei brevetti sul software.
</p>

<hr />

<p>
Il Progetto Debian ringrazia il <a
href="http://www.softwarefreedom.org">Software Freedom Law Center</a> (SFLC)
per il suo parere giuridico su questi temi.
</p>
