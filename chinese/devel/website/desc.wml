#use wml::debian::template title="www.debian.org 是如何被制作出来的" MAINPAGE="true"
#use wml::debian::toc
#use wml::debian::translation-check translation="835e2e2ec1141439d03f50e38dba3a48eaaf6e81"

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<ul class="toc">
<li><a href="#look">外观</a></li>
<li><a href="#sources">源代码</a></li>
<li><a href="#scripts">脚本</a></li>
<li><a href="#generate">生成网站</a></li>
<li><a href="#help">怎样帮忙</a></li>
<li><a href="#faq">怎样帮不上忙……（FAQ）</a></li>
</ul>

<h2><a id="look">外观</a></h2>

<p>Debian 网站是位于 <em>www-master.debian.org</em> 上\
的 <code>/org/www.debian.org/www</code> 目录的文件和目录的集合。页面的大部分内容\
都是静态 HTML 文件。它们不包含 CGI 或 PHP 脚本之类的动态元素，因为\
网站需要能被镜像。
</p>

<p>Debian 网站使用网站元语言（Website Meta Language，\
<a href="https://packages.debian.org/unstable/web/wml">WML</a>）生成 HTML 页面，\
包括页眉和页脚、标题、目录等等。尽管 <code>.wml</code> 文件乍看\
起来像是一个 HTML 文件，但是 HTML 只是 .wml 中可以使用的其中一种额外信息。您也\
可以在页面中加入 Perl 代码，然后就可以做几乎任何事情。
</p>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> 我们的网页目前遵守 <a href="http://www.w3.org/TR/html4/">HTML 4.01 Strict</a> 标准。</p>
</aside>

<p>
在 WML 在一个文件上运行其各种筛选器后，得到的最终产品就是真正的 HTML 文件。\
请注意，虽然 WML 能检查（有时能自动纠正）您的 HTML 代码的最基础的正确性，您还是\
应该安装 <a href="https://packages.debian.org/unstable/web/weblint">weblint</a> \
和/或 <a href="https://packages.debian.org/unstable/web/tidy">tidy</a> 之类的工具\
作为最基本的语法和编码风格检查工具。
</p>

<h2><a id="sources">源代码</a></h2>

<p>
我们使用 Git 存储 Debian 网站的源代码。这一版本控制系统让我们可以跟踪所有的\
更改，我们可以看到谁更改了什么、什么时候更改的，以及为什么更改。Git 提供了一个在\
多个作者并行编辑源文件时加以控制的安全的方法，由于 Debian 网站团队规模很大，\
所以这一点对我们来说至关重要。
</p>

<p style="text-align:center"><button type="button"><span class="fas fa-book-open fa-2x"></span> <a href="using_git">阅读更多关于 Git 的内容</a></button></p>

<p>
以下是关于源代码如何组织的一些背景信息：
</p>

<ul>
  <li>Git 存储库中最顶层的目录（<code>webwml</code>）包含以网站所使用的语言\
命名的目录、两个 Makefile 文件以及几个脚本。翻译目录名称应使用英文小写字母\
（例如使用 <code>german</code> 而不是 <code>Deutsch</code>）。</li>

  <li><code>Makefile.common</code> 文件尤其重要，因为它包含一些通用规则，\
其它 Makefile 文件通过包含该文件来应用这些规则。</li>

  <li>每种语言所在的子目录都包含 Makefile、各种 <code>.wml</code> 源文件和更多的\
子目录。所有文件名和目录名都遵循特定的模式，以便令链接适用于所有语言。有些目录\
还包含一个 <code>.wmlrc</code> 配置文件，其中包含额外的命令和配置信息。</li>

  <li><code>webwml/english/template</code> 目录包含特殊的 WML 模板文件。\
它们能被所有其它文件通过 <code>#use</code> 指令来引用。</li>
</ul>

<p>
请注意：为了使对模板的更改传播到使用它们的文件，所以其它文件在 Makefile 中依赖\
它们。绝大多数文件都使用 <code>template</code> 模板，所以它们的开头都有如下一行：
</p>

<p>
<code>#use wml::debian::template</code>
</p>

<p>
当然，这条规则也有例外。
</p>

<h2><a id="scripts">脚本</a></h2>

<p>
用到的脚本主要用 shell 或 Perl 语言编写。其中一些能独立工作，\
还有一些已集成到 WML 源文件中。
</p>

<ul>
  <li><a href="https://salsa.debian.org/webmaster-team/cron.git">webmaster-team/cron</a>: 
这一 Git 存储库包含了更新 Debian 网站需要的所有脚本，例如重新构建 \
<code>www-master</code> 的脚本的源代码。</li>
  <li><a href="https://salsa.debian.org/webmaster-team/packages">webmaster-team/packages</a>: 
这一 Git 存储库包含了重新构建 <code>packages.debian.org</code> 的脚本的源代码。</li>
</ul>

<h2><a id="generate">生成网站</a></h2>

<p>
WML、模板和 shell 或 Perl 脚本是您生成 Debian 网站需要的全部原料：
</p>

<ul>
  <li>大部分内容是通过 WML 生成的（来自此 <a href="$(DEVEL)/website/using_git">Git 存储库</a>）。</li>
  <li>文档是从对应的 Debian 软件包通过 DocBook XML（<a href="$(DOC)/vcs"><q>ddp</q>Git 存储库</a>）\
或 <a href="#scripts">cron 脚本</a>生成的。</li> 
  <li>网站的部分内容是使用其他来源的脚本生成的，例如，邮件列表订阅/取消订阅页面。</li>
</ul>

<p>
每天运行六次自动更新（从 Git 存储库和其他来源到 webtree）。此外，我们还定期对\
整个网站运行以下检查：
</p>

<ul>
  <li><a href="https://www-master.debian.org/build-logs/urlcheck/">URL check</a>
  <li><a href="https://www-master.debian.org/build-logs/validate/">wdg-html-validator</a>
  <li><a href="https://www-master.debian.org/build-logs/tidy/">tidy</a>
</ul>

<p>
网站最新的构建日志可以在 <url "https://www-master.debian.org/build-logs/"> 找到。
</p>

<p>如果您想参与网站开发，开始时请<strong>不要</strong>简单地编辑 <code> www/</code>\
目录中的文件或添加新项目。请首先与 <a href="mailto:webmaster@debian.org">webmaster 团队</a>联系。
</p>

<aside>
<p><span class="fas fa-cogs fa-3x"></span> 一个技术细节：\
所有的文件与目录均属于 <code>debwww</code> 组，而且该组具有写入权限，因此 web 团队可以修改 web \
目录中的文件。目录的 <code>2775</code> 模式意味着在该目录下创建的任何文件都将继承该组（<code>debwww</code>）。\
该组的所有成员都应设置 <code>umask 002</code>，以使该组对创建的文件有写入权限。</p>
</aside>

<h2><a id="help">怎样帮忙</a></h2>

<p>
我们鼓励任何人帮助改善 Debian 网站。如果您认为我们的页面上\
缺少一些关于 Debian 的有价值的信息，请\
<a href="mailto:debian-www@lists.debian.org">联系我们</a>，我们一定会加入该信息。\
此外，也请查看上面提到的\
<a href="https://www-master.debian.org/build-logs/">构建日志</a>，看看您是否有\
修复其中问题的建议。
</p>

<p>
我们也需要可以帮助设计网页（图像和布局等）的人。\
如果您熟悉英语，我们希望您能对我们的页面进行校对并向我们\
<a href="mailto:debian-www@lists.debian.org">报告</a>其中的错误。\
如果您使用其他语言，您可能想帮助我们翻译现有的页面，或者修复已翻译的页面的问题。\
不论何种情况，都请查看<a href="translation_coordinators">翻译协调员</a>列表，然后\
联系负责人。请参阅<a href="translating">翻译页面</a>以获取更多信息。
</p>

<p style="text-align:center"><button type="button"><span class="fas fa-book-open fa-2x"></span> <a href="todo">阅读我们的待办事项列表</a></button></p>


<aside class="light">
  <span class="fa fa-question fa-5x"></span>
</aside>

<h2><a id="faq">怎样帮不上忙……（FAQ）</a></h2>

<p>
<strong>[问] 我想给 Debian 网站添加<em>一个华丽的功能</em>，可以吗？</strong>
</p>

<p>
[答] 不行。我们希望 www.debian.org 尽可能易于访问，因此
</p>

<ul>
    <li>不要使用浏览器特定的<q>扩展</q>（译注：指特定浏览器在 Web 标准之外额外支持的渲染特性），
    <li>不要仅依靠图像。图像有时会被用来更好的解释某一项内容，但 www.debian.org 上的信息\
        必须保证能被诸如 lynx 等纯文本网页浏览器访问。
</ul>

<p>
<strong>[问] 我想提出一个好主意。 您可以在 www.debian.org 的 HTTP 服务器中启用 \
<em>foo</em> 或者 <em>bar</em> 吗？</strong>
</p>

<p>
[答] 不行。我们希望管理员可以轻松地镜像 www.debian.org，所以请不要使用特殊的 \
HTTPD 功能。不，即使是 SSI（Server Side Includes）也不行。内容协商已作为例外进行处理，因为它是提供多种语言服务的\
唯一的一种可靠方法。
</p>
