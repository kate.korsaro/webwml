#use wml::debian::template title="Con người: Chúng tôi là ai và làm gì" MAINPAGE="true"
#include "$(ENGLISHDIR)/releases/info"
#use wml::debian::translation-check translation="5eef343480ec39f5bb8db5e4e69bbb9e8b926c9b" maintainer="Trần Ngọc Quân"

# translators: some text is taken from /intro/about.wml

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<div id="toc">
  <ul class="toc">
    <li><a href="#history">Nó được bắt đầu như thế nào</a>
    <li><a href="#devcont">Nhà phát triển và người đóng góp</a>
    <li><a href="#supporters">Cá nhân và tổ chức hỗ trợ Debian</a>
    <li><a href="#users">Người dùng Debian</a>
  </ul>
</div>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> Như nhiều người đã hỏi: Debian được phát âm <span style="white-space: nowrap;">/&#712;de.bi.&#601;n/.</span> Nó xuất phát từ sự kết hợp tên của người tạo ra Debian, Ian Murdock, và vợ ông là Debra.</p>
</aside>

<h2><a id="history">Nó được bắt đầu như thế nào</a></h2>

<p>Debian được bắt đầu vào tháng 8 năm 1993 bởi Ian Murdock, là một hệ điều hành
mới được thực hiện một cách công khai, theo tinh thần của Linux và
GNU. Ông gửi một lời mời mở tới những nhà phát triển phần mềm khác, mời họ
đóng góp cho một bản phân phối phần mềm dựa trên nhân Linux,
cái mà khá mới tại thời điểm đó. Debian được sắp đặt kỹ lưỡng và chu toàn,
được bảo trì và hỗ trợ với chế độ
giống như vậy, nắm lấy một thiết kế mở, những người đóng góp và hỗ trợ
đến từ cộng đồng Phần mềm tự do.</p>

<p>Bắt đầu từ một nhóm các hacker phần mềm tự do quy mô nhỏ nhưng chặt
chẽ và dần dần phát triển thành nhóm lớn, một cộng đồng có tổ chức gồm nhiều nhà
phát triển và người dùng.</p>

<p style="text-align:center"><button type="button"><span class="fas fa-book-open fa-2x"></span> <a href="$(DOC)/manuals/project-history/">Đọc toàn bộ lịch sử</a></button></p>

<h2><a id="devcont">Nhà phát triển và người đóng góp</a></h2>

<p>
Debian là một tổ chức hoàn toàn tình nguyện. Có hàng ngàn
nhà phát triển <a href="$(DEVEL)/developers.loc">trên khắp thế giới</a>
làm việc với Debian trong thời gian rảnh của họ. Rất ít người trong số đó biết mặt nhau.
Thay vào đó họ giao tiếp chủ yếu thông qua thư điện tử (bó thư
tại <a href="https://lists.debian.org/">lists.debian.org</a>) và IRC
(kênh Debian tại irc.debian.org).
</p>

<p>
Danh sách đầy đủ các thành viên Debian chính thức có thể tìm thấy ở
<a href="https://nm.debian.org/members">nm.debian.org</a>, và
<a href="https://contributors.debian.org">contributors.debian.org</a>
hiển thị một danh sách những người đóng góp và các nhóm những người mà làm việc
trên bản phân phối Debian.</p>

<p>Debian có <a href="organization">cơ cấu tổ chức</a> chặt chẽ.
Để biết thêm thông tin cụ thể về Debian, xin vui xem
<a href="$(DEVEL)/">Góc của các nhà phát triển</a>.</p>

<p style="text-align:center"><button type="button"><span class="fas fa-book-open fa-2x"></span> <a href="philosophy">Đọc Triết lý sống của chúng tôi</a></button></p>

<h2><a id="supporters">Cá nhân và tổ chức hỗ trợ Debian</a></h2>

<p>Ngoài các nhà phát triển và người đóng góp, nhiều các nhân và
tổ chức khác cũng là một phần của cộng đồng Debian:</p>

<ul>
  <li><a href="https://db.debian.org/machines.cgi">Hosting và nhà tài trợ phần cứng</a></li>
  <li><a href="../mirror/sponsors">Tài trợ máy lưu bản sao</a></li>
  <li><a href="../partners/">Đối tác phát triển và dịch vụ</a></li>
  <li><a href="../consultants">Tư vấn</a></li>
  <li><a href="../CD/vendors">Nhà cung cấp phương tiện cài đặt Debian</a></li>
  <li><a href="../distrib/pre-installed">Nhà cung cấp máy tính cung cấp các máy được cài đặt sẵn Debian</a></li>
  <li><a href="../events/merchandise">Nhà cung cấp hàng hóa</a></li>
</ul>

<h2><a id="users">Người dùng Debian</a></h2>

<p>
Debian được sử dụng bởi nhiều tổ chức lớn nhỏ, cũng như
hàng nghìn các nhân riêng lẻ. Xem trang thông tin <a href="../users/">Ai đang dùng Debian?</a>
để có danh sách các tổ chức giáo dục, kinh doanh và tổ chức phi lợi nhuận
cũng như là chính phủ chia sẻ đôi lời về cách thức và
lý do họ sử dụng Debian.
</p>
