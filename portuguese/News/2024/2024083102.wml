#use wml::debian::translation-check translation="f93b89c74962ae015feaa58f013cef8f7ec08dd9"
<define-tag pagetitle>Atualização Debian 11: 11.11 lançado</define-tag>
<define-tag release_date>2024-08-31</define-tag>
#use wml::debian::news
# $Id:

<define-tag release>11</define-tag>
<define-tag codename>bullseye</define-tag>
<define-tag revision>11.11</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>O projeto Debian está feliz em anunciar a décima primeira e última
atualização de sua antiga versão estável (oldstable) do Debian <release>
(codinome <q><codename></q>).
Esta versão pontual adiciona principalmente correções para problemas de
segurança, além de pequenos ajustes para problemas mais sérios. Avisos de
segurança já foram publicados em separado e são referenciados quando
necessário.</p>

<p>Por favor, note que a versão pontual não constitui uma nova versão do Debian
<release>, mas apenas atualiza alguns dos pacotes já incluídos. Não há
necessidade de jogar fora as antigas mídias do <q><codename></q>. Após a
instalação, os pacotes podem ser atualizados para as versões atuais usando um
espelho atualizado do Debian.</p>

<p>Aquelas pessoas que frequentemente instalam atualizações a partir de
security.debian.org não terão que atualizar muitos pacotes, e a maioria de tais
atualizações estão incluídas na versão pontual.</p>

<p>Novas imagens de instalação logo estarão disponíveis nos locais
habituais.</p>

<p>A atualização de uma instalação existente para esta revisão pode ser feita
apontando o sistema de gerenciamento de pacotes para um dos muitos espelhos
HTTP do Debian. Uma lista abrangente de espelhos está disponível em:</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>


<h2>Secure Boot e outros sistemas operacionais</h2>

<p>Pessoas que inicializam outros sistemas operacionais no mesmo hardware, e
que possuem Secure Boot habilitado, devem estar cientes que o shim 15.8
(incluso no Debian <revision>) revoga assinaturas espalhadas em versões
anteriores do shim no firmware UEFI.
Isso pode causar a falha de inicialização de outros sistemas operacionais que
utilizem uma versão shim anterior à 15.8.</p>

<p>Pessoas afetadas podem desabilitar temporariamente o Secure Boot antes de
atualizar outros sistemas operacionais.</p>


<h2>Correções gerais de bugs</h2>

<p>Esta atualização da antiga versão estável (oldstable) adiciona algumas
correções importantes para os seguintes pacotes:</p>

<table border=0>
<tr><th>Pacote</th>               <th>Justificativa</th></tr>
<correction amd64-microcode "New upstream release; security fixes [CVE-2023-31315]; SEV firmware fixes [CVE-2023-20584 CVE-2023-31356]">
<correction ansible "New usptream stable release; fix template injection issue [CVE-2021-3583], information disclosure issue [CVE-2021-3620], file overwrite issue [CVE-2023-5115], template injection issue [CVE-2023-5764], information disclosure issues [CVE-2024-0690 CVE-2022-3697]; document workaround for ec2 private key leak [CVE-2023-4237]">
<correction apache2 "New upstream stable release; fix content disclosure issue [CVE-2024-40725]">
<correction base-files "Update for the point release">
<correction bind9 "Allow the limits introduced to fix CVE-2024-1737 to be configured">
<correction calibre "Fix cross site scripting issue [CVE-2024-7008], SQL injection issue [CVE-2024-7009]">
<correction choose-mirror "Update list of available mirrors">
<correction cjson "Add NULL checks to cJSON_SetValuestring and cJSON_InsertItemInArray [CVE-2023-50472 CVE-2023-50471 CVE-2024-31755]">
<correction cups "Fix issues with domain socket handling [CVE-2024-35235]; fix regression when domain sockets only are used">
<correction curl "Fix ASN.1 date parser overread issue [CVE-2024-7264]">
<correction debian-installer "Increase Linux kernel ABI to 5.10.0-32; rebuild against proposed-updates">
<correction debian-installer-netboot-images "Rebuild against proposed-updates">
<correction dropbear "Fix <q>noremotetcp</q> behaviour of keepalive packets in combination with the <q>no-port-forwarding</q> authorized_keys(5) restriction">
<correction fusiondirectory "Backport compatibility with php-cas version addressing CVE 2022-39369; fix improper session handling issue [CVE-2022-36179]; fix cross site scripting issue [CVE-2022-36180]">
<correction gettext.js "Fix server side request forgery issue [CVE-2024-43370]">
<correction glewlwyd "Fix buffer overflow during webauthn signature assertion [CVE-2022-27240]; prevent directory traversal in static_compressed_inmemory_website_callback.c [CVE-2022-29967]; copy bootstrap, jquery, fork-awesome instead of linking them; buffer overflow during FIDO2 signature validation [CVE-2023-49208]">
<correction glibc "Fix ffsll() performance issue depending on code alignment; performance improvements for memcpy() on arm64; fix y2038 regression in nscd following CVE-2024-33601 and CVE-2024-33602 fix">
<correction graphviz "Fix broken scaling">
<correction gtk+2.0 "Avoid looking for modules in current working directory [CVE-2024-6655]">
<correction gtk+3.0 "Avoid looking for modules in current working directory [CVE-2024-6655]">
<correction healpix-java "Fix build failure">
<correction imagemagick "Fix divide by zero issues [CVE-2021-20312 CVE-2021-20313]; fix incomplete fix for CVE-2023-34151">
<correction indent "Reinstate ROUND_UP macro and adjust the initial buffer size to fix memory handling problems; fix out-of-buffer read in search_brace()/lexi(); fix heap buffer overwrite in search_brace() [CVE-2023-40305]; heap buffer underread in set_buf_break() [CVE-2024-0911]">
<correction intel-microcode "New upstream release; security fixes [CVE-2023-42667 CVE-2023-49141 CVE-2024-24853 CVE-2024-24980 CVE-2024-25939]">
<correction libvirt "Fix sVirt confinement issue [CVE-2021-3631], use after free issue [CVE-2021-3975], denial of service issues [CVE-2021-3667 CVE-2021-4147 CVE-2022-0897 CVE-2024-1441 CVE-2024-2494 CVE-2024-2496]">
<correction midge "Exclude examples/covers/* for DFSG-compliance; add build-arch/build-indep build targets; use quilt (3.0) source package format">
<correction mlpost "Fix build failure with newer ImageMagick versions">
<correction net-tools "Drop build-dependency on libdnet-dev">
<correction nfs-utils "Pass all valid export flags to nfsd">
<correction ntfs-3g "Fix use-after-free in <q>ntfs_uppercase_mbs</q> [CVE-2023-52890]">
<correction nvidia-graphics-drivers-tesla-418 "Fix use of GPL-only symbols causing build failures">
<correction nvidia-graphics-drivers-tesla-450 "New upstream stable release">
<correction nvidia-graphics-drivers-tesla-460 "New upstream stable release">
<correction ocsinventory-server "Backport compatibility with php-cas version addressing CVE 2022-39369">
<correction onionshare "Demote obfs4proxy dependency to Recommends, to allow removal of obfs4proxy">
<correction php-cas "Fix Service Hostname Discovery Exploitation issue [CVE-2022-39369]">
<correction poe.app "Make comment cells editable; fix drawing when an NSActionCell in the preferences is acted on to change state">
<correction putty "Fix weak ECDSA nonce generation allowing secret key recovery [CVE-2024-31497]">
<correction riemann-c-client "Prevent malformed payload in GnuTLS send/receive operations">
<correction runc "Fix busybox tarball url; prevent buffer overflow writing netlink messages [CVE-2021-43784]; fix tests on newer kernels; prevent write access to user-owned cgroup hierarchy <q>/sys/fs/cgroup/user.slice/...</q> [CVE-2023-25809]; fix access control regression [CVE-2023-27561 CVE-2023-28642]">
<correction rustc-web "New upstream stable release, to support building new chromium and firefox-esr versions">
<correction shim "New upstream release">
<correction shim-helpers-amd64-signed "Rebuild against shim 15.8.1">
<correction shim-helpers-arm64-signed "Rebuild against shim 15.8.1">
<correction shim-helpers-i386-signed "Rebuild against shim 15.8.1">
<correction shim-signed "New upstream stable release">
<correction symfony "Fix autoloading of HttpClient">
<correction trinity "Fix build failure by dropping support for DECNET">
<correction usb.ids "Update included data list">
<correction xmedcon "Fix heap overflow [CVE-2024-29421]">
</table>


<h2>Atualizações de segurança</h2>


<p>Esta revisão adiciona as seguintes atualizações de segurança para a antiga
versão estável (oldstable).
A equipe de segurança já lançou um aviso para cada uma dessas atualizações:</p>

<table border=0>
<tr><th>ID do aviso</th>  <th>Pacote</th></tr>
<dsa 2024 5718 org-mode>
<dsa 2024 5719 emacs>
<dsa 2024 5721 ffmpeg>
<dsa 2024 5722 libvpx>
<dsa 2024 5723 plasma-workspace>
<dsa 2024 5725 znc>
<dsa 2024 5726 krb5>
<dsa 2024 5727 firefox-esr>
<dsa 2024 5728 exim4>
<dsa 2024 5729 apache2>
<dsa 2024 5730 linux-signed-amd64>
<dsa 2024 5730 linux-signed-arm64>
<dsa 2024 5730 linux-signed-i386>
<dsa 2024 5730 linux>
<dsa 2024 5734 bind9>
<dsa 2024 5736 openjdk-11>
<dsa 2024 5737 libreoffice>
<dsa 2024 5738 openjdk-17>
<dsa 2024 5739 wpa>
<dsa 2024 5740 firefox-esr>
<dsa 2024 5742 odoo>
<dsa 2024 5743 roundcube>
<dsa 2024 5746 postgresql-13>
<dsa 2024 5747 linux-signed-amd64>
<dsa 2024 5747 linux-signed-arm64>
<dsa 2024 5747 linux-signed-i386>
<dsa 2024 5747 linux>
</table>


<h2>Pacotes removidos</h2>

<p>Os seguintes pacotes foram removidos por circunstâncias fora de nosso
controle:</p>

<table border=0>
<tr><th>Pacote</th>               <th>Justificativa</th></tr>
<correction bcachefs-tools "Buggy, obsolete">
<correction dnprogs "Buggy, obsolete">
<correction iotjs "Unmaintained, security concerns">
<correction obfs4proxy "Security issues">

</table>


<h2>Instalador do Debian</h2>

<p>O instalador foi atualizado para incluir as correções incorporadas
na antiga versão estável (oldstable) pela versão pontual.</p>


<h2>URLs</h2>

<p>As listas completas dos pacotes que foram alterados por esta revisão:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>A atual antiga versão estável (oldstable):</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/oldstable/">
</div>

<p>Atualizações propostas (proposed updates) para a antiga versão estável
(oldstable):</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/oldstable-proposed-updates">
</div>

<p>Informações da antiga versão estável (oldstable) (notas de lançamento,
errata, etc):</p>

<div class="center">
  <a
  href="$(HOME)/releases/oldstable/">https://www.debian.org/releases/oldstable/</a>
</div>

<p>Anúncios de segurança e informações:</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>


<h2>Sobre o Debian</h2>

<p>O projeto Debian é uma associação de desenvolvedores(as) de Software Livre
que dedicam seu tempo e esforço como voluntários(as) para produzir o sistema
operacional completamente livre Debian.</p>


<h2>Informações de contato</h2>

<p>Para mais informações, por favor visite as páginas web do Debian em
<a href="$(HOME)/">https://www.debian.org/</a>, envie um e-mail (em inglês) para
&lt;press@debian.org&gt;, ou entre em contato (em inglês) com a equipe de
lançamento da versão estável (stable) em
&lt;debian-release@lists.debian.org&gt;.</p>
