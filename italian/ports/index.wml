#use wml::debian::template title="Port di Debian su altre architetture"
#use wml::debian::translation-check translation="e0652549b916d3585e3eed9d093a6711af721019" maintainer="Luca Monducci"
#include "$(ENGLISHDIR)/releases/info"
#use wml::debian::toc


<toc-display/>

<toc-add-entry name="intro">Introduzione</toc-add-entry>
<p>
Come molti sanno, <a href="https://www.kernel.org/">Linux</a> è solo un
kernel. E, per lungo tempo, il kernel Linux ha funzionato solo sulle macchine
Intel x86, dal 386 in su.
</p>
<p>
Ad ogni modo, ora non è più così. Il kernel Linux è stato portato su
un gran numero (che continua a crescere) di architetture.
Seguendolo da vicino, abbiamo portato la distribuzione Debian su
queste architetture. In generale questo è un processo che ha un inizio
piuttosto complesso (per avere la libc e il linker dinamico funzionanti
correttamente), per proseguire con una certa routine di ricompilazioni
di pacchetti sulla nuova architettura.
</p>
<p>
Debian è un sistema operativo (SO), non un kernel (in realtà è più di un
sistema operativo poiché include migliaia di programmi applicativi). Di
conseguenza, nonostante la maggior parte dei port sono basti su Linux,
esistono anche dei port basati sui kernel FreeBSD, NetBSD e Hurd.
</p>

<div class="important">
<p>
Questa pagina è in continua evoluzione. Non tutti i
port hanno ancora delle pagine e molti di loro sono su siti esterni. Stiamo
lavorando per raccogliere tutte le informazioni sui vari port, da poter poi
distribuire sui mirror tramite il sito web Debian. Altri port potrebbero
essere <a href="https://wiki.debian.org/CategoryPorts">elencati</a> nel wiki.
</p>
</div>

<toc-add-entry name="portlist-released">Elenco dei port ufficiali</toc-add-entry>

<p>
Queste port sono le architetture ufficialmente supportate dal progetto Debian e
fanno parte di un rilascio ufficiale o di un rilascio imminente.
</p>

<table class="tabular" summary="">
<tbody>
<tr>
<th>Port</th>
<th>Architettura</th>
<th>Descrizione</th>
<th>Aggiunto</th>
<th>Stato</th>
</tr>
<tr>
<td><a href="amd64/">amd64</a></td>
<td>64-bit PC (amd64)</td>
<td>Port per i processori AMD64 a 64-bit, gestisce uno spazio utente a
32-bit e a 64-bit. Il port supporta i processori AMD Opteron, Athlon e
Sempron a 64 bit e i processori Intel con supporto Intel 64 compreso il
Pentium&nbsp;D e le serie Xeon e Core.
</td>
<td>4.0</td>
<td><a href="$(HOME)/releases/stable/amd64/release-notes/">rilasciato</a></td>
</tr>
<tr>
<td><a href="arm/">arm64</a></td>
<td>64-bit ARM (AArch64)</td>
<td>Port per l'architettura ARM a 64-bit con la nuova versione 8 del set
di istruzioni (chiamato AArch64),per processori quali Applied Micro X-Gene,
AMD Seattle e Cavium ThunderX.</td>
<td>8</td>
<td><a href="$(HOME)/releases/stable/arm64/release-notes/">rilasciato</a></td>
</tr>
<tr>
<td><a href="arm/">armel</a></td>
<td>EABI ARM</td>
<td>Port per l'architettura ARM a 32-bit little-endian con Embedded ABI,
supporta le CPU ARM compatibili con il set d'istruzioni v5te. Questo port
non trae vantaggio dalla presenza di unità per i calcoli in virgola mobile
(FPU).</td>
<td>5.0</td>
<td><a
href="$(HOME)/releases/stable/armel/release-notes/">rilasciato</a></td>
</tr>
<tr>
<td><a href="arm/">armhf</a></td>
<td>Hard Float ABI ARM</td>
<td>Port per l'architettura ARM a 32-bit little-endian per schede e
dispositivi con unità per i calcoli in virgola mobile (FPU) e altre moderne
funzionalità delle CPU ARM. Questo port ha come requisito una CPU ARMv7 con
Thumb-2 e l'unità di calcolo a virgola mobile VFPv3-D16.</td>
<td>7.0</td>
<td><a
href="$(HOME)/releases/stable/armhf/release-notes/">rilasciato</a></td>
</tr>
<tr>
<td><a href="i386/">i386</a></td>
<td>32-bit PC (i386)</td>
<td>Port per processori x86 a 32-bit, Linux è stato sviluppato
in origine per i processori Intel 386, da cui l'abbreviazione. Debian
supporta tutti i processori IA-32, fabbricati da Intel (tutta la serie
Pentium e le recenti Core Duo in modalità a 32-bit), AMD (K6, tutta la
serie Athlon e la serie Athlon64 in modalità a 32-bit), Cyrix e altri
produttori.</td>
<td>1.1</td>
<td><a href="$(HOME)/releases/stable/i386/release-notes/">rilasciato</a></td>
</tr>
<tr>
<td><a href="https://wiki.debian.org/mips64el">mips64el</a></td>
<td>MIPS (64-bit in modalità little-endian)</td>
<td>
Port per harware ABI N64 little-endian, per ISA MIPS64r1 e hardware
floating-point.
</td>
<td>9</td>
<td><a href="$(HOME)/releases/stable/mips64el/release-notes/">rilasciato</a></td>
</tr>
<tr>
<td><a href="powerpc/">ppc64el</a></td>
<td>POWER7+, POWER8</td>
<td>Port per l'architettura little-endian a 64-bit POWER, utilizza la nuova
ABI Open Power ELFv2.</td>
<td>8</td>
<td><a href="$(HOME)/releases/stable/ppc64el/release-notes/">rilasciato</a></td>
</tr>
<tr>
<td><a href="https://wiki.debian.org/RISC-V">riscv64</a></td>
<td>RISC-V (64-bit little endian)</td>
<td>Port per <a href="https://riscv.org/">RISC-V</a> a 64-bit little-endian,
un ISA libero/aperto.</td>
<td>13</td>
<td>test in corso</td>
</tr>
<tr>
<td><a href="s390x/">s390x</a></td>
<td>System z</td>
<td>Port con spazio utente a 64-bit per mainframe IBM System z.</td>
<td>7.0</td>
<td><a href="$(HOME)/releases/stable/s390x/release-notes/">rilasciato</a></td>
</tr>
</tbody>
</table>

<toc-add-entry name="portlist-other">Elenco degli altri port</toc-add-entry>

<p>
Questi port sono sia lavori attivi che intendono essere promossi tra le
architetture ufficialmente rilasciate, sia port che in passato erano
supportati ufficialmente ma che non sono più rilasciati perché non hanno
superato le verifiche per la qualificazione, perché l'interesse degli
sviluppatori era limitato oppure sono port su cui non si lavora più e
che vengono elencati per interesse storico.
</p>

<p>
Questi port, se sono ancora attivamente mantenuti, sono disponibili su
l'infrastruttura <url "https://www.ports.debian.org/">.
</p>

<div class="tip">
<p>
Per alcuni dei seguenti port sono disponibili delle immagini per
l'installazione non-ufficiali su <a
href="https://cdimage.debian.org/cdimage/ports">https://cdimage.debian.org/cdimage/ports</a>.
Tali immagini sono gestite dai corrispondenti team di Debian Port.
</p>
</div>

<table class="tabular" summary="">
<tbody>
<tr>
<th>Port</th>
<th>Architettura</th>
<th>Descrizione</th>
<th>Aggiunto</th>
<th>Rimosso</th>
<th>Stato</th>
<th>Sostituito da</th>
</tr>
<tr>
<td><a href="alpha/">alpha</a></td>
<td>Alpha</td>
<td>Port per l'architettura RISC Alpha a 64-bit.</td>
<td>2.1</td>
<td>6.0</td>
<td>attivo</td>
<td>-</td>
</tr>
<tr>
<td><a href="arm/">arm</a></td>
<td>OABI ARM</td>
<td>Port per l'architettura ARM con la precedente ABI.
</td>
<td>2.2</td>
<td>6.0</td>
<td>chiuso definitivamente</td>
<td>armel</td>
</tr>
<tr>
<td><a href="https://web.archive.org/web/20130326061253/http://avr32.debian.net/">avr32</a></td>
<td>Atmel 32-bit RISC</td>
<td>Port sull'architettura RISC a 32 bit di Atmel, AVR32.</td>
<td>-</td>
<td>-</td>
<td>chiuso definitivamente</td>
<td>-</td>
</tr>
<tr>
<td><a href="hppa/">hppa</a></td>
<td>HP PA-RISC</td>
<td>Port per l'architettura PA-RISC di Hewlett-Packard.
</td>
<td>3.0</td>
<td>6.0</td>
<td>attivo</td>
<td>-</td>
</tr>
<tr>
<td><a href="hurd/">hurd-i386</a></td>
<td>32-bit PC (i386)</td>
<td>GNU Hurd è un nuovo sistema operativo creato dal gruppo GNU.
Debian GNU/Hurd sarà uno (forse il primo) dei SO GNU.
Attualmente il progetto è basato sull'architettura i386.
</td>
<td>-</td>
<td>-</td>
<td>attivo</td>
<td>-</td>
</tr>
<tr>
<td><a href="hurd/">hurd-amd64</a></td>
<td>64-bit PC (amd64)</td>
<td>
Port del sistema operativo Debian GNU/Hurd per processori x86 a 64-bit.
Supporta solo 64-bit, non 32-bit insieme a 64-bit.
</td>
<td>-</td>
<td>-</td>
<td>attivo</td>
<td>-</td>
</tr>
<tr>
<td><a href="ia64/">ia64</a></td>
<td>Intel Itanium IA-64</td>
<td>Port per la prima architettura Intel a 64-bit. Nota: questo port <em>non</em>
deve essere confuso con le recenti estensioni a 64-bit che Intel ha rilasciato
per i processori Pentium 4 e Celeron, chiamate Intel 64; per queste estensioni
si veda il port amd64.
</td>
<td>3.0</td>
<td>8</td>
<td>attivo</td>
<td>-</td>
</tr>
<tr>
<td><a href="kfreebsd-gnu/">kfreebsd-amd64</a></td>
<td>64-bit PC (amd64)</td>
<td>Port del kernel di FreeBSD con glibc. È stato il primo port non-Linux
rilasciato da Debian come anteprima tecnologica. Lo sviluppo del port non
ufficiale è stato interrotto nel 2023.</td>
<td>6.0</td>
<td>8</td>
<td><a href="https://lists.debian.org/debian-devel/2023/05/msg00306.html">chiuso definitivamente</a></td>
<td>-</td>
</tr>
<tr>
<td><a href="kfreebsd-gnu/">kfreebsd-i386</a></td>
<td>32-bit PC (i386)</td>
<td>Port del kernel di FreeBSD con glibc. È stato il primo port non-Linux
rilasciato da Debian come anteprima tecnologica. Lo sviluppo del port non
ufficiale è stato interrotto nel 2023.
</td>
<td>6.0</td>
<td>8</td>
<td><a href="https://lists.debian.org/debian-devel/2023/05/msg00306.html">chiuso definitivamente</a></td>
<td>-</td>
</tr>
<tr>
<td><a href="https://wiki.debian.org/LoongArch">loong64</a></td>
<td>LoongArch (64-bit little endian)</td>
<td>Port per l'architettura 64-bit little-endian LoongArch.</td>
<td>-</td>
<td>-</td>
<td>attivo</td>
<td>-</td>
</tr>
<tr>
<td><a href="http://www.linux-m32r.org/">m32</a></td>
<td>M32R</td>
<td>Port per i processori RISC a 32-bit della Renesas Technology.</td>
<td>-</td>
<td>-</td>
<td>chiuso definitivamente</td>
<td>-</td>
</tr>
<tr>
<td><a href="m68k/">m68k</a></td>
<td>Motorola 68k</td>
<td>Port per la serie di processori Motorola m68k; in particolare sulla
serie di workstation Sun3 e sui personal computer Macintosh Apple, Atari e
Amiga.</td>
<td>2.0</td>
<td>4.0</td>
<td>attivo</td>
<td>-</td>
</tr>
<tr>
<td><a href="mips/">mips</a></td>
<td>MIPS (modalità big-endian)</td>
<td>Port per l'architettura MIPS usata nelle macchine SGI (debian-mips
&mdash; big-endian) e Digital DECstation (debian-mipsel &mdash;
little-endian).</td>
<td>3.0</td>
<td>11</td>
<td><a href="https://lists.debian.org/debian-release/2019/08/msg00582.html">chiuso definitivamente</a></td>
<td>-</td>
</tr>
<tr>
<td><a href="mips/">mipsel</a></td>
<td>MIPS (modalità little-endian)</td>
<td>Port sull'architettura MIPS usata nelle (little-endian) Digital DECstation.
</td>
<td>3.0</td>
<td>13</td>
<td><a href="https://lists.debian.org/debian-devel-announce/2023/09/msg00000.html">chiuso definitivamente</a></td>
<td>-</td>
</tr>
<tr>
<td><a href="https://web.archive.org/web/20230605050614/http://www.debian.org/ports/netbsd/">netbsd-i386</a></td>
<td>32-bit PC (i386)</td>
<td>
Port sul kernel e libc di NetBSD, per processori x86 a 32-bit.
</td>
<td>-</td>
<td>-</td>
<td>chiuso definitivamente</td>
<td>-</td>
</tr>
<tr>
<td><a href="https://web.archive.org/web/20230605050614/http://www.debian.org/ports/netbsd/">netbsd-alpha</a></td>
<td>Alpha</td>
<td>
Port sul kernel e libc di NetBSD, per processori Alpha a 64-bit.
</td>
<td>-</td>
<td>-</td>
<td>chiuso definitivamente</td>
<td>-</td>
</tr>
<tr>
<td><a href="https://web.archive.org/web/20150905061423/http://or1k.debian.net/">or1k</a></td>
<td>OpenRISC 1200</td>
<td>Port per la CPU open source <a href="https://openrisc.io/">OpenRISC</a>
1200.</td>
<td>-</td>
<td>-</td>
<td>chiuso definitivamente</td>
<td>-</td>
</tr>
<tr>
<td><a href="powerpc/">powerpc</a></td>
<td>Motorola/IBM PowerPC</td>
<td>Port per molti modelli Apple Macintosh PowerPC e macchine con
architettura CHRP e PReP.</td>
<td>2.2</td>
<td>9</td>
<td>attivo</td>
<td>-</td>
</tr>
<tr>
<td><a href="https://wiki.debian.org/PowerPCSPEPort">powerpcspe</a></td>
<td>PowerPC Signal Processing Engine</td>
<td>
Port sull'hardware <q>Signal Processing Engine</q> presente sulle CPU
a basso consumo FreeScale a 32-bit e IBM <q>e500</q>.
</td>
<td>-</td>
<td>-</td>
<td>chiuso definitivamente</td>
<td>-</td>
</tr>
<tr>
<td><a href="s390/">s390</a></td>
<td>S/390 e zSeries</td>
<td>Port per i server IBM S/390.</td>
<td>3.0</td>
<td>8</td>
<td>chiuso definitivamente</td>
<td>s390x</td>
</tr>
<tr>
<td><a href="sparc/">sparc</a></td>
<td>Sun SPARC</td>
<td>Port per le workstation Sun della serie UltraSPARC
e su alcuni modelli successivi con architettura sun4.</td>
<td>2.1</td>
<td>8</td>
<td>chiuso definitivamente</td>
<td>sparc64</td>
</tr>
<tr>
<td><a href="https://wiki.debian.org/Sparc64">sparc64</a></td>
<td>64-bit SPARC</td>
<td>
Port per i processori SPARC a 64-bit.
</td>
<td>-</td>
<td>-</td>
<td>attivo</td>
<td>-</td>
</tr>
<tr>
<td><a href="https://wiki.debian.org/SH4">sh4</a></td>
<td>SuperH</td>
<td>
Port per i processori Hitachi SuperH. Supporta
anche i processori open source <a href="https://j-core.org/">J-Core</a>.
</td>
<td>-</td>
<td>-</td>
<td>attivo</td>
<td>-</td>
</tr>
<tr>
<td><a href="https://wiki.debian.org/X32Port">x32</a></td>
<td>64-bit PC con puntatori a 32-bit</td>
<td>
Port per la ABI amd64/x86_64 x32 che utilizza il set di istruzioni amd64 e
puntatori a 32-bit per combinare l'insieme più ampio di registri rispetto a
ISA con il minor impatto sulla memoria e sulla cache risultante dall'uso di
puntatori a 32-bit.
</td>
<td>-</td>
<td>-</td>
<td>attivo</td>
<td>-</td>
</tr>
</tbody>
</table>



<div class="note">
<p>Molti tra i nomi dei computer e processori
sopracitati sono coperti da marchi registrati dei rispettivi produttori.
</p>
</div>
