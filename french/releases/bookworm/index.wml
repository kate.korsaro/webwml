#use wml::debian::template title="Informations sur la version «&nbsp;Bookworm&nbsp;» de Debian"
#use wml::debian::translation-check translation="77c9da4860132027cb6546223ff0e9b84071a5b7" maintainer="Jean-Pierre Giraud"
#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/releases/bookworm/release.data"
#include "$(ENGLISHDIR)/releases/arches.data"

# Translators:
# cf. ../<other_release>/index.html

<p>
La version&nbsp;<current_release_bookworm> de Debian (connue sous le nom
de <em>Bookworm</em>) a été publiée le <a href="$(HOME)/News/<current_release_newsurl_bookworm/>"><current_release_date_bookworm></a>.
<ifneq "12.0" "<current_release>"
  "La version 12.0 a été initialement publiée le <:=spokendate('2023-06-10'):>."
/>
Cette version
comprend de nombreuses modifications décrites dans notre <a
href="$(HOME)/News/2023/20230610">communiqué de presse</a> et les <a
href="releasenotes">notes de publication</a>.
</p>

# <p><strong>Debian 12 a été remplacée par
# <a href="../trixie/">Debian 13 (<q>Trixie</q>)</a>.
# </strong></p>

### This paragraph is orientative, please review before publishing!
# <p><strong>Néanmoins, Bookworm bénéficie de la prise en charge à long terme
# (<q>Long Term Support</q> – LTS) jusqu'au 30 juin 2028.
# Cette prise en charge est limitée aux architectures i386, amd64, armel,
# armhf et arm64. Toutes les autres architectures ne sont plus prises en
# charge dans Bookworm.
# Pour de plus amples informations, veuillez consulter la
# <a href="https://wiki.debian.org/fr/LTS">section dédiée à LTS du wiki Debian</a>.
# </strong></p>

<p>
Le cycle de vie de Debian 12 couvre cinq ans : une prise en charge complète
par Debian pendant les trois premières années, jusqu'au
<:=spokendate('2026-06-10'):>, puis deux ans de prise en charge à long terme
(Long Term Support – LTS) jusqu'au <:=spokendate('2028-06-30'):>. Le nombre
d'architectures prises en charge est réduit pendant la durée de prise en charge
à long terme. Pour de plus amples informations, veuillez consulter la page du
site web consacrée aux <a href="$(HOME)/security/">informations de sécurité</a> et la
<a href="https://wiki.debian.org/LTS">section LTS du Wiki de Debian</a>.
</p>

<p>
Pour obtenir et installer Debian, veuillez vous reporter à la page
des <a href="debian-installer/">informations d'installation</a> et au <a
href="installmanual">guide d'installation</a>. Pour mettre à niveau à partir d'une ancienne
version de Debian, veuillez vous reporter aux instructions des <a
href="releasenotes">notes de publication</a>.
</p>

### Activate the following when LTS period starts.
#<p>Architectures prises en charge durant la prise en charge à long terme :</p>
#
#<ul>
#<:
#foreach $arch (@archeslts) {
#	print "<li><a href=\"$(HOME)/ports/$arch/\">$arches{$arch}</a>\n";
#}
#:>
#</ul>

<p>
Les architectures suivantes sont gérées par la version initiale de Bookworm :
</p>

<ul>
<:
foreach $arch (@arches) {
	print "<li><a href=\"$(HOME)/ports/$arch/\">$arches{$arch}</a>\n";
}
:>
</ul>

<p>
Contrairement à nos souhaits, certains problèmes pourraient toujours exister
dans cette version, même si elle est déclarée <em>stable</em>. Nous avons
réalisé <a href="errata">une liste des principaux problèmes connus</a>, et vous
pouvez toujours nous <a href="../reportingbugs">signaler d'autres problèmes</a>.
</p>

<p>
Enfin, nous avons une liste de <a href="credits">personnes à remercier</a> pour
leur participation à cette publication.
</p>
