#use wml::debian::translation-check translation="36ca846f02647a287e0cbec3fe1ad38117ad4ad4" maintainer="Jean-Pierre Giraud"
<define-tag pagetitle>Publication de la mise à jour de Debian 12.8</define-tag>
<define-tag release_date>2024-11-09</define-tag>
#use wml::debian::news
# $Id:

<define-tag release>12</define-tag>
<define-tag codename>Bookworm</define-tag>
<define-tag revision>12.8</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>
Le projet Debian a l'honneur d'annoncer la huitième mise à jour de sa
distribution stable Debian <release> (nom de code <q><codename></q>).
Tout en réglant quelques problèmes importants, cette mise à jour corrige
principalement des problèmes de sécurité de la version stable. Les annonces de
sécurité ont déjà été publiées séparément et sont simplement référencées dans
ce document.
</p>

<p>
Veuillez noter que cette mise à jour ne constitue pas une nouvelle version de
Debian <release> mais seulement une mise à jour de certains des paquets qu'elle
contient. Il n'est pas nécessaire de jeter les anciens médias de la
version <codename>. Après installation, les paquets peuvent être mis à niveau
vers les versions actuelles en utilisant un miroir Debian à jour.
</p>

<p>
Les personnes qui installent fréquemment les mises à jour à partir de
security.debian.org n'auront pas beaucoup de paquets à mettre à jour et la
plupart des mises à jour de security.debian.org sont comprises dans cette mise
à jour.
</p>

<p>
De nouvelles images d'installation seront prochainement disponibles à leurs
emplacements habituels.
</p>

<p>
Mettre à jour une installation vers cette révision peut se faire en faisant
pointer le système de gestion de paquets sur l'un des nombreux miroirs HTTP de
Debian. Une liste complète des miroirs est disponible à l'adresse :
</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>


<h2>Corrections de bogues divers</h2>

<p>
Cette mise à jour de la version stable apporte quelques corrections importantes
aux paquets suivants :
</p>

<table border=0>
<tr><th>Paquet</th>               <th>Raison</th></tr>
<correction 7zip "Correction d'un dépassement de tampon de tas dans le gestionnaire de NTFS [CVE-2023-52168] ; correction d'une lecture hors limites dans le gestionnaire de NTFS [CVE-2023-52169]">
<correction amanda "Mise à jour de la correction incomplète du CVE-2022-37704, rétablissant le fonctionnement avec xfsdump">
<correction apr "Utilisation systématique de permissions 0600 pour la mémoire partagée nommée [CVE-2023-49582]">
<correction base-files "Mise à jour pour cette version">
<correction btrfs-progs "Correction des erreurs de calcul des sommes de contrôle durant la conversion de volume dans btrfs-convert">
<correction calamares-settings-debian "Correction de l'absence de lanceur sur les bureaux KDE ; correction des montages btrfs">
<correction cjson "Correction d'un problème d'erreur de segmentation [CVE-2024-31755]">
<correction clamav "Nouvelle version amont stable ; correction d'un problème de déni de service [CVE-2024-20505], d'un problème de corruption de fichier [CVE-2024-20506]">
<correction cloud-init "Ajout de la prise en charge de multiples sections Route de networkd">
<correction cloud-initramfs-tools "Ajout de dépendances manquantes dans l'initramfs">
<correction curl "Correction d'une gestion incorrecte de certaines réponses OCSP [CVE-2024-8096]">
<correction debian-installer "Rétablissement de certaines cibles de démarrage réseau armel (openrd) ; passage de l'ABI du noyau Linux à la version 6.1.0-27 ; reconstruction avec proposed-updates">
<correction debian-installer-netboot-images "Reconstruction avec proposed-updates">
<correction devscripts "bts : toujours mise à niveau vers STARTTLS pour 587/tcp ; build-rdeps : ajout de la prise en charge de non-free-firmware ; chdist : mise à jour des exemples de sources.list avec non-free-firmware ; build-rdeps : utilisation par défaut de toutes les distributions disponibles">
<correction diffoscope "Correction d'un échec de construction lors du traitement d'un fichier zip avec des entrées se chevauchant volontairement dans les tests">
<correction distro-info-data "Ajout d'Ubuntu 25.04">
<correction docker.io "Correction d'un contournement des greffons AuthZ dans certaines circonstances [CVE-2024-41110]">
<correction dpdk "Nouvelle version amont stable">
<correction exim4 "Correction d'un plantage dans dbmnz lors de la recherche de clés sans contenu">
<correction fcgiwrap "Configuration correcte de la propriété des dépôts dans le dorsal de git">
<correction galera-4 "Nouvelle version amont stable">
<correction glib2.0 "Fourniture de libgio-2.0-dev à partir de libglib2.0-dev et de libgio-2.0-dev-bin à partir de libglib2.0-dev-bin">
<correction glibc "Modification de la localisation croate pour utiliser l'euro comme monnaie ; suppression de la modification amont qui modifiait l'ABI GLIBC_PRIVATE, provoquant des plantages avec certains binaires statiques sur arm64 ; vfscanf() : correction des correspondances plus longues que INT_MAX ; ungetc() : correction d'une lecture non initialisée lors de l'insertion dans des flux inutilisés, fuite de tampon de sauvegarde à la sortie du programme ; mremap() : correction de la prise en charge de l'option MREMAP_DONTUNMAP ; resolv : correction des délais provoqués par les réponses d'erreur courtes ou quand le mode single-request est activé dans resolv.conf">
<correction gtk+3.0 "Correction permettant à Orca d'annoncer le focus initial">
<correction ikiwiki-hosting "Lecture permise de tous les dépôts d'utilisateur">
<correction intel-microcode "Nouvelle version stable ; corrections de sécurité [CVE-2024-23984 CVE-2024-24968]">
<correction ipmitool "Correction d'un problème de dépassement de tampon dans l'interface <q>open</q> ; correction de <q>échec de l'impression réseau avec des paramètres non pris en charge</q> ; correction de la lecture des capteurs de température ; correction de l'utilisation de valeurs hexadécimales lors de l'envoi de données brutes">
<correction iputils "Correction d'une gestion incorrecte des réponses ICMP destinées à d'autres processus">
<correction kexec-tools "Masquage de kexec.service pour éviter que le script d'init.d traite un processus kexec sur un système avec systemd activé">
<correction lemonldap-ng "Correction d'une vulnérabilité de script intersite sur la page de connexion [CVE-2024-48933]">
<correction lgogdownloader "Correction de l'analyse des URL Galaxy">
<correction libskk "Plantage évité avec un échappement JSON non valable">
<correction libvirt "Correction de l'exécution de VM i686 avec AppArmor sur l'hôte ; impossibilité d'amorçage ou disparition de certains clients évitées pendant la mise à niveau">
<correction linux "Nouvelle version stable ; passage de l'ABI à la version 27">
<correction linux-signed-amd64 "Nouvelle version stable ; passage de l'ABI à la version 27">
<correction linux-signed-arm64 "Nouvelle version stable ; passage de l'ABI à la version 27">
<correction linux-signed-i386 "Nouvelle version stable ; passage de l'ABI à la version 27">
<correction llvm-toolchain-15 "Reconstruction spécifique à l'architecture sur mips64el pour synchroniser la version avec les autres architectures">
<correction nghttp2 "Correction d'un problème de déni de service [CVE-2024-28182]">
<correction ninja-build "Prise en charge des grands numéros d'inœud sur les systèmes 32 bits">
<correction node-dompurify "Correction de problèmes de pollution de prototype [CVE-2024-45801 CVE-2024-48910]">
<correction node-es-module-lexer "Correction d'un échec de construction">
<correction node-globby "Correction d'un échec de construction">
<correction node-mdn-browser-compat-data "Correction d'un échec de construction">
<correction node-rollup-plugin-node-polyfills "Correction d'un échec de construction">
<correction node-tap "Correction d'un échec de construction">
<correction node-xterm "Correction de déclarations TypeScript">
<correction node-y-protocols "Correction d'un échec de construction">
<correction node-y-websocket "Correction d'un échec de construction">
<correction node-ytdl-core "Correction d'un échec de construction">
<correction notify-osd "Correction du chemin de l'exécutable dans le fichier du lanceur du bureau">
<correction ntfs-3g "Correction d'une utilisation de mémoire après libération dans <q>ntfs-uppercase-mbs</q> ; reclassement de fuse comme Depends et non Pre-Depends">
<correction openssl "Nouvelle version amont stable ; correction d'un problème de lecture hors limites de tampon [CVE-2024-5535], d'un accès mémoire hors limites [CVE-2024-9143]">
<correction ostree "Plantage de libflatpak évité lors de l'utilisation de curl 8.10">
<correction puppetserver "Rétablissement d'une tâche programmée pour nettoyer les rapports après 30 jours, évitant l'épuisement de l'espace disque">
<correction puredata "Correction d'un problème d'élévation de privilèges [CVE-2023-47480]">
<correction python-cryptography "Correction d'un déréférencement de pointeur NULL lors du chargement de certificats PKCS7 [CVE-2023-49083] ; correction d'un déréférencement de pointeur NULL lorsque la clé et le certificat PKCS#12 ne correspondent pas [CVE-2024-26130]">
<correction python3.11 "Correction d'une régression dans zipfile.Path ; vulnérabilité de déni de service par expression régulière avec des archives tar contrefaites">
<correction reprepro "Blocages évités lors de l'exécution d'unzstd">
<correction sqlite3 "Correction d'un problème de lecture hors limites de tampon [CVE-2023-7104], d'un problème d'un dépassement de pile et d'un problème de dépassement d'entier">
<correction sumo "Correction d'une situation de compétition lors de la construction de la documentation">
<correction systemd "Nouvelle version amont stable">
<correction tgt "chap : utilisation d'une source d'entropie appropriée [CVE-2024-45751]">
<correction timeshift "Ajout de la dépendance manquante à pkexec">
<correction util-linux "lscpu autorisé à identifier les nouveaux cœurs Arm">
<correction vmdb2 "Réglage de la locale à UTF-8">
<correction wireshark "Nouvelle version amont de sécurité [CVE-2024-0208, CVE-2024-0209, CVE-2024-2955, CVE-2024-4853, CVE-2024-4854, CVE-2024-4855, CVE-2024-8250, CVE-2024-8645]">
<correction xfpt "Correction d'un dépassement de tampon [CVE-2024-43700]">
</table>


<h2>Mises à jour de sécurité</h2>


<p>
Cette révision ajoute les mises à jour de sécurité suivantes à la version
stable. L'équipe de sécurité a déjà publié une annonce pour chacune de ces
mises à jour :
</p>

<table border=0>
<tr><th>Identifiant</th>  <th>Paquet</th></tr>
<dsa 2024 5729 apache2>
<dsa 2024 5733 thunderbird>
<dsa 2024 5744 thunderbird>
<dsa 2024 5758 trafficserver>
<dsa 2024 5759 python3.11>
<dsa 2024 5760 ghostscript>
<dsa 2024 5761 chromium>
<dsa 2024 5762 webkit2gtk>
<dsa 2024 5763 pymatgen>
<dsa 2024 5764 openssl>
<dsa 2024 5765 firefox-esr>
<dsa 2024 5766 chromium>
<dsa 2024 5767 thunderbird>
<dsa 2024 5768 chromium>
<dsa 2024 5769 git>
<dsa 2024 5770 expat>
<dsa 2024 5771 php-twig>
<dsa 2024 5772 libreoffice>
<dsa 2024 5773 chromium>
<dsa 2024 5774 ruby-saml>
<dsa 2024 5775 chromium>
<dsa 2024 5776 tryton-server>
<dsa 2024 5777 booth>
<dsa 2024 5778 cups-filters>
<dsa 2024 5779 cups>
<dsa 2024 5780 php8.2>
<dsa 2024 5781 chromium>
<dsa 2024 5782 linux-signed-amd64>
<dsa 2024 5782 linux-signed-arm64>
<dsa 2024 5782 linux-signed-i386>
<dsa 2024 5782 linux>
<dsa 2024 5783 firefox-esr>
<dsa 2024 5784 oath-toolkit>
<dsa 2024 5785 mediawiki>
<dsa 2024 5786 libgsf>
<dsa 2024 5787 chromium>
<dsa 2024 5788 firefox-esr>
<dsa 2024 5789 thunderbird>
<dsa 2024 5790 node-dompurify>
<dsa 2024 5791 python-reportlab>
<dsa 2024 5792 webkit2gtk>
<dsa 2024 5793 chromium>
<dsa 2024 5794 openjdk-17>
<dsa 2024 5795 python-sql>
<dsa 2024 5796 libheif>
<dsa 2024 5797 twisted>
<dsa 2024 5798 activemq>
<dsa 2024 5799 chromium>
<dsa 2024 5800 xorg-server>
<dsa 2024 5802 chromium>
</table>



<h2>Installateur Debian</h2>
<p>
L'installateur a été mis à jour pour inclure les correctifs incorporés dans
cette version de stable.
</p>

<h2>URL</h2>

<p>
Liste complète des paquets qui ont été modifiés dans cette version :
</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>
Adresse de l'actuelle distribution stable :
</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/stable/">
</div>

<p>Mises à jour proposées à la distribution stable :</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/proposed-updates">
</div>

<p>
Informations sur la distribution stable (notes de publication,
<i>errata</i>, etc.) :
</p>

<div class="center">
  <a
  href="$(HOME)/releases/stable/">https://www.debian.org/releases/stable/</a>
</div>

<p>
Annonces et informations de sécurité :
</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>À propos de Debian</h2>
<p>
Le projet Debian est une association de développeurs de logiciels libres qui
offrent volontairement leur temps et leurs efforts pour produire le système
d'exploitation complètement libre Debian.
</p>

<h2>Contacts</h2>

<p>
Pour de plus amples informations, veuillez consulter le site Internet de
Debian <a href="$(HOME)/">https://www.debian.org/</a> ou envoyez un courrier
électronique à &lt;press@debian.org&gt; ou contactez l'équipe de publication de
la version stable à &lt;debian-release@lists.debian.org&gt;.
</p>

