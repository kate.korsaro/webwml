#! /bin/sh

#  Strip out wrong or unwanted language entries from language list
#  (in ../english/international/l10n/data/langs).

#  Usage: strip-langs.sh ../english/international/l10n/data/langs

unwanted='api app apt aym ber bin bos_DE bos_ES bos_FI bos_FR bos_HU bos_IT bos_LT bos_NL bos_SV bos_TR bos_NG bwr_NG ckl_NG cli cpf cut cz dcc_DE dcc_ES dcc_FR dcc_HU dcc_HR dcc_NL dcc_IT dcc_PL dcc_PT dcc_RU dcc_SR dcc_TR de_PY dk doc en en_BR en_DE en_FR en_HK en_PR en@arabic en@boldquot en@cyrillic en@greek en@hebrew en@piglatin en@quot en@shaw en@truecase en_AU en_CA en_GB en_IE en_NZ en_US en_ZA fil_PH fr_PY gui hia_NG ibo it_PY kde log log_DE log_ES log_FI log_FR log_HR log_HU log_IT log_NL log_PL log_PT log_RU log_SR log_TR mac map mfi_NG mnw_MM mrt_NG nah new_DE new_ES new_FR new_IT raw tag xml sp src str zh_PY'

cp ../data/langs ../data/langs_saved

for l in $unwanted
    do

    sed -i s/\ $l\ /\ / ../data/langs

done
