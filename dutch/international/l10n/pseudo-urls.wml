#use wml::debian::template title="Coördinatie van l10n-teams"
#use wml::debian::translation-check translation="1469899244087c9d456baf8dc6d1e40a328c9b34"


<h1>Pseudo-URL's</h1>

<p>
Het programma dat naar lijsten debian-l10n-* luistert, begrijpt pseudo-URL's in
het onderwerp-veld.
De pseudo-URL's moeten de volgende vorm hebben.
</p>
<div class="center"><code>[&lt;status&gt;]&nbsp;&lt;type&gt;://&lt;pakket&gt;/&lt;bestand&gt;</code></div>

<p>
De <i>status</i> moet een van de volgende zijn:
TAF, MAJ, ITT, RFR, LCFC, BTS#&lt;bugnummer&gt;, DONE, of HOLD.
</p>

<dl>
  <dt>
    <tt>TAF</tt> (<i>Travail À Faire</i>)
  </dt>
  <dd>
    Verzonden door een <strong>coördinator</strong> (niet door een willekeurig
    lid van de lijst) om aan te geven dat er een document is waaraan gewerkt
    moet worden.
  </dd>
  <dt>
    <tt>MAJ</tt> (<i>Mise À Jour</i>)
  </dt>
  <dd>
    Verzonden door een <strong>coördinator</strong> (niet door een willekeurig
    lid van de lijst) om aan te geven dat er een document is dat bijgewerkt
    moet worden en dat het werk gereserveerd is voor de vorige vertaler.
  </dd>
  <dt>
    <tt>ITT</tt> (Intent To Translate)
  </dt>
  <dd>
    Verzonden om aan te geven dat u van plan bent om aan de vertaling te werken;
    gebruikt om dubbel werk te vermijden.<br />
    Als u een <tt>[ITT]</tt>-bericht stuurt en iemand anders stuurt een ander
    [ITT]-bericht voor hetzelfde bestand, stuur dan onmiddellijk een nieuw bericht
    op de mailinglijst om hem/haar eraan te herinneren dat u voorrang heeft. Het
    doel is om nutteloos werk te voorkomen.
  </dd>
  <dt>
    <tt>RFR</tt> (Request For Review)
  </dt>
  <dd>
    Een eerste conceptvertaling is bijgevoegd. Anderen op de lijst worden verzocht
    deze op fouten te controleren en een antwoord te sturen (mogelijk buiten de
    lijst om als ze geen fouten hebben gevonden).<br />
    Er kunnen nog andere RFR's volgen als er substantiële wijzigingen zijn
    aangebracht.
  </dd>
  <dt>
    <tt>ITR</tt> (Intent To Review)
  </dt>
  <dd>
    Wordt gebruikt om te voorkomen dat LCFC's worden verzonden als er
    beoordelingen aan de gang zijn.<br />
    Voornamelijk gebruikt wanneer u verwacht dat uw controle pas over enkele dagen
    klaar zal zijn (omdat de vertaling groot is, of omdat u geen tijd heeft voor
    het weekend, enz.)<br />
    De inhoud van het bericht moet een indicatie bevatten van wanneer de
    beoordeling verwacht kan worden.<br />
    Houd er rekening mee dat ITR-pseudo-URL's door de spider worden genegeerd.<br />
  </dd>
  <dt>
    <tt>LCFC</tt> (Last Chance For Comment)
  </dt>
  <dd>
    Geeft aan dat de vertaling is voltooid, inclusief de wijzigingen uit het
    beoordelingsproces, en dat deze naar de juiste plaats zal worden
    verzonden.<br />
    Kan worden verzonden als er geen ITR's zijn en de discussie na de laatste RFR
    al een paar dagen is beëindigd.<br />
    Mag niet worden verzonden voordat er minstens één beoordeling is geweest.
  </dd>
  <dt>
    <tt>BTS#&lt;bugnummer&gt;</tt> (Bug Tracking System)
  </dt>
  <dd>
    Wordt gebruikt om een bugnummer te registreren nadat u de vertaling bij het
    BTS heeft ingediend.<br />
    De spider zal regelmatig controleren of een open bugrapport een oplossing
    heeft gekregen of gesloten is.
  </dd>
  <dt>
    <tt>DONE</tt>
  </dt>
  <dd>
    Wordt gebruikt om een overleg te sluiten zodra de vertaling is afgehandeld;
    handig als deze niet naar het BTS is verzonden.
  </dd>
  <dt>
    <tt>HOLD</tt>
  </dt>
  <dd>
    Wordt gebruikt om een vertaling in de wacht te zetten, bijvoorbeeld als er
    meer wijzigingen nodig zijn (als er in het pakket fouten zitten die invloed
    hebben op de vertaling of als de vertaling ergens anders beschikbaar is).<br />
    Het doel van deze status is om onnodig werk te vermijden.
  </dd>
</dl>

<p>
Het <i>type</i> kan alles zijn wat aangeeft over welk type document het gaat, zoals
po-debconf, po, po4a, of wml.
</p>

<p>
<i>pakket</i> is de naam van het pakket waaruit het document komt.
Gebruik <i>www.debian.org</i> of niets voor de WML-bestanden van de website
van Debian.
</p>

<p>
<i>bestand</i> is de bestandsnaam van het document; deze kan andere informatie
bevatten om het document uniek te identificeren, zoals het pad naar het bestand.
Het is gewoonlijk een naam zoals <i>tc</i>.po, waarbij <i>tc</i> de taalcode is
(bijv.: <i>de</i> voor Duits, of <i>pt_BR</i> voor Braziliaans Portugees).
</p>

<p>
De structuur van <i>bestand</i> hangt van het gekozen type af en natuurlijk
ook van de taal.
In principe is het gewoon een identificatie, maar omdat het op deze webpagina's
wordt gebruikt om de status van vertalingen bij te houden, wordt het sterk
aangeraden om het onderstaande schema te volgen.
</p>

<ul>
<li><code>po-debconf://pakketnaam/tc.po</code> (voor de configuratie-interface van het installatiesysteem)</li>
<li><code>po://pakketnaam/pad-in-bronpakket/tc.po</code> (voor een klassiek po-bestand)</li>
<li><code>po4a://pakketnaam/pad-in-bronpakket/tc.po</code> (voor documentatie omgezet in po-formaat)</li>
<li><code>wml://pad_onder_taalnaam_in_CVS</code> (voor pagina's van de website)</li>
<li><code>ddp://document/bestandsnaam.po</code> (voor Debian documentatie)</li>
<li><code>xml://installation-guide/taal/pad-in-bronpakket/bestand.xml</code> (voor de installatiehandleiding)</li>
</ul>

<p>
De BTS-status is enigszins speciaal; deze registreert een bugnummer zodat de
l10n-bot de status van de vertaling kan volgen nadat deze is ingediend bij
het BTS door te controleren of er openstaande bugrapporten zijn gesloten.
Dus bijvoorbeeld op de lijst debian-l10n-spanish zou men het volgende kunnen
gebruiken:
</p>
<div class="center"><code>[BTS#123456] po-debconf://cups/es.po</code></div>

<p>
Als u van plan bent veel pakketten te vertalen, kunt u in één keer een ITT sturen
voor al deze pakketten.
Een voorbeeld (voor de lijst debian-l10n-danish):
</p>
<div class="center"><code>[ITT] po-debconf://{cups,courier,apache2}/da.po</code></div>
<p>
Plaats de pakketten dus tussen accolades en scheid ze met komma's.
Geen extra spaties!
</p>
