#use wml::debian::cdimage title="Medios de instalación de Debian usando USB, CD, DVD" BARETITLE=true
#use wml::debian::release_info
#use wml::debian::translation-check translation="cce3f57454e0a9b64a58c195dee6676bc46963b8" maintainer="Laura Arjona Reina"


<div class="tip">
<p>Si simplemente quiere instalar Debian y tiene una conexión de Internet
en el sistema donde va a instalarlo, recomendamos los medios para la <a
href="netinst/">instalación en red</a> que son descargas más pequeñas.</p> </div>

<div class="tip">
<p>En las arquitecturas i386 y amd64, todas las imágenes de USB/CD/DVD también pueden
<a href="https://www.debian.org/CD/faq/#write-usb">usarse en memorias USB</a>.</div>

<ul>

 <li><a href="http-ftp/">Descarga de imágenes de CD/DVD usando HTTP.</a> </li>

  <li><a href="torrent-cd/">Descarga de imágenes de CD/DVD con BitTorrent.</a>
  El sistema «peer to peer» Bittorrent permite descargar imágenes de forma
  cooperativa a muchos usuarios al mismo tiempo, lo que puede acelerar la descarga.
  </li>

  <li><a href="live/">Descarga de imágenes vivas («live») usando HTTP,
  FTP o BitTorrent.</a>
Las imágenes vivas son para arrancar un sistema vivo sin instalar.
Permiten probar Debian primero y posteriormente instalar el contenido de la imagen.</li>

  <li><a href="vendors/">Compre medios de instalación de Debian terminados.</a> </li>

  <li><a href="jigdo-cd/">Descarga de imágenes de USB/CD/DVD con jigdo.</a>
  Solo para usuarios avanzados. El diseño de «jigdo» le permite seleccionar la más rápida de las
  300 réplicas de Debian de todo el mundo para su descarga. Proporciona una
  fácil selección de la réplica y «actualización» de imágenes viejas a la
  última versión. También es la única manera de descargar imágenes de DVD de
  Debian para <em>todas</em> las arquitecturas.</li>

  <li>En caso de problemas, por favor revise las <a href="faq/">preguntas frecuentes sobre CDs/DVDs de Debian</a>.</li>

</ul>

<p>Las imágenes oficiales en CD/DVD estan firmadas, lo cual permite <a
href="verify">verificar que son auténticas</a>.</p>

<p>Debian está disponible para diferentes arquitecturas de
computadoras (la mayoría de la gente necesitará imágenes
para <q>amd64</q>, por ejemplo, para los sistemas PC compatibles de 64 bits).</p>

      <div class="cdflash" id="latest">Última versión oficial
      «estable» de las imágenes de CD/DVD:
        <strong><current-cd-release></strong>.
      </div>

<p>Se puede encontrar información sobre incidencias conocidas en la instalación en la página de 
<a href="$(HOME)/releases/stable/debian-installer/">información de
instalación</a>.<br>
</p>
