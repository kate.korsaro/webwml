#use wml::debian::translation-check translation="7e9ce6b3f7065fbbce6287410a40acb00b516392"
<define-tag pagetitle>تحديث دبيان 11: الإصدار 11.8</define-tag>
<define-tag release_date>2023-10-07</define-tag>
#use wml::debian::news

<define-tag release>11</define-tag>
<define-tag codename>bullseye</define-tag>
<define-tag revision>11.8</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>
يسعد مشروع دبيان الإعلان عن التحديث الثامن لتوزيعته المستقرة دبيان <release> (الاسم الرمزي <q><codename></q>).
بالإضافة إلى تسوية بعض المشكلات الحرجة يصلح هذا التحديث بالأساس مشاكلات الأمان. تنبيهات الأمان أعلنت بشكل منفصل ومشار إليها فقط في هذا الإعلان.
</p>

<p>
يرجى ملاحظة أن هذا التحديث لا يشكّل إصدار جديد لدبيان <release> بل فقط تحديثات لبعض الحزم المضمّنة
وبالتالي ليس بالضرورة رمي الوسائط القديمة للإصدار <q><codename></q>، يمكن تحديث الحزم باستخدام مرآة دبيان محدّثة.
</p>

<p>
الذين يثبّتون التحديثات من security.debian.org باستمرار لن يكون عليهم تحديث العديد من الحزم،
أغلب التحديثات مضمّنة في هذا التحديث.
</p>

<p>
صور جديدة لأقراص التثبيت ستكون متوفرة في موضعها المعتاد.
</p>

<p>
يمكن الترقية من  تثبيت آنيّ إلى هذه المراجعة بتوجيه نظام إدارة الحزم إلى إحدى مرايا HTTP الخاصة بدبيان.
قائمة شاملة لمرايا دبيان على المسار:
</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>




<h2>إصلاح العديد من العلاّت</h2>

<p>أضاف هذا التحديث للإصدار المستقر بعض الإصلاحات المهمة للحزم التالية:</p>

<table border=0>
<tr><th>الحزمة</th>               <th>السبب</th></tr>
<correction adduser "Fix command injection vulnerability in deluser">
<correction aide "Fix handling of extended attributes on symlinks">
<correction amd64-microcode "Update included microcode, including fixes for <q>AMD Inception</q> on AMD Zen4 processors [CVE-2023-20569]">
<correction appstream-glib "Handle &lt;em&gt; and &lt;code&gt; tags in metadata">
<correction asmtools "Backport to bullseye for future openjdk-11 builds">
<correction autofs "Fix missing mutex unlock; do not use rpcbind for NFS4 mounts; fix regression determining reachability on dual-stack hosts">
<correction base-files "Update for the 11.8 point release">
<correction batik "Fix Server Side Request Forgery issues [CVE-2022-44729 CVE-2022-44730]">
<correction bmake "Conflict with bsdowl (&lt;&lt; 2.2.2-1.2~) to ensure smooth upgrades">
<correction boxer-data "Backport thunderbird compatibility fixes">
<correction ca-certificates-java "Work around unconfigured jre during new installations">
<correction cairosvg "Handle data: URLs in safe mode">
<correction cargo-mozilla "New <q>upstream</q> version, to support building newer firefox-esr versions">
<correction clamav "New upstream stable release; fix denial of service vulnerability via HFS+ parser [CVE-2023-20197]">
<correction cpio "Fix arbitrary code execution issue [CVE-2021-38185]; replace Suggests: on libarchive1 with libarchive-dev">
<correction cryptmount "Fix memory-initialization in command-line parser">
<correction cups "Fix heap-based buffer overflow issues [CVE-2023-4504 CVE-2023-32324], unauthenticated access issue [CVE-2023-32360], use-after-free issue [CVE-2023-34241]">
<correction curl "Fix code execution issues [CVE-2023-27533 CVE-2023-27534], information disclosure issues [CVE-2023-27535 CVE-2023-27536 CVE-2023-28322], inappropriate connection re-use issue [CVE-2023-27538], improper certificate validation issue [CVE-2023-28321]">
<correction dbus "New upstream stable release; fix denial of service issue [CVE-2023-34969]">
<correction debian-design "Rebuild using newer boxer-data">
<correction debian-installer "Increase Linux kernel ABI to 5.10.0-26; rebuild against proposed-updates">
<correction debian-installer-netboot-images "Rebuild against proposed-updates">
<correction debian-parl "Rebuild using newer boxer-data">
<correction debian-security-support "Set DEB_NEXT_VER_ID=12 as bookworm is the next release; security-support-limited: add gnupg1">
<correction distro-info-data "Add Debian 14 <q>forky</q>; correct Ubuntu 23.04 release date; add Ubuntu 23.10 Mantic Minotaur; add the planned release date for Debian bookworm">
<correction dkimpy "New upstream bugfix release">
<correction dpdk "New upstream stable release">
<correction dpkg "Add support for loong64 CPU; handle missing Version when formatting source:Upstream-Version; fix varbuf memory leak in pkg_source_version()">
<correction flameshot "Disable uploads to imgur by default; fix name of d/NEWS file in previous upload">
<correction ghostscript "Fix buffer overflow issue [CVE-2023-38559]; try and secure the IJS server startup [CVE-2023-43115]">
<correction gitit "Rebuild against new pandoc">
<correction grunt "Fix race condition in symlink copying [CVE-2022-1537]">
<correction gss "Add Breaks+Replaces: libgss0 (&lt;&lt; 0.1)">
<correction haskell-hakyll "Rebuild against new pandoc">
<correction haskell-pandoc-citeproc "Rebuild against new pandoc">
<correction hnswlib "Fix double free in init_index when the M argument is a large integer [CVE-2023-37365]">
<correction horizon "Fix open redirect issue [CVE-2022-45582]">
<correction inetutils "Check return values for set*id() functions, avoiding potential security issues [CVE-2023-40303]">
<correction krb5 "Fix free of uninitialised pointer [CVE-2023-36054]">
<correction kscreenlocker "Fix authentication error when using PAM">
<correction lacme "Handle CA ready, processing and valid states correctly">
<correction lapack "Fix eigenvector matrix">
<correction lemonldap-ng "Fix open redirection when OIDC RP has no redirect URIs; fix Server Side Request Forgery issue [CVE-2023-44469]; fix open redirection due to incorrect escape handling">
<correction libapache-mod-jk "Remove implicit mapping functionality, which could lead to unintended exposure of the status worker and/or bypass of security constraints [CVE-2023-41081]">
<correction libbsd "Fix infinite loop in MD5File">
<correction libclamunrar "New upstream stable release">
<correction libprelude "Make Python module usable">
<correction libreswan "Fix denial of service issue [CVE-2023-30570]">
<correction libsignal-protocol-c "Fix integer overflow issue [CVE-2022-48468]">
<correction linux "New upstream stable release">
<correction linux-signed-amd64 "New upstream stable release">
<correction linux-signed-arm64 "New upstream stable release">
<correction linux-signed-i386 "New upstream stable release">
<correction logrotate "Avoid replacement of /dev/null with a regular file if used for the state file">
<correction ltsp "Avoid using <q>mv</q> on init symlink in order to work around overlayfs issue">
<correction lttng-modules "Fix build issues with newer kernel versions">
<correction lua5.3 "Fix use after free in lua_upvaluejoin (lapi.c) [CVE-2019-6706]; fix segmentation fault in getlocal and setlocal (ldebug.c) [CVE-2020-24370]">
<correction mariadb-10.5 "New upstream bugfix release [CVE-2022-47015]">
<correction mujs "Security fix">
<correction ncurses "Disallow loading of custom terminfo entries in setuid/setgid programs [CVE-2023-29491]">
<correction node-css-what "Fix regular expression-based denial of service issue [CVE-2022-21222 CVE-2021-33587]">
<correction node-json5 "Fix prototype pollution issue [CVE-2022-46175]">
<correction node-tough-cookie "Security fix: prototype pollution [CVE-2023-26136]">
<correction nvidia-graphics-drivers "New upstream release [CVE-2023-25515 CVE-2023-25516]; improve compatibility with recent kernels">
<correction nvidia-graphics-drivers-tesla-450 "New upstream release [CVE-2023-25515 CVE-2023-25516]">
<correction nvidia-graphics-drivers-tesla-470 "New upstream bugfix release [CVE-2023-25515 CVE-2023-25516]">
<correction openblas "Fix results of DGEMM on AVX512-capable hardware, when the package has been built on pre-AVX2 hardware">
<correction openssh "Fix remote code execution issue via a forwarded agent socket [CVE-2023-38408]">
<correction openssl "New upstream stable release; fix denial of service issues [CVE-2023-3446 CVE-2023-3817]">
<correction org-mode "Fix command injection vulnerability [CVE-2023-28617]">
<correction pandoc "Fix arbitrary file write issues [CVE-2023-35936 CVE-2023-38745]">
<correction pev "Fix buffer overflow issue [CVE-2021-45423]">
<correction php-guzzlehttp-psr7 "Fix improper input validation [CVE-2023-29197]">
<correction php-nyholm-psr7 "Fix improper input validation issue [CVE-2023-29197]">
<correction postgis "Fix axis order regression">
<correction protobuf "Security fixes: DoS in Java [CVE-2021-22569]; NULL pointer dereference [CVE-2021-22570]; memory DoS [CVE-2022-1941]">
<correction python2.7 "Fix <q>parameter cloaking</q> issue [CVE-2021-23336], URL injection issue [CVE-2022-0391], use-after-free issue [CVE-2022-48560], XML External Entity issue [CVE-2022-48565]; improve constant-time comparisons in compare_digest() [CVE-2022-48566]; improve URL parsing [CVE-2023-24329]; prevent reading unauthenticated data on an SSLSocket [CVE-2023-40217]">
<correction qemu "Fix infinite loop [CVE-2020-14394], NULL pointer dereference issue [CVE-2021-20196], integer overflow issue [CVE-2021-20203], buffer overflow issues [CVE-2021-3507 CVE-2023-3180], denial of service issues [CVE-2021-3930 CVE-2023-3301], use-after-free issue [CVE-2022-0216], possible stack overflow and use-after-free issues [CVE-2023-0330], out-of-bounds read issue [CVE-2023-1544]">
<correction rar "New upstream release; fix directory traversal issue [CVE-2022-30333]; fix arbitrary code execution issue [CVE-2023-40477]">
<correction rhonabwy "Fix aesgcm buffer overflow [CVE-2022-32096]">
<correction roundcube "New upstream stable release; fix cross-site scripting issue [CVE-2023-43770]; Enigma: Fix initial synchronization of private keys">
<correction rust-cbindgen "New <q>upstream</q> version, to support building newer firefox-esr versions">
<correction rustc-mozilla "New <q>upstream</q> version, to support building newer firefox-esr versions">
<correction schleuder "Add versioned dependency on ruby-activerecord">
<correction sgt-puzzles "Fix various security issues in game loading [CVE-2023-24283 CVE-2023-24284 CVE-2023-24285 CVE-2023-24287 CVE-2023-24288 CVE-2023-24291]">
<correction spip "Several security fixes; security fix for extended authentification data filtering">
<correction spyder "Fix broken patch in previous update">
<correction systemd "Udev: fix creating /dev/serial/by-id/ symlinks for USB devices; fix memory leak on daemon-reload; fix a calendar spec calculation hang on DST change if TZ=Europe/Dublin">
<correction tang "Fix race condition when creating/rotating keys; assert restrictive permissions on key directory [CVE-2023-1672]; make tangd-rotate-keys executable">
<correction testng7 "Backport to oldstable for future openjdk-17 builds">
<correction tinyssh "Work around incoming packets which don't honour max packet length">
<correction unrar-nonfree "Fix file overwrite issue [CVE-2022-48579]; fix remote code execution issue [CVE-2023-40477]">
<correction xen "New upstream stable release; fix security issues [CVE-2023-20593 CVE-2023-20569 CVE-2022-40982]">
<correction yajl "Memory leak security fix; security fixes: potential denial of service with crafted JSON file [CVE-2017-16516]; heap memory corruption when dealing with large (~2GB) inputs [CVE-2022-24795]; fix incomplete patch for CVE-2023-33460">
</table>


<h2>تحديثات الأمان</h2>


<p>
أضافت هذه المراجعة تحديثات الأمان التالية للإصدار المستقر.
سبق لفريق الأمان نشر تنبيه لكل تحديث:
</p>

<table border=0>
<tr><th>معرَّف التنبيه</th>  <th>الحزمة</th></tr>
<dsa 2023 5394 ffmpeg>
<dsa 2023 5395 nodejs>
<dsa 2023 5396 evolution>
<dsa 2023 5396 webkit2gtk>
<dsa 2023 5397 wpewebkit>
<dsa 2023 5398 chromium>
<dsa 2023 5399 odoo>
<dsa 2023 5400 firefox-esr>
<dsa 2023 5401 postgresql-13>
<dsa 2023 5402 linux-signed-amd64>
<dsa 2023 5402 linux-signed-arm64>
<dsa 2023 5402 linux-signed-i386>
<dsa 2023 5402 linux>
<dsa 2023 5403 thunderbird>
<dsa 2023 5404 chromium>
<dsa 2023 5405 libapache2-mod-auth-openidc>
<dsa 2023 5406 texlive-bin>
<dsa 2023 5407 cups-filters>
<dsa 2023 5408 libwebp>
<dsa 2023 5409 libssh>
<dsa 2023 5410 sofia-sip>
<dsa 2023 5411 gpac>
<dsa 2023 5412 libraw>
<dsa 2023 5413 sniproxy>
<dsa 2023 5414 docker-registry>
<dsa 2023 5415 libreoffice>
<dsa 2023 5416 connman>
<dsa 2023 5417 openssl>
<dsa 2023 5418 chromium>
<dsa 2023 5419 c-ares>
<dsa 2023 5420 chromium>
<dsa 2023 5421 firefox-esr>
<dsa 2023 5422 jupyter-core>
<dsa 2023 5423 thunderbird>
<dsa 2023 5424 php7.4>
<dsa 2023 5426 owslib>
<dsa 2023 5427 webkit2gtk>
<dsa 2023 5428 chromium>
<dsa 2023 5430 openjdk-17>
<dsa 2023 5431 sofia-sip>
<dsa 2023 5432 xmltooling>
<dsa 2023 5433 libx11>
<dsa 2023 5434 minidlna>
<dsa 2023 5435 trafficserver>
<dsa 2023 5436 hsqldb1.8.0>
<dsa 2023 5437 hsqldb>
<dsa 2023 5438 asterisk>
<dsa 2023 5439 bind9>
<dsa 2023 5440 chromium>
<dsa 2023 5441 maradns>
<dsa 2023 5442 flask>
<dsa 2023 5443 gst-plugins-base1.0>
<dsa 2023 5444 gst-plugins-bad1.0>
<dsa 2023 5445 gst-plugins-good1.0>
<dsa 2023 5446 ghostscript>
<dsa 2023 5447 mediawiki>
<dsa 2023 5449 webkit2gtk>
<dsa 2023 5450 firefox-esr>
<dsa 2023 5451 thunderbird>
<dsa 2023 5452 gpac>
<dsa 2023 5453 linux-signed-amd64>
<dsa 2023 5453 linux-signed-arm64>
<dsa 2023 5453 linux-signed-i386>
<dsa 2023 5453 linux>
<dsa 2023 5455 iperf3>
<dsa 2023 5456 chromium>
<dsa 2023 5457 webkit2gtk>
<dsa 2023 5459 amd64-microcode>
<dsa 2023 5461 linux-signed-amd64>
<dsa 2023 5461 linux-signed-arm64>
<dsa 2023 5461 linux-signed-i386>
<dsa 2023 5461 linux>
<dsa 2023 5463 thunderbird>
<dsa 2023 5464 firefox-esr>
<dsa 2023 5465 python-django>
<dsa 2023 5467 chromium>
<dsa 2023 5468 webkit2gtk>
<dsa 2023 5470 python-werkzeug>
<dsa 2023 5471 libhtmlcleaner-java>
<dsa 2023 5472 cjose>
<dsa 2023 5473 orthanc>
<dsa 2023 5474 intel-microcode>
<dsa 2023 5475 linux-signed-amd64>
<dsa 2023 5475 linux-signed-arm64>
<dsa 2023 5475 linux-signed-i386>
<dsa 2023 5475 linux>
<dsa 2023 5476 gst-plugins-ugly1.0>
<dsa 2023 5478 openjdk-11>
<dsa 2023 5479 chromium>
<dsa 2023 5480 linux-signed-amd64>
<dsa 2023 5480 linux-signed-arm64>
<dsa 2023 5480 linux-signed-i386>
<dsa 2023 5480 linux>
<dsa 2023 5481 fastdds>
<dsa 2023 5482 tryton-server>
<dsa 2023 5483 chromium>
<dsa 2023 5484 librsvg>
<dsa 2023 5485 firefox-esr>
<dsa 2023 5486 json-c>
<dsa 2023 5487 chromium>
<dsa 2023 5489 file>
<dsa 2023 5490 aom>
<dsa 2023 5491 chromium>
<dsa 2023 5493 open-vm-tools>
<dsa 2023 5494 mutt>
<dsa 2023 5495 frr>
<dsa 2023 5497 libwebp>
<dsa 2023 5500 flac>
<dsa 2023 5502 xorgxrdp>
<dsa 2023 5502 xrdp>
<dsa 2023 5503 netatalk>
<dsa 2023 5504 bind9>
<dsa 2023 5505 lldpd>
<dsa 2023 5507 jetty9>
<dsa 2023 5510 libvpx>
</table>


<h2>الحزم المزالة</h2>

<p>
الحزم التالية أزيلت لأسباب خارجة عن سيطرتنا:
</p>

<table border=0>
<tr><th>الحزمة</th>               <th>السبب</th></tr>
<correction atlas-cpp "unstable upstream, unsuitable for Debian">
<correction ember-media "unstable upstream, unsuitable for Debian">
<correction eris "unstable upstream, unsuitable for Debian">
<correction libwfut "unstable upstream, unsuitable for Debian">
<correction mercator "unstable upstream, unsuitable for Debian">
<correction nomad "security fixes no longer available">
<correction nomad-driver-lxc "depends on to-be-removed nomad">
<correction skstream "unstable upstream, unsuitable for Debian">
<correction varconf "unstable upstream, unsuitable for Debian">
<correction wfmath "unstable upstream, unsuitable for Debian">

</table>

<h2>مُثبِّت دبيان</h2>
<p>
حدِّث المُثبِّت ليتضمن الإصلاحات المندرجة في هذا الإصدار المستقر.
</p>

<h2>المسارات</h2>

<p>
القائمة الكاملة للحزم المغيّرة في هذه المراجعة:
</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>التوزيعة المستقرة الحالية:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/stable/">
</div>

<p>التحديثات المقترحة للتوزيعة المستقرة:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/proposed-updates">
</div>

<p>معلومات حول التوزيعة المستقرة (ملاحظات الإصدار والأخطاء إلخ):</p>

<div class="center">
  <a
  href="$(HOME)/releases/stable/">https://www.debian.org/releases/stable/</a>
</div>

<p>معلومات وإعلانات الأمان:</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>حول دبيان</h2>

<p>
مشروع دبيان هو اتحاد لمطوري البرمجيات الحرة تطوعوا بالوقت والمجهود لإنتاج نظام تشعيل دبيان حر بالكامل.
</p>

<h2>معلومات الاتصال</h2>

<p>
لمزيد من المعلومات يرجى زيارة موقع دبيان
<a href="$(HOME)/">https://www.debian.org/</a>
أو إرسال بريد إلكتروني إلى &lt;press@debian.org&gt;
أو الاتصال بفريق إصدار المستقرة على
&lt;debian-release@lists.debian.org&gt;.
</p>


