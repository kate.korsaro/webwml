#use wml::debian::template title="Informatie over Debian 2.1 (slink)" BARETITLE=yes
#use wml::debian::release
#use wml::debian::translation-check translation="9d55b2307fc18a23f41144b417c9ec941cc3abf8"

<strong>Debian 2.1 werd vervangen.</strong>

<p>

Omdat er <a href="../">nieuwere releases</a> zijn gemaakt, is release 2.1 achterhaald. Deze pagina's worden bewaard voor historische doeleinden. U dient zich ervan bewust te zijn dat Debian 2.1 niet langer wordt onderhouden.

<p>

  De volgende architecturen werden in Debian 2.1 ondersteund:

<ul>
<li> alpha
<li> i386
<li> mk68k
<li> sparc
</ul>


<h2><a name="release-notes"></a>Notities bij de release</h2>

<p>

Raadpleeg de Notities bij de release voor uw architectuur om te weten wat er nieuw is in Debian 2.1. De Notities bij de release bevatten ook instructies voor gebruikers die opwaarderen vanaf eerdere releases.

<ul>
<li> <a href="alpha/release-notes.txt">Notities bij de release voor Alpha<a>
<li> <a href="i386/release-notes.txt">Notities bij de release voor 32-bits pc (i386)<a>
<li> <a href="m68k/release-notes.txt">Notities bij de release voor Motorola 680x0<a>
<li> <a href="sparc/release-notes.txt">Notities bij de release voor SPARC<a>
</ul>

<h2><a name="errata"></a>Errata</h2>

<p>

Soms wordt bij kritieke problemen of beveiligingsupdates de vrijgegeven distributie (in dit geval Slink) bijgewerkt. Over het algemeen worden deze aangeduid als tussenreleases. De huidige tussenrelease is Debian 2.1r5. U kunt de <a
href="http://archive.debian.org/debian/dists/slink/ChangeLog">ChangeLog</a>
vinden op elke Debian-archiefspiegelserver.

<p>

Slink is gecertificeerd voor gebruik met de 2.0.x-serie Linux-kernels. Als je de Linux 2.2.x-kernel met slink wilt gebruiken, bekijk dan de <a href="running-kernel-2.2">lijst met bekende problemen</a>.


<h3>APT</h3>

<p>

Een bijgewerkte versie van <code>apt</code> is beschikbaar in Debian sinds
2.1r3. Het voordeel van deze bijgewerkte versie is vooral dat deze de installatie vanaf meerdere cd's zelf kan afhandelen. Dit maakt de acquisitieoptie <code>dpkg-multicd</code> in <code>dselect</code> overbodig. Uw cd met 2.1 kan echter een oudere <code>apt</code> bevatten. U wilt dus misschien opwaarderen naar de versie die nu in Slink zit.



<h2><a name="acquiring"></a>Debian 2.1 verkrijgen</h2>

<p>

Debian is elektronisch of via cd-verkopers verkrijgbaar.

<h3>Debian op cd kopen</h3>

<p>

We houden een <a href="../../CD/vendors/">lijst met cd-verkopers</a>
bij die cd's met Debian 2.1 verkopen.


<h3>Debian downloaden via internet</h3>

<p>

We houden een <a href="../../distrib/ftplist">lijst met sites</a> bij
die de distributie spiegelen.


<!-- Keep this comment at the end of the file
Local variables:
mode: sgml
sgml-indent-data:nil
sgml-doctype:"../.doctype"
End:
-->
