<define-tag pagetitle>The Debian Project mourns the loss of Jérémy Bobbio (Lunar)</define-tag>
<define-tag release_date>2024-11-19</define-tag>
#use wml::debian::news
# $Id$

<p>The Debian Project sadly shares the news of the passing of Jérémy Bobbio
(Lunar) on Friday, November 8, 2024.</p>

<p>Lunar was well recognized and respected in several areas of the Linux and
Free/Libre Open Source Software (FLOSS) communities; he was equally well known
as a staunch proponent of individual and collective freedoms.</p>

<p>Lunar was dedicated to and always present in the formulation and discussion
of <a href="https://reproducible-builds.org/">Reproducible Builds</a>.  For
many people their introduction to this system of checks and balances in
software came directly from his mouth at numerous conferences, talks, and
mailing lists.</p>

<p>Lunar was also a valiant proponent of and an outspoken ambassador over
concerns of surveillance and censorship and privacy protection, prompting him
to work in and develop relationships and software inside of the
<a href="https://torproject.org/">Tor Project</a>.  Lunar was instrumental in
helping the network develop tools, establish relays, and address legal concerns
for operators and administrators.</p>

<p>Lunar's passing leaves a hole in many communities. We will carry on, we
will all continue to expand in the areas that were important to him, and we
will continue that work in his legacy.  Lunar you will be missed!</p>

<p>Tributes and remembrances were expressed around the world at the news of
Lunar's passing; here we share some of those reflections:</p>

<blockquote>
<p>His work on Reproducible Builds, alongside some other folks on this list,
was transformational for Debian and, dare I say, ahead of its time for an
industry that is slowly discovering the immense threat that supply chain
attacks present (most recently with XZ).</p>
</blockquote>
<p>&mdash;&nbsp;Faidon Liambotis</p>

<blockquote>
<p>I always watched and paid attention to his level of detail on what others
would have considered trivial, but through understanding and explanation from
him those minor details transformed into a critical understanding. A true
mentor.</p>
</blockquote>
<p>&mdash;&nbsp;Donald Norwood</p>

<blockquote>
<p>Lunar was an incredible person, and I miss them deeply, despite getting to
see them only every now and then. Creative, thoughtful, smart, and kind but not
indiscriminately so. Prickly when prickliness was called for, but also willing
to extend a hand or grace as needed. I'll miss their sharp wit, incisive
observations, and provocations to be a better person.</p>
</blockquote>

<blockquote>
<p>The people who work on free software and adjacent projects form a better
community for having had Lunar among us. I continue to try to live up to the
standards Lunar set.</p>
</blockquote>
<p>&mdash;&nbsp;dkg</p>

<blockquote>
<p>I basically remember one of the last conversations I had with Lunar, a few
months ago, when he told me how proud he was not only of having started Nos
Oignons and contributed to the ignition of Reproducible Builds, but
specifically about the fact that both initiatives were now thriving without
being dependent on him. He was likely thinking about a future world without
him, but also realizing how impactful his activism had been on the past and
present world.</p>
</blockquote>
<p>&mdash;&nbsp;Stefano Zacchiroli</p>

<blockquote>
<p>I remember quite some moments but funnily the moment I most often think
about is meeting him on campus in Portland at DebConf14 when he just came back
from buying a big box of vegan donuts to share. He was so happy about that! On
a more serious note I do think his impact on Tor, Debian &amp; Reproducible
Builds can hardly be overestimated and then there's a wide area of "offline
stuff" he also deeply cared about, and yet he always stayed humble and helpful.
I'm incredible thankful for all his work and having been able to share some
work and good times together.</p>
</blockquote>
<p>&mdash;&nbsp;Holger Levsen</p>

<h2>About Debian</h2>
<p>The Debian Project is an association of Free Software developers who
volunteer their time and effort in order to produce a completely free
operating system known as Debian.</p>

<h2>Contact Information</h2>
<p>For further information, please visit the Debian web pages at
<a href="$(HOME)/">https://www.debian.org/</a> or send mail to
&lt;<a href="mailto:press@debian.org">press@debian.org</a>&gt;.</p>
