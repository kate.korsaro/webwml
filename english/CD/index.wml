#use wml::debian::cdimage title="Debian installation media for USB, CD, DVD" BARETITLE=true
#use wml::debian::release_info


<div class="tip">
<p>If you simply want to install Debian and have an Internet connection on the
target computer we recommend the <a href="netinst/">Network
Install</a> media which is a smaller download.</p> </div>

<div class="tip">
<p>On i386 and amd64 architectures, all USB/CD/DVD images can be
<a href="https://www.debian.org/CD/faq/#write-usb">used on a USB stick</a> too.</div>

<ul>

  <li><a href="http-ftp/">Download USB/CD/DVD images using HTTP.</a> </li>

  <li><a href="torrent-cd/">Download USB/CD/DVD images with BitTorrent.</a>
  The Bittorrent peer to peer system lets many users cooperatively download
  images at the same time which may speed up your download.
  </li>

  <li><a href="live/">Download live images using HTTP, FTP or BitTorrent.</a>
  Live images are for booting a live system without installing.
  You can use to try Debian first and then install the contents of the
  image.</li>

  <li><a href="vendors/">Buy finished Debian media.</a> </li>

  <li><a href="jigdo-cd/">Download USB/CD/DVD images with jigdo.</a>
  This is only for advanced users. The "jigdo" scheme allows you to pick the fastest out of
  300 Debian mirrors worldwide for your download. It features easy
  mirror selection and "upgrading" of older images to the latest
  release. Also, it is the only way to download all Debian DVD
  images.</li>

  <li>In case of problems, please check the <a href="faq/">FAQ about
  Debian CDs/DVDs</a>.</li>


</ul>

<p>Official USB/CD/DVD releases are signed so that you can <a
href="verify">verify they are authentic</a>.</p>

<p>Debian is available for different computer architectures (Most people
will need images for <q>amd64</q>, i.e. 64-bit PC-compatible systems).</p>

      <div class="cdflash" id="latest">Latest official release
      of the "stable" USB/CD/DVD images:
        <strong><current-cd-release></strong>.
      </div>

<p>Information about known installation issues can be found on the
<a href="$(HOME)/releases/stable/debian-installer/">installation
information</a> page.<br>
</p>
