msgid ""
msgstr ""
"Project-Id-Version: homepage.nl.po\n"
"POT-Creation-Date: \n"
"PO-Revision-Date: \n"
"Last-Translator: Frans Spiesschaert <Frans.Spiesschaert@yucom.be>\n"
"Language-Team: Debian Dutch l10n Team <debian-l10n-dutch@lists.debian.org>\n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 2.2.1\n"

#: ../../english/index.def:8
msgid "The Universal Operating System"
msgstr "Het universele Besturingssysteem"

#: ../../english/index.def:12
msgid "DebConf is underway!"
msgstr "DebConf is bezig!"

#: ../../english/index.def:15
msgid "DebConf Logo"
msgstr "DebConf Logo"

#: ../../english/index.def:19
msgid "DC22 Group Photo"
msgstr "DC22 Groepsfoto"

#: ../../english/index.def:22
msgid "DebConf22 Group Photo"
msgstr "DebConf22 Groepsfoto"

#: ../../english/index.def:26
msgid "DC23 Group Photo"
msgstr "DC23 Groepsfoto"

#: ../../english/index.def:29
msgid "DebConf23 Group Photo"
msgstr "DebConf23 Groepsfoto"

#: ../../english/index.def:33
msgid "DC24 Group Photo"
msgstr "DC24 Groepsfoto"

#: ../../english/index.def:36
msgid "DebConf24 Group Photo"
msgstr "DebConf24 Groepsfoto"

#: ../../english/index.def:40
msgid "MiniDebConf Berlin 2024"
msgstr "Mini-DebConf Berlijn 2024"

#: ../../english/index.def:43
msgid "Group photo of the MiniDebConf Berlin 2024"
msgstr "Groepsfoto van de Mini-DebConf in Berlijn 2024"

#: ../../english/index.def:47
msgid "MiniDebConf Brasília 2023"
msgstr "Mini-DebConf Brasilia 2023"

#: ../../english/index.def:50
msgid "Group photo of the MiniDebConf Brasília 2023"
msgstr "Groepsfoto van de Mini-DebConf in Brasilia 2023"

#: ../../english/index.def:54
msgid "Mini DebConf Regensburg 2021"
msgstr "Mini-DebConf Regensburg 2021"

#: ../../english/index.def:57
msgid "Group photo of the MiniDebConf in Regensburg 2021"
msgstr "Groepsfoto van de Mini-DebConf in Regensburg 2021"

#: ../../english/index.def:61
msgid "Screenshot Calamares Installer"
msgstr "Schermafdruk Calamares installatieprogramma"

#: ../../english/index.def:64
msgid "Screenshot from the Calamares installer"
msgstr "Schermafdruk van het Calamares installatieprogramma"

#: ../../english/index.def:68 ../../english/index.def:71
msgid "Debian is like a Swiss Army Knife"
msgstr "Debian is als een Zwitsers zakmes"

#: ../../english/index.def:75
msgid "People have fun with Debian"
msgstr "Mensen hebben plezier met Debian"

#: ../../english/index.def:78
msgid "Debian people at Debconf18 in Hsinchu really having fun"
msgstr "Debian-mensen beleven echt plezier op Debconf18 in Hsinchu"
