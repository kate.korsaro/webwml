#use wml::debian::translation-check translation="3349f1cfd8d3f3eceed8b216922ec76404aa15f5" maintainer="Erik Pfannenstein"
<define-tag pagetitle>DebConf23 schließt in Kochi; Veranstaltungsort der DebConf24 bekanntgegeben</define-tag>
<define-tag release_date>2023-09-18</define-tag>
<define-tag frontpage>yes</define-tag>

#use wml::debian::news

<p>
Am gestrigen Sonntag, dem 17. September 2023, kam die jährliche Debian
Entwickler- und Unterstützerkonferenz zu ihrem Ende.
</p>

<p>
Aus 35 Ländern in aller Welt kamen 474 Personen zusammen und besuchten
insgesamt 89 Veranstaltungen. Es gab Vorträge, Diskussionen,
»Birds-of-a-Feather« (BoF)-Versammlungen und Workshops sowie Aktivitäten zur
Weiterentwicklung unserer Distribution, zum Lernen von unseren
Mentoren und Mitstreitern und zur Stärkung der Gemeinschaft. Auch der Spaß
kam nicht zu kurz.
</p>

<p>
Der Konferenz ging vom 3. bis 9. September das jährliche
<a href="https://wiki.debian.org/DebCamp">DebCamp</a> voraus. In dieser
Hacking-Versammlung konzentrierten sich Debian-Entwickler und -Unterstützer auf
ihre individeuellen Debian-bezogenen Projekte oder nahmen an Team-Sprints teil,
um sich während der Arbeit an Debian auch mal persönlich gegenüberzusitzen.

In diesem Jahr ging es bei den Sprints hauptsächlich um das Vorantreiben von
Mobian/Debian, Reproducible Builds und Python in Debian. Außerdem gab es
heuer ein BootCamp, in welchem eine Gruppe engagierter Mentoren Neuankömmlingen
praktische Erfahrungen mit Debian sowie ein tieferes Verstädndnis der Arbeitsweise
der Gemeinschaft verschafften.
</p>

<p>
Die eigentliche Debian-Entwicklerkonferenz begann am Sonntag, dem 10. September 2023.

Im Anschluss an den üblichen 'Bits from the DPL'-Vortrag, der durchgehenden
Schlüssel-Signierungs-Feier, den Blitzvorträgen und der Ankündigung der nächstjährigen
DebConf24 gab es mehrere Sitzungen, in denen interne Projekte und Teams ihre
Neuigkeiten vorstellten.

Viele der moderierten Diskussionsrunden wurden von unseren technischen Teams
geführt, wobei sie ihre Arbeiten rund um die Langzeitunterstützung (LTS), die
Android-Werkzeuge, Debian-Derivate, den Debian-Installer, die Debian-Abbilder
und Debian Science in das Rampenlicht stellten. Darüber hinaus teilten die
Teams der Programmiersprachen Python, Perl und Ruby mit, was sie derzeit umtreibt.

Debian Brasil und Debian India, zwei der größeren Debian-Gemeinschaften vor Ort,
berichteten von ihren Kollaborationen. Sie erzählten, wie sie mit ihren Ratgebertexten
Gemeinschafts-Engagement demonstrierten und neue Mitglieder anzogen sowie Chancen sowohl
in Debian bzw. FOSS als auch in den Wissenschaften eröffneten.
</p>


<p>
Der <a href="https://debconf23.debconf.org/schedule/">Fahrplan</a> wurde jeden
Tag mit geplanten und spontanen Veranstaltungen durch die Anwesenden ergänzt. Mehrere
Aktivitäten, die in den letzten Jahren wegen der COVID-19-Pandemie nicht möglich waren,
sind nachgeholt worden: eine Jobbörse, die offene Bühne, die Lyrik-Nächte, die
traditionelle Käse-und-Wein-Fete, Gruppenfotos und Tagesausflüge.
</p>

<p>
Für alle, die nicht dabei sein konnten, wurden die meisten Sitzungen und Vorträge
live übertragen und aufgezeichnet; die Videoaufnahmen werden später in die
<a href="https://meetings-archive.debian.net/pub/debian-meetings/2023/DebConf23/">Debian-Meetings-Archiv-Website</a>
hochgeladen.
Fast alle Sitzungen waren offen für Fernteilnahme via IRC oder gemeinsam bearbeitete
Textdateien, sodass auch Zuseher von zuhause aus ihre Fragen oder Kommentare
mit den Vortragenden und dem versammelten Publikum zukommen lassen konnten.
</p>

<p>
Während der DebConf23 kamen 4,3 TiB Daten, 55 Stunden an Vortragsaufzeichnungen,
1.463 Stunden Videostreams in 35 Länder und fünf Tagesausflüge zusammen. Dafür
wurden 23 Netzwerk-Access Points, 11 Netzwerkswitches und 400 Meter Gafferband verwendet;
insgesamt wurden 75 Kilogramm an Ausrüstung importiert. 461 T-Shirts wurden verkauft
und im Schnitt 169 Mahlzeiten pro Tag eingeplant.

Diese ganzen Veranstaltungen, Aktivitäten, Gespräche und Streams, zusammen
mit unserer Liebe, dem Interesse und der Beteiligung an Debian bzw. FOSS
machten diese Konferenz sowohl hier in Kochi (Indien) als auch online in
aller Welt zu einem durchgehenden Erfolg.
</p>

<p>
Die <a href="https://debconf23.debconf.org/">DebConf23-Website</a>
wird zu Archivierungszwecken aktiv bleiben und Links zu den Präsentationen
und Videos der Vorträge und Veranstaltungen anbieten.
</p>

<p>
Die <a href="https://wiki.debian.org/DebConf/24">DebConf24</a> wird in Haifa in
Israel stattfinden.
Der Tradition gemäß werden die Organisatoren vor Ort in Israel der DebConf wieder
das DebCamp vorausgehen lassen und dabei den Fokus auf individuelle und Team-Arbeiten
zur Verbesserung der Distribution legen.
</p>

<p>
Der DebConf ist es wichtig, allen Anwesenden eine sichere und einladende Umgebung zu schaffen.
Weitere Details dazu können Sie im <a href="https://debconf23.debconf.org/about/coc/">Verhaltenskodex auf der DebConf23-Website</a>
nachlsesen.
</p>

<p>
Debian dankt den zahlreichen <a href="https://debconf23.debconf.org/sponsors/">Sponsoren</a> für ihre
Unterstützung für die DebConf23, vor allem danken wir unseren Platin-Sponsoren:
<a href="https://www.infomaniak.com">Infomaniak</a>,
<a href="https://www.proxmox.com/">Proxmox</a> und
<a href="https://www.siemens.com/">Siemens</a>.
</p>

<p>
Außerdem möchten wir uns bei unseren Video- und Infrastruktur-Teams, den
DebConf23- und DebConf-Ausschüssen, unserem Gastgeberland Indien sowie bei jeder
einzelnen Person, die bei dieser Veranstaltung oder Debian allgemein
mithilft, bedanken.

Vielen Dank für alle eure Beiträge, durch die Debian auch weiterhin
<q>das universelle Betriebssystem</q> sein kann.

Wir sehen uns im nächsten Jahr!
</p>


<h2>Über Debian</h2>

<p>
Das Debian-Projekt wurde 1993 von Ian Murdock gegründet und sollte ein
vollständig freies Gemeinschaftsprojekt sein. Seitdem ist es zu
einem der größten und einflussreichsten Open-Source-Projekte angewachsen.
Tausende von Freiwilligen aus aller Welt helfen zusammen, um Debian-Software
zu erschaffen und zu pflegen. Wegen seiner Verfügbarkeit in 70 Sprachen
und der Unterstützung für viele verschiedene Computer-Typen bezeichnet sich
Debian als das <q>universelle Betriebssystem</q>.
</p>


<h2>Über die DebConf</h2>

<p>
Die DebConf ist Debians Entwicklerkonferenz. Neben einem Zeitplan voller
technischer, sozialer und politischer Vorträge bietet die DebConf eine Möglichkeit
für Entwickler, Unterstützer und interessierte Personen, sich persönlich zu treffen
und enger zusammenzuarbeiten. Sie findet seit dem Jahr 2000 an verschiedenen Orten
statt, unter anderem in Schottland, Argentinien und Bosnien-Herzegowina. Weitere
Informationen über die DebConf sind auf <a href="https://debconf.org/">https://debconf.org</a>
zu finden.
</p>


<h2>Über Infomaniak</h2>
<p>
Die <a href="https://www.infomaniak.com">Infomaniak</a> ist eine Hauptakteurin auf dem
europäischen Cloud-Markt und führende Entwicklerin von Web-Technik in der Schweiz. Ihr
Ziel ist eine unabhängige europäische Alternative zu den Web-Giganten und sie hat sich
einem ethischen und nachhaltigen Netz verschrieben, welches die Privatsphäre respektiert
und Arbeitsstellen vor Ort erzeugt. Informaniak entwickelt Cloud-Lösungen (IaaS, PaaS,
VPS), Produktivitätswerkzeuge zur Online-Zusammenarbeit sowie Video- und Radio-Streamingdienste.
</p>

<h2>Über Proxmox</h2>
<p>
<a href="https://www.proxmox.com/">Proxmox</a> entwickelt leistungsstarke und
trotzdem leicht zu bedienende quelloffene Server-Software. Proxmox' Produktportfolio
umfasst Server-Virtualisierung, Backup und E-Mail-Sicherheit; es hilft Unternehnmen
egal welcher Größe, Sektor oder Industrie, ihre IT-Infrastruktur zu vereinfachen.
Die Proxmox-Lösungen basieren auf der großartigen Debian-Plattform und wir freuen uns
sehr, durch das Sponsoring der DebConf23 der Gemeinschaft etwas zurückgeben zu können.
</p>

<h2>Über Siemens</h2>
<p>
<a href="https://www.siemens.com/">Siemens</a> ist ein Technik-Unternehmen mit
Fokus auf Industrie, Infrastruktur und Transport. Ob ressourceneffiziente Fabriken,
widerstandsfähige Lieferketten, smartere Gebäude und Netze oder saubere und komfortable
Transportmöglichkeiten und fortschrittliches Gesundheitswesen, Siemens entwickelt
Technologie, die Kunden echte Mehrwerte schafft. Durch Zusammenführung der materiellen
und der digitalen Welten versetzt das Unternehmen seine Kunden in die Lage, ihre
Industrien und Märkte zu transformieren und so den Alltag von Milliarden Menschen
zu verbessern.

<h2>Kontaktinformationen</h2>

<p>Für weitere Informationen besuchen Sie bitte die DebConf23-Website unter
<a href="https://debconf23.debconf.org/">https://debconf23.debconf.org/</a>
oder senden Sie eine Mail (auf Englisch) an &lt;press@debian.org&gt;.</p>
