#use wml::debian::translation-check translation="9d41ab1625a3bbe9bf95b782d91e91b766a3f664"
<define-tag pagetitle>Debian 12 is bijgewerkt: 12.5 werd uitgebracht</define-tag>
<define-tag release_date>2024-02-10</define-tag>
#use wml::debian::news

<define-tag release>12</define-tag>
<define-tag codename>bookworm</define-tag>
<define-tag revision>12.5</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>Het Debian-project kondigt met genoegen de vijfde update aan van zijn
stabiele distributie Debian <release> (codenaam <q><codename></q>).
Deze tussenrelease voegt voornamelijk correcties voor beveiligingsproblemen toe,
samen met een paar aanpassingen voor ernstige problemen. Beveiligingsadviezen
werden reeds afzonderlijk gepubliceerd en, waar beschikbaar, wordt hiernaar
verwezen.</p>

<p>Merk op dat de tussenrelease geen nieuwe versie van Debian <release> is,
maar slechts een update van enkele van de meegeleverde pakketten. Het is niet
nodig om oude media met <q><codename></q> weg te gooien. Na de installatie
kunnen pakketten worden opgewaardeerd naar de huidige versie door een
bijgewerkte Debian-spiegelserver te gebruiken.</p>

<p>Wie regelmatig updates installeert vanuit security.debian.org zal niet veel
pakketten moeten updaten, en de meeste van dergelijke updates zijn opgenomen in
de tussenrelease.</p>

<p>Nieuwe installatie-images zullen binnenkort beschikbaar zijn op de gewone
plaatsen.</p>

<p>Het upgraden van een bestaande installatie naar deze revisie kan worden
bereikt door het pakketbeheersysteem naar een van de vele HTTP-spiegelservers
van Debian te verwijzen. Een uitgebreide lijst met spiegelservers is
beschikbaar op:</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>




<h2>Oplossingen voor diverse problemen</h2>

<p>Met deze update van stable, de stabiele distributie, worden een paar
belangrijke correcties aangebracht aan de volgende pakketten:</p>

<table border=0>
<tr><th>Pakket</th>               <th>Reden</th></tr>
<correction apktool "Willekeurig schrijven van bestanden met kwaadaardige bronnamen voorkomen [CVE-2024-21633]">
<correction atril "Oplossen van crash bij het openen van sommige epub-bestanden; het laden van de index voor bepaalde epub-documenten repareren; terugvalmogelijkheid toevoegen in check_mime_type voor verkeerd opgemaakte epub-bestanden; libarchive gebruiken in plaats van een extern commando voor het extraheren van documenten [CVE-2023-51698]">
<correction base-files "Update voor tussenrelease 12.5">
<correction caja "Reparatie voor artefacten op het bureaublad na een resolutiewijziging; probleem met het gebruik van het <q>informele</q> datumformaat oplossen">
<correction calibre "Oplossing voor <q>HTML-Invoer: Standaard geen bronnen toevoegen die bestaan buiten de maphiërarchie met als basis de bovenliggende map van het invoer-HTML-bestand</q> [CVE-2023-46303]">
<correction compton "Aanbeveling van picom verwijderen">
<correction cryptsetup "cryptsetup-initramfs: Ondersteuning voor gecomprimeerde kernelmodules toevoegen; cryptsetup-suspend-wrapper: Niet uitvallen wegens fout bij het ontbreken van de map /lib/system/system-sleep; add_modules(): De logica voor het laten vallen van achtervoegsels veranderen om overeen te komen met initramfs-tools">
<correction debian-edu-artwork "Voorzien in een grafisch ontwerp voor Debian Edu 12 dat op het Smaragd-thema gebaseerd is">
<correction debian-edu-config "Nieuwe bovenstroomse release">
<correction debian-edu-doc "Meegeleverde documentatie en vertalingen bijwerken">
<correction debian-edu-fai "Nieuwe bovenstroomse release">
<correction debian-edu-install "Nieuwe bovenstroomse release; beveiliging van sources.list herstellen">
<correction debian-installer "Linux kernel ABI verhogen naar 6.1.0-18; opnieuw bouwen tegen proposed-updates">
<correction debian-installer-netboot-images "Opnieuw bouwen tegen proposed-updates">
<correction debian-ports-archive-keyring "Automatische ondertekeningssleutel voor het archief Debian Ports toevoegen (2025)">
<correction dpdk "Nieuwe bovenstroomse stabiele release">
<correction dropbear "Oplossing voor <q>terrapin attack</q> [CVE-2023-48795]">
<correction engrampa "Verschillende geheugenlekken repareren; herstellen van archieffunctionaliteit <q>save as</q>">
<correction espeak-ng "Oplossing voor problemen met bufferoverloop [CVE-2023-49990 CVE-2023-49992 CVE-2023-49993], bufferonderloop-probleem [CVE-2023-49991], probleem met drijvende komma-uitzondering [CVE-2023-49994]">
<correction filezilla "Voorkomen van <q>terrapin</q>-aanval [CVE-2023-48795]">
<correction fish "Niet-afdrukbare Unicode-tekens betrouwbaar afhandelen wanneer deze worden gegeven als opdrachtvervanging [CVE-2023-49284]">
<correction fssync "Zwakke tests uitschakelen">
<correction gnutls28 "Assertiefout bij het verifiëren van een certificaatketen met een cyclus van kruiselingse handtekeningen verhelpen [CVE-2024-0567]; beveiligingsoplossing voor een zijkanaal-timingaanval [CVE-2024-0553]">
<correction indent "Probleem met buffer onder-lezen oplossen [CVE-2024-0911]">
<correction isl "Oplossing voor gebruik op oudere CPU's">
<correction jtreg7 "Nieuw bronpakket om compilaties van openjdk-17 te ondersteunen">
<correction libdatetime-timezone-perl "Meegeleverde tijdzonegegevens bijwerken">
<correction libde265 "Oplossen van problemen met bufferoverloop [CVE-2023-49465 CVE-2023-49467 CVE-2023-49468]">
<correction libfirefox-marionette-perl "Compatibiliteit herstellen met nieuwere firefox-esr versies">
<correction libmateweather "URL van aviationweather.gov repareren">
<correction libspreadsheet-parsexlsx-perl "Mogelijke geheugenbom verhelpen [CVE-2024-22368]; reparatie voor probleem van XML externe entiteitsaanval [CVE-2024-23525]">
<correction linux "Nieuwe bovenstroomse stabiele release; ABI verhogen naar 18">
<correction linux-signed-amd64 "Nieuwe bovenstroomse stabiele release; ABI verhogen naar 18">
<correction linux-signed-arm64 "Nieuwe bovenstroomse stabiele release; ABI verhogen naar 18">
<correction linux-signed-i386 "Nieuwe bovenstroomse stabiele release; ABI verhogen naar 18">
<correction localslackirc "Autorisatie- en cookie-headers naar de websocket sturen">
<correction mariadb "Nieuwe bovenstroomse stabiele release; probleem met denial of service oplossen [CVE-2023-22084]">
<correction mate-screensaver "Geheugenlekken repareren">
<correction mate-settings-daemon "Geheugenlekken repareren; Hoge DPI-limieten versoepelen; behandeling van meervoudige rfkill-gebeurtenissen verbeteren">
<correction mate-utils "Verschillende geheugenlekken repareren">
<correction monitoring-plugins "Reparatie voor check_http plugin wanneer <q>--no-body</q> gebruikt wordt en het bovenstroomse antwoord gefragmenteerd is">
<correction needrestart "Regressie van microcodecontrole op AMD CPU's verhelpen">
<correction netplan.io "Herstelling van autopkgtests met nieuwere systemd versies">
<correction nextcloud-desktop "Herstelling voor <q>slaagt er niet in bestanden met speciale tekens zoals ':' te synchroniseren</q>; tweefactorauthenticatiemeldingen repareren">
<correction node-yarnpkg "Oplossing voor gebruik met Commander 8">
<correction onionprobe "Reparatie van initialisatie van Tor bij het gebruik van gehashte wachtwoorden">
<correction pipewire "Om geheugen vrij te geven malloc_trim() gebruiken als dat beschikbaar is">
<correction pluma "Problemen met geheugenlekken oplossen; dubbele activering van extensies repareren">
<correction postfix "Nieuwe bovenstroomse stabiele release; SMTP smokkelprobleem aanpakken [CVE-2023-51764]">
<correction proftpd-dfsg "Oplossing voor de Terrapin-aanval implementeren [CVE-2023-48795]; oplossen van probleem van lezen buiten het bereik [CVE-2023-51713]">
<correction proftpd-mod-proxy "Oplossing voor de Terrapin-aanval implementeren [CVE-2023-48795]">
<correction pypdf "Probleem met oneindige lus verhelpen [CVE-2023-36464]">
<correction pypdf2 "Probleem met oneindige lus verhelpen [CVE-2023-36464]">
<correction pypy3 "Een rpython assertiefout in de JIT vermijden als integerbereiken elkaar niet overlappen in een lus">
<correction qemu "Nieuwe bovenstroomse stabiele release; virtio-net: de vnet-header correct kopiëren bij het leegmaken van TX [CVE-2023-6693]; oplossen van probleem van null pointer dereference [CVE-2023-6683]; patch terugdraaien die regressies veroorzaakte in suspend / resume-functionaliteit">
<correction rpm "De alleen-lezen BerkeleyDB-backend inschakelen">
<correction rss-glx "Schermbeveiligingen installeren in /usr/libexec/xscreensaver; GLFinish() aanroepen vóór glXSwapBuffers()">
<correction spip "Twee problemen met cross-site scripting oplossen">
<correction swupdate "Voorkomen dat beheerdersrechten worden verkregen door ongepaste socketmodus">
<correction systemd "Nieuwe bovenstroomse stabiele release; oplossing voor probleem met ontbrekende verificatie in systemd-resolved [CVE-2023-7008]">
<correction tar "Bereikcontrole in base-256 decoder repareren [CVE-2022-48303], hantering van uitgebreide headervoorvoegsels [CVE-2023-39804]">
<correction tinyxml "Assertie-probleem oplossen [CVE-2023-34194]">
<correction tzdata "Nieuwe bovenstroomse stabiele release">
<correction usb.ids "Meegeleverde gegevenslijst bijwerken">
<correction usbutils "Reparatie voor het feit dat usb-devices niet alle apparaten afdrukt">
<correction usrmerge "Biarch-mappen opruimen wanneer ze niet nodig zijn; convert-etc-shells niet opnieuw uitvoeren op geconverteerde systemen; omgaan met aangekoppelde /lib/modules op Xen-systemen; foutenrapportage verbeteren; toevoegen van een versiegebonden tegenstrijdigheid met libc-bin, dhcpcd, libparted1.8-10 en lustre-utils">
<correction wolfssl "Beveiligingsprobleem oplossen wanneer client noch PSK noch KSE-extensies stuurde [CVE-2023-3724]">
<correction xen "Nieuwe bovenstroomse stabiele release; beveiligingsoplossingen [CVE-2023-46837 CVE-2023-46839 CVE-2023-46840]">
</table>


<h2>Beveiligingsupdates</h2>


<p>Met deze revisie worden de volgende beveiligingsupdates toegevoegd aan de
stabiele release. Het beveiligingsteam heeft voor elk van deze updates
al een advies uitgebracht:</p>

<table border=0>
<tr><th>Advies-ID</th>  <th>Pakket</th></tr>
<dsa 2023 5572 roundcube>
<dsa 2023 5573 chromium>
<dsa 2023 5574 libreoffice>
<dsa 2023 5576 xorg-server>
<dsa 2023 5577 chromium>
<dsa 2023 5578 ghostscript>
<dsa 2023 5579 freeimage>
<dsa 2023 5581 firefox-esr>
<dsa 2023 5582 thunderbird>
<dsa 2023 5583 gst-plugins-bad1.0>
<dsa 2023 5584 bluez>
<dsa 2023 5585 chromium>
<dsa 2023 5586 openssh>
<dsa 2023 5587 curl>
<dsa 2023 5588 putty>
<dsa 2023 5589 node-undici>
<dsa 2023 5590 haproxy>
<dsa 2023 5591 libssh>
<dsa 2023 5592 libspreadsheet-parseexcel-perl>
<dsa 2024 5593 linux-signed-amd64>
<dsa 2024 5593 linux-signed-arm64>
<dsa 2024 5593 linux-signed-i386>
<dsa 2024 5593 linux>
<dsa 2024 5595 chromium>
<dsa 2024 5597 exim4>
<dsa 2024 5598 chromium>
<dsa 2024 5599 phpseclib>
<dsa 2024 5600 php-phpseclib>
<dsa 2024 5601 php-phpseclib3>
<dsa 2024 5602 chromium>
<dsa 2024 5603 xorg-server>
<dsa 2024 5605 thunderbird>
<dsa 2024 5606 firefox-esr>
<dsa 2024 5607 chromium>
<dsa 2024 5608 gst-plugins-bad1.0>
<dsa 2024 5609 slurm-wlm>
<dsa 2024 5610 redis>
<dsa 2024 5611 glibc>
<dsa 2024 5612 chromium>
<dsa 2024 5613 openjdk-17>
<dsa 2024 5614 zbar>
<dsa 2024 5615 runc>
</table>



<h2>Het Debian-installatiesysteem</h2>
<p>Het installatiesysteem werd bijgewerkt om de reparaties die met deze tussenrelease in stable, de stabiele release, opgenomen werden, toe te voegen.</p>

<h2>URL's</h2>

<p>De volledige lijsten met pakketten die met deze revisie gewijzigd werden:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>De huidige stabiele distributie:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/stable/">
</div>

<p>Voorgestelde updates voor de stabiele distributie:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/proposed-updates">
</div>

<p>Informatie over de stabiele distributie (notities bij de release, errata, enz.):</p>

<div class="center">
  <a
  href="$(HOME)/releases/stable/">https://www.debian.org/releases/stable/</a>
</div>

<p>Beveiligingsaankondigingen en -informatie:</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>Over Debian</h2>

<p>Het Debian-project is een samenwerkingsverband van ontwikkelaars van vrije
software die vrijwillig tijd en moeite steken in het produceren van het
volledig vrije besturingssysteem Debian.</p>

<h2>Contactinformatie</h2>

<p>Ga voor verdere informatie naar de webpagina's van Debian op
<a href="$(HOME)/">https://www.debian.org/</a>, stuur een e-mail naar
&lt;press@debian.org&gt;, of neem contact met het release-team voor de stabiele
release op &lt;debian-release@lists.debian.org&gt;.</p>


