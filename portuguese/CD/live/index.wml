#use wml::debian::cdimage title="Imagens de instalação live"
#use wml::debian::release_info
#use wml::debian::installer
#include "$(ENGLISHDIR)/releases/images.data"
#use wml::debian::translation-check translation="7791836a42fa0b102a9f31cfc78e2c4345504388"

<p>Uma imagem de <q>instalação live</q> contém um sistema Debian que pode
inicializar sem modificar qualquer arquivo no disco rígido e também
permite a instalação do Debian a partir do conteúdo da imagem.
</p>

<p><a name="choose_live"><strong>Uma imagem live é adequada para mim?</strong></a>
Aqui estão algumas coisas a se considerar que o ajudarão a decidir.
<ul>
<li><b>Sabores:</b> As imagens live vêm em vários <q>sabores</q>, proporcionando
uma escolha de ambientes de área de trabalho (GNOME, KDE, LXDE, Xfce,
Cinnamon e MATE).

<li><b>Arquitetura:</b> Atualmente somente imagens para PC de 64 bits (amd64)
são fornecidas.

<li><b> Instalador:</b> as imagens live contêm o
amigável ao usuário final <a href="https://calamares.io">instalador Calamares</a>,
um framework de instalação independente de distribuição.

<li><b>Tamanho:</b> Cada imagem é muito menor que o conjunto completo de
imagens de DVD, mas é maior que uma mídia de instalação via rede.

<li><b>Idiomas:</b> As imagens não contém um conjunto completo de pacotes de
suporte à idiomas. Se você precisa de métodos de entrada, fontes e pacotes
adicionais para seu idioma, você precisará instalá-los depois.
</ul>

</div>

<div class="line">
<div class="item col50">
<h2 id="live-install-stable">Imagens oficiais de instalação live para a versão <q>estável (stable)</q></h2>

<p>Essas imagens são adequadas para testar um sistema Debian e depois instalá-lo
a partir da mesma mídia. Elas podem ser gravadas em pendrives e DVD-R(W).</p>
    <ul class="quicklist downlist">
      <li><a title="Baixar a ISO do Gnome live para Intel 64 bits e AMD PC"
          href="<live-images-url/>/amd64/iso-hybrid/debian-live-<current-cd-release/>-amd64-gnome.iso">Gnome Live</a></li>
      <li><a title="Baixar a ISO do Xfce live para Intel 64 bits e AMD PC"
          href="<live-images-url/>/amd64/iso-hybrid/debian-live-<current-cd-release/>-amd64-xfce.iso">Xfce Live</a></li>
      <li><a title="Baixar a ISO do KDE live para Intel 64 bits e AMD PC"
          href="<live-images-url/>/amd64/iso-hybrid/debian-live-<current-cd-release/>-amd64-kde.iso">KDE Live</a></li>
      <li><a title="Baixar outras ISOs live ISO para Intel 64 bits e AMD PC"
            href="<live-images-url/>/amd64/iso-hybrid/">Outras ISOs live</a></li>
      <li><a title="Baixar os torrents para live para Intel 64 bits e AMD PC"
          href="<live-images-url/>/amd64/bt-hybrid/">Torrents para live</a></li>
    </ul>

</div>


<div class="item col50 lsatcol">
<h2 id="live-install-testing">Imagens oficiais de instalação live para a versão <q>teste (testing)</q></h2>

<p>Essas imagens são adequadas para testar um sistema Debian e depois instalá-lo
a partir da mesma mídia. Elas podem ser gravadas em pendrives e DVD-R(W).</p>
    <ul class="quicklist downlist">
      <li>IMAGENS TESTE:</li>
      <li><a title="Baixar a ISO Gnome live para Intel 64 bits e AMD PC"
          href="<live-images-testing-url/>/amd64/iso-hybrid/debian-live-testing-amd64-gnome.iso">Gnome Live</a></li>
      <li><a title="Baixar a ISO Xfce live para Intel 64 bits e AMD PC"
          href="<live-images-testing-url/>/amd64/iso-hybrid/debian-live-testing-amd64-xfce.iso">Xfce Live</a></li>
      <li><a title="Baixar a ISO KDE live para Intel 64 bits e AMD PC"
          href="<live-images-testing-url/>/amd64/iso-hybrid/debian-live-testing-amd64-kde.iso">KDE Live</a></li>
      <li><a title="Baixar outras ISOs live ISO para Intel 64 bits e AMD PC"
            href="<live-images-testing-url/>/amd64/iso-hybrid/">Outras ISOs live</a></li>
    </ul>

</div>

</div>

<p>Para informações sobre o que são estes arquivos e como usá-los, por favor,
veja a <a href="../faq/">FAQ</a>.</p>

<p>Veja a <a href="$(HOME)/devel/debian-live">página do Projeto Debian Live</a>
para mais informações sobre os sistemas Debian Live fornecidos por essas
imagens.</p>
