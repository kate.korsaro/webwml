#use wml::debian::translation-check translation="d673db021e55bd42a14712c110ed1253cd2c8b1e"
<define-tag pagetitle>Debian 11 actualizado: publicada la versión 11.3</define-tag>
<define-tag release_date>2022-03-26</define-tag>
#use wml::debian::news

<define-tag release>11</define-tag>
<define-tag codename>bullseye</define-tag>
<define-tag revision>11.3</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>El proyecto Debian se complace en anunciar la tercera actualización de su
distribución «estable» Debian <release> (nombre en clave <q><codename></q>).
Esta versión añade, principalmente, correcciones de problemas de seguridad
junto con unos pocos ajustes para problemas graves. Los avisos de seguridad
se han publicado ya de forma independiente, y aquí hacemos referencia a ellos donde corresponde.</p>

<p>Tenga en cuenta que esta actualización no constituye una nueva versión completa de Debian
<release>, solo actualiza algunos de los paquetes incluidos. No es
necesario deshacerse de los viejos medios de instalación de <q><codename></q>. Tras la instalación de Debian,
los paquetes instalados pueden pasarse a las nuevas versiones utilizando una réplica Debian
actualizada.</p>

<p>Quienes instalen frecuentemente actualizaciones desde security.debian.org no tendrán
que actualizar muchos paquetes, y la mayoría de dichas actualizaciones están
incluidas en esta nueva versión.</p>

<p>Pronto habrá disponibles nuevas imágenes de instalación en los sitios habituales.</p>

<p>Puede actualizar una instalación existente a esta nueva versión haciendo
que el sistema de gestión de paquetes apunte a una de las muchas réplicas HTTP de Debian.
En la dirección siguiente puede encontrar el listado completo de réplicas:</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>




<h2>Corrección de fallos varios</h2>

<p>Esta actualización de la distribución «estable» añade unas pocas correcciones importantes a los paquetes siguientes:</p>

<table border=0>
<tr><th>Paquete</th>               <th>Motivo</th></tr>
<correction apache-log4j1.2 "Resuelve problemas de seguridad [CVE-2021-4104 CVE-2022-23302 CVE-2022-23305 CVE-2022-23307] eliminando el soporte de los módulos JMSSink, JDBCAppender, JMSAppender y Apache Chainsaw">
<correction apache-log4j2 "Corrige problema de ejecución de código remoto [CVE-2021-44832]">
<correction apache2 "Nueva versión del proyecto original; corrige caída debida a lectura de área aleatoria de memoria [CVE-2022-22719]; corrige problema de «contrabando» de peticiones HTTP («HTTP request smuggling») [CVE-2022-22720]; corrige problemas de escritura fuera de límites [CVE-2022-22721 CVE-2022-23943]">
<correction atftp "Corrige problema de fuga de información [CVE-2021-46671]">
<correction base-files "Actualizado para la versión 11.3">
<correction bible-kjv "Corrige error por uno («off-by-one-error») en búsquedas">
<correction chrony "Permite leer el fichero de configuración de chronyd que genera timemaster(8)">
<correction cinnamon "Corrige caída al añadir una cuenta en línea que requiere autenticación («login»)">
<correction clamav "Nueva versión «estable» del proyecto original; corrige problema de denegación de servicio [CVE-2022-20698]">
<correction cups-filters "Apparmor: permite leer del fichero de configuración «cups-browsed» en Debian Edu">
<correction dask.distributed "Corrige situación no deseada: trabajadores escuchando en interfaces públicas [CVE-2021-42343]; corrige compatibilidad con Python 3.9">
<correction debian-installer "Recompilado contra proposed-updates; actualiza la ABI del núcleo Linux a la 5.10.0-13">
<correction debian-installer-netboot-images "Recompilado contra proposed-updates">
<correction debian-ports-archive-keyring "Añade <q>Debian Ports Archive Automatic Signing Key (2023)</q>; mueve la clave para firmar de 2021 al anillo de claves eliminadas">
<correction django-allauth "Corrige el soporte de OpenID">
<correction djbdns "Incrementa el límite de datos de axfrdns, dnscache y tinydns">
<correction dpdk "Nueva versión «estable» del proyecto original">
<correction e2guardian "Corrige problema de falta de validación de certificados SSL [CVE-2021-44273]">
<correction epiphany-browser "Solución provisional de un fallo en GLib, corrigiendo caída de un proceso de la interfaz de usuario">
<correction espeak-ng "Elimina espera espuria de 50ms al tratar eventos">
<correction espeakup "debian/espeakup.service: protege espeakup ante sobrecargas del sistema">
<correction fcitx5-chinese-addons "fcitx5-table: añade dependencias con fcitx5-module-pinyinhelper y con fcitx5-module-punctuation, que faltaban">
<correction flac "Corrige problema de escritura fuera de límites [CVE-2021-0561]">
<correction freerdp2 "Inhabilita escritura de depuración en el log">
<correction galera-3 "Nueva versión del proyecto original">
<correction galera-4 "Nueva versión del proyecto original">
<correction gbonds "Usa la API del Tesoro para obtener los valores de reembolso">
<correction glewlwyd "Corrige posible elevación de privilegios">
<correction glibc "Corrige conversión errónea desde ISO-2022-JP-3 con iconv [CVE-2021-43396]; corrige problemas de desbordamiento de memoria [CVE-2022-23218 CVE-2022-23219]; corrige problema de «uso tras liberar» [CVE-2021-33574]; deja de reemplazar versiones más antiguas de /etc/nsswitch.conf; simplifica la comprobación de versiones del núcleo soportadas, puesto que los núcleos 2.x ya no lo están; soporta la instalación con núcleos con número de versión mayor de 255">
<correction glx-alternatives "Tras la configuración inicial de los desvíos («diversions»), instala una alternativa mínima a los ficheros desviados de forma que no falten librerías hasta que glx-alternative-mesa procese sus disparadores">
<correction gnupg2 "scd: corrige controlador CCID para SCM SPR332/SPR532; evita que el generador interactúe con la red, lo que podría dar lugar a cuelgues">
<correction gnuplot "Corrige división por cero [CVE-2021-44917]">
<correction golang-1.15 "Corrige IsOnCurve para valores big.Int que no son coordenadas válidas [CVE-2022-23806]; math/big: evita gran consumo de memoria en Rat.SetString [CVE-2022-23772]; cmd/go: impide que las ramas se materialicen en versiones [CVE-2022-23773]; corrige agotamiento de pila al compilar expresiones con muchos niveles de anidamiento [CVE-2022-24921]">
<correction golang-github-containers-common "Actualiza soporte de seccomp para habilitar el uso de versiones más recientes del núcleo">
<correction golang-github-opencontainers-specs "Actualiza soporte de seccomp para habilitar el uso de versiones más recientes del núcleo">
<correction gtk+3.0 "Corrige ausencia de resultados de búsqueda con NFS; evita el bloqueo del portapapeles de Wayland en ciertos casos marginales; mejora la impresión en impresoras encontradas por medio de mDNS">
<correction heartbeat "Corrige la creación de /run/heartbeat en sistemas que usan systemd">
<correction htmldoc "Corrige problema de lectura fuera de límites [CVE-2022-0534]">
<correction installation-guide "Actualiza documentación y traducciones">
<correction intel-microcode "Actualiza el microcódigo incluido; mitiga algunos problemas de seguridad [CVE-2020-8694 CVE-2020-8695 CVE-2021-0127 CVE-2021-0145 CVE-2021-0146 CVE-2021-33120]">
<correction ldap2zone "Usa <q>mktemp</q> en lugar del obsoleto <q>tempfile</q>, evitando mensajes de aviso">
<correction lemonldap-ng "Corrige proceso de autorización en extensiones («plugins») de prueba de contraseñas [CVE-2021-40874]">
<correction libarchive "Corrige la extracción de enlaces duros a enlaces simbólicos; corrige el tratamiento de las ACL de los enlaces simbólicos [CVE-2021-23177]; no sigue nunca enlaces simbólicos al establecer indicadores de ficheros [CVE-2021-31566]">
<correction libdatetime-timezone-perl "Actualiza los datos incluidos">
<correction libgdal-grass "Recompilado contra grass 7.8.5-1+deb11u1">
<correction libpod "Actualiza soporte de seccomp para habilitar el uso de versiones más recientes del núcleo">
<correction libxml2 "Corrige problema de «uso tras liberar» [CVE-2022-23308]">
<correction linux "Nueva versión «estable» del proyecto original; [rt] actualiza a la 5.10.106-rt64; incrementa la ABI a la 13">
<correction linux-signed-amd64 "Nueva versión «estable» del proyecto original; [rt] actualiza a la 5.10.106-rt64; incrementa la ABI a la 13">
<correction linux-signed-arm64 "Nueva versión «estable» del proyecto original; [rt] actualiza a la 5.10.106-rt64; incrementa la ABI a la 13">
<correction linux-signed-i386 "Nueva versión «estable» del proyecto original; [rt] actualiza a la 5.10.106-rt64; incrementa la ABI a la 13">
<correction mariadb-10.5 "Nueva versión del proyecto original; correcciones de seguridad [CVE-2021-35604 CVE-2021-46659 CVE-2021-46661 CVE-2021-46662 CVE-2021-46663 CVE-2021-46664 CVE-2021-46665 CVE-2021-46667 CVE-2021-46668 CVE-2022-24048 CVE-2022-24050 CVE-2022-24051 CVE-2022-24052]">
<correction mpich "Añade Rompe: sobre versiones más antiguas de libmpich1.0-dev, resolviendo algunos problemas en actualizaciones">
<correction mujs "Corrige problema de desbordamiento de memoria [CVE-2021-45005]">
<correction mutter "Retroadapta varias correcciones de la rama «estable» del proyecto original">
<correction node-cached-path-relative "Corrige problema de contaminación de prototipo [CVE-2021-23518]">
<correction node-fetch "No reenvía cabeceras seguras a dominios de terceros [CVE-2022-0235]">
<correction node-follow-redirects "No envía la cabecera Cookie entre dominios [CVE-2022-0155]; no envía cabeceras confidenciales entre esquemas [CVE-2022-0536]">
<correction node-markdown-it "Corrige problema de denegación de servicio relacionado con expresiones regulares [CVE-2022-21670]">
<correction node-nth-check "Corrige problema de denegación de servicio relacionado con expresiones regulares [CVE-2021-3803]">
<correction node-prismjs "Codifica las marcas en la salida de la línea de órdenes para evitar su evaluación («escape») [CVE-2022-23647]; actualiza ficheros minimizados para asegurar que se resuelve un problema de denegación de servicio relacionado con expresiones regurales [CVE-2021-3801]">
<correction node-trim-newlines "Corrige problema de denegación de servicio relacionado con expresiones regulares [CVE-2021-33623]">
<correction nvidia-cuda-toolkit "cuda-gdb: inhabilita soporte de python, que no es funcional y causa violaciones de acceso; usa un snapshot de openjdk-8-jre (8u312-b07-1)">
<correction nvidia-graphics-drivers-tesla-450 "Nueva versión del proyecto original; corrige problemas de denegación de servicio [CVE-2022-21813 CVE-2022-21814]; nvidia-kernel-support: proporciona /etc/modprobe.d/nvidia-options.conf como plantilla">
<correction nvidia-modprobe "Nueva versión del proyecto original">
<correction openboard "Corrige el icono de la aplicación">
<correction openssl "Nueva versión del proyecto original; corrige autenticación de puntero en armv8">
<correction openvswitch "Corrige problema de «uso tras liberar» [CVE-2021-36980]; corrige instalación de libofproto">
<correction ostree "Corrige compatibilidad con eCryptFS; evita recursión infinita al recuperarse de ciertos errores; marca commits como parciales antes de descargar; corrige un fallo de aserción al usar una compilación local o retroadaptada («backport») de GLib &gt;= 2.71; corrige la capacidad de leer contenido OSTree desde rutas que contienen caracteres no URI (como contrabarras) o no ASCII">
<correction pdb2pqr "Corrige compatibilidad de propka con Python 3.8 o superior">
<correction php-crypt-gpg "Evita el paso de opciones adicionales a GPG [CVE-2022-24953]">
<correction php-laravel-framework "Corrige problema de ejecución de scripts entre sitios («cross-site scripting») [CVE-2021-43808], bloquea la subida de contenido ejecutable [CVE-2021-43617]">
<correction phpliteadmin "Corrige problema de ejecución de scripts entre sitios («cross-site scripting») [CVE-2021-46709]">
<correction prips "Corrige ciclo infinito si un rango llega a 255.255.255.255; corrige la salida CIDR con direcciones que se diferencian en el primer bit">
<correction pypy3 "Corrige fallos de compilación eliminando un #endif superfluo de import.h">
<correction python-django "Corrige problema de denegación de servicio [CVE-2021-45115], problema de revelación de información [CVE-2021-45116] y problema de escalado de directorios [CVE-2021-45452]; corrige un error cerca del tratamiento de RequestSite/get_current_site() debido a un import circular">
<correction python-pip "Evita una condición de carrera al usar dependencias zip-importadas">
<correction rust-cbindgen "Nueva versión «estable» del proyecto original para soportar compilaciones de versiones más recientes de firefox-esr y de thunderbird">
<correction s390-dasd "Deja de pasar a dasdfmt la opción obsoleta («deprecated») -f">
<correction schleuder "Restaura la funcionalidad migrando valores booleanos a enteros si se usa el adaptador de conexión a SQLite3 ActiveRecord">
<correction sphinx-bootstrap-theme "Corrige la funcionalidad de búsqueda">
<correction spip "Corrige varios problemas de ejecución de scripts entre sitios («cross-site scripting»)">
<correction symfony "Corrige problema de inyección de CSV [CVE-2021-41270]">
<correction systemd "Corrige recursión no controlada en systemd-tmpfiles [CVE-2021-3997]; degrada systemd-timesyncd de Depende a Recomienda, eliminando una dependencia cíclica; corrige fallo al hacer un montaje «bind» de un directorio en un contenedor usando machinectl; corrige regresión en udev que da lugar a retrasos largos al procesar particiones con la misma etiqueta; corrige una regresión cuando se usa systemd-networkd en un contenedor LXD sin privilegios">
<correction sysvinit "Corrige el análisis sintáctico de <q>shutdown +0</q>; aclara que shutdown no devuelve el control cuando es llamado con un <q>tiempo</q>">
<correction tasksel "Instala CUPS para todas las tareas *-desktop, puesto que ya no existe task-print-service">
<correction usb.ids "Actualiza los datos incluidos">
<correction weechat "Corrige problema de denegación de servicio [CVE-2021-40516]">
<correction wolfssl "Corrige varios problemas relacionados con el tratamiento del OCSP [CVE-2021-3336 CVE-2021-37155 CVE-2021-38597] y el soporte de TLS1.3 [CVE-2021-44718 CVE-2022-25638 CVE-2022-25640]">
<correction xserver-xorg-video-intel "Corrige caída por SIGILL en las CPU sin SSE2">
<correction xterm "Corrige problema de desbordamiento de memoria [CVE-2022-24130]">
<correction zziplib "Corrige problema de denegación de servicio [CVE-2020-18442]">
</table>


<h2>Actualizaciones de seguridad</h2>


<p>Esta versión añade las siguientes actualizaciones de seguridad a la distribución «estable».
El equipo de seguridad ya ha publicado un aviso para cada una de estas
actualizaciones:</p>

<table border=0>
<tr><th>ID del aviso</th>  <th>Paquete</th></tr>
<dsa 2021 5000 openjdk-11>
<dsa 2021 5001 redis>
<dsa 2021 5012 openjdk-17>
<dsa 2021 5021 mediawiki>
<dsa 2021 5023 modsecurity-apache>
<dsa 2021 5024 apache-log4j2>
<dsa 2021 5025 tang>
<dsa 2021 5027 xorg-server>
<dsa 2021 5028 spip>
<dsa 2021 5029 sogo>
<dsa 2021 5030 webkit2gtk>
<dsa 2021 5031 wpewebkit>
<dsa 2021 5033 fort-validator>
<dsa 2022 5035 apache2>
<dsa 2022 5037 roundcube>
<dsa 2022 5038 ghostscript>
<dsa 2022 5039 wordpress>
<dsa 2022 5040 lighttpd>
<dsa 2022 5041 cfrpki>
<dsa 2022 5042 epiphany-browser>
<dsa 2022 5043 lxml>
<dsa 2022 5046 chromium>
<dsa 2022 5047 prosody>
<dsa 2022 5048 libreswan>
<dsa 2022 5049 flatpak-builder>
<dsa 2022 5049 flatpak>
<dsa 2022 5050 linux-signed-amd64>
<dsa 2022 5050 linux-signed-arm64>
<dsa 2022 5050 linux-signed-i386>
<dsa 2022 5050 linux>
<dsa 2022 5051 aide>
<dsa 2022 5052 usbview>
<dsa 2022 5053 pillow>
<dsa 2022 5054 chromium>
<dsa 2022 5055 util-linux>
<dsa 2022 5056 strongswan>
<dsa 2022 5057 openjdk-11>
<dsa 2022 5058 openjdk-17>
<dsa 2022 5059 policykit-1>
<dsa 2022 5060 webkit2gtk>
<dsa 2022 5061 wpewebkit>
<dsa 2022 5062 nss>
<dsa 2022 5063 uriparser>
<dsa 2022 5064 python-nbxmpp>
<dsa 2022 5065 ipython>
<dsa 2022 5067 ruby2.7>
<dsa 2022 5068 chromium>
<dsa 2022 5070 cryptsetup>
<dsa 2022 5071 samba>
<dsa 2022 5072 debian-edu-config>
<dsa 2022 5073 expat>
<dsa 2022 5075 minetest>
<dsa 2022 5076 h2database>
<dsa 2022 5077 librecad>
<dsa 2022 5078 zsh>
<dsa 2022 5079 chromium>
<dsa 2022 5080 snapd>
<dsa 2022 5081 redis>
<dsa 2022 5082 php7.4>
<dsa 2022 5083 webkit2gtk>
<dsa 2022 5084 wpewebkit>
<dsa 2022 5085 expat>
<dsa 2022 5087 cyrus-sasl2>
<dsa 2022 5088 varnish>
<dsa 2022 5089 chromium>
<dsa 2022 5091 containerd>
<dsa 2022 5092 linux-signed-amd64>
<dsa 2022 5092 linux-signed-arm64>
<dsa 2022 5092 linux-signed-i386>
<dsa 2022 5092 linux>
<dsa 2022 5093 spip>
<dsa 2022 5095 linux-signed-amd64>
<dsa 2022 5095 linux-signed-arm64>
<dsa 2022 5095 linux-signed-i386>
<dsa 2022 5095 linux>
<dsa 2022 5098 tryton-server>
<dsa 2022 5099 tryton-proteus>
<dsa 2022 5100 nbd>
<dsa 2022 5101 libphp-adodb>
<dsa 2022 5102 haproxy>
<dsa 2022 5103 openssl>
<dsa 2022 5104 chromium>
<dsa 2022 5105 bind9>
</table>


<h2>Paquetes eliminados</h2>

<p>Se han eliminado los paquetes listados a continuación por circunstancias ajenas a nosotros:</p>

<table border=0>
<tr><th>Paquete</th>               <th>Motivo</th></tr>
<correction angular-maven-plugin "Ya no es útil">
<correction minify-maven-plugin "Ya no es útil">

</table>

<h2>Instalador de Debian</h2>
<p>Se ha actualizado el instalador para incluir las correcciones incorporadas
por esta nueva versión en la distribución «estable».</p>

<h2>URL</h2>

<p>Las listas completas de paquetes que han cambiado en esta versión:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>La distribución «estable» actual:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/stable/">
</div>

<p>Actualizaciones propuestas a la distribución «estable»:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/proposed-updates">
</div>

<p>Información sobre la distribución «estable» (notas de publicación, erratas, etc.):</p>

<div class="center">
  <a
  href="$(HOME)/releases/stable/">https://www.debian.org/releases/stable/</a>
</div>

<p>Información y anuncios de seguridad:</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>Acerca de Debian</h2>

<p>El proyecto Debian es una asociación de desarrolladores de software libre que
aportan de forma voluntaria su tiempo y esfuerzo para producir el sistema operativo
Debian, un sistema operativo completamente libre.</p>

<h2>Información de contacto</h2>

<p>Para más información, visite las páginas web de Debian en
<a href="$(HOME)/">https://www.debian.org/</a>, envíe un correo electrónico a
&lt;press@debian.org&gt; o contacte con el equipo responsable de la publicación en
&lt;debian-release@lists.debian.org&gt;.</p>


