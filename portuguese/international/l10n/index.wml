#use wml::debian::template title="Estatísticas centrais de traduções Debian"
#use wml::debian::translation-check translation="dd24a93d7caa398032ec51664b6a425be2a22278"

<p>Estas páginas exibem o número de pacotes no Debian que estão prontos
para serem traduzidos e quantos pacotes já estão traduzidos.</p>

<p>Perceba que esse processo é somente uma parte da internacionalização
(a qual é abreviada como <i>i18n</i>, pois existem 18 letras entre o "i"
e o "n") e localização (ou <i>l10n</i>). i18n define a infraestrutura
de globalização e l10n adiciona uma camada específica para cada idioma
e país dentro dessa infraestrutura. Por este motivo, as tarefas a serem feitas
para l10n dependem do que foi alcançado pela i18n. Por exemplo, caso sua i18n
lhe permita somente mudar o texto de uma mensagem, a l10n significa meramente
traduzir esse texto. Caso a i18n lhe permita mudar a maneira como a data
é impressa, você pode expressar o formato de data de acordo com o local do
mundo em que você vive. Caso a i18n lhe permita mudar a codificação de
caracteres, a l10n é o ato de definir a codificação de caracteres que você
precisa para um dado idioma. Por favor, note que o suporte a codificação,
incluindo multibyte, largura dupla (<q>doublewidth</q>), combinagem (<q>combining</q>),
bi-direção (<q> bi-direction</q>) e assim por diante, é um pré-requisito para todas as
outras partes da i18n e l10n, incluindo tradução, para alguns idiomas
(geralmente não Europeus).</p>

<p>l10n e i18n estão ligadas, mas as dificuldades relacionadas a cada uma
delas são bem diferentes. Não é realmente difícil permitir que o programa
mude o texto a ser exibido baseado em configurações do(a) usuário(a), mas
traduzir as mensagens é algo que consome bastante tempo. Por outro lado, definir
a codificação de caracteres é trivial, mas adaptar o código para usar diversas
codificações de caracteres é um problema realmente difícil.</p>

<p>Aqui você pode navegar por estatísticas sobre a l10n do Debian:</p>

<ul>
 <li>Estado da <q>l10n</q> em arquivos PO, ou seja, o quão bem os pacotes estão
     traduzidos:
 <ul>
  <li><a href="https://www.debian.org/international/l10n/po/pt_BR">Página do pt_BR</a></li>
  <li><a href="po/">Lista de idiomas</a></li>
  <li><a href="po/rank">Ranking entre idiomas</a></li>
 </ul></li>
 <li>Estado da <q>l10n</q> em arquivos PO de templates <b>debconf</b> gerenciados via
     gettext:
 <ul>
  <li><a href="https://www.debian.org/international/l10n/po-debconf/pt_BR">Página do pt_BR</a></li>
  <li><a href="po-debconf/">Lista de idiomas</a></li>
  <li><a href="po-debconf/rank">Ranking entre idiomas</a></li>
  <li><a href="po-debconf/pot">Arquivos originais</a></li>
 </ul></li>
 <li>Estado da documentação (<b>man</b>, etc) <q>l10n</q> em arquivos PO gerenciados com po4a:
 <ul>
  <li><a href="https://www.debian.org/international/l10n/po4a/pt_BR">Página do pt_BR</a></li>
  <li><a href="po4a/">Lista de idiomas</a></li>
  <li><a href="po4a/rank">Ranking entre idiomas</a></li>
 </ul></li>
 <li><a href="https://ddtp.debian.org/">Estado da <q>l10n</q> das descrições dos
     pacotes Debian.</a></li>
 <ul>
  <li><a href="https://ddtp.debian.org/ddtss/index.cgi/pt_BR">DDTSS para pt_BR</a></li>
 </ul></li>
 <li><a href="$(HOME)/devel/website/stats/">Estatística das traduções do web
     site Debian</a></li>
 <li>Estatística das traduções das notas de lançamento: <a href="$(HOME)/releases/stable/statistics.html">para a estável (stable)</a>,
     <a href="$(HOME)/releases/testing/statistics.html">para a teste (testing)</a></li>
 <li><a href="https://d-i.debian.org/l10n-stats/translation-status.html">\
     Página de status da tradução do instalador Debian <q>Debian-Installer</q></a></li>
</ul>
