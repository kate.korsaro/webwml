#use wml::debian::template title="Informations sur la publication &ldquo;Wheezy&rdquo; de Debian"
#include "$(ENGLISHDIR)/releases/info"
#use wml::debian::translation-check translation="fc0a147ef1585beaa5ef80938ca7e595d27fa365" maintainer="Jean-Paul Guillonneau"



<p>La version de Debian <current_release_wheezy> a été publiée le <a href="$(HOME)/News/<current_release_newsurl_wheezy/>"><current_release_date_wheezy></a>.
<ifneq "7.0" "<current_release>"
  "La version 7.0 a été initialement publiée le <:=spokendate('2013-05-04'):>."
/>
Cette version comprend de nombreuses modifications décrites dans notre
<a href="$(HOME)/News/2013/20130504">communiqué de presse</a> et les
<a href="releasenotes">notes de publication</a>.</p>

<p><strong>Debian 7 a été remplacée par
<a href="../jessie/">Debian 8 (<q>Jessie</q>)</a>.
# Security updates have been discontinued as of <:=spokendate('XXXXXXXXXXX'):>.
</strong></p>

<p><strong>Wheezy a bénéficié de la prise en charge à long terme (LTS) jusqu’à
la fin mai 2018. Cette prise en charge était limitée aux architectures i386,
amd64, armel et armhf.
Pour plus d’informations, veuillez consulter la <a
href="https://wiki.debian.org/LTS">section LTS du wiki de Debian</a>.
</strong></p>

<p>
Pour obtenir et installer Debian, veuillez vous reporter à la page des
<a href="debian-installer/">informations d'installation</a> et au <a
href="installmanual">guide d'installation</a>. Pour mettre à niveau à
partir d'une ancienne version de Debian, veuillez vous reporter aux
instructions des <a href="releasenotes">notes de publication</a>.

<p>Les architectures suivantes sont prises en charge par cette version&nbsp;:</p>



<ul>
<li><a href="../../ports/amd64/">PC 64 bits (amd64)</a>
<li><a href="../../ports/i386/">PC 32 bits (i386)</a>
<li><a href="../../ports/armel/">EABI ARM (armel)</a>
<li><a href="../../ports/powerpc/">PowerPC</a>
<li><a href="../../ports/armhf/">ARM avec unité de calcul flottant (armhf)</a>
<li><a href="../../ports/sparc/">SPARC</a>
<li><a href="../../ports/kfreebsd-amd64/">kFreeBSD PC 64 bits (amd64)</a>
<li><a href="../../ports/ia64/">Intel Itanium IA-64</a>
<li><a href="../../ports/mipsel/">MIPS (petit-boutiste)</a>
<li><a href="../../ports/kfreebsd-i386/">kFreeBSD PC 32 bits (i386)</a>
<li><a href="../../ports/s390/">IBM S/390</a>
<li><a href="../../ports/mips/">MIPS (gros-boutiste)</a>
<li><a href="../../ports/s390x/">IBM System z</a>
</ul>

<p>
Contrairement à nos souhaits, certains problèmes pourraient toujours exister
dans cette version, même si elle est déclarée <em>stable</em>. Nous avons
réalisé <a href="errata">une liste des principaux problèmes connus</a>, et vous
pouvez toujours nous <a href="../reportingbugs">signaler d'autres problèmes</a>.
</p>
