#use wml::debian::template title="Informations sur la publication « Buster » de Debian"

#include "$(ENGLISHDIR)/releases/info"
#use wml::debian::translation-check translation="014a50a881a72ff5b9866da7ebf1131eb84ce57b" maintainer="Jean-Paul Guillonneau"

<p>La version de Debian <current_release_buster> a été publiée le
<a href="$(HOME)/News/<current_release_newsurl_buster/>"><current_release_date_buster></a>.
<ifneq "10.0" "<current_release>"
  "Debian 10.0 a été initialement publiée le <:=spokendate('2019-07-06'):>."
/>
Cette version comprend de nombreuses modifications décrites dans le
<a href="$(HOME)/News/2019/20190706">communiqué de presse</a> et les
<a href="releasenotes">notes de publication</a>.</p>

<p><strong>Debian 10 a été remplacée par
<a href="../bullseye/">Debian 11 (<q>Bullseye</q>)</a>.
La prise en charge des mises à jour de sécurité a cessé depuis le <:=spokendate('2022-06-30'):>.
</strong></p>

<p><strong>Cependant, Buster a bénéficié de la prise en charge à long terme (LTS)
jusqu’au 30 juin 2024. Cette prise en charge était limitée aux architectures i386,
amd64, armel, armhf et arm64.
Toutes les autres architectures n’étaient plus prises en charge dans Buster.
Pour plus d’informations, veuillez consulter la <a
href="https://wiki.debian.org/LTS">section LTS du wiki de Debian</a>.
</strong></p>

<p>Des parties tierces proposent une prise en charge payante pour Buster,
jusqu’à <:=spokendate('2029-06-30'):>. Consulter :
<a href="https://wiki.debian.org/LTS/Extended">prise en charge à long terme
étendue</a>.
</p>

<p> Pour mettre à niveau à partir d'une ancienne version de Debian, veuillez
vous reporter aux instructions des
<a href="releasenotes">notes de publication</a>.</p>

<p>Les architectures suivantes étaient prises en charge par LTS :</p>
<ul>
<li><a href="../../ports/amd64/">PC 64 bits (amd64)</a>
<li><a href="../../ports/i386/">PC 32 bits (i386)</a>
<li><a href="../../ports/armhf/">ARM avec unité de calcul flottant (armhf)</a>
<li><a href="../../ports/arm64/">ARM 64 bits (AArch64)</a>
</ul>
<p>Les architectures suivantes étaient gérées par la version initiale de Buster :</p>
<ul>
<li><a href="../../ports/amd64/">PC 64 bits (amd64)</a>
<li><a href="../../ports/arm64/">ARM64 bits (AArch64)</a>
<li><a href="../../ports/armel/">ARM EABI (armel)</a>
<li><a href="../../ports/armhf/">ARM avec unité de calcul flottant (armhf)</a>
<li><a href="../../ports/i386/">PC 32 bits (i386)</a>
<li><a href="../../ports/mips/">MIPS (grand boutiste)</a>
<li><a href="../../ports/mipsel/">MIPS (petit boutiste)</a>
<li><a href="../../ports/mips64el/">MIPS 64 bits (petit boutiste)</a>
<li><a href="../../ports/ppc64el/">Processeurs POWER</a>
<li><a href="../../ports/s390x/">IBM System z</a>
</ul>

<p>
Contrairement à nos souhaits, certains problèmes pourraient toujours exister
dans cette version, même si elle est déclarée <em>stable</em>. Nous avons
réalisé <a href="errata">une liste des principaux problèmes connus</a>, et vous
pouvez toujours nous <a href="../reportingbugs">signaler d'autres problèmes</a>.
</p>
