#use wml::debian::cdimage title="Изтегляне на образи на дискове с Дебиан с BitTorrent" BARETITLE=true
#use wml::debian::toc
#include "$(ENGLISHDIR)/releases/info"
#use wml::debian::installer
#include "$(ENGLISHDIR)/releases/images.data"
#include "$(ENGLISHDIR)/devel/debian-installer/images.data"
#use wml::debian::translation-check translation="f917b9adf4a1c15cca8405e010043d380e4b1b83"

<p><a href="https://bg.wikipedia.org/wiki/BitTorrent">BitTorrent</a> е децентрализирана система за изтегляне на файлове, оптимизирана за голям брой теглещи. Тя натоварва минимално нашите сървъри, защото клиентите си споделят свалените части от файловете, което разпределя натоварването по цялата мрежа и прави възможно тегленето с максимална скорост.
</p>
<div class="tip">
<p><strong>Първият</strong> диск на Дебиан съдържа всичко необходимо за
инсталиране на стандартна система.<br />
За да избегнете ненужен трафик, <strong>не</strong> теглете другите дискове освен ако сте сигурни, че ви трябват.</p>
</div>

<p>
За да теглите дискове с Дебиан по този начин е нужен клиент за BitTorrent.
В дистрибуцията на Дебиан са включени следните клиенти:
<a href="https://packages.debian.org/bittornado">BitTornado</a>,
<a href="https://packages.debian.org/ktorrent">KTorrent</a> и оригиналните
инструменти <a href="https://packages.debian.org/bittorrent">BitTorrent</a>.
За други операционни системи вижте официалните страници на <a
href="http://www.bittornado.com/download.html">BitTornado</a> и <a
href="https://www.bittorrent.com/download">BitTorrent</a>.
</p>

<h3>Официални торенти за стабилното издание</h3>

<div class="line">
<div class="item col50">
<p><strong>Компактдиск</strong></p>
  <stable-full-cd-torrent>
</div>
<div class="item col50 lastcol">
<p><strong>DVD</strong></p>
  <stable-full-dvd-torrent>
</div>
<div class="clear"></div>
</div>


<p>Уверете се че сте прегледали документацията преди да инсталирате.
<strong>Най-важната информация</strong> е събрана в бързия преглед на процеса
на инсталиране <a
href="$(HOME)/releases/stable/i386/apa">Как да инсталираме</a>.
Друга полезна документация: </p>
<ul>
<li><a href="$(HOME)/releases/stable/installmanual">Ръководство за
    инсталиране</a>, подробни инструкции за инсталиране</li>
<li><a href="https://wiki.debian.org/DebianInstaller">Документация на инсталатора
    на Дебиан</a>. Включва и отговори на често задавани въпроси</li>
<li><a href="$(HOME)/releases/stable/debian-installer/#errata">Списък с
    известни проблеми</a> с процеса на инсталиране</li>
</ul>

# <h3>Официални торенти за <q>тестовата</q> дистрибуция</h3>
# 
# <ul>
# 
#   <li><strong>Компактдиск</strong>:<br />
#   <full-cd-torrent>
#   </li>
# 
#   <li><strong>DVD</strong>:<br />
#   <full-dvd-torrent>
#   </li>
# 
# </ul>

<p>
Ако имате възможност, след като сте изтеглили нужните ви файлове, не
прекъсвайте програмата за теглене за да помогнете на други желаещи да ги изтеглят
по-бързо.
</p>














<div id="firmware_nonfree" class="important">
<p>
Ако някои от хардуерните компоненти на вашия компютър <strong>изискват зареждането на затворен фърмуер</strong>, може да използвате <a href="https://cdimage.debian.org/cdimage/unofficial/non-free/firmware/stable/current/">комплектите с популярни пакети с фърмуер</a> или да изтеглите <strong>неофициален</strong> образ, включващ <strong>затворен</strong> фърмуер. Инструкциите за използването на комплектите с фърмуер и обща информация относно зареждането на фърмуер по време на инсталацията има в <a href="../releases/stable/amd64/ch06s04">Ръководството за инсталиране</a>. </p> <p>
<a href="https://cdimage.debian.org/cdimage/unofficial/non-free/cd-including-firmware/current/">неофициални образи за инсталиране на стабилното издание с включен фърмуер</a>
</p>
</div>
