# SOME DESCRIPTIVE TITLE.
# Copyright ©
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
msgid ""
msgstr ""
"Project-Id-Version: Debian webwml organization\n"
"POT-Creation-Date: \n"
"PO-Revision-Date: 2016-09-20 12:30+0100\n"
"Last-Translator: Automatically generated\n"
"Language-Team: none\n"
"Language: ro\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.8.8\n"

#: ../../english/template/debian/stats_tags.wml:6
msgid "Debian web site translation statistics"
msgstr "Statistici cu privire la traducerea site-ului Debian"

#: ../../english/template/debian/stats_tags.wml:10
msgid "There are %d pages to translate."
msgstr "Mai sunt %d pagini de tradus."

#: ../../english/template/debian/stats_tags.wml:14
msgid "There are %d bytes to translate."
msgstr "Mai sunt %d bytes de tradus."

#: ../../english/template/debian/stats_tags.wml:18
msgid "There are %d strings to translate."
msgstr "Mai sunt %d șiruri de caractere de tradus."

#: ../../stattrans.pl:282 ../../stattrans.pl:495
msgid "Wrong translation version"
msgstr "Versiune greșită a traducerii"

#: ../../stattrans.pl:284
msgid "This translation is too out of date"
msgstr "Această traducere este învechită."

#: ../../stattrans.pl:286
msgid "The original is newer than this translation"
msgstr "Versiunea originală este mai nouă decât traducerea."

#: ../../stattrans.pl:290 ../../stattrans.pl:495
msgid "The original no longer exists"
msgstr "Versiunea originală nu mai există."

#: ../../stattrans.pl:474
msgid "hit count N/A"
msgstr "număr de clicuri N/A"

#: ../../stattrans.pl:474
msgid "hits"
msgstr "clicuri"

#: ../../stattrans.pl:600 ../../stattrans.pl:739
msgid "Created with <transstatslink>"
msgstr ""

#: ../../stattrans.pl:605
msgid "Translation summary for"
msgstr "Rezumatul traducerii pentru"

#: ../../stattrans.pl:608 ../../stattrans.pl:763 ../../stattrans.pl:809
#: ../../stattrans.pl:852
msgid "Not translated"
msgstr "Netradus"

#: ../../stattrans.pl:608 ../../stattrans.pl:762 ../../stattrans.pl:808
msgid "Outdated"
msgstr "Învechit"

#: ../../stattrans.pl:608
msgid "Translated"
msgstr "Tradus"

#: ../../stattrans.pl:608 ../../stattrans.pl:687 ../../stattrans.pl:761
#: ../../stattrans.pl:807 ../../stattrans.pl:850
msgid "Up to date"
msgstr "La zi"

#: ../../stattrans.pl:609 ../../stattrans.pl:610 ../../stattrans.pl:611
#: ../../stattrans.pl:612
msgid "files"
msgstr "fișiere"

#: ../../stattrans.pl:615 ../../stattrans.pl:616 ../../stattrans.pl:617
#: ../../stattrans.pl:618
msgid "bytes"
msgstr "bytes"

#: ../../stattrans.pl:625
msgid ""
"Note: the lists of pages are sorted by popularity. Hover over the page name "
"to see the number of hits."
msgstr ""
"Notă: aceste liste de pagini sunt ordonate după popularitate. Pune cursorul "
"pe numele paginii ca să vezi numărul de clicuri,"

#: ../../stattrans.pl:631
msgid "Outdated translations"
msgstr "Traduceri învechite"

#: ../../stattrans.pl:633 ../../stattrans.pl:686
msgid "File"
msgstr "Fișier"

#: ../../stattrans.pl:635
msgid "Diff"
msgstr "Diferențe"

#: ../../stattrans.pl:637
msgid "Comment"
msgstr "Comentariu"

#: ../../stattrans.pl:638
msgid "Git command line"
msgstr ""

#: ../../stattrans.pl:640
msgid "Log"
msgstr "Log"

#: ../../stattrans.pl:641
msgid "Translation"
msgstr "Traducere"

#: ../../stattrans.pl:642
msgid "Maintainer"
msgstr "Menținător"

#: ../../stattrans.pl:644
msgid "Status"
msgstr "Stare"

#: ../../stattrans.pl:645
msgid "Translator"
msgstr "Traducător"

#: ../../stattrans.pl:646
msgid "Date"
msgstr "Dată"

#: ../../stattrans.pl:653
msgid "General pages not translated"
msgstr "Pagini generale netraduse"

#: ../../stattrans.pl:654
msgid "Untranslated general pages"
msgstr "Pagini generale netraduse"

#: ../../stattrans.pl:659
msgid "News items not translated"
msgstr "Știri netraduse"

#: ../../stattrans.pl:660
msgid "Untranslated news items"
msgstr "Știri netraduse"

#: ../../stattrans.pl:665
msgid "Consultant/user pages not translated"
msgstr "Pagini de consultanță/ pentru utilizatori netradus"

#: ../../stattrans.pl:666
msgid "Untranslated consultant/user pages"
msgstr "Pagini de consultanță/ pentru utilizatori netraduse"

#: ../../stattrans.pl:671
msgid "International pages not translated"
msgstr "Pagini internaționale netraduse"

#: ../../stattrans.pl:672
msgid "Untranslated international pages"
msgstr "Pagini internaționale netraduse"

#: ../../stattrans.pl:677
msgid "Translated pages (up-to-date)"
msgstr "Pagini traduse (la zi)"

#: ../../stattrans.pl:684 ../../stattrans.pl:834
msgid "Translated templates (PO files)"
msgstr "Tipare de tradus (fișiere PO)"

#: ../../stattrans.pl:685 ../../stattrans.pl:837
msgid "PO Translation Statistics"
msgstr "Evidență a traducerilor PO"

#: ../../stattrans.pl:688 ../../stattrans.pl:851
msgid "Fuzzy"
msgstr "Fuzzy"

#: ../../stattrans.pl:689
msgid "Untranslated"
msgstr "Netraduse"

#: ../../stattrans.pl:690
msgid "Total"
msgstr "Total"

#: ../../stattrans.pl:707
msgid "Total:"
msgstr "Total:"

#: ../../stattrans.pl:741
msgid "Translated web pages"
msgstr "Pagini web traduse"

#: ../../stattrans.pl:744
msgid "Translation Statistics by Page Count"
msgstr "Evidență a traducerilor după numarul de pagini"

#: ../../stattrans.pl:759 ../../stattrans.pl:805 ../../stattrans.pl:849
msgid "Language"
msgstr "Limbi"

#: ../../stattrans.pl:760 ../../stattrans.pl:806
msgid "Translations"
msgstr "Traduceri"

#: ../../stattrans.pl:787
msgid "Translated web pages (by size)"
msgstr "Pagini web traduse (ordonate după marime)"

#: ../../stattrans.pl:790
msgid "Translation Statistics by Page Size"
msgstr "Evidență a traducerilor după mărimea paginilor"

#~ msgid "Click to fetch diffstat data"
#~ msgstr "Clic aici ca să afli informații despre modificare"

#~ msgid "Colored diff"
#~ msgstr "Diferențe în culoare"

#, fuzzy
#~| msgid "Colored diff"
#~ msgid "Commit diff"
#~ msgstr "Diferențe în culoare"

#~ msgid "Created with"
#~ msgstr "Create cu"

#~ msgid "Diffstat"
#~ msgstr "Modificări"

#~ msgid "Unified diff"
#~ msgstr "Diferențe unificate"
