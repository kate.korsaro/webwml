#use wml::debian::template title="Выпуски Debian"
#include "$(ENGLISHDIR)/releases/info"
#use wml::debian::translation-check translation="13e629a7f229b013bb482899912fef0928b23f08" maintainer="Lev Lamberov"

<p>Debian активно поддерживает как минимум три
выпуска: <q>стабильный</q>, <q>тестируемый</q> и <q>нестабильный</q>.
</p>

<dl>
<dt><a href="stable/">стабильный</a></dt>
<dd>
<p>
  <q>Стабильный</q> дистрибутив содержит последнюю официально выпущенную
  версию дистрибутива Debian.
</p>
<p>
  Это <q>боевой</q> выпуск Debian, который мы в первую очередь рекомендуем
  использовать.
</p>
<p>
  Текущий <q>стабильный</q> выпуск Debian имеет версию
  <:=substr '<current_initial_release>', 0, 2:>, кодовое имя <em><current_release_name></em>.
<ifeq "<current_initial_release>" "<current_release>"
  "Он вышел <current_release_date>."
/>
<ifneq "<current_initial_release>" "<current_release>"
  "Первоначальная версия <current_initial_release> была выпущена
  <current_initial_release_date> и его последнее
  обновление, версия <current_release>, вышло <current_release_date>."
/>
</p>
</dd>

<dt><a href="testing/">тестируемый</a></dt>
<dd>
<p>
  Текущий <q>тестируемый</q> дистрибутив содержит пакеты, которые ещё
  не вошли в <q>стабильный</q> выпуск, но ожидается, что они туда попадут.
  Главным преимуществом использования этого дистрибутива является то,
  что в него включены более свежие версии программного обеспечения.
</p>
<p>
  См. <a href="$(DOC)/manuals/debian-faq/">Часто задаваемые вопросы о Debian</a>
  предоставляют дополнительную информации о том,
  <a href="$(DOC)/manuals/debian-faq/ftparchives#testing">что такое <q>тестируемый</q></a>
  дистрибутив и <a href="$(DOC)/manuals/debian-faq/ftparchives#frozen">как он становится
  <q>стабильным</q></a>.
</p>
<p>
  Текущий <q>тестируемый</q> дистрибутив&nbsp;&mdash; это <em><current_testing_name></em>.
</p>
</dd>

<dt><a href="unstable/">нестабильный</a></dt>
<dd>
<p>
  <q>Нестабильный</q> дистрибутив&nbsp;&mdash; это дистрибутив, находящийся в активной
  разработке в данный момент. Обычно его используют разработчики и те, кто
  предпочитает жить <q>на грани</q>, используя самые последние версии пакетов. Пользователям,
  использующим нестабильный дистрибутив рекомендуется подписаться на список рассылки
  debian-devel-announce, чтобы получать сообщения о серьёзных изменениях (например, обновлениях,
  которые могут повредить систему).
</p>
<p>
  <q>Нестабильный</q> дистрибутив всегда носит название <em>sid</em>.
</p>
</dd>
</dl>

<h2>Жизненный цикл выпуска</h2>
<p>
  Debian регулярно сообщает о своём новом стабильном выпуске. Жизненный
  цикл выпуска составляет пять лет: сначала три года полной поддержки, затем
  два года долгосрочной поддержки (LTS).
</p>

<p>
  Подробную информацию см. на вики-страницах <a href="https://wiki.debian.org/DebianReleases">
  Выпуски Debian</a> и <a href="https://wiki.debian.org/LTS">
  Долгосрочная поддержка Debian</a>.
</p>

<h2>Указатель выпусков</h2>

<table border="1">
<tr>
  <th>Версия</th>
  <th>Кодовое имя</th>
  <th>Дата выпуска</th>
  <th>Конец жизни (EOL)</th>
  <th>EOL LTS</th>
  <th>EOL ELTS</th>
  <th>Статус</th>
</tr>
<tr>
  <td>14</td>
  <td>Forky</td>
  <td>Будет объявлено</td>
  <td>Будет объявлено</td>
  <td>Будет объявлено</td>
  <td>Будет объявлено</td>
  <td>Кодовое имя объявлено</td>
</tr>
<tr>
  <td>13</td>
  <td><a href="<current_testing_name>/">Trixie</a></td>
  <td>Будет объявлено</td>
  <td>Будет объявлено</td>
  <td>Будет объявлено</td>
  <td>Будет объявлено</td>
  <td><q>тестируемый</q> &mdash; дата выпуска не установлена</td>

</tr>
<tr>
  <td>12</td>
  <td><a href="bookworm/">Bookworm</a></td>
  <td>2023-06-10</td>
  <td>2026-06-10</td>
  <td>2028-06-30</td>
  <td>2033-06-30</td>
  <td>Текущий <q>стабильный</q> выпуск</td>
</tr>
<tr>
  <td>11</td>
  <td><a href="bullseye/">Bullseye</a></td>
  <td>2021-08-14</td>
  <td>2024-08-14</td>
  <td>2026-08-31</td>
  <td>2031-06-30</td>
  <td>Текущий <q>старый стабильный</q> выпуск</td>
</tr>
<tr>
  <td>10</td>
  <td><a href="buster/">Buster</a></td>
  <td>2019-07-06</td>
  <td>2022-09-10</td>
  <td>2024-06-30</td>
  <td>2029-06-30</td>
  <td>Архивный выпуск, находится под поддержкой третьих лиц с платной <a href="https://wiki.debian.org/LTS/Extended">расширенной LTS поддержкой</a></td>
</tr>
<tr>
  <td>9</td>
  <td><a href="stretch/">Stretch</a></td>
  <td>2017-06-17</td>
  <td>2020-07-18</td>
  <td>2022-07-01</td>
  <td>2027-06-30</td>
  <td>Архивный выпуск, находится под поддержкой третьих лиц с платной <a href="https://wiki.debian.org/LTS/Extended">расширенной LTS поддержкой</a></td>
</tr>
<tr>
  <td>8</td>
  <td><a href="jessie/">Jessie</a></td>
  <td>2015-04-25</td>
  <td>2018-06-17</td>
  <td>2020-06-30</td>
  <td>2025-06-30</td>
  <td>Архивный выпуск, находится под поддержкой третьих лиц с платной <a href="https://wiki.debian.org/LTS/Extended">расширенной LTS поддержкой</a></td>
</tr>
</table>

<p>Старые выпуски:</p>

<ul>
  <li><a href="wheezy/">Debian 7.0 (<q>wheezy</q>)</a> &mdash; устаревший стабильный выпуск</li>
  <li><a href="squeeze/">Debian 6.0 (<q>squeeze</q>)</a> &mdash; устаревший стабильный выпуск</li>
  <li><a href="lenny/">Debian GNU/Linux 5.0 (<q>lenny</q>)</a> &mdash; устаревший стабильный выпуск</li>
  <li><a href="etch/">Debian GNU/Linux 4.0 (<q>etch</q>)</a> &mdash; устаревший стабильный выпуск</li>
  <li><a href="sarge/">Debian GNU/Linux 3.1 (<q>sarge</q>)</a> &mdash; устаревший стабильный выпуск</li>
  <li><a href="woody/">Debian GNU/Linux 3.0 (<q>woody</q>)</a> &mdash; устаревший стабильный выпуск</li>
  <li><a href="potato/">Debian GNU/Linux 2.2 (<q>potato</q>)</a> &mdash; устаревший стабильный выпуск</li>
  <li><a href="slink/">Debian GNU/Linux 2.1 (<q>slink</q>)</a> &mdash; устаревший стабильный выпуск</li>
  <li><a href="hamm/">Debian GNU/Linux 2.0 (<q>hamm</q>)</a> &mdash; устаревший стабильный выпуск</li>
</ul>

<p>Web-страницы старых выпусков Debian сохраняются в неприкосновенности,
хотя сами эти выпуски могут быть найдены лишь в специальном
<a href="$(HOME)/distrib/archive">архиве</a>.
</p>

<p>См. <a href="$(HOME)/doc/manuals/debian-faq/">Часто задаваемые вопросы по Debian</a> для объяснения
<a href="$(HOME)/doc/manuals/debian-faq/ftparchives#sourceforcodenames">происхождения всех этих кодовых имен</a>.</p>

<h2>Целостность данных в выпусках</h2>

<p>Целостность данных обеспечивается цифровой подписью файла
<code>Release</code>. Чтобы обеспечить, что все файлы выпуска действительно
входят в него, в файл <code>Release</code> копируются контрольные суммы
всех файлов <code>Packages</code>.</p>

<p>Цифровые подписи этого файла, полученные с использованием текущей версии
ключа для подписи архива, хранятся в файле <code>Release.gpg</code>.
Для <q>stable</q> и <q>oldstable</q> генерируются дополнительные
подписи с помощью недоступного по сети ключа, специально созданного для
выпуска членом <a href="$(HOME)/intro/organization#release-team">
команды стабильного выпуска</a>.</p>
