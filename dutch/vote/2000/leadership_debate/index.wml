#use wml::debian::template title="Debian leiderschapsdebat"
#include "$(ENGLISHDIR)/vote/style.inc"
#use wml::debian::votebar
#use wml::debian::translation-check translation="b8114b588961778dbd04974c1464a2f388a90c28"

<h3>Datum</h3>

<p>Het debat vond plaats op dinsdag 15 februari 2000 om 19.00 uur UTC.</p>

<h3>Waar</h3>

<p><code>irc.debian.org</code> op kanaal <code>#debian-debate</code></p>

<h3>Vorm</h3>

<p>24 uur voor het debat e-mailden alle kandidaten hun <q>openingstoespraak</q>
naar de organisator van het debat, Jason Gunthorpe. Daarna werden ze op deze pagina
geplaatst. Alles werd tegelijkertijd toegevoegd om eerlijkheid te garanderen.</p>

<p>Het debat zelf bestond uit twee delen. Ten eerste een sterk gemodereerd
traditioneel debat:</p>
<ol>
<li>De moderator stelde een kandidaat een vraag. De kandidaat kreeg vervolgens een
    redelijke tijd om te antwoorden.</li>
<li>Na het antwoord reageerde elk van de andere kandidaten om de beurt.</li>
<li>De eerste kandidaat mocht afsluitende opmerkingen maken over de vraag.</li>
<li>De volgorde van de kandidaten werd voor elke vraag geroteerd.</li>
</ol>

<p>Het tweede deel van het debat verliep vrijer. Er werden vragen gesteld door
het publiek en de ontwikkelaars. Elke kandidaat kreeg een korte periode om te
reageren.</p>

<h3>Standpunten van de kandidaten</h3>

<p>De kandidaten worden vermeld in volgorde van de nominaties. In de meeste
gevallen is er geen duidelijk onderscheid tussen de aankondiging en de
programmaverklaring, dus worden ze gewoon als een genummerde lijst weergegeven.</p>

<dl>
<dt>Ben Collins</dt>
<dd>Aankondiging en programmaverklaring:
   <a href="https://lists.debian.org/debian-vote-0001/msg00000.html">1</a>
   <a href="https://lists.debian.org/debian-vote-0001/msg00004.html">2</a></dd>
<dd><a href="ben-speech.html">Openingstoespraak voor het debat</a></dd>
<dt>Wichert Akkerman</dt>
<dd>Aankondiging en programmaverklaring:
   <a href="https://lists.debian.org/debian-vote-0001/msg00001.html">1</a></dd>
<dd><a href="wichert-speech.html">Openingstoespraak voor het debat</a></dd>
<dt>Joel Klecker</dt>
<dd>Aankondiging en programmaverklaring:
   <a href="https://lists.debian.org/debian-vote-0001/msg00009.html">1</a></dd>
<dd><a href="joel-speech.html">Openingstoespraak voor het debat</a></dd>
<dt>Matthew Vernon</dt>
<dd>Aankondiging en programmaverklaring:
   <a href="https://lists.debian.org/debian-vote-0001/msg00003.html">1</a>
   <a href="https://lists.debian.org/debian-vote-0001/msg00008.html">2</a></dd>
<dd><a href="mat-speech.html">Openingstoespraak voor het debat</a></dd>
</dl>

<h3>Log van het kandidatendebat</h3>

<p>Het <a href="transcript.txt">ruwe transcript</a> en een
<a href="transcript.html">opgemaakte versie</a> zijn beschikbaar, evenals
de ruwe <a href="topics.html">topics</a> en de <a href="informal.txt">informele
vragen</a> voor het debat. Niet alle onderwerpen kwamen aan bod.</p>
