#use wml::debian::template title="Platform for Andreas Tille" BARETITLE="true" NOHEADER="true"
#include "$(ENGLISHDIR)/vote/style.inc"

<div class="main">
<table class="title">
<tr><td style="vertical-align: top;">
<h1 class="titlemain"><big><b>Andreas Tille</b></big><br>
    <small>DPL Platform</small><br>
    <small>2024-03-19</small>
</h1>

<a href="mailto:tille@debian.org"><tt>tille@debian.org</tt></a><br />
<a href="https://people.debian.org/~tille/">Debian Developer page</a><br />
<a href="https://people.debian.org/~tille/talks/">Debian related talks</a><br />
<a href="http://fam-tille.de"><tt>Private homepage</tt></a><br />

</td>
<td width=50%>&nbsp;</td>
<td style="vertical-align: bottom;">
<div style="min-height: 210px;">
<img style="float: left; padding-right: 10px;" src="20140827_2.jpg" alt="Image of Andreas Tille" width="140">
</div>
</td>
</tr>
</table>

<h2 class="section">1. Introduction</h2>

<p>My name is Andreas Tille. It took me more than 25 years with the
Debian account <em>tille</em> to run for DPL.</p>

<p>
I am married and a proud grandfather (thanks to my son and
daughter-in-law).  I am also a father of two adopted daughters (all
thanks to Debian) and one of these daughters made me a grandfather
again.  My background as a
physicist has fostered a keen interest in practical applications of IT
solutions in science. I have a lifelong passion for various sports,
particularly swimming. Concerned about our environment and the climate
crisis, I have dedicated myself to planting numerous trees. I am an avid
cyclist and have chosen not to own a car, prioritizing sustainable
transportation methods.
</p>

<p>
For me, among other things, freedom means not being available at all
times. That's why I decided against owning a smartphone, for instance.
Therefore, it is important for you to know that as your potential DPL,
there may be times when I am offline and cannot be reached.  I value
freedom deeply, and I am grateful for the privilege of making choices
that are sound with my values.
</p>

<h2 class="section">2. Why I am running for DPL</h2>

<h4>Short answer</h4>

<p>
Debian has been a significant part of about half of my
life.  While contributing packages has been the primary part of my
involvement,  I feel compelled to give back more to my friends and the
community.
</p>

<h4>Long answers</h4>
<p>
After initiating the Debian Med project, I have learned a lot about
managing a team of volunteers. While I was frequently addressed as the
Debian Med leader, I usually refused the term "leader" since I was not
elected for this position (and there is no point in having such a leader
position for a small team at all).  However, I actually fulfilled tasks
a leader is expected to do, like establishing a friendly climate amongst
the team members and caring for the healthy growth of the team.  I
learned that leading volunteers is way harder than leading employees,
and I consider it a great challenge to motivate volunteers to enjoy
working for the team.
</p>
<p>
In evaluating my capacity to build an effective team, I place value on
two qualities that I consider instrumental in achieving success:
my background as a physicist and my experience as an athlete.
These personal attributes have played a significant role in shaping my
ability to lead and collaborate effectively, contributing to my own
success in assembling and managing teams.  As a physicist, I possess a
keen sense to identify logical and sensible solutions. This analytical
skill enables me to make informed decisions and navigate complex
challenges effectively. Furthermore, my experience as a sportsman has
instilled in me a strong sense of persistence and determination. I am
committed to applying both of these qualities as DPL, leveraging my
analytical mindset to make strategic decisions and my persistence to
drive initiatives forward and overcome obstacles.
</p>
<p>
As you can read in <a
href="https://people.debian.org/~tille/index.html#statistics">the stats
section of my developer page</a>, I have uploaded numerous packages and
fixed many bugs. But Debian is not only about uploading packages. It's
about technical and social problems, reaching out to newcomers, and
ensuring that we remain relevant within the IT universe.
</p>
<p>
Especially at Debian Conferences but also at other events, I have met
many very knowledgeable people whom I would love to work with and from
whom I would happily take advice on the new tasks that I might have to
tackle.
</p>


<h4>What makes me afraid about running for DPL</h4>
<p>
I love Debian because it enables me, as a volunteer, to choose the tasks
I love to do. Luckily, there are so many challenging tasks I have
enjoyed over the years.  I have mostly worked in the background -
although making some presentations in various talks has occasionally
brought me into the spotlight. Being in the spotlight is something I
really don't enjoy, which was one reason I resisted for years when my
Debian friends suggested that I run for DPL.
</p>
<p>
I also anticipate that there are several non-technical tasks awaiting
the DPL, which may not be as pleasant as the work I am currently doing in
Debian.  However, I approach these challenges with optimism. The
prospect of establishing a DPL advisory board, from which I can seek
guidance and support in navigating difficult questions, alleviates any
apprehension I may have about these tasks.
</p>
<p>
Finally, I am a bit concerned about the workload I'll leave to my
teammates since I plan to stop my uploading work to fully concentrate on
DPL tasks.
</p>


<h4>Short summary</h4>
<p>
I am uncertain whether it's possible to initiate significant changes
within Debian during a single DPL term, despite recognizing the need
across various aspects. I aim to be realistic and sincere in my
commitments, avoiding the temptation to make promises that I may not be
able to fulfill. Instead, I prioritize laying a solid foundation for
future DPLs to effectively implement necessary changes. I hereby commit
to leveraging the experience I gather for the benefit of those future
DPLs.
</p>

<h2 class="section">3. Agenda</h2>

<h3 class="subsection">3.1. Ensuring Debian stays relevant in a changing OS ecosystem</h3>

<h4>External perception</h4>
<p>
Sometimes I wonder whether Debian is a victim of its own success by
being the most frequently derived distribution. Countless times I've
encountered Linux newbies who never heard about Debian but know what
Ubuntu is. While this somehow fits my personality as stated above, to
stay in the background, it would probably attract more contributors if
Debian were more widely known amongst people who do not consider
themselves Linux experts.
</p>

<h4>Reaching Out to Learn</h4>
<p>
I will try to establish contact with other distributions. From
derivatives, I would like to create some kind of wish list for what we,
as their upstream, could do better or what we possibly can learn. I also
plan to talk to distributions with different technical bases like
ArchLinux, Fedora, OpenSUSE, Nix, etc., to see whether we can learn from
them to solve problems in terms of organization of work and
infrastructure. Maybe we will be able to draw some conclusions, for
instance, why ArchWiki is famous for good documentation but
wiki.debian.org is not.
</p>

<h4>Preparing for future</h4>
<p>
For the Trixie release, we are facing the 64-bit time_t transition. This
ensures that 32-bit architectures in Trixie and later will be capable of
handling timestamps referring to times beyond 2038. Since other major
distributions decided to drop 32-bit architectures, Debian might be even
more relevant to work nicely in special hardware applications.  That's
another challenge we are facing to prepare Debian for the future.
</p>

<h4>Packaging standards</h4>
<p>
To uphold Debian's esteemed standard of excellence, I am dedicated to
addressing areas where improvements can be made.  While
we are appreciated for our high quality packages, there is a list of
'smelly packages' (see <a href="https://trends.debian.net/">Debian
Trends</a>). My goal is to reduce the barriers to updating these
packages and ensure that they meet current packaging standards. This
includes encouraging contributors to maintain Git repositories on Salsa,
preferably within team spaces, as the default method for maintaining a
package.
</p>

<h4>Outreach</h4>
<p>
I am committed to investing my energy in outreach projects targeting
younger developers to ensure Debian's future relevance. By engaging with
and empowering the next generation, we can foster innovation and sustain
Debian's position as a leading force in the open-source ecosystem.
Through mentorship and educational initiatives, we can inspire emerging
developers to contribute, ensuring Debian's continued impact for years
to come.
</p>

<h3 class="subsection">3.2. Work of infrastructure and packaging teams</h3>

<h4>Infrastructure teams</h4>
<p>
Debian heavily depends on the work of various infrastructure teams like
DSA, the release team, the ftpmaster team, and others. If I am elected
as DPL, I will engage with the members of all those teams to identify
and solve problems. In the past, I have proposed enhancements for the
work of the ftpmaster team. I admit that among several reasons for my
DPL candidature, one is to enhance the process of integrating new
packages.
</p>

<h4>Reduce manual work</h4>
<p>
I am also considering ways to streamline and automate tasks wherever
possible. One example that comes to mind is the process of removing
packages for specific architectures, which currently relies on manual
intervention of ftpmaster triggered by a bug report. While I appreciate
the value of having an additional pair of eyes checking dependencies in
the past, we now have autopkgtests that signal potential chain of
dependency issues.  I believe that implementing sensible tooling can
significantly reduce manual workload, speeding up processes and
providing maintainers with greater control.
</p>

<h4>Cooperation</h4>
<p>
In addition to my own packaging work, I have always prioritized strong
team maintenance.  To assess team sustainability, I have established
some team metrics.  It has shown that teams in Debian can vary greatly.
It ranges from a large number of people cooperating very closely,
teams with lots of members just using a common team space and follow a
common policy, to teams that are less than a hand full of really
active people doing all the work.  I hope that I will be able to
advertise a cooperative team culture of many active contributors,
including creating a friendly environment for new team members.  In
Blends teams we sometimes succeeded in teaching and involving upstream
developers as well as users of the software we are packaging.  That's
why I am very much in favor of promoting Blends more strongly.
</p>

<h4>Team culture</h4>
<p>
I have gained insight into several teams within Debian. I am a big
advocate for teams since they lower the barrier to updating and fixing
packages that are no longer "private". However, I have also observed
packages becoming "team orphaned" when the original uploader silently
moved on to other tasks while relying on other team members to maintain
the original work. To address this, I would like to promote a stronger
team culture to monitor problems within the entire team for every
member.
</p>

<h4>Building redundancy</h4>
<p>
I envision a future where every crucial task in Debian - whether it's
maintaining infrastructure or managing non-leaf packages - is handled by
at least two individuals to ensure comprehensive backup and support.
History has shown us instances where contributors have had to prioritize
personal commitments or unforeseen circumstances over their Debian
responsibilities, such as pursuing ultra-marathons or navigating the
demands of parenthood. As volunteers may inevitably need to step away
from their Debian tasks, it's essential that we establish mechanisms to
manage such transitions effectively. This is why I hold reservations
about the 'traditional' model of package ownership by a single
maintainer.
</p>

<p>
In other words: If you think single maintainership of packages
is the right way to cope with future problems for Debian you should
probably rank me below "None of the above".
</p>

<h4>Packaging standards, salvaging packages</h4>
<p>
I am a strong proponent of the adoption of Debian-wide packaging
standards that aim to streamline workflows and provide easier access for
both contributors moving between teams as well as for newcomers. I
envision the implementation of standards such as making maintenance
mandatory on Salsa, utilizing Salsa CI for continuous integration,
ensuring autopkgtests for packages, and leveraging janitor tools, among
others. In the long run, I believe these efforts could facilitate
package uploads directly from Salsa, further improving efficiency and
collaboration within the Debian ecosystem.
As a precondition for this goal, I advocate for implementing our
<a href="https://wiki.debian.org/PackageSalvaging">salvage mechanism</a>
to transfer packages not yet hosted on Salsa to team repositories whenever
feasible otherwise in "<tt>debian</tt>-team".
</p>


<h3 class="subsection">3.3. Outreach and fostering a friendly environment inside Debian</h3>

<h4>Face to face meetings</h4>
<p>
Having worked in Debian for nearly half of my life, I have had the great
pleasure of meeting many wonderful people. I appreciate the social
environment Debian has established, and I am committed to enhancing it
even further. As I value in-person meetings like DebConf, MiniDebConfs,
and team meetings, I will support these to the best of my ability. As a
follow-up to the BoF <a
href="https://debconf23.debconf.org/talks/80-face-to-face-debian-meetings-in-a-climate-crisis/">Face-to-face
Debian meetings in a climate crisis</a> at DebConf23 , I would encourage
everyone to minimize air travel whenever possible. Fortunately, I've
noticed a tendency among Debian community members to prefer land travel
over flights anyway.
</p>

<h4>Outreach</h4>
<p>
One of the things I am most proud of in my work in Debian is the fact
that my pet project, Debian Med, has attracted an average of one new
developer per year of its existence. That's about 2% of the number of
Debian developers.  This was achieved by participating in outreachy
projects and by finding our own means to attract contributors in both
directions (upstream and downstream). If I am elected as DPL, I will
continue to actively reach out to new contributors and have some rough
ideas on how to do so.
</p>

<h4>Tiny tasks</h4>
<p>
For instance, I envision organizing a continuous bug squashing party,
where contributors come together to address random bugs, thus exploring
all corners of Debian. Drawing from my experience as one of the top 10
bug squashers within Debian, I am keen to implement initiatives such as
these. Creating a script to select a random bug from the Bug Tracking
System (BTS) and featuring it as the topic of a dedicated Matrix channel
would be straightforward.
This approach could showcase to newcomers the readily accessible
opportunities to contribute, serving as a motivating factor.
Additionally, I believe that offering guidance on
where to seek help within Debian is crucial for attracting and
supporting new contributors. I am fully committed to facilitating such
efforts, provided that my responsibilities as DPL allow me sufficient
time to do so.
</p>

<h4>Fostering Collaboration</h4>
<p>
In a similar vein, I am considering implementing autopkgtests for a
random package each day within another dedicated Matrix channel. I aim
to leverage the expertise of Outreachy students whom I have mentored in
these tasks over the past couple of years. Their valuable insights and
experience will be instrumental in ensuring the success of this
initiative. Additionally, I hope to attract other experienced Debian
contributors to join these efforts. By bringing together newcomers and
long-term contributors, we can foster collaboration and further enhance
the effectiveness of our initiatives.
</p>

<h4>Diversity</h4>
<p>
Within the Debian community, there exists an uneven distribution in
terms of gender representation and geographic diversity. Currently,
there is a notable over representation of male contributors originating
from countries typically considered industrialized.
</p>

<h4>Inclusivity</h4>
<p>
I have observed that the makeup of our developer community often
reflects the diversity present within our user base, albeit to varying
degrees. Therefore, it stands to reason that actively working to
increase the representation of underrepresented groups among Debian
contributors could serve to better align the project with the diverse
needs and perspectives of our user base. By fostering greater
inclusivity and diversity within our contributor pool, we can enhance
the relevance and effectiveness of our project, ensuring that it remains
accessible and beneficial to a broader range of individuals and
communities.
</p>

<h4>Lower barriers</h4>
<p>
As part of my commitment to fostering inclusivity and diversity within
the Debian community, I am actively exploring ways to make it easier for
newcomers to start contributing. While we have made significant progress
in addressing geographic disparities through our translation efforts,
there is still room to further strengthen this endeavor. Therefore, I
intend to offer support to the localization team to ensure that we
continue to improve accessibility for contributors from all regions.
Additionally, in tackling gender imbalances, it's crucial to acknowledge
the various societal factors at play. For instance, I've encountered the
argument that in many cultures, women have less leisure time than men,
which can hinder their ability to participate in open-source projects.
As a potential solution, we might consider introducing tasks such as bug
squashing, autopkgtest writing, and other short-term assignments that
require minimal time commitments. This approach aims to lower barriers
to entry and encourage participation from individuals with diverse
backgrounds and time constraints, as these tasks are self-contained and
do not necessitate ongoing maintenance.
</p>

<h4>Make joining Debian fun and profit</h4>
<p>
The <a href="https://wiki.debian.org/L10n">localization team wiki</a>
explicitly mentions the availability of small but valuable tasks,
emphasizing that even dedicating just an hour a week can contribute
significantly. Similarly, the ideas I've presented above, such as the
'bug of the day' and 'autopkgtest of the day,' offer small,
self-contained tasks that can be completed within a short time frame.
Moreover, simply observing how others tackle these tasks collaboratively
could serve as an engaging entry point for newcomers. Ultimately, my
goal is to make joining Debian an enjoyable and rewarding experience.
</p>


<h3 class="subsection">3.4. Navigating Constructive Criticism: Embracing Feedback for Growth and Success</h3>

<h4>Learn from people who left</h4>
<p>
Within the vibrant landscape of the Free Software world, Debian
inevitably encounters criticism -- a testament to its significance.
While the proliferation of derivatives might initially appear positive,
it also signifies unmet needs within Debian itself, implicitly prompting
reflection and critique. Past instances, such as the departures of
longstanding contributors like Joey Hess and Michael Stapelberg,
underscore the importance of addressing criticism. For instance, Michael
Stapelberg's <a
href="https://michael.stapelberg.ch/posts/2019-03-10-debian-winding-down/">comprehensive
articulation of reasons for leaving Debian</a> serves as a valuable
source of insight and reflection for the community.  I will put this
article under my pillow, specifically the strong words about striving
towards more unification and a cultural shift from "this package is
my domain, how dare you touch it" to a shared sense of ownership.  I
also fully subscribe to the need for unique workflows to possibly profit
from Debian-wide changes.
</p>

<h4>Consulting experts</h4>
<p>
I find several compelling ideas in this article, that I believe are
highly relevant. I am eager to engage with the Debian community to
identify longstanding issues that have yet to be addressed. I am
committed to facilitating discussions with knowledgeable experts, to
actively seek solutions to these challenges.
</p>


<h2 class="section">4. Thank You for Your Trust</h2>

<p>
If you choose to vote for me, I am committed to ensuring transparency in
my work. I intend to maintain a daily log in a public Git repository,
provided the information can be shared with a public audience.
Additionally, I will send a monthly summary to debian-devel-announce to
keep you informed about my activities and progress.
</p>

<p>
As pillars of our community, your support is paramount to me. Debian
holds a significant place in my life, and I am committed to serving you
diligently. I place my trust in your judgment to choose the right
leader. Thank you for dedicating your time to review my platform and for
considering casting your vote in my favor.
</p>


<h2> A. Changelog </h2>

<p> This platform is version controlled in a <a href="https://salsa.debian.org/tille/dpl-platform">git repository.</a> </p>

<ul>
<li><a href="https://salsa.debian.org/tille/dpl-platform/tags/0.7">0.7</a>: Platform for 2024 DPL elections.</li>
</ul>

<br>

</div>
