# Status: published
# $Rev$
#use wml::debian::translation-check translation="27cb73692f0206ae5f0a3f04e7090405b4df902a"

<define-tag pagetitle>Verklaring over Daniel Pocock</define-tag>
<define-tag release_date>2024-06-06</define-tag>
#use wml::debian::news

<p>Na de onrechtmatige registratie van het handelsmerk Debian in Zwitserland door het bedrijf van een voormalige Debian-ontwikkelaar - die sinds 2018 een zware lastercampagne voert tegen Debian - heeft het Debian-project krachtige actie ondernomen om zijn handelsmerken en belangen wereldwijd veilig te stellen.</p>

<p>In november 2023 <a href="https://www.debian.org/News/2024/judgement.pdf">oordeelde</a> het kantongerechtshof van het kanton Vaud, na een rechtszaak aangespannen door Debian, dat de registratie van het merk DEBIAN in Zwitserland door Open Source Developer Freedoms SA in liquidatie (OSDF) - voorheen Software Freedom Institute SA - een onrechtmatige toe-eigening was van het merk Debian, aangezien dit laatste merk wereldwijd bekend en gevestigd is in de IT-sector. Er is geen beroep aangetekend tegen de uitspraak, die daarom definitief is.</p>

<p>In zijn vonnis oordeelde het gerechtshof dat:</p>

<p><em>“Daniel Pocock de enige beheerder is van de beklaagde [OSDF].”</em></p>

<p><em>"Door aldus te handelen heeft beklaagde, die op de hoogte was van het bestaan van het betwiste merk en de bekendheid ervan en die wist dat de eiser de eigenaar ervan was vermits de enige beheerder van de verwerende partij een Debian-ontwikkelaar en een voormalig lid van de gelijknamige gemeenschap was, zich het genoemde merk toegeëigend en verwarring bij het publiek gesticht."</em></p>

<p>Het gerechtshof beval dat de Zwitserse handelsmerkregistratie moet worden overgedragen aan de door het Debian-project vertrouwde organisatie Software in the Public Interest (SPI) Inc., en dat de kosten van de actie door de beklaagde moeten worden betaald. OSDF werd ook veroordeeld tot het publiceren van de uitspraak op haar website. Voordat de definitieve uitspraak van de rechtbank kon worden betekend, annuleerde OSDF abrupt de registratie en ging later in liquidatie zonder SPI op de hoogte te stellen en zonder de kosten van de rechtszaak te betalen. Tot op heden heeft Daniel Pocock geen gehoor gegeven aan de bevelen van de rechtbank en Debian zag zich genoodzaakt om een invorderingsactie te starten voor de kosten.</p>

<p>Ondertussen werd Debian tussen 2020 en februari 2024 op de hoogte gebracht van minstens veertien domeinregistraties en bijbehorende websites die in strijd waren met ons handelsmerkbeleid. Alle domeinen zijn geregistreerd en gecontroleerd door Daniel Pocock. In mei 2024 gaf de World Intellectual Property Organisation (WIPO) de relevante domeinregisters opdracht om alle 14 registraties <a href="https://www.wipo.int/amc/en/domains/search/case.jsp?case=D2024-0770">over te dragen</a> aan SPI Inc., vertrouwensorganisatie voor Debian, <a href="https://www.wipo.int/amc/en/domains/search/text.jsp?case=D2024-0770">met de vermelding</a> dat:</p>

<p><em>"[...] Verweerder de betwiste domeinnamen te kwader trouw geregistreerd en gebruikt heeft, waardoor verwarring kan ontstaan over de bron van of verwantschap met het handelsmerk van de Eiser, voor zowel commerciële als kritische doeleinden, en met volledige kennis van het handelsmerk en het merkenbeleid van de Eiser."</em></p>

<p>Het Debian-project zal alle noodzakelijke maatregelen blijven nemen om haar handelsmerken en andere belangen te beschermen en te verdedigen. Ons handelsmerkbeleid heeft in meerdere rechtsgebieden de toets doorstaan.</p>

<p>We maken daarom van deze gelegenheid gebruik om de leden van onze gemeenschap te bedanken die veel moeite hebben gedaan om het gebruik en de verspreiding van Debian over de hele wereld te documenteren ter voorbereiding op het aanvechten van de Zwitserse handelsmerkregistratie, ondanks de zware - en oneerlijke - lastercampagne waarmee veel van onze vrijwilligers de afgelopen jaren werden geconfronteerd.</p>

<p>We blijven ons inzetten om de juiste juridische middelen in te zetten om onze gemeenschap en anderen te beschermen tegen verdere intimidatie.</p>

<h2>Aanvullende informatie</h2>

<p>Het vonnis van het kantongerechtshof van het kanton Vaud van 27 november 2023 is te vinden op <url "https://www.debian.org/News/2024/judgement.pdf"> en de volledige tekst van het WIPO-besluit, gedateerd 3 mei 2024, is beschikbaar op <a
href="https://www.wipo.int/amc/en/domains/search/text.jsp?case=D2024-0770">\
https://www.wipo.int/amc/en/domains/search/text.jsp?case=D2024-0770</a></p>


<h2>Over Debian</h2>

<p>Het Debian-project is een samenwerkingsverband van ontwikkelaars van vrije
software die vrijwillig tijd en moeite steken in het produceren van het
volledig vrije besturingssysteem Debian.</p>

<h2>Contactinformatie</h2>

<p>Ga voor verdere informatie naar de webpagina's van Debian op
<a href="$(HOME)/">https://www.debian.org/</a> of stuur een e-mail naar
&lt;press@debian.org&gt;.</p>

