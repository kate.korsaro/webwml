#
# Trần Ngọc Quân <vnwildman@gmail.com>, 2015.
#
msgid ""
msgstr ""
"Project-Id-Version: webwml countries\n"
"PO-Revision-Date: 2015-07-08 09:30+0700\n"
"Last-Translator: Trần Ngọc Quân <vnwildman@gmail.com>\n"
"Language-Team: Vietnamese <debian-l10n-vietnamese@lists.debian.org>\n"
"Language: vi\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Gtranslator 2.91.7\n"

#: ../../english/template/debian/countries.wml:111
msgid "United Arab Emirates"
msgstr "Các Tiểu Vương Quốc A Rập Thống Nhất"

#: ../../english/template/debian/countries.wml:114
msgid "Albania"
msgstr "An-ba-ni"

#: ../../english/template/debian/countries.wml:117
msgid "Armenia"
msgstr "Ác-mê-ni-a"

#: ../../english/template/debian/countries.wml:120
msgid "Argentina"
msgstr "Ác-hen-ti-na"

#: ../../english/template/debian/countries.wml:123
msgid "Austria"
msgstr "Áo"

#: ../../english/template/debian/countries.wml:126
msgid "Australia"
msgstr "Úc"

#: ../../english/template/debian/countries.wml:129
msgid "Bosnia and Herzegovina"
msgstr "Bô-x-ni-a và Héc-xê-gô-vi-na"

#: ../../english/template/debian/countries.wml:132
msgid "Bangladesh"
msgstr "Băng-la-đét"

#: ../../english/template/debian/countries.wml:135
msgid "Belgium"
msgstr "Bỉ"

#: ../../english/template/debian/countries.wml:138
msgid "Bulgaria"
msgstr "Bun-ga-ri"

#: ../../english/template/debian/countries.wml:141
msgid "Brazil"
msgstr "Bra-xin"

#: ../../english/template/debian/countries.wml:144
msgid "Bahamas"
msgstr "Ba-ha-ma"

#: ../../english/template/debian/countries.wml:147
msgid "Belarus"
msgstr "Be-la-rút"

#: ../../english/template/debian/countries.wml:150
msgid "Canada"
msgstr "Ca-na-đa"

#: ../../english/template/debian/countries.wml:153
msgid "Switzerland"
msgstr "Thụy Sĩ"

#: ../../english/template/debian/countries.wml:156
msgid "Chile"
msgstr "Chi Lê"

#: ../../english/template/debian/countries.wml:159
msgid "China"
msgstr "Trung Quốc"

#: ../../english/template/debian/countries.wml:162
msgid "Colombia"
msgstr "Cô-lom-bi-a"

#: ../../english/template/debian/countries.wml:165
msgid "Costa Rica"
msgstr "Cốt-x-tha Ri-ca"

#: ../../english/template/debian/countries.wml:168
msgid "Czech Republic"
msgstr "Cộng hòa Séc"

#: ../../english/template/debian/countries.wml:171
msgid "Germany"
msgstr "Đức"

#: ../../english/template/debian/countries.wml:174
msgid "Denmark"
msgstr "Đan Mạch"

#: ../../english/template/debian/countries.wml:177
msgid "Dominican Republic"
msgstr "Cộng Hòa Đô-mi-ni-ca"

#: ../../english/template/debian/countries.wml:180
msgid "Algeria"
msgstr "An-giê-ri"

#: ../../english/template/debian/countries.wml:183
msgid "Ecuador"
msgstr "Ê-cu-a-đo"

#: ../../english/template/debian/countries.wml:186
msgid "Estonia"
msgstr "Et-tô-ni-a"

#: ../../english/template/debian/countries.wml:189
msgid "Egypt"
msgstr "Ai Cập"

#: ../../english/template/debian/countries.wml:192
msgid "Spain"
msgstr "Tây Ban Nha"

#: ../../english/template/debian/countries.wml:195
msgid "Ethiopia"
msgstr "E-ti-ô-pi-a"

#: ../../english/template/debian/countries.wml:198
msgid "Finland"
msgstr "Phần Lan"

#: ../../english/template/debian/countries.wml:201
msgid "Faroe Islands"
msgstr "Quần Đảo Pha-rô"

#: ../../english/template/debian/countries.wml:204
msgid "France"
msgstr "Pháp"

#: ../../english/template/debian/countries.wml:207
msgid "United Kingdom"
msgstr "Vương quốc Anh"

#: ../../english/template/debian/countries.wml:210
msgid "Grenada"
msgstr "Grenada"

#: ../../english/template/debian/countries.wml:213
msgid "Georgia"
msgstr "Georgia"

#: ../../english/template/debian/countries.wml:216
msgid "Greenland"
msgstr "Greenland"

#: ../../english/template/debian/countries.wml:219
msgid "Greece"
msgstr "Hy Lạp"

#: ../../english/template/debian/countries.wml:222
msgid "Guatemala"
msgstr "Gua-tê-ma-la"

#: ../../english/template/debian/countries.wml:225
msgid "Hong Kong"
msgstr "Hồng Kông"

#: ../../english/template/debian/countries.wml:228
msgid "Honduras"
msgstr "Hôn-đu-rát"

#: ../../english/template/debian/countries.wml:231
msgid "Croatia"
msgstr "Croatia"

#: ../../english/template/debian/countries.wml:234
msgid "Hungary"
msgstr "Hung-ga-ri"

#: ../../english/template/debian/countries.wml:237
msgid "Indonesia"
msgstr "In-đô-nê-xi-a"

#: ../../english/template/debian/countries.wml:240
msgid "Iran"
msgstr "I-ran"

#: ../../english/template/debian/countries.wml:243
msgid "Ireland"
msgstr "Ai Len"

#: ../../english/template/debian/countries.wml:246
msgid "Israel"
msgstr "Israel"

#: ../../english/template/debian/countries.wml:249
msgid "India"
msgstr "Ấn Độ"

#: ../../english/template/debian/countries.wml:252
msgid "Iceland"
msgstr "Ai-xơ-len"

#: ../../english/template/debian/countries.wml:255
msgid "Italy"
msgstr "Ý"

#: ../../english/template/debian/countries.wml:258
msgid "Jordan"
msgstr "Jordan"

#: ../../english/template/debian/countries.wml:261
msgid "Japan"
msgstr "Nhật Bản"

#: ../../english/template/debian/countries.wml:264
msgid "Kenya"
msgstr "Ken-ia"

#: ../../english/template/debian/countries.wml:267
#, fuzzy
msgid "Kyrgyzstan"
msgstr "Kazakhstan"

#: ../../english/template/debian/countries.wml:270
msgid "Cambodia"
msgstr ""

#: ../../english/template/debian/countries.wml:273
msgid "Korea"
msgstr "Hàn Quốc"

#: ../../english/template/debian/countries.wml:276
msgid "Kuwait"
msgstr "Kuwait"

#: ../../english/template/debian/countries.wml:279
msgid "Kazakhstan"
msgstr "Kazakhstan"

#: ../../english/template/debian/countries.wml:282
msgid "Sri Lanka"
msgstr "Sri Lanka"

#: ../../english/template/debian/countries.wml:285
msgid "Lithuania"
msgstr "Lithuania"

#: ../../english/template/debian/countries.wml:288
msgid "Luxembourg"
msgstr "Lúc-xăm-bua"

#: ../../english/template/debian/countries.wml:291
msgid "Latvia"
msgstr "Lát-vi-a"

#: ../../english/template/debian/countries.wml:294
msgid "Morocco"
msgstr "Ma Rốc"

#: ../../english/template/debian/countries.wml:297
msgid "Monaco"
msgstr ""

#: ../../english/template/debian/countries.wml:300
msgid "Moldova"
msgstr "Moldova"

#: ../../english/template/debian/countries.wml:303
msgid "Montenegro"
msgstr "Mon-te-nê-gợ-rô"

#: ../../english/template/debian/countries.wml:306
msgid "Madagascar"
msgstr "Ma-đa-ga-x-că"

#: ../../english/template/debian/countries.wml:309
msgid "Macedonia, Republic of"
msgstr "Cộng Hòa Đô-mi-ni-ca"

#: ../../english/template/debian/countries.wml:312
msgid "Mongolia"
msgstr "Mông Cổ"

#: ../../english/template/debian/countries.wml:315
msgid "Malta"
msgstr "Malta"

#: ../../english/template/debian/countries.wml:318
msgid "Mexico"
msgstr "Mê-hi-cô"

#: ../../english/template/debian/countries.wml:321
msgid "Malaysia"
msgstr "Ma-lay-xi-a"

#: ../../english/template/debian/countries.wml:324
msgid "New Caledonia"
msgstr "Calédonia mới"

#: ../../english/template/debian/countries.wml:327
msgid "Nicaragua"
msgstr "Ni-ca-ra-gua"

#: ../../english/template/debian/countries.wml:330
msgid "Netherlands"
msgstr "Hà Lan"

#: ../../english/template/debian/countries.wml:333
msgid "Norway"
msgstr "Na Uy"

#: ../../english/template/debian/countries.wml:336
msgid "New Zealand"
msgstr "Niu Di-lân"

#: ../../english/template/debian/countries.wml:339
msgid "Panama"
msgstr "Pa-na-ma"

#: ../../english/template/debian/countries.wml:342
msgid "Peru"
msgstr "Pê-ru"

#: ../../english/template/debian/countries.wml:345
msgid "French Polynesia"
msgstr "Pô-li-nê-xi Pháp"

#: ../../english/template/debian/countries.wml:348
msgid "Philippines"
msgstr "Phi-líp-pin"

#: ../../english/template/debian/countries.wml:351
msgid "Pakistan"
msgstr "Pa-kít-xtan"

#: ../../english/template/debian/countries.wml:354
msgid "Poland"
msgstr "Ba Lan"

#: ../../english/template/debian/countries.wml:357
msgid "Portugal"
msgstr "Bồ Đào Nha"

#: ../../english/template/debian/countries.wml:360
msgid "Réunion"
msgstr ""

#: ../../english/template/debian/countries.wml:363
msgid "Romania"
msgstr "Ru-ma-ni"

#: ../../english/template/debian/countries.wml:366
msgid "Serbia"
msgstr "Xéc-bi"

#: ../../english/template/debian/countries.wml:369
msgid "Russia"
msgstr "Nga"

#: ../../english/template/debian/countries.wml:372
msgid "Saudi Arabia"
msgstr "A Rập Xau-đi"

#: ../../english/template/debian/countries.wml:375
msgid "Sweden"
msgstr "Thụy Điển"

#: ../../english/template/debian/countries.wml:378
msgid "Singapore"
msgstr "Xing-ga-po"

#: ../../english/template/debian/countries.wml:381
msgid "Slovenia"
msgstr "Xlô-ven"

#: ../../english/template/debian/countries.wml:384
msgid "Slovakia"
msgstr "Xlô-vác"

#: ../../english/template/debian/countries.wml:387
msgid "El Salvador"
msgstr "En-xan-va-đoa"

#: ../../english/template/debian/countries.wml:390
msgid "Thailand"
msgstr "Thái Lan"

#: ../../english/template/debian/countries.wml:393
msgid "Tajikistan"
msgstr "Ta-ji-kít-xtan"

#: ../../english/template/debian/countries.wml:396
msgid "Tunisia"
msgstr "Tu-ni-xi-a"

#: ../../english/template/debian/countries.wml:399
msgid "Turkey"
msgstr "Thổ Nhĩ Kỳ"

#: ../../english/template/debian/countries.wml:402
msgid "Taiwan"
msgstr "Đài Loan"

#: ../../english/template/debian/countries.wml:405
msgid "Ukraine"
msgstr "Ukraine"

#: ../../english/template/debian/countries.wml:408
msgid "United States"
msgstr "Hợp chủng quốc Hoa Kỳ"

#: ../../english/template/debian/countries.wml:411
msgid "Uruguay"
msgstr "U-ru-guay"

#: ../../english/template/debian/countries.wml:414
msgid "Uzbekistan"
msgstr "U-xơ-bê-ki-x-tanh"

#: ../../english/template/debian/countries.wml:417
msgid "Venezuela"
msgstr "Vê-nê-du-ê-la"

#: ../../english/template/debian/countries.wml:420
msgid "Vietnam"
msgstr "Việt Nam"

#: ../../english/template/debian/countries.wml:423
msgid "Vanuatu"
msgstr "Vanuatu"

#: ../../english/template/debian/countries.wml:426
msgid "South Africa"
msgstr "Nam Phi"

#: ../../english/template/debian/countries.wml:429
msgid "Zimbabwe"
msgstr "Zim-ba-buê"

#~ msgid "Great Britain"
#~ msgstr "Vương Quốc Thống Nhất Anh và Bắc Ai-len"
