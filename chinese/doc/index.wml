#use wml::debian::template title="說明[CN:文档:][HKTW:文件:]" MAINPAGE="true"
#use wml::debian::recent_list
#use wml::debian::translation-check translation="019351f156c8f8ffc0f7922929f7062fb4b732db"

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<ul class="toc">
  <li><a href="#quick_start">快速入門</a></li>
  <li><a href="#manuals">使用手册</a></li>
  <li><a href="#other">其他较短的说明[CN:文档:][HKTW:文件:]</a></li>
</ul>

<p>
[CN:創建:][HKTW:建立:]一个高品质的自由操作系统的过程包括撰写技术手册，以描述程序的操作和用法。Debian 计划花了很大的心血為它的[CN:用户:][HKTW:使用者:]們提供好讀而[CN:适:][HKTW:適:]當的說明[CN:文档:][HKTW:文件:]。本页面包含了指向安装手册、HOWTO [CN:文档:][HKTW:文件:]、常见问题（FAQ）、发行说明、我们的维基（Wiki），以及更多其他[CN:文档:][HKTW:文件:]的链接。
</p>


<h2><a id="quick_start">快速入門</a></h2>

<p>
如果您才剛開始使用 Debian，我們建議您可以先閱讀以下两份[CN:文档:][HKTW:文件:]：
</p>

<aside class="light">
  <span class="fas fa-fast-forward fa-5x"></span>
</aside>

<ul>
  <li><a href="$(HOME)/releases/stable/installmanual">安装手册</a></li>
  <li><a href="manuals/debian-faq/">Debian GNU/Linux 常见问题（FAQ）</a></li>
</ul>

<p>
在您第一次安裝 Debian 時先準備好以上[CN:文档:][HKTW:文件:]，它們\
可以回答許多問題並幫助您使用您新裝好的 Debian
系統。
</p>

<p>
之後您可能想繼續閱讀以下[CN:文档:][HKTW:文件:]：
</p>

<ul>
  <li><a href="manuals/debian-reference/">Debian 参考手册</a>：[CN:一个简洁的命令行用户指南:][HKTW:一個簡潔的命令列指令使用導覽:]</li>
  <li><a href="$(HOME)/releases/stable/releasenotes">发行说明</a>：通常随 Debian 的新版本发布，写给正在升级系统的人</li>
  <li><a href="https://wiki.debian.org/">Debian 维基</a>：Debian 官方维基</li>
</ul>

<p style="text-align:center"><button type="button"><span class="fas fa-print fa-2x"></span> <a href="https://www.debian.org/doc/manuals/refcard/refcard">打印 Debian GNU/Linux 参考卡</a></button></p>

<h2><a id="manuals">使用手册</a></h2>

<p>
大部份 Debian 所含的說明[CN:文档:][HKTW:文件:]都是為一般的 GNU/Linux
所寫的，不过也有專為 Debian 寫的一些[CN:文档:][HKTW:文件:]。\
這些[CN:文档:][HKTW:文件:]大致分為以下[CN:几种:][HKTW:幾種:]：
</p>

<ul>
  <li>使用手册：这些手册和书籍类似，廣泛地描述一项主題。下面列出的使用手册大多数既可以在线阅读，也可以通过 Debian 软件包进行安装。事实上，网站上的使用手册有很多都是从对应的 Debian 软件包中解压得到的。选择下面的使用手册之一，以获得它的包名和/或在线版本的链接。</li>
  <li>HOWTO 文档：正如其名，<a href="https://tldp.org/HOWTO/HOWTO-INDEX/categories.html">HOWTO 文档</a>是告訴您<em>如何（how to）</em>完成一项特定任务的[CN:文档:][HKTW:文件:]。也就是说，这类文档提供了关于如何做某件事情的详细而实用的建议。</li>
  <li>常见问题（FAQ）：我们编写了几份回答<em>常见问题</em>的[CN:文档:][HKTW:文件:]。特別針對 Debian 的問答集在 <a href="manuals/debian-faq/">Debian 常见问题</a>中。此外還有<a href="../CD/faq/">[CN:关于:][HKTW:關於:] Debian USB/CD/DVD 映像檔的 FAQ</a>，回答了各种关于安装介质的问题。</li>
  <li>其他較短的說明[CN:文档:][HKTW:文件:]：也请查阅较短的说明[CN:文档:][HKTW:文件:]的<a href="#other">列表</a>。</li>
</ul>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> Debian 的使用手冊及其他說明[CN:文档:][HKTW:文件:]的完整[CN:列表:][HKTW:清單:]请见 <a href="ddp">Debian [CN:文档计划:][HKTW:文件計畫:]</a>页面。另外，有一些面向 Debian GNU/Linux 用户的說明[CN:文档:][HKTW:文件:]已经印[CN:制:][HKTW:製:]成<a href="books">[CN:纸质:][HKTW:紙本:]書籍</a>。</p>
</aside>

<p>此处列出的使用手册大多数既可以在线阅读，也可以通过 Debian 软件包进行安装。事实上，网站上的使用手册有很多都是从对应的 Debian 软件包中解压得到的。选择下面的使用手册之一，以获得它的包名和/或在线版本的链接。</p>

<h3>專為 Debian 撰寫的使用手冊</h3>

<div class="line">
  <div class="item col50">
    
    <h4><a href="user-manuals">用户手册</a></h4>
    <ul>
      <li><a href="https://debian-beginners-handbook.tuxfamily.org/index-en.html">Debian Bookworm 初学者手册</a></li>
      <li><a href="user-manuals#faq">Debian GNU/Linux 常见问题</a></li>
      <li><a href="user-manuals#install">Debian 安裝手冊</a></li>
      <li><a href="user-manuals#relnotes">Debian 發行說明</a></li>
      <li><a href="user-manuals#refcard">Debian 參考卡</a></li>
      <li><a href="user-manuals#debian-handbook">Debian 管理员手册</a></li>
      <li><a href="user-manuals#quick-reference">Debian 參考手冊</a></li>
      <li><a href="user-manuals#securing">Debian 安全手冊</a></li>
      <li><a href="user-manuals#aptitude">aptitude 用户手册</a></li>
      <li><a href="user-manuals#apt-guide">APT 用户指南</a></li>
      <li><a href="user-manuals#java-faq">Debian GNU/Linux 与 Java 常見問題</a></li>
      <li><a href="user-manuals#hamradio-maintguide">Debian Hamradio 维护者指南</a></li>
    </ul>

  </div>

  <div class="item col50 lastcol">

    <h4><a href="devel-manuals">開發者手冊</a></h4>
    <ul>
      <li><a href="devel-manuals#policy">Debian 政策[CN:文档:][HKTW:文件:]</a></li>
      <li><a href="devel-manuals#devref">Debian 開發者參考[CN:文档:][HKTW:文件:]</a></li>
      <li><a href="devel-manuals#debmake-doc">Debian [CN:维护者:][HKTW:維護者:]指南</a></li>
      <li><a href="devel-manuals#packaging-tutorial">Debian [CN:软件包:][HKTW:套件:][CN:制:][HKTW:製:]作介绍</a></li>
      <li><a href="devel-manuals#menu">Debian [CN:菜单:][HKTW:選單:]系統</a></li>
      <li><a href="devel-manuals#d-i-internals">Debian [CN:安装程序:][HKTW:安裝程式:]内部</a></li>
      <li><a href="devel-manuals#dbconfig-common">使用[CN:数据库:][HKTW:資料庫:]的[CN:软件包:][HKTW:套件:][CN:维护者:][HKTW:維護者:]指南</a></li>
      <li><a href="devel-manuals#dbapp-policy">使用[CN:数据库:][HKTW:資料庫:]的[CN:软件包:][HKTW:套件:]的政策</a></li>
    </ul>

  </div>
</div>

<p style="text-align:center"><button type="button"><span class="fas fa-book-open fa-2x"></span> <a href="misc-manuals#history">阅读 Debian 计划的历史</a></button></p>

<h2><a id="other">其他較短的說明[CN:文档:][HKTW:文件:]</a></h2>

<p>以下[CN:文档:][HKTW:文件:]包含一些較快速、較短的說明：</p>

<p>
<dl>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> 如果您找遍了上面所述的各[CN:种:][HKTW:種:]資源，仍然無法解決您使用 Debian 上遇到的問題，可以來我們的<a href="../support">[CN:支持:][HKTW:支援:][CN:网:][HKTW:網:]頁</a>來看看。</p>
</aside>

  <dt><strong>手册页（manpages）</strong></dt>
    <dd>传统上，所有的 Unix [CN:程序:][HKTW:程式:]都有相關的<em>手册\
页（manpages）</em>，可以透過 <tt>man</tt> [CN:命令:][HKTW:指令:]來觀看相關的\
參考說明。这些手册页虽然通常不是写给新手看的，但是包含了一个命令的所有功能的详细说明。您可以\
在 <a href="https://manpages.debian.org/cgi-bin/man.cgi">https://manpages.debian.org/</a> 閱讀\
所有 Debian 包含的手冊页。
    </dd>

  <dt><strong><a href="https://www.gnu.org/software/texinfo/manual/texinfo/html_node/index.html">info [CN:文档:][HKTW:文件:]（info files）</a></strong></dt>
    <dd>許多 GNU 軟體都透過 <em>info [CN:文档:][HKTW:文件:]（info files）</em>而不是手册页\
提供參考[CN:文档:][HKTW:文件:]。這些[CN:文档:][HKTW:文件:]包含了\
关于[CN:程序:][HKTW:程式:]本身，參數以及範例等的詳細\
資訊，可以透過 <tt>info</tt> [CN:命令:][HKTW:指令:]來觀看。
    </dd>

  <dt><strong>讀我（README）[CN:文件:][HKTW:檔案:]</strong></dt>
    <dd><em>讀我（README）</em>[CN:文件:][HKTW:檔案:]是簡單的\
[CN:文本文件:][HKTW:文字檔:]，描述单个项目，通常是[CN:软件包:][HKTW:套件:]。您可以\
在您 Debian 系统的 <tt>/usr/share/doc/</tt> 底下很多子目錄中找到一大堆\
讀我[CN:文件:][HKTW:檔案:]。有的子目錄除了讀我[CN:文件:][HKTW:檔案:]以外，\
還有[CN:配置文件样例:][HKTW:設定範例檔:]。注意，通常大型程式的文档\
都是在獨立的[CN:软件包:][HKTW:套件:]內（與原[CN:软件包:][HKTW:套件:]同名，但以\
<em>-doc</em> 結尾）。
    </dd>
</dl>
</p>
