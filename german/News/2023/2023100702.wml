#use wml::debian::translation-check translation="7e9ce6b3f7065fbbce6287410a40acb00b516392" maintainer="Erik Pfannenstein"
<define-tag pagetitle>Debian 11 aktualisiert: 11.8 veröffentlicht</define-tag>
<define-tag release_date>2023-10-07</define-tag>
#use wml::debian::news
# $Id:

<define-tag release>11</define-tag>
<define-tag codename>Bullseye</define-tag>
<define-tag revision>11.8</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>
Das Debian-Projekt freut sich, die achte Aktualisierung seiner Oldstable-Distribution
Debian <release> (Codename <q><codename></q>) ankündigen zu dürfen. Diese
Zwischenveröffentlichung behebt hauptsächlich Sicherheitslücken
sowie einige ernste Probleme. Es sind bereits separate Sicherheitsankündigungen
veröffentlicht worden, auf die, wo möglich, verwiesen wird.
</p>
<p>
Bitte beachten Sie, dass diese Zwischenveröffentlichung keine neue Version von
Debian <release> darstellt, sondern nur einige der enthaltenen Pakete auffrischt.
Es gibt keinen Grund, <q><codename></q>-Medien zu entsorgen, da deren Pakete
auch nach der Installation durch einen aktualisierten Debian-Spiegelserver auf
den neuesten Stand gebracht werden können.
</p>

<p>Wer häufig Aktualisierungen von security.debian.org herunterlädt, wird nicht viele
Pakete auf den neuesten Stand bringen müssen. Die meisten dieser Aktualisierungen sind
in dieser Revision enthalten.</p>

<p>Neue Installationsabbilder können bald von den gewohnten Orten bezogen werden.</p>

<p>Vorhandene Installationen können auf diese Revision angehoben werden, indem
das Paketverwaltungssystem auf einen der vielen HTTP-Spiegel von Debian verwiesen
wird. Eine vollständige Liste der Spiegelserver ist verfügbar unter:</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>

<h2>Verschiedene Fehlerbehebungen</h2>

<p>Diese Oldstable-Aktualisierung fügt den folgenden Paketen einige wichtige Korrekturen hinzu:</p>

<table border=0>
<tr><th>Paket</th>               <th>Grund</th></tr>
<correction adduser "Anfälligkeit für Befehlsinjektion in deluser behoben">
<correction aide "Umgang mit erweiterten Attributen in symbolischen Links behoben">
<correction amd64-microcode "Enthaltenen Mikrocode aktualisiert, darunter auch Korrekturen für <q>AMD Inception</q> auf AMD Zen4-Prozessoren [CVE-2023-20569]">
<correction appstream-glib "Handhabung der Tags &lt;em&gt; und &lt;code&gt; in Metadaten">
<correction asmtools "Für künftige openjdk-11-Kompilate nach Bullseye zurückportiert">
<correction autofs "Fehlende mutex-Entsperrung hinzugefügt; kein rpcbind für NFS4-Einhängungen verwenden; Regression beim Feststellen der Erreichbarkeit auf Dual-Stack-Maschinen behoben">
<correction base-files "Aktualisierung auf die Zwischenveröffentlichung 11.8">
<correction batik "Probleme mit serverseitiger Anfragefälschung behoben [CVE-2022-44729 CVE-2022-44730]">
<correction bmake "Konflikt mit bsdowl (&lt;&lt; 2.2.2-1.2~) eingetragen, um glattlaufende Upgrades sicherzustellen">
<correction boxer-data "Thunderbird-Kompatibilitäts-Korrekturen zurückportiert">
<correction ca-certificates-java "Behelfslösung für unkonfiguriertes jre während der Installation eingebaut">
<correction cairosvg "Data:-URLs im sicheren Modus verarbeiten">
<correction cargo-mozilla "Neue Version der Originalautoren, um die Kompilierung neuerer firefox-esr-Versionen zu unterstützen">
<correction clamav "Neue stabile Version der Originalautoren; Anfälligkeit für Dienstblockade via HFS+-Parser behoben [CVE-2023-20197]">
<correction cpio "Eigenmächtige Codeausführung unterbunden [CVE-2021-38185]; Empfehlung von libarchive1 mit Empfehlung von libarchive-dev ersetzt">
<correction cryptmount "Speicher-Initialisierung im Befehlszeilen-Parser überarbeitet">
<correction cups "Heap-basierten Pufferüberlauf [CVE-2023-4504 CVE-2023-32324], unautorisierten Zugriff [CVE-2023-32360], Use-after-free [CVE-2023-34241] abgestellt">
<correction curl "Codeausführung [CVE-2023-27533 CVE-2023-27534], Informationsoffenlegung [CVE-2023-27535 CVE-2023-27536 CVE-2023-28322], unzulässige Wiederverwendung von Verbindungen [CVE-2023-27538], ungeeignete Zertifikatsüberprüfung [CVE-2023-28321] behoben">
<correction dbus "Neue stabile Version der Originalautoren; Dienstblockade abgestellt [CVE-2023-34969]">
<correction debian-design "Neukompilierung mit neuerem boxer-data">
<correction debian-installer "Linux-Kernel-ABI auf 5.10.0-26 angehoben; Neukompilierung gegen proposed-updates">
<correction debian-installer-netboot-images "Neukompilierung gegen proposed-updates">
<correction debian-parl "Neukompilierung mit neuerem boxer-data">
<correction debian-security-support "DEB_NEXT_VER_ID=12 gesetzt, weil Bookworm die nächste Veröffentlichung ist; security-support-limited: gnupg1 hinzugefügt">
<correction distro-info-data "Debian 14 <q>Forky</q> hinzugefügt; Veröffentlichungsdatum von Ubuntu 23.04 korrigiert; Ubuntu 23.10 Mantic Minotaur hinzugefügt; geplantes Veröffentlichungsdatum für Debian Bookworm hinzugefügt">
<correction dkimpy "Neue Fehlerkorrektur-Veröffentlichung der Originalautoren">
<correction dpdk "Neue stabile Version der Originalautoren">
<correction dpkg "Unterstützung für die loong64-CPU hinzugefügt; beim Formatieren von source:Upstream-Version nicht von fehlender Version stören lassen; varbuf-Speicherleck in pkg_source_version() geflickt">
<correction flameshot "Hochladen zu imgur standardmäßig deaktiviert; Name der d/NEWS-Datei aus dem vorherigen Upload korrigiert">
<correction ghostscript "Pufferüberlauf unterbunden [CVE-2023-38559]; versucht, den IJS-Server-Start abzusichern [CVE-2023-43115]">
<correction gitit "Neukompilierung gegen neues pandoc">
<correction grunt "Race-Condition beim Kopieren symbolischer Links behoben [CVE-2022-1537]">
<correction gss "Beschädigt+Ersetzt hinzugefügt: libgss0 (&lt;&lt; 0.1)">
<correction haskell-hakyll "Neukompilierung gegen neues pandoc">
<correction haskell-pandoc-citeproc "Neukompilierung gegen neues pandoc">
<correction hnswlib "Doppel-Free in init_index behoben, das auftrat, wenn das M-Argument eine große Ganzzahl ist [CVE-2023-37365]">
<correction horizon "Offene Weiterleitung geschlossen [CVE-2022-45582]">
<correction inetutils "Rückgabewerte der set*id()-Funktionen kontrollieren, um potenziellen Sicherheitsproblemen vorzubeugen [CVE-2023-40303]">
<correction krb5 "Free eines nicht initialisierten Zeigers abgestellt [CVE-2023-36054]">
<correction kscreenlocker "Authentifizierungsfehler bei Verwendung mit PAM behoben">
<correction lacme "Richtig mit CA ready-, processing- und valid-Zuständen umgehen">
<correction lapack "eigenvector-Matrix nachgebessert">
<correction lemonldap-ng "Offene Weiterleitung in dem Fall, dass OIDC RP keine Weiterleitungs-URIs hat, geschlossen; serverseitige Anfragefälschung behoben [CVE-2023-44469]; offene Weiterleitung wegen fehlerhaftem Umgang mit Escaping behoben">
<correction libapache-mod-jk "Funktionalität für implizites Mapping, die zu unbeabsichtigter Freilegung des Status-Workers und/oder Umgehung der Sicherheitsbeschränkungen führen kann, entfernt [CVE-2023-41081]">
<correction libbsd "Endlosschleife in MD5File behoben">
<correction libclamunrar "Neue stabile Version der Originalautoren">
<correction libprelude "Python-Modul verwendbar gemacht">
<correction libreswan "Dienstblockade behoben [CVE-2023-30570]">
<correction libsignal-protocol-c "Ganzzahlüberlauf behoben [CVE-2022-48468]">
<correction linux "Neue stabile Version der Originalautoren">
<correction linux-signed-amd64 "Neue stabile Version der Originalautoren">
<correction linux-signed-arm64 "Neue stabile Version der Originalautoren">
<correction linux-signed-i386 "Neue stabile Version der Originalautoren">
<correction logrotate "Ersetzung von /dev/null mit regulärer Datei, wenn sie für das State-File benutzt wird, vermeiden">
<correction ltsp "Verwendung von <q>mv</q> auf Init-Symlink vermeiden, um Problem mit overlayfs aus dem Weg zu gehen">
<correction lttng-modules "Kompilierungsprobleme mit neueren Kernel-Versionen behoben">
<correction lua5.3 "Use-after-free in lua_upvaluejoin (lapi.c) behoben [CVE-2019-6706]; Speicherzugriffsfehler in getlocal und setlocal (ldebug.c) abgestellt [CVE-2020-24370]">
<correction mariadb-10.5 "Neue Fehlerkorrektur-Veröffentlichung der Originalautoren [CVE-2022-47015]">
<correction mujs "Sicherheitskorrektur">
<correction ncurses "Laden von eigenen terminfo-Einträgen in setuid/setgid-Programmen verboten [CVE-2023-29491]">
<correction node-css-what "Auf regulären Ausdrücken basierende Dienstblockade abgestellt [CVE-2022-21222 CVE-2021-33587]">
<correction node-json5 "Prototype Pollution beseitigt [CVE-2022-46175]">
<correction node-tough-cookie "Sicherheitskorrektur: Prototype Pollution [CVE-2023-26136]">
<correction nvidia-graphics-drivers "Neue Veröffentlichung durch die Originalautoren [CVE-2023-25515 CVE-2023-25516]; Kompatiblität mit neueren Kerneln verbessert">
<correction nvidia-graphics-drivers-tesla-450 "Neue Veröffentlichung durch die Originalautoren [CVE-2023-25515 CVE-2023-25516]">
<correction nvidia-graphics-drivers-tesla-470 "Neue Fehlerkorrektur-Veröffentlichung der Originalautoren [CVE-2023-25515 CVE-2023-25516]">
<correction openblas "Ergebnisse von DGEMM auf AVX512-fähiger Hardware, wenn das Paket auf vof-AVX2-Geräten kompiliert wurde, korrigiert">
<correction openssh "Codeausführung aus der Ferne durch einen weitergeleiteten Agent-Socket unterbunden [CVE-2023-38408]">
<correction openssl "Neue stabile Version der Originalautoren; Dienstblockade behoben [CVE-2023-3446 CVE-2023-3817]">
<correction org-mode "Anfälligkeit für Befehlsinjektion korrigiert [CVE-2023-28617]">
<correction pandoc "Eigenmächtige schreibende Dateizugriffe unterbunden [CVE-2023-35936 CVE-2023-38745]">
<correction pev "Pufferüberlauf abgestellt [CVE-2021-45423]">
<correction php-guzzlehttp-psr7 "Unzureichende Eingabeüberprüfung verbessert [CVE-2023-29197]">
<correction php-nyholm-psr7 "Unzureichende Eingabeüberprüfung verbessert [CVE-2023-29197]">
<correction postgis "Regression bei Achsenreihenfolge entfernt">
<correction protobuf "Sicherheitskorrekturen: Dienstblockade in Java [CVE-2021-22569]; Nullzeiger-Dereferenzierung [CVE-2021-22570]; Speicher-Dienstblockade [CVE-2022-1941]">
<correction python2.7 "<q>parameter cloaking</q> (Parameter-Verbergen) [CVE-2021-23336], URL-Injektion [CVE-2022-0391], Use-after-free [CVE-2022-48560], XML External Entity [CVE-2022-48565] behoben; zeitlich konstante Vergleiche in compare_digest() überarbeitet [CVE-2022-48566]; URL-Verarbeitung verbessert [CVE-2023-24329]; Lesen von unauthentifizierten Daten auf einem SSL-Socket verhindert [CVE-2023-40217]">
<correction qemu "Endlosschleife behoben [CVE-2020-14394], Nullzeiger-Dereferenzierung [CVE-2021-20196], Ganzzahlüberlauf [CVE-2021-20203], Pufferüberlauf [CVE-2021-3507 CVE-2023-3180], Dienstblockade [CVE-2021-3930 CVE-2023-3301], Use-after-free [CVE-2022-0216], mögliche Ganzzahlüberläufe und Use-after-free [CVE-2023-0330], Lesezugriff außerhalb der Grenzen [CVE-2023-1544]">
<correction rar "Neue Veröffentlichung durch die Originalautoren; Verzeichnisüberschreitung behoben [CVE-2022-30333]; Eigenmächtige Codeausführung unterbunden [CVE-2023-40477]">
<correction rhonabwy "aesgcm-Pufferüberlauf behoben [CVE-2022-32096]">
<correction roundcube "Neue stabile Version der Originalautoren; seitenübergreifendes Skripting unterbunden [CVE-2023-43770]; Enigma: Erstsynchronisierung der privaten Schlüssel korrigiert">
<correction rust-cbindgen "Neue Version der Originalautoren, um die Kompilierung neuerer firefox-esr-Versionen zu unterstützen">
<correction rustc-mozilla "Neue Version der Originalautoren, um die Kompilierung neuerer firefox-esr-Versionen zu unterstützen">
<correction schleuder "Versionierte Abhängigkeit von ruby-activerecord hinzugeügt">
<correction sgt-puzzles "Verschiedene Sicherheitsprobleme beim Laden von Spielen behoben [CVE-2023-24283 CVE-2023-24284 CVE-2023-24285 CVE-2023-24287 CVE-2023-24288 CVE-2023-24291]">
<correction spip "Mehrere Sicherheitskorrekturen; Sicherheitskorrektur für erweiterte Authentifizierungsdaten-Filterung">
<correction spyder "Fehlerhafte Korrektur aus vorheriger Aktualisierung nachgebessert">
<correction systemd "Udev: Erstellung von symbolischen Links für USB-Geräte unter /dev/serial/by-id/ überarbeiet; Speicherleck beim Neuladen des Daemons geflickt; Hänger beim Berechnen der Kalenderdaten behoben, der beim Sommerzeit-/Winterzeitwechsel in der Zeitzone Europe/Dublin auftrat">
<correction tang "Race Condition beim Erzeugen/Rotieren von Schlüsseln behoben; strenge Berechtigungen auf das Schlüsselverzeichnis anwenden [CVE-2023-1672]; tangd-rotate-keys ausführbar machen">
<correction testng7 "Für künftige openjdk-17-Kompilate nach Oldstable zurückportiert">
<correction tinyssh "Problemumgehung für eingehenden Pakete, die sich nicht an die Maximallänge halten">
<correction unrar-nonfree "Problem mit Dateiüberschreitung behoben [CVE-2022-48579]; Codeausführung aus der Ferne abgestellt [CVE-2023-40477]">
<correction xen "Neue stabile Version der Originalautoren; Sicherheitsprobleme behoben [CVE-2023-20593 CVE-2023-20569 CVE-2022-40982]">
<correction yajl "Sicherheitskorrektur für Speicherleck; Sicherheitskorrekturen: potenzielle Dienstblockade mit speziell angefertigter JSON-Datei [CVE-2017-16516]; Heap-Speicher-Beschädigung beim Umgang mit großen (~2GB) Eingaben [CVE-2022-24795]; unvollständige Korrektur für CVE-2023-33460 fertiggestellt">
</table>


<h2>Sicherheitsaktualisierungen</h2>

<p>Diese Revision fügt der Oldstable-Veröffentlichung die folgenden
Sicherheitsaktualisierungen hinzu. Das Sicherheitsteam hat bereits für
jede davon eine Ankündigung veröffentlicht:</p>


<table border=0>
<tr><th>Ankündigungs-ID</th>  <th>Paket</th></tr>
<dsa 2023 5394 ffmpeg>
<dsa 2023 5395 nodejs>
<dsa 2023 5396 evolution>
<dsa 2023 5396 webkit2gtk>
<dsa 2023 5397 wpewebkit>
<dsa 2023 5398 chromium>
<dsa 2023 5399 odoo>
<dsa 2023 5400 firefox-esr>
<dsa 2023 5401 postgresql-13>
<dsa 2023 5402 linux-signed-amd64>
<dsa 2023 5402 linux-signed-arm64>
<dsa 2023 5402 linux-signed-i386>
<dsa 2023 5402 linux>
<dsa 2023 5403 thunderbird>
<dsa 2023 5404 chromium>
<dsa 2023 5405 libapache2-mod-auth-openidc>
<dsa 2023 5406 texlive-bin>
<dsa 2023 5407 cups-filters>
<dsa 2023 5408 libwebp>
<dsa 2023 5409 libssh>
<dsa 2023 5410 sofia-sip>
<dsa 2023 5411 gpac>
<dsa 2023 5412 libraw>
<dsa 2023 5413 sniproxy>
<dsa 2023 5414 docker-registry>
<dsa 2023 5415 libreoffice>
<dsa 2023 5416 connman>
<dsa 2023 5417 openssl>
<dsa 2023 5418 chromium>
<dsa 2023 5419 c-ares>
<dsa 2023 5420 chromium>
<dsa 2023 5421 firefox-esr>
<dsa 2023 5422 jupyter-core>
<dsa 2023 5423 thunderbird>
<dsa 2023 5424 php7.4>
<dsa 2023 5426 owslib>
<dsa 2023 5427 webkit2gtk>
<dsa 2023 5428 chromium>
<dsa 2023 5430 openjdk-17>
<dsa 2023 5431 sofia-sip>
<dsa 2023 5432 xmltooling>
<dsa 2023 5433 libx11>
<dsa 2023 5434 minidlna>
<dsa 2023 5435 trafficserver>
<dsa 2023 5436 hsqldb1.8.0>
<dsa 2023 5437 hsqldb>
<dsa 2023 5438 asterisk>
<dsa 2023 5439 bind9>
<dsa 2023 5440 chromium>
<dsa 2023 5441 maradns>
<dsa 2023 5442 flask>
<dsa 2023 5443 gst-plugins-base1.0>
<dsa 2023 5444 gst-plugins-bad1.0>
<dsa 2023 5445 gst-plugins-good1.0>
<dsa 2023 5446 ghostscript>
<dsa 2023 5447 mediawiki>
<dsa 2023 5449 webkit2gtk>
<dsa 2023 5450 firefox-esr>
<dsa 2023 5451 thunderbird>
<dsa 2023 5452 gpac>
<dsa 2023 5453 linux-signed-amd64>
<dsa 2023 5453 linux-signed-arm64>
<dsa 2023 5453 linux-signed-i386>
<dsa 2023 5453 linux>
<dsa 2023 5455 iperf3>
<dsa 2023 5456 chromium>
<dsa 2023 5457 webkit2gtk>
<dsa 2023 5459 amd64-microcode>
<dsa 2023 5461 linux-signed-amd64>
<dsa 2023 5461 linux-signed-arm64>
<dsa 2023 5461 linux-signed-i386>
<dsa 2023 5461 linux>
<dsa 2023 5463 thunderbird>
<dsa 2023 5464 firefox-esr>
<dsa 2023 5465 python-django>
<dsa 2023 5467 chromium>
<dsa 2023 5468 webkit2gtk>
<dsa 2023 5470 python-werkzeug>
<dsa 2023 5471 libhtmlcleaner-java>
<dsa 2023 5472 cjose>
<dsa 2023 5473 orthanc>
<dsa 2023 5474 intel-microcode>
<dsa 2023 5475 linux-signed-amd64>
<dsa 2023 5475 linux-signed-arm64>
<dsa 2023 5475 linux-signed-i386>
<dsa 2023 5475 linux>
<dsa 2023 5476 gst-plugins-ugly1.0>
<dsa 2023 5478 openjdk-11>
<dsa 2023 5479 chromium>
<dsa 2023 5480 linux-signed-amd64>
<dsa 2023 5480 linux-signed-arm64>
<dsa 2023 5480 linux-signed-i386>
<dsa 2023 5480 linux>
<dsa 2023 5481 fastdds>
<dsa 2023 5482 tryton-server>
<dsa 2023 5483 chromium>
<dsa 2023 5484 librsvg>
<dsa 2023 5485 firefox-esr>
<dsa 2023 5486 json-c>
<dsa 2023 5487 chromium>
<dsa 2023 5489 file>
<dsa 2023 5490 aom>
<dsa 2023 5491 chromium>
<dsa 2023 5493 open-vm-tools>
<dsa 2023 5494 mutt>
<dsa 2023 5495 frr>
<dsa 2023 5497 libwebp>
<dsa 2023 5500 flac>
<dsa 2023 5502 xorgxrdp>
<dsa 2023 5502 xrdp>
<dsa 2023 5503 netatalk>
<dsa 2023 5504 bind9>
<dsa 2023 5505 lldpd>
<dsa 2023 5507 jetty9>
<dsa 2023 5510 libvpx>
</table>

<h2>Entfernte Pakete</h2>

<p>Die folgenden Pakete wurden wegen Umständen entfernet, die sich unserer Kontrolle entziehen:</p>

<table border=0>
<tr><th>Paket</th>               <th>Grund</th></tr>
<correction atlas-cpp "Version der Originalautoren zu unbeständig für Debian">
<correction ember-media "Version der Originalautoren zu unbeständig für Debian">
<correction eris "Version der Originalautoren zu unbeständig für Debian">
<correction libwfut "Version der Originalautoren zu unbeständig für Debian">
<correction mercator "Version der Originalautoren zu unbeständig für Debian">
<correction nomad "Sicherheitskorrekturen nicht länger verfügbar">
<correction nomad-driver-lxc "hängt von nomad ab, das entfernt wird">
<correction skstream "Version der Originalautoren zu unbeständig für Debian">
<correction varconf "Version der Originalautoren zu unbeständig für Debian">
<correction wfmath "Version der Originalautoren zu unbeständig für Debian">
</table>

h2>Debian-Installer</h2>

<p>Der Installer wurde aktualisiert, damit er die Korrekturen enthält,
die mit dieser Zwischenveröffentlichung in Oldstable eingeflossen sind.</p>

<h2>URLs</h2>

<p>Die vollständige Liste von Paketen, die sich mit dieser Revision geändert haben:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>Die derzeitige Oldstable-Distribution:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/oldstable/">
</div>

<p>Vorgeschlagene Aktualisierungen für die Oldstable-Distribution:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/oldstable-proposed-updates">
</div>

<p>Informationen zur Oldstable-Distribution (Veröffentlichungshinweise, Errata usw.):</p>

<div class="center">
  <a
  href="$(HOME)/releases/oldstable/">https://www.debian.org/releases/oldstable/</a>
</div>

<p>Sicherheitsankündigungen und -informationen:</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>Über Debian</h2>

<p>Das Debian-Projekt ist ein Zusammenschluss von Entwicklern
Freier Software, die ihre Zeit und Mühen einbringen, um das
vollständig freie Betriebssystem Debian zu erschaffen.</p>

<h2>Kontaktinformationen</h2>

<p>Für weitere Informationen besuchen Sie bitte die Debian-Website unter
<a href="$(HOME)/">https://www.debian.org/</a>, schicken Sie eine Mail
(auf Englisch) an &lt;press@debian.org&gt; oder kontaktieren Sie das
Oldstable-Veröffentlichungs-Team (auf Englisch)
unter &lt;debian-release@lists.debian.org&gt;.</p>
