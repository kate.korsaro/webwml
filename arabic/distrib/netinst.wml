#use wml::debian::template title="تثبيت دبيان من خلال الإنترنت" BARETITLE=true
#use wml::debian::toc
#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/releases/images.data"
#use wml::debian::translation-check translation="65bd97b2cb00e0ef96c0166b86b98831e72e6592"

<p>
تتطلب هذه الطريقة توفر اتصال إنترنت <em>خلال</em> عملية التثبيت.
خلافا للطرق الأخرى ستنزِّل بيانات أقل وفقا لحتياجاتك. كل من الاتصال
السلكي واللاسلكي مدعومان. بطاقات ISDN الداخلية <em>ليست</em> مدعومة للأسف.
</p>
<p>هناك ثلاث خيارات للتثبيت من الشبكة:</p>

<toc-display />
<div class="line">
<div class="item col50">

<toc-add-entry name="smallcd">
أقراص مدمجة صغيرة أو مفاتيح الـ USB
</toc-add-entry>

<p>
الملفات التالية هي صور أقراص.
اختر البُنية المناسبة لمعالجك أدناه.
</p>

<stable-netinst-images />
</div>
<div class="clear"></div>
</div>

<p>لمزيد من التفاصيل، يرجى مراجعة: <a href="../CD/netinst/">التثبيت الشبكي من القرص القياسي</a></p>

<div class="line">
<div class="item col50">

<toc-add-entry name="verysmall">
أقراص مدمجة جد صغيرة و مفاتيح الـ USB مخصصة إلخ.
</toc-add-entry>

<p>
يمكنك تنزيل بعض ملفات الصور صغيرة الحجم، مناسبة
لمفاتيح الـ USB والأجهزة المماثلة، يجب حرقها على الوسيط
والإقلاع من خلاله لبدء التثبيت.
</p>

<p>
هناك بعض الاختلافات بين البنُى فيما يخص دعم التثبيت من الأقراص الصغيرة.
</p>

<p>
لمزيد من التفاصيل، يُرجى الرجوع إلى
<a href="$(HOME)/releases/stable/installmanual">كُتيّب التثبيت الخاص بالبُنية</a>،
وخاصة الفصل
<q>الحصول على وسائط تثبيت النظام</q>.
</p>

<p>
هذه روابط ملفات الصور المتوفرة (لمزيد من المعلومات، يرجى قراءة الملف MANIFEST).
</p>

<stable-verysmall-images />
</div>
<div class="item col50 lastcol">

<toc-add-entry name="netboot">الإقلاع من الشبكة</toc-add-entry>

<p>
يمكنك إعداد خادوم TFTP و DHCP (أو BOOTP أو RARP) على شبكتك المحلية
ليعمل كوسيط للتثبيت. إن كان بيوس (BIOS) الأجهزة العميلة يدعم هذه الميزة،
يمكنك إذن إقلاع نظام تثبيت دبيان من خلال الشبكة
(باستخدام PXE و TFTP) ثم المضي قُدما في تثبيت ما تبقى من دبيان.
</p>

<p>
ليست كل الأجهزة تدعم الإقلاع من الشبكة.
لا يوصى بهذه الطريقة للمستخدمين المبتدئين بسبب الإعدادات الإضافية التي تتطلبها.
</p>

<p>
لمزيد من التفاصيل، يرجى الرجوع إلى
<a href="$(HOME)/releases/stable/installmanual">كُتيّب التثبيت الخاص بالبُنية</a>،
وخاصة الفصل
<q>إعداد الملفات للإقلاع من خلال الشبكة باستخدام TFTP</q>.
</p>
<p>
هذه روابط لملفات الصور (لمزيد من المعلومات، يرجى قراءة الملف MANIFEST):
</p>

<stable-netboot-images />
</div>
</div>
