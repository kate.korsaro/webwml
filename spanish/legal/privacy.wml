#use wml::debian::template title="Política de privacidad" NOCOMMENTS="yes"
#use wml::debian::translation-check translation="3c6ffb2bcd885b73ca6311b72caa3ab54fdaeea1" maintainer="Alfredo Quintero"

<p><strong>NOTA:</strong> Esta es una traducción de la política de privacidad
del proyecto Debian, y por lo tanto no tiene ningún valor jurídico. Si desea
ver la versión original debe acudir a la <a href="/legal/privacy.en.html">página original</a>
en este mismo sitio web.

<p>El <a href="https://www.debian.org/">proyecto Debian</a> es una asociación
voluntaria de individuos que han hecho causa común para crear un sistema
operativo libre, referido como Debian.</p>

<p>No es necesario que nadie que desee usar Debian proporcione al proyecto 
ninguna información personal; se puede descargar libremente sin registrarse,
o cualquier otra forma de identificación, desde las réplicas oficiales gestionadas
por el proyecto y numerosas terceras personas.</p>

<p>Sin embargo, otros aspectos de la interacción con el proyecto Debian
involucrarán la recolección de información personal. Se trata principalmente
de nombres y direcciones de correo electrónico en correos electrónicos recibidos
por el proyecto; todas las listas de correo de Debian están archivadas públicamente,
como todas las interacciones con el sistema de seguimiento de fallos. Esto
está en consonancia con nuestro <a href="https://www.debian.org/social_contract">contrato social</a>,
en particular nuestra declaración de que apoyaremos a la comunidad de software
libre (#2), y que no ocultaremos nuestros problemas (#3). No realizamos
ningún procesamiento posterior de la información que manejamos, pero hay
instancias en donde es automáticamente compartida con terceras personas 
(tales como correos electrónicos a listas de correo o interacciones con el
sistema de seguimiento de fallos).</p>

<p>La lista siguiente clasifica los diferentes servicios manejados por el
proyecto, la información utilizada por esos servicios y las razones por la
que es requerida.</p>

<p>Por favor, tenga en cuenta que los hosts y servicios bajo el dominio
<strong>debian.net</strong> no son parte oficial del proyecto Debian; son
gestionados por individuos que tienen alguna relación con el proyecto más que
una participación directa en él. Las preguntas acerca de exactamente
qué datos manejan esos servicios deben dirigirse a los propietarios de los
servicios en lugar de al proyecto Debian.</p>

<h2>Contribuidores (<a href="https://contributors.debian.org/">contributors.debian.org</a>)</h2>

<p>El sitio de los contribuidores a Debian proporciona una agregación de
datos acerca de dónde alguien ha contribuido con el proyecto Debian, ya 
sea a través de la presentación de un reporte de fallos, haciendo una carga 
al archivo, publicar en una lista de correo o diferentes interacciones con
el proyecto. Recibe su información desde los servicios en cuestión (los 
detalles acerca de un identificador, tales como un nombre de inicio de sesión
y la fecha de la última contribución) y proporciona un único punto de referencia
para ver dónde el proyecto está almacenando la información acerca de un 
individuo.</p>

<h2>El archivo (<a href="https://ftp.debian.org/debian/">ftp.debian.org</a>)</h2>

<p>El principal método de distribución de Debian es a través de su red pública
de archivos. El archivo consiste en todos los paquetes binarios y su código
fuente asociado, que incluye información personal en la forma de nombres y
direcciones de correo electrónico almacenada como parte de los registros
de cambio, información de derechos de autor y documentación general. La
mayoría de esta información es proporcionada a través del código fuente 
distribuido por los autores del software original, con Debian añadiendo
información adicional para rastrear la autoría y los derechos de autor, 
para asegurarse de que las licencias están siendo documentadas correctamente
y que se cumplen las directrices de software libre de Debian.</p>

<h2>El sistema de seguimiento de fallos (<a href="https://bugs.debian.org/">bugs.debian.org</a>)</h2>

<p>Se interactúa con el sistema de seguimiento de fallos a través de correo
electrónico y almacena todos los correos electrónicos recibidos con relación
a un fallo, como parte de la historia de ese fallo. Con la finalidad de 
que el proyecto pueda lidiar con los problemas encontrados en la distribución
y permitir a los usuarios ver detalles acerca de esos problemas y si hay
una corrección o solución disponible, la totalidad del sistema de seguimiento
de fallos es abiertamente accesible. Por lo tanto, cualquier información,
incluyendo nombres y direcciones de correo electrónico como parte de cabeceras
de correo electrónico, enviada al sistema de seguimiento de fallos será 
archivada y estará públicamente disponible.</p>

<h2>DebConf (<a href="https://www.debconf.org/">debconf.org</a>)</h2>

<p>La estructura de registro de DebConf almacena los detalles de los asistentes
a la conferencia. Son requeridos para determinar la elegibilidad para becas,
relación con el proyecto, y para contactar a los asistentes con la información
adecuada. También pueden ser compartidos con los proveedores de la conferencia,
por ejemplo, el nombre y la fecha de asistencia de los asistentes que se 
queden en el alojamiento proporcionado por la conferencia serán compartidos
con el proveedor de alojamiento.</p>

<h2>LDAP de desarrolladores (<a href="https://db.debian.org">db.debian.org</a>)</h2>

<p>Los contribuidores al proyecto (desarrolladores y otras personas con cuenta de invitado) que
tienen cuenta de acceso a máquinas dentro de la infraestructura de Debian tienen
sus detalles almacenados dentro de la infraestructura LDAP del proyecto. Esta
almacena principalmente el nombre, nombre de usuario e información de autenticación.
Sin embargo también tiene la facilidad opcional para los contribuidores de proporcionar
información adicional como género, cuentas de mensajería instantánea (IRC/XMPP), país y
detalles de domicilio o teléfono, y un mensaje a mostrar si están de vacaciones.
</p>

<p>El nombre, nombre de usuario y algunos detalles proporcionados voluntariamente
están abiertamente disponibles a través de la interfaz web o una búsqueda
en el LDAP. Los detalles adicionales son compartidos únicamente con otros
individuos que tienen cuentas de acceso a la infraestructura de Debian, y
tiene la finalidad de proporcionar una ubicación centralizada para los miembros
del proyecto para intercambiar tal información de contacto. No se recolecta
explícitamente en ningún momento y puede ser removida iniciando sesión en
la interfaz web de db.debian.org o enviando un correo electrónico firmado
a la interfaz de correo electrónico. Véase <a href="https://db.debian.org/">https://db.debian.org/</a> y
<a href="https://db.debian.org/doc-general.html">https://db.debian.org/doc-general.html</a> para más detalles.</p>

<h2>Gitlab (<a href="https://salsa.debian.org/">salsa.debian.org</a>)</h2>

<p>salsa.debian.org proporciona una instancia de la herramienta de manejo
de ciclo de vida DevOps de <a href="https://about.gitlab.com/">GitLab</a>.
Es utilizada principalmente por el proyecto para permitir que los contribuidores
del proyecto alojen repositorios de software usando Git y fomentar la colaboración
entre contribuidores. Como resultado, requiere varias piezas de información
personal para gestionar las cuentas. Para miembros del proyecto, esto está
atado al sistema LDAP central de Debian, pero los invitados también pueden
registrar una cuenta y tendrán que proporcionar sus nombres y detalles
de correo electrónico con la finalidad de facilitar la configuración y uso
de la cuenta.</p>

<p>Debido a la naturaleza técnica de las contribuciones de git a los repositorios
de git mantenidos en salsa, los commits de git contendrán el nombre y la 
direcciones de correo electrónico. La naturaleza encadenada del sistema 
git significa que cualquier modificación a los detalles de estos commits,
una vez incorporados a los repositorios, son extremadamente disruptivos 
y en algunos casos (como cuando se usan commits firmados) imposibles.</p>

<h2>Gobby (<a href="https://gobby.debian.org/">gobby.debian.org</a>)</h2>

<p>Gobby es un editor de texto colaborativo en línea que rastrea contribuciones
y cambios en relación con los usuarios conectados. No se requiere de autenticación
para conectarse al sistema y los usuarios pueden escoger cualquier nombre
de usuario que deseen. Sin embargo, mientras que el servicio no realiza 
ningún intento de rastrear a quién pertenecen los nombres de usuario, debe
entenderse que puede identificarse a qué persona corresponde
cada nombre de usuario basándose en el uso común de ese nombre de usuario 
o en el contenido que pudiera haber publicado esa persona en algún documento
colaborativo dentro del sistema.</p>

<h2>Listas de correo (<a href="https://lists.debian.org/">lists.debian.org</a>)</h2>

<p>Las listas de correo son el mecanismo primario de comunicación del proyecto
Debian. Casi todas las listas de correo relacionadas con el proyecto están
abiertas y, por ende, disponibles para cualquier que quiera leer o publicar
en ellas. Todas las listas también están archivadas; para listas públicas
esto significa que son accesibles a través de la web. Esto cumple con el compromiso
de transparencia del proyecto, y ayuda a nuestros usuarios y desarrolladores
a entender lo que está sucediendo en el proyecto o las razones históricas
de ciertos aspectos del proyecto. Debido a la naturaleza del correo electrónico
estos archivos pueden por tanto contener información personal, como nombres y
direcciones de correo.</p>

<h2>El sitio de los nuevos miembros (<a href="https://nm.debian.org/">nm.debian.org</a>)</h2>

<p>Los contribuidores del proyecto Debian que deseen formalizar su participación
pueden solicitarlo a través del proceso de nuevos miembros. Esto les permite
ganar la capacidad de subir sus propios paquetes (haciéndose responsables o «mantenedores» 
de dichos paquetes) o convertirse en miembros del proyecto con derecho a voto y con
derechos de cuenta (desarrolladores de Debian, en sus variantes de subida
y no-subida). Como parte de este proceso varios datos personales son 
recolectados, comenzando por el nombre, dirección de correo electrónico y
detalles sobre su llave de firma/cifrado. El proceso completo de la solicitud también
requiere que el solicitante mantenga una conversación
con un gestor de solicitud, con quien mantendrá una conversación de correo
electrónico, para asegurarse de que el nuevo miembro entiende los principios
detrás de Debian y tiene las capacidades apropiadas para interactuar con la
infraestructura del proyecto. Esta conversación de correo electrónico está
archivada y disponible para el solicitante y los gestores de solicitud 
a través de la interfaz de nm.debian.org. En el sitio son públicamente visibles,
también, detalles adicionales de los solicitantes cuya solicitud todavía
no se ha resuelto, permitiendo que cualquiera
pueda ver el estado del proceso de nuevo miembro dentro del proyecto para
asegurar un nivel de transparencia apropiado.</p>

<h2>Concurso de popularidad (<a href="https://popcon.debian.org/">popcon.debian.org</a>)</h2>

<p>«popcon» rastrea qué paquetes están instalados en un sistema  Debian 
para permitir la recopilación de estadísticas sobre qué paquetes se usan 
ampliamente y cuáles ya no se usan. Utiliza el paquete opcional «popularity-contest» 
para recopilar esta información, lo que requiere un consentimiento explícito 
para hacerlo. Esto proporciona una orientación útil sobre dónde dedicar 
recursos de desarrollador, por ejemplo, al migrar a versiones más recientes 
de la librería y tener que gastar esfuerzo en la portabilidad de aplicaciones 
más antiguas. Cada instancia de popcon genera un ID aleatorio de 128 bits 
único que se utiliza para realizar un seguimiento de los envíos desde el
mismo host. No se hace ningún intento de mapear esto a un individuo sobre 
los envíos que se hacen por correo electrónico o HTTP y, por lo tanto, es 
posible que la información personal se filtre en la forma de la dirección 
IP utilizada para el acceso o las cabeceras de correo electrónico. Esta 
información sólo está disponible para los administradores del sistema Debian 
y los administradores de popcon; todos estos metadatos se eliminan antes 
de que los envíos sean accesibles para el proyecto en su conjunto. Sin embargo, 
los usuarios deben ser conscientes de que las firmas únicas de los paquetes 
(como los paquetes creados localmente o los paquetes con un número muy bajo 
de instalaciones) pueden hacer que las máquinas sean deducibles como pertenecientes 
a individuos particulares.</p>

<p>Los envíos son almacenados en bruto durante 24 horas, para permitir
su reenvío en el caso de que haya problemas con los mecanismos del procesamiento.
Las solicitudes anonimizadas se mantienen durante un máximo de 20 días. Los
informes de resumen, que no contienen información personal identificable,
son mantenidos indefinidamente.</p>


<h2>Instantáneas (<a href="http://snapshot.debian.org/">snapshot.debian.org</a>)</h2>

<p>El archivo de instantáneas proporciona una vista histórica del archivo
de Debian (ftp.debian.org anterior), permitiendo el acceso a paquetes antiguos
en base a fechas y números de versión. No contiene información adicional 
sobre el archivo principal (y por lo tanto puede contener información personal 
en forma de nombres + dirección de correo electrónico en los registros de 
cambios, declaraciones de derechos de autor y otra documentación), pero puede 
contener paquetes que ya no forman parte de las publicaciones de Debian. 
Esto proporciona un recurso útil para los desarrolladores y usuarios a la 
hora de realizar seguimientos de regresiones en paquetes de software, o 
de proporcionar un entorno específico para ejecutar una aplicación en particular.</p>

<h2>Votaciones (<a href="https://vote.debian.org/">vote.debian.org</a>)</h2>

<p>El sistema de seguimiento de votos (devotee) hace un seguimiento del estado 
de las resoluciones generales en curso y de los resultados de las votaciones 
anteriores. En la mayoría de los casos esto significa que una vez que el 
período de votación ha terminado, los detalles de quién votó (nombres de 
usuario + mapeo de nombres) y cómo votó se hacen visibles públicamente. 
Sólo los miembros del proyecto son votantes válidos para los propósitos 
de devotee, y sólo los votos válidos son rastreados por el sistema.</p>

<h2>Wiki (<a href="https://wiki.debian.org/">wiki.debian.org</a>)</h2>

<p>La wiki de Debian proporciona un recurso de soporte y documentación para 
el proyecto, que es editable por todos. Como parte de esto, las contribuciones 
son rastreadas a lo largo del tiempo y asociadas con las cuentas de usuario 
en la wiki; cada modificación a una página es rastreada para permitir que 
las ediciones erróneas sean revertidas y que la información actualizada 
sea fácilmente examinada. Este seguimiento proporciona detalles del usuario 
responsable del cambio, que puede utilizarse para evitar abusos bloqueando 
a los usuarios abusivos o a las direcciones IP para que no realicen ediciones. 
Las cuentas de usuario también permiten a los usuarios suscribirse a las 
páginas para estar atentos a los cambios, o ver los detalles de los cambios 
en toda la wiki desde la última vez que la revisaron. En general, las cuentas 
de usuario llevan el nombre del usuario, pero no se realiza ninguna validación 
de los nombres de cuenta y el usuario puede elegir cualquier nombre de cuenta 
disponible. Se requiere una dirección de correo electrónico con el fin de 
suministrar un mecanismo para el restablecimiento de la contraseña de la 
cuenta  y notificar al usuario de cualquier cambio en las páginas a las 
que está suscrito.</p>

<h2>Echelon</h2>

<p>Echelon es un sistema utilizado por el proyecto para realizar seguimiento 
a la actividad de los miembros; en particular, vigila la lista de correo 
y las infraestructuras del archivo, buscando mensajes y cargas para registrar 
que un miembro de Debian está activo. Sólo se almacena la actividad más 
reciente en el registro LDAP del miembro. Por lo tanto, se limita a rastrear 
sólo los detalles de los individuos que tienen cuentas dentro de la infraestructura
de Debian. Esta información se utiliza para determinar si un miembro del 
proyecto está inactivo o desaparecido y, por tanto, si existe un requisito 
operativo para bloquear su cuenta o reducir sus permisos de acceso para 
garantizar la seguridad de los sistemas de Debian.</p>

<h2>Servicio relacionado con los registros</h2>

<p>Además de los servicios listados explícitamente arriba, la infraestructura 
de Debian registra detalles sobre los accesos al sistema con el fin de asegurar 
la disponibilidad y fiabilidad del servicio, y para permitir la depuración 
y el diagnóstico de problemas cuando surgen. Este registro incluye detalles 
de los correos enviados/recibidos a través de la infraestructura de Debian, 
solicitudes de acceso a páginas web enviadas a Debian e información de 
inicio de sesión para sistemas Debian (como inicios de sesión SSH en las 
máquinas del proyecto). Esta información no se utiliza para ningún otro fin 
que no sean necesidades operativas y sólo se almacena durante 15 días en 
el caso de los registros del servidor web, 10 días en el caso de los registros 
de correo y 4 semanas en el caso de los registros de autenticación/ssh.</p>
