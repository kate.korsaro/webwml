#use wml::debian::template title="Debian &ldquo;testing&rdquo; distribution" BARETITLE=true
#include "$(ENGLISHDIR)/releases/info"
#use wml::debian::translation-check translation="8900568ad5473cc15ea75f71f489b4f2b7ae659e"

<p>有关 testing 发行版的基本的、面向用户的信息，请参阅 <a 
href="$(DOC)/manuals/debian-faq/ftparchives#testing">Debian FAQ</a>。</p>

<p>对于常规用户和 testing 的开发人员，需要注意的重要一点是：testing 的安全更新\
<strong>不由安全团队管理</strong>。有关更多信息，请参见<a 
href="../security/faq#testing">安全团队 FAQ</a>。</p>

<p>本页面主要涵盖对 Debian 开发者来说重要的 <q>testing</q> 方面。</p>

<h2><q>testing</q> 是怎样工作的</h2>

<p><q>testing</q> 发行版是一个自动生成的发行版。它是由一组脚本从 <q>unstable</q> \
发行版生成的，这些脚本试图移动很可能没有致命缺陷（release-critical bugs）的软件包。\
他们这样做是为了确保始终可以满足 testing 中其它软件包的依赖关系。</p>

<p>一个（特定版本的）软件包将在满足以下所有条件后进入 testing：</p>

<ol>
  <li>它必须已经在 unstable 中存在 10、5 或 2 天，这取决于它上传时的紧急程度；</li>

  <li>它必须在之前 unstable 编译的所有体系架构上被编译且保持最新版本；</li>

  <li>它不能存在致命缺陷，这些错误也不适用于已在 <q>testing</q> 中的当前版本\
  （有关<a href="#faq">更多信息</a>，请参见下文）；</li>

  <li>它的所有依赖关系都必须由已在 <q>testing</q> 中的软件包满足，<em>或者</em>\
  由将要同时安装的一组软件包满足；</li>

  <li>将软件包安装到 <q>testing</q> 中的操作不得损坏当前在 <q>testing</q> 中的任何软件包。\
  （有关<a href="#faq">更多信息</a>，请参见下文。）</li>
</ol>

<p>满足以上前三个条件的软件包被称为 <q>有效候选者</q>。</p>

<p>更新脚本显示每个软件包何时可能从 <q>unstable</q> 移到 <q>testing</q>。输出是双重的：</p>

<ul>
  <li>The <a href="https://release.debian.org/britney/update_excuses.html">\
      update excuses</a>
      [<a href="https://release.debian.org/britney/update_excuses.html.gz">\
      gzipped</a>]:
      所有候选软件包版本的列表以及它们传入到 <q>testing</q> 的基本状态；该列表比\
      下面的更短和更好看
  </li>
  <li>The <a href="https://release.debian.org/britney/update_output.txt">\
      update output</a>
      [<a href="https://release.debian.org/britney/update_output.txt.gz">\
      gzipped</a>]:
      <q>testing</q> 脚本通过候选软件包递归时的完整而十分粗略的输出
  </li>
</ul>

<h2><a name="faq">常问问题解答</a></h2>

# Note to translators: these two first items are almost the same as
# https://www.debian.org/doc/manuals/developers-reference/pkgs.html#faq

<h3><q>什么是致命缺陷，它们是怎么被计数的？</q></h3>

<p>默认情况下，所有具有较高严重性的错误都被认为是<em><a 
href="https://bugs.debian.org/release-critical/">致命缺陷</a></em>；当前，这些是\
<strong>紧要</strong>和<strong>严重</strong>的错误。</p>

<p>此类错误被假定为会影响到其软件包随 Debian 的稳定发行版一起发布的机会：通常，如果一个\
软件包中存在被归于其下的打开的致命缺陷，它就不会移到 <q>testing</q>，也因此不会在 \
<q>stable</q> 发布。</p>

<p>
<q>testing</q> 缺陷计数是所有被标记为应用到在 <q>testing</q> 中发布的体系架构里可用的\
软件包/版本组合致命缺陷的数量。</p>

<h3><q>在 <q>testing</q> 中安装软件包可能会损坏其它软件包是怎么回事？</q></h3>

<p>分发档案的结构使得它们只能包含一个软件包的一个版本；一个软件包由其名称定义。\
因此，将源码包 <tt>acmefoo</tt> 伴随其二进制软件包 <tt>acme-foo-bin</tt>、\
<tt>acme-bar-bin</tt>、<tt>libacme-foo1</tt> 和 <tt>libacme-foo-dev</tt> 安装到 \
<q>testing</q> 时，其旧版本将被删除。</p>

<p>但是，该软件包的旧版本可能提供了一个带有其依赖库的旧 soname 的二进制软件包，例如 \
<tt>libacme-foo0</tt>。删除旧的 <tt>acmefoo</tt> 将会删除 <tt>libacme-foo0</tt>，\
这将损坏所有依赖它的软件包。</p>

<p>显然，这主要影响提供不同版本的二进制软件包集（反过来说，主要是依赖库）的软件包。\
但是，这还会影响已声明 ==, &lt;= 或 &lt;&lt; 变体的版本依赖项的软件包。</p>

<p>当一个源码包提供的二进制软件包集以这种方式更改时，所有依赖于旧二进制文件的软件包都\
必须更新为依赖于新二进制文件。因为在 <q>testing</q> 安装这样的源码包会破坏 <q>testing</q> \
中依赖它的所有软件包，所以现在必须格外小心：所有依赖的软件包都必须更新并可以自行安装，\
以免它们被损坏；以及，一切准备就绪后，通常需要发布管理员或助理进行手动干预。</p>

<p>如果您对像这样的复杂软件包组有疑问，请联系 debian-devel 或 debian-release 以获得帮助。</p>

<h3><q>我还是不懂！<q>testing</q> 脚本说这个软件包是一个有效候选者，但它还是\
没有移动到 <q>testing</q>。</q></h3>

<p>当以某种方式直接或间接地安装软件包会损坏某些其它的软件包时，往往会发生这种情况。</p>

<p>记得要考虑您的软件包的依赖。假设您的软件包依赖于 libtool 或 libltdl<var>X</var>，\
在准备好正确版本的 libtool 之前，您的软件包将不会移动到 <q>testing</q>。</p>

<p>反过来说，在安装 libtool 不会损坏已在 <q>testing</q> 中的东西前，移动到 <q>testing</q> \
的情况都不会发生。换句话说，直到依赖于 libltdl<var>Y</var>（其中 Y 是较早的版本）的所有其它\
软件包被重新编译，并且它们的所有致命缺陷都消失了，等等这些问题已被解决为止，所有这些软件包\
在此之前都不会进入 <q>testing</q>。</p>

<p>这是<a href="https://release.debian.org/britney/update_output.txt">\
文本形式输出</a>［<a href="https://release.debian.org/britney/update_output.txt.gz">
gzipped</a>］有用的地方：它提供了提示（尽管很简洁），提示在将有效候选者添加到 \
<q>testing</q> 时哪个软件包会损坏（有关更多详细信息，请参阅 <a
href="$(DOC)/manuals/developers-reference/pkgs.html#details">\
开发者参考手册</a>）。</p>

<h3><q>为什么有时难以将 <kbd>Architecture: all</kbd> 软件包移动到 <q>testing</q>？</q></h3>

<p>如果要安装 <kbd>Architecture: all</kbd> 软件包，则必须能够满足它在<strong>所有</strong>\
架构的依赖。如果它依赖于某些只能在 Debian 的特定体系结构上编译的软件包，那么它就不能移动\
到 <q>testing</q>。</p>

<p>不过，暂时而言，<q>testing</q> 会忽略 <kbd>Architecture: all</kbd> 软件包在非 i386 \
体系结构上的可安装性。（<q>这是一个非常 hack 的做法，我真的对此感到不高兴，但是您当然\
可以这样做。</q>&mdash;aj）</p>

<h3><q>因为我的软件包在某些体系结构上已过时，所以它停住了。我该怎么办？</q></h3>

<p>在<a href="https://buildd.debian.org/build.php">构建日志数据库</a>中检查您的软件包的\
状态。如果该软件包没有被构建出来，那它将被标记为 <em>failed</em>；调查构建日志并修复由您的\
软件包源代码引起的任何问题。</p>

<p>如果您偶然发现某些体系结构已经构建了您的软件包的新版本，但是未在 <q>testing</q> 脚本\
的输出中显示出来，那么您只需要耐心一点，等各个 buildd 维护者将文件上传到 Debian 软件\
档案库。</p>

<p>如果您注意到尽管您已上传了针对较早失败的修复代码，但某些体系架构还是没有构建您的软件包\
的新版本，那么原因可能是它被标记为等待依赖项（Dep-Wait）。您还可以查看这些被称为 \
<a href="https://buildd.debian.org/stats/">wanna-build 状态</a>的构建列表来确认。</p>

<p>这些问题通常最终会得到解决，但是如果您等待了较长的时间（例如两周或更长时间），请通知\
在<a href="$(HOME)/ports/">移植平台网页</a>上列出了地址的对应移植 buildd 维护者或\
移植平台的邮件列表。</p>

<p>如果您在 control 文件的体系架构列表中明确排除了某个架构，并且之前已为该架构构建了\
您的软件包，那么您需要请求在您的软件包能过渡到 testing 前从软件档案库中删除该架构的\
旧二进制软件包。您需要对 <q>ftp.debian.org</q> 提交一个错误报告，要求从 unstable 软件\
档案库中删除已排除的体系结构的软件包。一般来说，出于礼貌您应当在报告中告知相关的移植列表。</p>

<h3><q>有什么例外吗？我确定尽管 <tt>acmefoo</tt> 不满足所有要求，但它已进入 <q>testing</q>。</q></h3>

<p>发布管理员在两种情形下可以覆盖规则：</p>

<ul>
  <li>他们可以断定，由于安装新依赖库而导致的损坏将使事情变得更好而不是更糟，因此让其与\
  其依赖的软件包一起进入。</li>
  <li>他们也可以从 <q>testing</q> 中手动删除可能会损坏的软件包，以便可以安装新软件包。</li>
</ul>

<h3><q>你能否提供一个真实且重大的例子？</q></h3>

<p>这有一个例子：当源码包 <tt>apache</tt> 与其二进制软件包 <tt>apache</tt>、\
<tt>apache-common</tt>、<tt>apache-dev</tt> 和 <tt>apache-doc</tt> 一起安装到 \
<q>testing</q> 时，旧版本将被删除。</p>

<p>但是，由于所有 Apache 模块软件包都依赖于 <code>apache-common (&gt;=
<var>something</var>)、apache-common (&lt;&lt; <var>something</var>)</code>，所以这个\
更改将破坏所有这些依赖关系。因此，需要为新版本的 Apache 重新编译所有 Apache 模块，以便 \
<q>testing</q> 能够更新。</p>

<p>让我们再详细说明一下：在所有模块都更新到 unstable 以与新 Apache 一起工作后，\
<q>testing</q> 脚本尝试移动 apache-common，并发现由于模块有 <code>Depends: apache-common
(&lt;&lt; <var>the current version</var>)</code>，所以它破坏了所有 Apache 模块的依赖关系，\
接着尝试移动 <tt>libapache-<var>foo</var></tt> 并发现由于它有 <code>Depends: apache-common
(&gt;=<var>the new version</var>)</code> 所以不能被安装。</p>

<p>不过，接下来他们将使用不同的逻辑（有时需要人工干预）：他们将忽略 <tt>apache-common</tt> \
损坏东西的事实，并继续进行可行的工作；如果在我们竭尽所能后它仍然无法正常工作，那就太糟糕了，\
但也许它<strong>能</strong>正常工作。稍后，他们将尝试所有随机的 \
<tt>libapache-<var>foo</var></tt> 软件包，并发现它们确实可行。</p>

<p>在尝试了所有方法之后，他们将检查损坏了多少个软件包，计算它是比原始软件包好还是更坏，\
然后接受或忽略所有更新。您能在 <tt>update_output.txt</tt> 中的 <q><code>recur:</code></q> \
行看到此内容。</p>

<p>例如：</p>

<pre>
         recur: [<var>foo</var> <var>bar</var>] <var>baz</var>
</pre>

<p>基本上是说，<q>已经发现 <var>foo</var> 和 <var>bar</var> 使事情变得更好，我现在正在\
尝试 <var>baz</var> 看看会发生什么，即使那会损坏东西</q>。在 <tt>update_output.txt</tt> \
中以 <q><code>accepted</code></q> 为开头的行表示使事情变得更好的东西，\
<q><code>skipped</code></q> 行表示使事情变得更坏的东西。</p>

<h3><q><tt>update_output.txt</tt> 文件是完全不可读的！</q></h3>

<p>这不是问题。;-)</p>

<p>让我们举个例子：</p>

<pre>
 skipped: cln (0) (150+4)
     got: 167+0: a-40:a-33:h-49:i-45
     * i386: ginac-cint, libginac-dev
</pre>

<p>这意味着，如果 <tt>cln</tt> 进入 <q>testing</q>，那 <tt>ginac-cint</tt> 和 \
<tt>libginac-dev</tt> 会成为 i386 架构的 <q>testing</q> 中无法安装的软件包。请注意，\
脚本按字母顺序检查体系架构，并且仅显示第一个有问题的体系架构中的问题 &mdash; 这就是\
为何如此频繁地显示 alpha 架构的原因。</p>

<p><q>got</q> 行包括在不同体系架构（到第一个发现问题的体系架构 &mdash; 见上文）上的 \
<q>testing</q> 的问题数量。<q>i-45</q> 意味着如果 <tt>cln</tt> 能进入 <q>testing</q>，\
那在 i386 上会出现 45 个无法安装的软件包。<tt>cln</tt> 上方和下方的一些条目显示：\
此时在 i386 上的 <q>testing</q> 有 43 个无法安装的软件包。</p>

<p><q>skipped: cln (0) (150+4)</q> 行意味着在完成对所有软件包的检查之前，仍有 150 个\
软件包需要通过检查，并且已经找到了 4 个不打算更新的软件包，因为它们会破坏依赖。<q>(0)</q> \
是无关紧要的，您可以放心地忽略它。</p>

<p>请注意，在一个 <q>testing</q> 脚本运行中对所有软件包进行了多次检查。</p>

<p><em>Jules Bean 最初组织了常问问题和解答。</em></p>
# Created: Sat Dec  8 12:44:29 GMT 2001

<h2>附加信息</h2>

<p>以下页面提供了有关当前 testing 的状态以及软件包从 unstable 到 testing 的迁移的附加信息：</p>

<ul>
<li>过时二进制软件包的统计：
<a href="https://release.debian.org/britney/testing_outdate.txt">testing</a>,
<a href="https://release.debian.org/britney/stable_outdate.txt">stable</a>
<li>查看依赖问题：
<a href="https://qa.debian.org/debcheck.php?list=INDEX&amp;dist=testing">testing</a>,
<a href="https://qa.debian.org/debcheck.php?list=INDEX&amp;dist=stable">stable</a>
</ul>

<p>您可能有兴趣阅读旧的<a 
href="https://lists.debian.org/debian-devel-0008/msg00906.html">解释电子邮件</a>。\
它唯一的主要缺点是其没有考虑到软件包池，因为软件包池是由 James Troup 在该邮件被编写\
之后实现的。</p>

<p>testing 代码可从 \
<a href="https://release.debian.org/britney/update_out_code/">ftp-master</a> 取得。</p>

<p><em>Anthony Towns 因实现 testing 而获得赞誉。</em></p>
