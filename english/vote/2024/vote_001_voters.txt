-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
Count     Debian UID 	   Name 
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
    1           93sam	Steve McIntyre
    2             abe	Axel Beckert
    3        abhijith	Abhijith PA
    4           aboll	Andreas Boll
    5          absurd	Stephan Suerken
    6       adrianorg	Adriano Rafael Gomes
    7            adsb	Adam D. Barratt
    8             agi	Alberto Gonzalez Iniesta
    9             agx	Guido Guenther
   10           alexm	Alex Muntada
   11           alexp	Alex Pennace
   12            alfs	Stefan Alfredsson
   13        alteholz	Thorsten Alteholz
   14        amacater	Andrew Martin Adrian Cater
   15             ana	Ana Beatriz Guerrero López
   16         anarcat	Antoine Beaupré
   17            anbe	Andreas Beckmann
   18            andi	Andreas B. Mundt
   19           angel	Angel Abad
   20          anibal	Anibal Monsalve Salazar
   21           anupa	Anupa Ann Joseph
   22             apo	Markus Koschany
   23         arnaudr	Arnaud Rebillout
   24            aron	Aron Xu
   25         aurel32	Aurelien Jarno
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
Count     Debian UID 	   Name 
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
   26       awoodland	Alan Woodland
   27              az	Alexander Zangerl
   28          azazel	Jeremy Paul Arnold Sowden
   29        azekulic	Alen Zekulic
   30            bage	Bastian Germann
   31        ballombe	Bill Allombert
   32           bartm	Bart Martens
   33             bas	Bas Zoetekouw
   34          bbaren	Benjamin Barenblat
   35           bdale	Bdale Garbee
   36          bengen	Hilko Bengen
   37            benh	Ben Hutchings
   38          bernat	Vincent Bernat
   39            beuc	Sylvain Beucler
   40           biebl	Michael Biebl
   41           blade	Eduard Bloch
   42           bluca	Luca Boccassi
   43           bootc	Chris Boot
   44         broonie	Mark Brown
   45            bunk	Adrian Bunk
   46            bzed	Bernd Zeimetz
   47        calculus	Jerome Georges Benoit
   48          carnil	Salvatore Bonaccorso
   49           cavok	Domenico Andreoli
   50          cbiedl	Christoph Biedl
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
Count     Debian UID 	   Name 
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
   51       chronitis	Gordon Ball
   52        cjwatson	Colin Watson
   53           cklin	Chuan-kai Lin
   54           clint	Clint Adams
   55            cord	Cord Beermann
   56         coucouf	Aurélien Couderc
   57           cpina	Carles Pina i Estany
   58          crusoe	Michael Robin Crusoe
   59          csmall	Craig Small
   60             cts	Christian T. Steigies
   61           curan	Kai Wasserbäch
   62           cwryu	Changwoo Ryu
   63          czchen	ChangZhuo Chen
   64             dai	Daisuke Higuchi
   65          daissi	Dylan Aïssi
   66          daniel	Daniel Baumann
   67            dank	Nick Loren Black
   68           dapal	David Paleino
   69       debalance	Philipp Huebner
   70        deltaone	Patrick Franz
   71          dererk	Ulises Vitulli
   72             dkg	Daniel Kahn Gillmor
   73          dkogan	Dima Kogan
   74       dktrkranz	Luca Falavigna
   75          dlange	Daniel Lange
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
Count     Debian UID 	   Name 
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
   76             dmn	Damyan Ivanov
   77             dod	Dominique Dumont
   78         dogsleg	Lev Lamberov
   79         donkult	David Kalnischkies
   80        dparsons	Drew Parsons
   81       dtorrance	Douglas Andrew Torrance
   82            duck	Marc Dequènes
   83            dxld	Daniel Gröber
   84          eamanu	Emmanuel Arias
   85          ebourg	Emmanuel Bourg
   86             edd	Dirk Eddelbuettel
   87          eevans	Eric Evans
   88          elbrus	Paul Mathijs Gevers
   89             ema	Emanuele Rocca
   90        emollier	Étienne Mollier
   91         emorrp1	Phil Morrell
   92          enrico	Enrico Zini
   93            eric	Eric Dorland
   94           eriks	Erik Schanze
   95           eriol	Daniele Tricoli
   96           fabbe	Fabian Fagerholm
   97          fabian	Fabian Greffrath
   98             faw	Felipe Augusto van de Wiel
   99         florian	Florian Ernst
  100        formorer	Alexander Wirt
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
Count     Debian UID 	   Name 
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
  101         fpeters	Frederic Peters
  102        francois	Francois Marier
  103         frankie	Francesco Lovergine
  104            fsfs	Florian Schlichting
  105           fuddl	Bruno Kleinert
  106          gagath	Agathe Porte
  107         gaudenz	Gaudenz Steinlin
  108        georgesk	Georges Khaznadar
  109           ghedo	Alessandro Ghedini
  110        ghostbar	Jose Luis Rivas Contreras
  111          gibmat	Mathias Arthur Gibbens
  112             gio	Giovanni Mascellani
  113         giovani	Giovani Augusto Ferreira
  114           gladk	Anton Gladky
  115        glaubitz	John Paul Adrian Glaubitz
  116          glaweh	Henning Glawe
  117          glondu	Stéphane Glondu
  118          gniibe	NIIBE Yutaka
  119           gotom	Masanori Goto
  120          gregoa	Gregor Herrmann
  121            gris	Christoph Göhre
  122            gspr	Gard Spreemann
  123         guilhem	Guilhem Moulin
  124         guillem	Guillem Jover
  125          gusnan	Andreas Rönnquist
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
Count     Debian UID 	   Name 
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
  126            guus	Guus Sliepen
  127           gwolf	Gunnar Wolf
  128            haas	Christoph Haas
  129        hartmans	Sam Hartman
  130           hefee	Sandro Knauß
  131         helmutg	Helmut Grohne
  132         hertzog	Raphaël Hertzog
  133      hlieberman	Harlan Lieberman-Berg
  134             hmh	Henrique de Moraes Holschuh
  135         hoexter	Sven Hoexter
  136          holger	Holger Levsen
  137      hvhaugwitz	Hannes von Haugwitz
  138            ianw	Ian Wienand
  139       intrigeri	Intrigeri
  140           ivodd	Ivo De Decker
  141        iwamatsu	Nobuhiro Iwamatsu
  142             jak	Julian Andres Klode
  143         jaldhar	Jaldhar H. Vyas
  144             jan	Jan Niehusmann
  145           jandd	Jan Dittberner
  146          jaqque	John Robinson
  147          jbicha	Jeremy Bicha
  148             jcc	Jonathan Cristopher Carter
  149            jcfp	Jeroen Ploemen
  150             jdg	Julian Gilbey
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
Count     Debian UID 	   Name 
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
  151          jelmer	Jelmer Vernooij
  152          jipege	Jean-Pierre Giraud
  153          jlines	John Lines
  154             jmm	Moritz Muehlenhoff
  155            jmtd	Jonathan Dowland
  156           joerg	Joerg Jaspert
  157         joostvb	Joost van Baal
  158           josch	Johannes Schauer Marin Rodrigues
  159           josue	Josué Ortega
  160          jpuydt	Julien Puydt
  161              js	Jonas Smedegaard
  162        jspricke	Jochen Sprickerhof
  163       jvalleroy	James Valleroy
  164            kaol	Kari Pahula
  165          kartik	Kartik Mistry
  166          keithp	Keith Packard
  167          kenhys	HAYASHI Kentaro
  168            kibi	Cyril Brulebois
  169            knok	Takatsugu Nokubi
  170           kobla	Ondřej Kobližek
  171          koster	Kanru Chen
  172           krala	Antonin Kral
  173         kreckel	Richard Kreckel
  174            kula	Marcin Kulisz
  175           lamby	Chris Lamb
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
Count     Debian UID 	   Name 
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
  176           lange	Thomas Lange
  177         larjona	Laura Arjona Reina
  178        lavamind	Jerome Charaoui
  179        lawrencc	Christopher Lawrence
  180         ldrolez	Ludovic Drolez
  181         lechner	Felix Lechner
  182             lee	Mark Lee Garrett
  183          leepen	Mark Hindley
  184             leo	Carsten Leonhardt
  185        lightsey	John Lightsey
  186        lisandro	Lisandro Damián Nicanor Pérez Meyer
  187          lkajan	Laszlo Kajan
  188   locutusofborg	Gianfranco Costamagna
  189          ltworf	Salvo Tomaselli
  190           lucab	Luca Bruno
  191           lucas	Lucas Nussbaum
  192         lyknode	Baptiste Beauplat
  193             mak	Matthias Klumpp
  194            manu	Emmanuel Kasper Kasprzyk
  195           mattb	Matthew Brown
  196          mattia	Mattia Rizzolo
  197            maxy	Maximiliano Curia
  198         mbehrle	Mathias Behrle
  199              md	Marco d'Itri
  200       mechtilde	Mechtilde Stehmann
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
Count     Debian UID 	   Name 
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
  201            mejo	Jonas Meurer
  202          merker	Karsten Merker
  203          merkys	Andrius Merkys
  204          meskes	Michael Meskes
  205           metal	Marcelo Jorge Vieira
  206             mfv	Matteo F. Vescovi
  207             mhy	Mark Hymers
  208           micha	Micha Lenk
  209             mih	Michael Hanke
  210            mika	Michael Prokop
  211           milan	Milan Kupcevic
  212         mitya57	Dmitry Shachnev
  213        mjeanson	Michael Jeanson
  214           mones	Ricardo Mones Lastra
  215           moray	Moray Allan
  216           morph	Sandro Tosi
  217           mpitt	Martin Pitt
  218        mquinson	Martin Quinson
  219              mt	Michael Tautschnig
  220           murat	Murat Demirten
  221            myon	Christoph Berg
  222             mzf	Francois Mazen
  223          nattie	Nattie Mayer-Hutchings
  224           nickm	Nick Morrott
  225          nilesh	Nilesh Patra
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
Count     Debian UID 	   Name 
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
  226          nodens	Clément Hermann
  227            noel	Noèl Köthe
  228         noodles	Jonathan McDowell
  229        nthykier	Niels Thykier
  230           ntyni	Niko Tyni
  231         obbardc	Christopher Michael Obbard
  232            odyx	Didier Raboud
  233           ohura	Makoto OHURA
  234           olasd	Nicolas Dandrimont
  235         olebole	Ole Streicher
  236         onlyjob	Dmitry Smirnov
  237           osamu	Osamu Aoki
  238            otto	Otto Kekäläinen
  239            pabs	Paul Wise
  240    paddatrapper	Kyle Robbertze
  241          paride	Paride Legovini
  242             peb	Pierre-Elliott Bécue
  243             pgt	Pierre Gruet
  244           philh	Philip Hands
  245            phls	Paulo Henrique de Lima Santana
  246            pini	Gilles Filippini
  247           piotr	Piotr Ożarowski
  248             pjb	Phil Brooke
  249           pkern	Philipp Kern
  250          plessy	Charles Plessy
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
Count     Debian UID 	   Name 
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
  251          pmhahn	Philipp Matthias Hahn
  252           pochu	Emilio Pozuelo Monfort
  253           pollo	Louis-Philippe Véronneau
  254       polverari	David da Silva Polverari
  255        porridge	Marcin Owsiany
  256         praveen	Praveen Arimbrathodiyil
  257        pvaneynd	Peter Van Eynde
  258          rafael	Rafael Laboissiere
  259             ras	Russell Stuart
  260             reg	Gregory Colpart
  261         reichel	Joachim Reichel
  262            rene	Rene Engelhard
  263      rfrancoise	Romain Francoise
  264           rgson	Robin Gustafsson
  265         ribalda	Ricardo Ribalda Delgado
  266           rinni	Philip Rinn
  267         rlaager	Richard Laager
  268        rmayorga	Rene Mayorga
  269             rmb	Mohammed Bilal
  270            roam	Peter Pentchev
  271         roberto	Roberto C. Sanchez
  272        roehling	Timo Röhling
  273          roland	Roland Rosenfeld
  274             ron	Ron Lee
  275        rousseau	Ludovic Rousseau
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
Count     Debian UID 	   Name 
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
  276           rover	Roberto Lumbreras
  277             rra	Russ Allbery
  278     rvandegrift	Ross Vandegrift
  279           sahil	Sahil Dhiman
  280        santiago	Santiago Ruano Rincón
  281         sanvila	Santiago Vila
  282           satta	Sascha Steinbiss
  283          sbadia	Sebastien Badia
  284        schultmc	Michael C. Schultheiss
  285             seb	Sebastien Delafond
  286       sebastien	Sébastien Villemot
  287             sez	Serafeim Zanikolas
  288             sjr	Simon Richter
  289           skitt	Stephen Kitt
  290           slyon	Lukas Matthias Märdian
  291            smcv	Simon McVittie
  292             smr	Steven Michael Robbins
  293         sophieb	Sophie Brun
  294           soren	Soren Benjamin Stoutner
  295       spwhitton	Sean Whitton
  296       sramacher	Sebastian Ramacher
  297            srud	Sruthi Chandran
  298          ssgelm	Stephen Gelman
  299        stappers	Geert Stappers
  300          steele	David Steele
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
Count     Debian UID 	   Name 
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
  301        stefanor	Stefano Rivera
  302            sten	Nicholas D Steeves
  303  stephanlachnit	Stephan Lachnit
  304       sthibault	Samuel Thibault
  305             sto	Sergio Talens-Oliag
  306            sune	Sune Vuorela
  307       sunweaver	Mike Gabriel
  308           sur5r	Jakob Haufe
  309           swt2c	Scott Talbert
  310           szlin	SZ Lin
  311          taffit	David Prévot
  312          takaki	Takaki Taniguchi
  313           talau	Marcos Talau
  314          tassia	Tássia Camões Araújo
  315             tbm	Martin Michlmayr
  316           tchet	Alexandre Detiste
  317        terceiro	Antonio Terceiro
  318              tg	Thorsten Glaser
  319         thansen	Tobias Hansen
  320           thijs	Thijs Kinkhorst
  321           tiago	Tiago Bortoletto Vaz
  322          tianon	Tianon Gravi
  323          tijuca	Carsten Schoenert
  324           tille	Andreas Tille
  325        tmancill	Tony Mancill
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
Count     Debian UID 	   Name 
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
  326            tobi	Tobias Frost
  327           toddy	Tobias Quathamer
  328         tolimar	Alexander Reichle-Schmehl
  329            toni	Toni Mueller
  330         treinen	Ralf Treinen
  331           troyh	Troy Heber
  332        tvainika	Tommi Vainikainen
  333        tvincent	Thomas Vincent
  334           tytso	Theodore Y. Ts'o
  335         tzafrir	Tzafrir Cohen
  336            ucko	Aaron M. Ucko
  337          uhoreg	Hubert Chathi
  338        ukleinek	Uwe Kleine-König
  339        umlaeute	IOhannes m zmölnig
  340         unit193	Unit 193  
  341           urbec	Judit Foglszinger
  342         vagrant	Vagrant Cascadian
  343         vasudev	Vasudev Sathish Kamath
  344        vdanjean	Vincent Danjean
  345           viiru	Arto Jantunen
  346          vorlon	Steve Langasek
  347          vvidic	Valentin Vidic
  348          wagner	Hanno Wagner
  349           waldi	Bastian Blank
  350        weinholt	Gwen Weinholt
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
Count     Debian UID 	   Name 
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
  351          winnie	Patrick Winnertz
  352          wookey	Wookey
  353          wouter	Wouter Verhelst
  354            wrar	Andrey Rahmatullin
  355             xam	Max Vozeler
  356          xluthi	Xavier Lüthi
  357            yadd	Xavier Guimard
  358            zack	Stefano Zacchiroli
  359            zeha	Christian Hofstaedtler
  360            zigo	Thomas Goirand
  361       zugschlus	Marc Haber
  362           zumbi	Hector Oron
