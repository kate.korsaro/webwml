#use wml::debian::template title="Logotipos Debian" BARETITLE=true
#include "$(ENGLISHDIR)/logos/index.data"
#use wml::debian::translation-check translation="97732ca8593e39ce8b981adf7f81657417b62c73"

<p>O Debian tem dois logotipos. Um <a href="#open-use">logotipo oficial</a>
  (também conhecido como <q>logotipo de uso aberto</q>) que contém a célebre
  <q>espiral</q> Debian e melhor representa a identidade visual do Projeto
  Debian. Outro <a href="#restricted-use">logotipo de uso restrito</a> que existe
  para uso exclusivo do Projeto Debian e de seus(suas) membros(as). Ao referir-se ao
  Debian, por favor prefira a utilização do logotipo de uso aberto.

<hr>

<table cellspacing="0" cellpadding="5" width="100%">
<colgroup span="2">
<col width="65%" />
<col width="35%" />
</colgroup>
<tr>
<th colspan="2"><a name="open-use">Logotipo Debian de uso aberto</a></th>
</tr>
<tr>
<td>

<p>O logotipo Debian de uso aberto vem em duas formas, com ou sem a
  inscrição &ldquo;Debian&rdquo; .</p>

<p>Os Logotipo(s) Debian de uso aberto tem registro de propriedade autoral
  Copyright (c) 1999 de
  <a href="https://www.spi-inc.org/">Software in the Public Interest, Inc.</a>,
  e estão disponíveis sob os termos da
  <a href="https://www.gnu.org/copyleft/lgpl.html">Licença Pública Geral Menor GNU</a>
  versão 3 ou posteriores, ou, a sua escolha, da
  <a href="https://creativecommons.org/licenses/by-sa/3.0/">Licença Atribuição-CompartilhaIgual 3.0 Não Adaptada</a>.</p>

<p>Nota: nós apreciaríamos se você tornar a imagem em um link para
<a href="$(HOME)">https://www.debian.org/</a> se usá-la em uma página web.</p>
</td>
<td>
<openlogotable>
</td>
</tr>
</table>

<hr />

<table cellspacing="0" cellpadding="5" width="100%">
<colgroup span="2">
<col width="65%" />
<col width="35%" />
</colgroup>
<tr>
<th colspan="2"><a name="restricted-use">Logotipo Debian de uso restrito</a></th>
</tr>
<tr>
<td>
<h3>Licença do logotipo Debian de uso restrito</h3>

<p>Copyright (c) 1999 Software in the Public Interest</p>
<ol>
	<li>Este logotipo pode ser usado somente se:
         <ul>
	    <li>o produto em que for utilizado é feito usando um
	    procedimento documentado conforme publicado em
	    www.debian.org (por exemplo, a criação de CDs oficiais), ou </li>
	    <li>for fornecida aprovação oficial pelo Debian para seu
	    uso neste propósito </li>
         </ul>
	 </li>
	<li>Pode ser utilizado se uma parte oficial do Debian (decidido
	usando as regras em I) é parte do produto completo, se estiver
	claro que somente esta parte é aprovada oficialmente </li>
	<li>Nós nos reservamos o direito de revogar uma licença para um
	produto</li>
</ol>

<p>Foi permitido o uso do logotipo de uso restrito em roupas (camisetas, bonés,
etc) contanto que sejam feitas por um(a) desenvolvedor(a) Debian e não
vendidas para obter lucro.</p>
</td>
<td>
<officiallogotable>
</td>
</tr>
</table>

<hr />

<h2>Sobre os logotipos Debian</h2>
<p>
Os logotipos Debian foram escolhidos em votação pelos(as) desenvolvedores(as) Debian em
1999. Eles foram criados por <a href="mailto:rsilva@debian.org">Raul Silva</a>.
A cor vermelha utilizada na fonte é denominada <strong>Rubine Red 2X
CVC</strong>. A cor equivalente mais atualizada é PANTONE Strong Red C (tratada
no RGB como #CE0056) ou PANTONE Rubine Red C (tratada no RGB como #CE0058). Você
pode encontrar mais detalhes sobre os logotipos, sobre as razões porque PANTONE
Rubine Red 2X CVC foi substituída e sobre outras cores vermelhas equivalentes na
<a href="https://wiki.debian.org/DebianLogo">página wiki sobre o logotipo Debian</a>.
</p>

<h2>Outras imagens promocionais</h2>

<h3>Botões Debian</h3>

<p><img class="ico" src="button-1.gif" alt="[Debian GNU/Linux]" />
Este é o primeiro botão feito para o Projeto. A licença deste logotipo é
equivalente à do logotipo de uso aberto.  O botão foi criado por
<a href="mailto:csmall@debian.org">Craig Small</a>.</p>

<p>Aqui estão alguns outros botões que foram feitos para o Debian:</p>
<br />
<morebuttons>

<h3>Logotipo Debian Diversidade</h3>

<p>Existe uma variedade do logotipo Debian para promover a diversidade em nossa
comunidade, e chama-se logotipo Debian Diversidade:
<br/>
<img src="diversity-2019.png" alt="[logotipo Debian Diversidade]" />
<br/>
O logotipo foi criado por <a href="https://wiki.debian.org/ValessioBrito">Valessio Brito</a>
e está licenciado sob GPLv3.
O fonte (formato SVG) está disponível no
<a href="https://gitlab.com/valessiobrito/artwork">repositório Git</a> do autor.
<br />
</p>

<h3>Adesivo hexagonal Debian</h3>

<p>Este é um adesivo que segue a
<a href="https://github.com/terinjokes/StickerConstructorSpec">especificações para adesivos hexagonais</a>:
<br/>
<img src="hexagonal.png" alt="[Debian GNU/Linux]" />
<br/>
O fonte (formato SVG) e o Makefile para gerar uma pré-visualização png e svg
estão disponíveis no
<a href="https://salsa.debian.org/debian/debian-flyers/tree/master/hexagonal-sticker">repositório Debian flyers</a>.

A licença deste adesivo é equivalente à licença do logotipo de uso aberto.
O adesivo foi criado por
<a href="mailto:valhalla@trueelena.org">Elena Grandi</a>.</p>
<br />

#
# Fonte do logotipo: Poppl Laudatio Condensed
#
# https://lists.debian.org/debian-www/2003/08/msg00261.html
#
