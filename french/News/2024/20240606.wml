#use wml::debian::translation-check translation="27cb73692f0206ae5f0a3f04e7090405b4df902a" maintainer="Jean-Pierre Giraud"
<define-tag pagetitle>Déclaration à propos de Daniel Pocock</define-tag>
<define-tag release_date>2024-06-06</define-tag>
#use wml::debian::news
# $Id$
# $Rev$

<p>À la suite de l'enregistrement abusif de la marque Debian en Suisse par
la société d'un ancien développeur Debian – qui a conduit une sévère
campagne de dénigrement contre Debian depuis 2018 – le projet Debian a
entrepris une action vigoureuse pour protéger ses marques déposées et
ses intérêts dans le monde entier.
</p>

<p>En novembre 2023, à la suite d'une action en justice introduite par
Debian, le Tribunal Cantonal du Canton de Vaud a <a
href="https://www.debian.org/News/2024/judgement.pdf">statué</a> que
l'enregistrement de la marque DEBIAN en Suisse par Open Source Developer
Freedom SA (OSDF) en liquidation – précédemment Software Freedom
Institute SA – était un détournement de la marque déposée Debian, dans la
mesure où cette dernière est notoire et établie dans le domaine
informatique au niveau mondial. Aucun recours n'a été déposé à l'encontre
du jugement qui est par conséquent définitif.</p>

<p>Dans son jugement le tribunal considère que :</p>

<p><em>« Daniel Pocock est l'administrateur unique de la défenderesse
[OSDF]. »</em></p>

<p><em>« En procédant de la sorte la défenderesse, qui connaissait
l’existence de la marque litigieuse ainsi que sa notoriété et qui savait
que la demanderesse en était titulaire, puisque son administrateur
unique était un développeur Debian et un ancien membre de la communauté
du même nom, a usurpé [la]dite marque et créé la confusion dans l’esprit
du public. »</em></p>

<p>Le tribunal a ordonné que l'enregistrement suisse de la marque déposée
soit transféré à l'organisme habilité du projet Debian, Software in the
Public Interest (SPI) Inc., que les frais de justice soient payés par la
défenderesse. OSDF a aussi été condamné à publier le jugement sur son
site web. Avant que le jugement définitif du tribunal soit rendu,
ODSF a annulé subitement l'enregistrement, puis est entré en liquidation
sans en aviser SPI et sans régler les frais de justice. À ce jour, Daniel
Pocock n'a rempli aucune des obligations ordonnées par la cour et Debian
a été contrainte d'introduire une action en recouvrement pour ses frais.</p>

<p>Pendant la période de 2020 à février 2024, Debian a eu connaissance
d'au moins quatorze enregistrements de domaines et de sites web
associés contrevenant à notre politique de marque déposée. Tous les
domaines ont été enregistrés et étaient contrôlés par Daniel Pocock. En
mai 2024 l'Organisation Mondiale de la Propriété Intellectuelle (OMPI
- en anglais World Intellectual Property Organisation – WIPO) a ordonné
aux bureaux d'enregistrement concernés de
<a href="https://www.wipo.int/amc/en/domains/search/case.jsp?case=D2024-0770">\
transférer</a> la totalité des quatorze enregistrements à SPI Inc.
en fiducie pour Debian,
<a href="https://www.wipo.int/amc/en/domains/search/text.jsp?case=D2024-0770">\
notant</a> :</p>

<p><em>« [...] la partie défenderesse a enregistré et utilisé les noms
de domaine du litige de mauvaise foi, créant un risque de confusion comme
source ou affiliation avec la marque déposée du plaignant, à des fins
aussi bien commerciales que critiques en pleine connaissance de la marque
déposée du plaignant et de sa politique de marque déposée. »</em></p>

<p>Le projet Debian continuera à prendre toutes les mesures nécessaires
pour protéger et défendre ses marques déposées et ses autres intérêts.
Notre politique de marque déposée a été reconnue par plusieurs
juridictions.</p>

<p>Nous profitons donc de l'occasion pour remercier les membres de notre
communauté qui ont consenti un effort important pour documenter l'usage
de Debian et son déploiement dans le monde entier et préparer notre
contestation de l'enregistrement de marques déposées en Suisse en dépit
de la sévère campagne de dénigrement abusive à laquelle beaucoup de nos
volontaires ont été confrontés ces dernières années.</p>

<p>Nous demeurons engagés à rechercher tous les moyens légaux appropriés
pour protéger notre communauté et les autres contre de nouveaux actes de
harcèlement.</p>

<h2>Pour plus d'informations</h2>

<p>Vous trouverez le jugement du Tribunal Cantonal du Canton de Vaud, en
date du 27 novembre 2023, à l'adresse
<url "https://www.debian.org/News/2024/judgement.pdf">
et le texte complet de la décision de l'OMPI, en date du 3 mai 2024, est
disponible à l'adresse <a
href="https://www.wipo.int/amc/en/domains/search/text.jsp?case=D2024-0770">\
https://www.wipo.int/amc/en/domains/search/text.jsp?case=D2024-0770</a>.</p>


<h2>À propos de Debian</h2>

<p>
Le projet Debian est une association de développeurs de logiciels libres
qui offrent volontairement leur temps et leurs efforts pour produire le
système d'exploitation complètement libre Debian.
</p>

<h2>Contacts</h2>

<p>
Pour de plus amples informations, veuillez consulter le site Internet de
Debian <a href="$(HOME)/">https://www.debian.org/</a> ou envoyez un
courrier électronique à &lt;press@debian.org&gt;.

