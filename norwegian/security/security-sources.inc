<ul>

<li><a href="https://security-tracker.debian.org/">Debians system for sporing av sikkerhetsfeil</a>
  er den primære kilden for all sikkerhets relatert informasjon, med søkemuligheter</li>

<li><a href="https://security-tracker.debian.org/tracker/data/json">JSON-listen for sporingssystemet</a>
  inneholder CVE-beskrivelse, pakkenavn, Debian feilnummer, pakkeversjon med rettelse, ingen DSA inkludert
</li>

<li>Lister over
  <a href="https://salsa.debian.org/security-tracker-team/security-tracker/-/raw/master/data/DSA/list">DSA</a> /
  <a href="https://salsa.debian.org/security-tracker-team/security-tracker/-/raw/master/data/DLA/list">DLA</a>
  med dato, relaterte CVE nummer, pakkeversjon med rettelse
</li>

<li>E-postlistene 
<a href="https://lists.debian.org/debian-security-announce/">debian-security-announce</a> (DSA) og 
<a href="https://lists.debian.org/debian-lts-announce/">debian-lts-announces</a> (DLA)
</li>

<li>RSS for
<a href="$(HOME)/security/dsa">DSA</a> / <a href="$(HOME)/security/dsa-long">DSA med meldingsteksten</a> og 
<a href="$(HOME)/security/dla">DLA</a> / <a href="$(HOME)/security/dla-long">DLA med meldingsteksten</a>
</li>

<li><a href="oval">OVAL-filer</a></li>

<li>DSA-oppslag - f.eks. <tt>https://security-tracker.debian.org/tracker/DSA-3814</tt></li>

<li>DLA-oppslag - f.eks. <tt>https://security-tracker.debian.org/tracker/DLA-867-1</tt></li>

<li>CVE-oppslag - f.eks. <tt>https://security-tracker.debian.org/tracker/CVE-2017-6827</tt></li>

</ul>
