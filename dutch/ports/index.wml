#use wml::debian::template title="Architecturen waarop Debian werkt (Ports)"
#use wml::debian::translation-check translation="e0652549b916d3585e3eed9d093a6711af721019"
#include "$(ENGLISHDIR)/releases/info"
#use wml::debian::toc

# Last Translation Update by $Author$
# Last Translation Update at $Date$

<toc-display/>

<toc-add-entry name="intro">Inleiding</toc-add-entry>
<p>
Zoals de meesten onder u wel zullen weten, is <a href="https://www.kernel.org/">Linux</a> niet meer dan een kernel. Een hele
tijd heeft Linux alleen gewerkt op computers met een processor die compatibel
was met de Intel x86 en dit vanaf de 386.
</p>
<p>
Tegenwoordig is dit echter absoluut niet meer waar. De Linux kernel is nu
geschikt gemaakt voor een lange, nog altijd groeiende, lijst
hardware-architecturen. Met Debian volgen we deze
ontwikkelingen op de voet en we hebben onze distributie ook voor deze
platformen geschikt gemaakt. Over het algemeen is dit een proces met een taai
begin (waarin we libc en de dynamische linker overzetten), en dan een
langdurige en routineuze karwei waarin we proberen al onze pakketten te
compileren voor de nieuwe architectuur.
</p>
<p>
Debian is een besturingssysteem, geen kernel (het is eigenlijk zelfs veel meer
dan een besturingssysteem, aangezien het ook duizenden applicaties bevat).
En zo kan men op sommige architecturen ook een versie van Debian gebruiken
die gebaseerd is op de kernel van FreeBSD, NetBSD of de Hurd, al is voor
de meeste architecturen enkel een versie van Debian beschikbaar die op Linux
gebaseerd is. In het jargon noemt men zo een aan een specifieke
architectuur aangepaste versie van Debian met een specifieke kernel <i>een
port</i> of ook wel een overzetting.
</p>

<div class="important">
<p>
Deze pagina is werk in uitvoering. Niet alle overzettingen hebben al hun eigen pagina,
en de meeste hebben hun eigen externe site. We proberen de informatie over alle
overzettingen te verzamelen, om deze mee aan te bieden op de website van Debian.
Op de wiki kunnen eventueel meer overzettingen <a
href="https://wiki.debian.org/CategoryPorts">vermeld zijn</a>.
</p>
</div>

<toc-add-entry name="portlist-released">Lijst van officiële overzettingen</toc-add-entry>

<p>
Deze overzettingen zijn de door het Debian-project officieel ondersteunde architecturen en maken deel uit van een officiële release of zullen deel uitmaken van een komende release.
</p>

<table class="tabular" summary="">
<tbody>
<tr>
<th>Overzetting</th>
<th>Architectuur</th>
<th>Beschrijving</th>
<th>Toegevoegd</th>
<th>Status</th>
</tr>
<tr>
<td><a href="amd64/">amd64</a></td>
<td>64-bits PC (amd64)</td>
<td>Overzetting naar 64-bits x86-processoren  om zowel 32-bits als 64-bits
toepassingssoftware te ondersteunen. Deze overzetting ondersteunt de
64-bitsprocessoren Opteron, Athlon en Sempron van AMD en de processoren met
Intel 64-ondersteuning van Intel, waaronder de Pentium D-modellen en diverse
modellen van Xeon en Core.</td>
<td>4.0</td>
<td><a href="$(HOME)/releases/stable/amd64/release-notes/">uitgebracht</a></td>
</tr>
<tr>
<td><a href="arm/">arm64</a></td>
<td>64-bits ARM (AArch64)</td>
<td>Overzetting naar 64-bits ARM-architectuur met de nieuwe versie 8 64-bits
instructieset genaamd (AArch64) voor processoren zoals de
Applied Micro X-Gene, de AMD Seattle en de Cavium ThunderX te ondersteunen.</td>
<td>8</td>
<td><a href="$(HOME)/releases/stable/arm64/release-notes/">uitgebracht</a></td>
</tr>
<tr>
<td><a href="arm/">armel</a></td>
<td>EABI ARM</td>
<td>Overzetting naar de 32-bits Little-endian ARM-architectuur met behulp van de Embedded ABI, die ARM-CPU's ondersteunt die compatibel zijn met de v5te-instructieset. Deze overzetting maakt geen gebruik van floating-point units (FPU).</td>
<td>5.0</td>
<td><a href="$(HOME)/releases/stable/armel/release-notes/">uitgebracht</a></td>
</tr>
<tr>
<td><a href="arm/">armhf</a></td>
<td>Hard Float ABI ARM</td>
<td>Overzetting naar de 32-bits Little Endian ARM-architectuur voor borden en apparaten die worden geleverd met een floating-point unit (FPU) en andere moderne ARM CPU-functies. Deze overzetting vereist minimaal een ARMv7 CPU met Thumb-2 en VFPv3-D16 ondersteuning voor zwevendekommagetallen.</td>
<td>7.0</td>
<td><a href="$(HOME)/releases/stable/armhf/release-notes/">uitgebracht</a></td>
</tr>
<tr>
<td><a href="i386/">i386</a></td>
<td>32-bits PC (i386)</td>
<td>Overzetting naar 32-bits x86-processors, waarbij Linux oorspronkelijk
werd ontwikkeld voor de Intel 386 processor, vandaar de afgekorte
naam.  Debian ondersteunt alle IA-32 processors, zowel die gefabriceerd door
Intel (inclusief de volledige Pentium-serie en recente Core Duo systemen in
32-bits modus), AMD (K6, de volledige Athlon-serie, de Athlon64-serie in 32-bits
modus), Cyrix en andere fabrikanten.</td>
<td>1.1</td>
<td><a href="$(HOME)/releases/stable/i386/release-notes/">uitgebracht</a></td>
</tr>
<tr>
<td><a href="https://wiki.debian.org/mips64el">mips64el</a></td>
<td>MIPS (64-bits little-endian modus)</td>
<td>Overzetting naar de little-endian N64 ABI voor de MIPS64r1 ISA en hardware floating-point.</td>
<td>9</td>
<td><a href="$(HOME)/releases/stable/mips64el/release-notes/">uitgebracht</a></td>
</tr>
<tr>
<td><a href="powerpc/">ppc64el</a></td>
<td>POWER7+, POWER8</td>
<td>Overzetting naar de 64-bits little-endian POWER-architectuur, gebaseerd op de
nieuwe Open Power ELFv2 ABI.</td>
<td>8</td>
<td><a
href="$(HOME)/releases/stable/ppc64el/release-notes/">uitgebracht</a></td>
</tr>
<tr>
<td><a href="https://wiki.debian.org/RISC-V">riscv64</a></td>
<td>RISC-V (64-bits little endian)</td>
<td>Overzetting naar 64-bits little-endian <a href="https://riscv.org/">RISC-V</a>,
een vrij/open ISA.</td>
<td>13</td>
<td>testing</td>
</tr>
<tr>
<td><a href="s390x/">s390x</a></td>
<td>System z</td>
<td>Overzetting naar de 64-bits gebruikersruimte
voor IBM System z mainframes.</td>
<td>7.0</td>
<td><a href="$(HOME)/releases/stable/s390x/release-notes/">uitgebracht</a></td>
</tr>
</tbody>
</table>

<toc-add-entry name="portlist-other">Lijst van andere overzettingen</toc-add-entry>

<p>
Deze overzettingen zijn ofwel werk in uitvoering waarbij het de bedoeling is dat ze uiteindelijk gepromoveerd worden tot officieel uitgebrachte architecturen, overzettingen die ooit officieel ondersteund werden maar niet meer uitgebracht werden omdat ze niet voldeden aan de release-kwalificatievereisten of omdat er beperkte interesse was van ontwikkelaars, of overzettingen waaraan niet meer gewerkt wordt en die op de lijst staan uit historische overwegingen.
</p>

<p>
Deze overzettingen zijn, wanneer ze nog actief worden onderhouden, beschikbaar op
de <url "https://www.ports.debian.org/"> infrastructuur.
</p>

<div class="tip">
<p>
 Voor sommige van de hieronder vermelde overzettingen zijn installatie-images beschikbaar op
 <a href="https://cdimage.debian.org/cdimage/ports">https://cdimage.debian.org/cdimage/ports</a>.
 Deze images worden door de betrokken overzettingsteams van Debian onderhouden.
</p>
</div>

<table class="tabular" summary="">
<tbody>
<tr>
<th>Overzetting</th>
<th>Architectuur</th>
<th>Beschrijving</th>
<th>Toegevoegd</th>
<th>Weggevallen</th>
<th>Status</th>
<th>Vervangen door</th>
</tr>
<tr>
<td><a href="alpha/">alpha</a></td>
<td>Alpha</td>
<td>Overzetting naar de 64-bits RISC Alpha architectuur.</td>
<td>2.1</td>
<td>6.0</td>
<td>ports</td>
<td>-</td>
</tr>
<tr>
<td><a href="arm/">arm</a></td>
<td>OABI ARM</td>
<td>Overzetting naar de ARM-architectuur met de oude ABI</td>
<td>2.2</td>
<td>6.0</td>
<td>dood</td>
<td>armel</td>
</tr>
<tr>
<td><a href="https://web.archive.org/web/20130326061253/http://avr32.debian.net/">avr32</a></td>
<td>Atmel 32-bits RISC</td>
<td>Overzetting naar de 32-bits RISC-architectuur van Atmel, AVR32.</td>
<td>-</td>
<td>-</td>
<td>dood</td>
<td>-</td>
</tr>
<tr>
<td><a href="hppa/">hppa</a></td>
<td>HP PA-RISC</td>
<td>Overzetting naar de PA-RISC architectuur van Hewlett-Packard.</td>
<td>3.0</td>
<td>6.0</td>
<td>ports</td>
<td>-</td>
</tr>
<tr>
<td><a href="hurd/">hurd-i386</a></td>
<td>32-bits pc (i386)</td>
<td>Overzetting naar het GNU Hurd besturingssysteem voor de 32-bits x86-processors.
</td>
<td>-</td>
<td>-</td>
<td>ports</td>
<td>-</td>
</tr>
<tr>
<td><a href="hurd/">hurd-amd64</a></td>
<td>64-bits pc (amd64)</td>
<td>Overzetting naar het GNU Hurd besturingssysteem voor de 64-bits x86-processors. Het ondersteunt alleen 64-bits, niet 32-bits naast 64-bits.
</td>
<td>-</td>
<td>-</td>
<td>ports</td>
<td>-</td>
</tr>
<tr>
<td><a href="ia64/">ia64</a></td>
<td>Intel Itanium IA-64</td>
<td>Overzetting naar
de eerste 64-bits architectuur van Intel. Merk op dat dit niet
verward mag worden met de laatste 64-bits extensies van Intel voor
processors van het type Pentium 4 en Celeron en die men Intel 64 noemt;
gebruik de amd64-overzetting voor deze processors.</td>
<td>3.0</td>
<td>8</td>
<td>ports</td>
<td>-</td>
</tr>
<tr>
<td><a href="kfreebsd-gnu/">kfreebsd-amd64</a></td>
<td>64-bits pc (amd64)</td>
<td>Overzetting naar de kernel van FreeBSD met behulp van de glibc.
Ze werd uitgebracht als de eerste niet-Linux overzetting van Debian als een
kennismakingsrelease (<i>technology preview</i>).</td>
<td>dood</td>
</tr>
<tr>
<td><a href="kfreebsd-gnu/">kfreebsd-i386</a></td>
<td>32-bits pc (i386)</td>
<td>Overzetting naar de kernel van FreeBSD met behulp van de glibc.
Ze werd uitgebracht als de eerste niet-Linux overzetting van Debian als een
kennismakingsrelease (<i>technology preview</i>).</td>
<td>6.0</td>
<td>8</td>
<td><a href="https://lists.debian.org/debian-devel/2023/05/msg00306.html">dood</a></td>
<td>-</td>
</tr>
<tr>
<td><a href="https://wiki.debian.org/LoongArch">loong64</a></td>
<td>LoongArch (64-bits little endian)</td>
<td>Overzetting naar de architectuur 64-bits little-endian LoongArch.</td>
<td>-</td>
<td>-</td>
<td>ports</td>
<td>-</td>
</tr>
<tr>
<td><a href="http://www.linux-m32r.org/">m32</a></td>
<td>M32R</td>
<td>Overzetting naar de 32-bits RISC microprocessor van Renesas Technology.</td>
<td>-</td>
<td>-</td>
<td>dood</td>
<td>-</td>
</tr>
<tr>
<td><a href="m68k/">m68k</a></td>
<td>Motorola 68k</td>
<td>Overzetting naar de Motorola 68k processorserie — in het bijzonder de Sun3-reeks werkstations, de Apple Macintosh-personal computers en de Atari- en Amiga-personal computers.</td>
<td>2.0</td>
<td>4.0</td>
<td>ports</td>
<td>-</td>
</tr>
<tr>
<td><a href="mips/">mips</a></td>
<td>MIPS (big-endian modus)</td>
<td>Overzetting naar de MIPS-architectuur die wordt gebruikt in (big-endian) SGI-machines.</td>
<td>3.0</td>
<td>11</td>
<td><a href="https://lists.debian.org/debian-release/2019/08/msg00582.html">dood</a></td>
<td>-</td>
</tr>
<tr>
<td><a href="mips/">mipsel</a></td>
<td>MIPS (little-endian modus)</td>
<td>Overzetting naar de MIPS-architectuur die wordt gebruikt in (little-endian) Digital DECstations.
</td>
<td>3.0</td>
<td>13</td>
<td><a href="https://lists.debian.org/debian-devel-announce/2023/09/msg00000.html">dood</a></td>
<td>-</td>
</tr>
<tr>
<td><a href="https://web.archive.org/web/20230605050614/http://www.debian.org/ports/netbsd/">netbsd-i386</a></td>
<td>32-bits pc (i386)</td>
<td>Overzetting naar de NetBSD-kernel en libc, voor de 32-bits x86-processors.</td>
<td>-</td>
<td>-</td>
<td>dood</td>
<td>-</td>
</tr>
<tr>
<td><a href="https://web.archive.org/web/20230605050614/http://www.debian.org/ports/netbsd/">netbsd-alpha</a></td>
<td>Alpha</td>
<td>Overzetting naar de NetBSD kernel en libc, voor de 64-bits Alpha-processors.</td>
<td>-</td>
<td>-</td>
<td>dood</td>
<td>-</td>
</tr>
<tr>
<td><a href="https://web.archive.org/web/20150905061423/http://or1k.debian.net/">or1k</a></td>
<td>OpenRISC 1200</td>
<td>Overzetting naar de <a href="https://openrisc.io/">OpenRISC</a> 1200 open source CPU.</td>
<td>-</td>
<td>-</td>
<td>dood</td>
<td>-</td>
</tr>
<tr>
<td><a href="powerpc/">powerpc</a></td>
<td>Motorola/IBM PowerPC</td>
<td> Overzetting naar veel
van de Apple Macintosh PowerMac computers en de CHRP en PReP open-architectuur machines.</td>
<td>2.2</td>
<td>9</td>
<td>ports</td>
<td>-</td>
</tr>
<tr>
<td><a href="https://wiki.debian.org/PowerPCSPEPort">powerpcspe</a></td>
<td>PowerPC Signal Processing Engine</td>
<td>
Overzetting naar de "Signal Processing Engine"-hardware die men vindt in low-power
32-bits FreeScale en IBM "e500" CPU's.
</td>
<td>-</td>
<td>-</td>
<td>dood</td>
<td>-</td>
</tr>
<tr>
<td><a href="s390/">s390</a></td>
<td>S/390 en z-serie</td>
<td>Overzetting naar de IBM S/390 servers.</td>
<td>3.0</td>
<td>8</td>
<td>dood</td>
<td>s390x</td>
</tr>
<tr>
<td><a href="sparc/">sparc</a></td>
<td>Sun SPARC</td>
<td>Overzetting naar de
Sun UltraSPARC werkstations en sommige opvolgers hiervan uit de sun4-architecturen.
</td>
<td>2.1</td>
<td>8</td>
<td>dood</td>
<td>sparc64</td>
</tr>
<tr>
<td><a href="https://wiki.debian.org/Sparc64">sparc64</a></td>
<td>64-bits SPARC</td>
<td>
Overzetting naar de 64-bits SPARC-processors.
</td>
<td>-</td>
<td>-</td>
<td>ports</td>
<td>-</td>
</tr>
<tr>
<td><a href="https://wiki.debian.org/SH4">sh4</a></td>
<td>SuperH</td>
<td>Overzetting naar de Hitachi SuperH processors. Ondersteunt ook de openbron
<a href="https://j-core.org/">J-Core</a> processor.
</td>
<td>-</td>
<td>-</td>
<td>ports</td>
<td>-</td>
</tr>
<tr>
<td><a href="https://wiki.debian.org/X32Port">x32</a></td>
<td>64-bits pc met 32-bits pointers</td>
<td>
Overzetting naar de amd64/x86_64 x32 ABI die gebruik maakt van de amd64 instructie
maar met set 32-bits pointers, om de grotere registerset van deze ISA te combineren met
het lagere geheugen- en cachegebruik dat voortvloeit uit het gebruik van 32-bits
pointers.
</td>
<td>-</td>
<td>-</td>
<td>ports</td>
<td>-</td>
</tr>
</tbody>
</table>


<div class="note">
<p>Veel van bovenstaande computer- en processornamen zijn handelsmerken
en geregistreerde handelsmerken van hun respectieve fabrikanten.</p>
</div>

