<define-tag pagetitle>Updated Debian 12: 12.7 released</define-tag>
<define-tag release_date>2024-08-31</define-tag>
#use wml::debian::news
# $Id:

<define-tag release>12</define-tag>
<define-tag codename>bookworm</define-tag>
<define-tag revision>12.7</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>The Debian project is pleased to announce the seventh update of its
stable distribution Debian <release> (codename <q><codename></q>). 
This point release mainly adds corrections for security issues,
along with a few adjustments for serious problems.  Security advisories
have already been published separately and are referenced where available.</p>

<p>Please note that the point release does not constitute a new version of Debian
<release> but only updates some of the packages included.  There is
no need to throw away old <q><codename></q> media. After installation,
packages can be upgraded to the current versions using an up-to-date Debian
mirror.</p>

<p>Those who frequently install updates from security.debian.org won't have
to update many packages, and most such updates are
included in the point release.</p>

<p>New installation images will be available soon at the regular locations.</p>

<p>Upgrading an existing installation to this revision can be achieved by
pointing the package management system at one of Debian's many HTTP mirrors.
A comprehensive list of mirrors is available at:</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>

<h2>Secure Boot and other operating systems</h2>

<p>Users who boot other operating systems on the same hardware, and who have
Secure Boot enabled, should be aware that shim 15.8 (included with Debian <revision>)
revokes signatures across older versions of shim in the UEFI firmware.
This may leave other operating systems using shim before 15.8 unable to boot.</p>

<p>Affected users can temporarily disable Secure Boot before updating other
operating systems.</p>


<h2>Miscellaneous Bugfixes</h2>

<p>This stable update adds a few important corrections to the following packages:</p>

<table border=0>
<tr><th>Package</th>               <th>Reason</th></tr>
<correction amd64-microcode "New upstream release; security fixes [CVE-2023-31315]; SEV firmware fixes [CVE-2023-20584 CVE-2023-31356]">
<correction ansible "New upstream stable release; fix key leakage issue [CVE-2023-4237]">
<correction ansible-core "New upstream stable release; fix information disclosure issue [CVE-2024-0690]; fix template injection issue [CVE-2023-5764]; fix path traversal issue [CVE-2023-5115]">
<correction apache2 "New upstream stable release; fix content disclosure issue [CVE-2024-40725]">
<correction base-files "Update for the point release">
<correction cacti "Fix remote code execution issues [CVE-2024-25641 CVE-2024-31459], cross site scripting issues [CVE-2024-29894 CVE-2024-31443 CVE-2024-31444], SQL injection issues [CVE-2024-31445 CVE-2024-31458 CVE-2024-31460], <q>type juggling</q> issue [CVE-2024-34340]; fix autopkgtest failure">
<correction calamares-settings-debian "Fix Xfce launcher permission issue">
<correction calibre "Fix remote code execution issue [CVE-2024-6782, cross site scripting issue [CVE-2024-7008], SQL injection issue [CVE-2024-7009]">
<correction choose-mirror "Update list of available mirrors">
<correction cockpit "Fix denial of service issue [CVE-2024-6126]">
<correction cups "Fix issues with domain socket handling [CVE-2024-35235]">
<correction curl "Fix ASN.1 date parser overread issue [CVE-2024-7264]">
<correction cyrus-imapd "Fix regression introduced in CVE-2024-34055 fix">
<correction dcm2niix "Fix potential code execution issue [CVE-2024-27629]">
<correction debian-installer "Increase Linux kernel ABI to 6.1.0-25; rebuild against proposed-updates">
<correction debian-installer-netboot-images "Rebuild against proposed-updates">
<correction dmitry "Security fixes [CVE-2024-31837 CVE-2020-14931 CVE-2017-7938]">
<correction dropbear "Fix <q>noremotetcp</q> behaviour of keepalive packets in combination with the <q>no-port-forwarding</q> authorized_keys(5) restriction">
<correction gettext.js "Fix server side request forgery issue [CVE-2024-43370]">
<correction glibc "Fix freeing uninitialized memory in libc_freeres_fn(); fix several performance issues and possible crashses">
<correction glogic "Require Gtk 3.0 and PangoCairo 1.0">
<correction graphviz "Fix broken scale">
<correction gtk+2.0 "Avoid looking for modules in the current working directory [CVE-2024-6655]">
<correction gtk+3.0 "Avoid looking for modules in the current working directory [CVE-2024-6655]">
<correction imagemagick "Fix segmentation fault issue; fix incomplete fix for CVE-2023-34151">
<correction initramfs-tools "hook_functions: Fix copy_file with source including a directory symlink; hook-functions: copy_file: Canonicalise target filename; install hid-multitouch module for Surface Pro 4 Keyboard; add hyper-keyboard module, needed to enter LUKS password in Hyper-V; auto_add_modules: Add onboard_usb_hub, onboard_usb_dev">
<correction intel-microcode "New upstream release; security fixes [CVE-2023-42667 CVE-2023-49141 CVE-2024-24853 CVE-2024-24980 CVE-2024-25939]">
<correction ipmitool "Add missing enterprise-numbers.txt file">
<correction libapache2-mod-auth-openidc "Avoid crash when the Forwarded header is not present but OIDCXForwardedHeaders is configured for it">
<correction libnvme "Fix buffer overflow during scanning devices that do not support sub-4k reads">
<correction libvirt "birsh: Make domif-setlink work more than once; qemu: domain: Fix logic when tainting domain; fix denial of service issues [CVE-2023-3750 CVE-2024-1441 CVE-2024-2494 CVE-2024-2496]">
<correction linux "New upstream release; bump ABI to 25">
<correction linux-signed-amd64 "New upstream release; bump ABI to 25">
<correction linux-signed-arm64 "New upstream release; bump ABI to 25">
<correction linux-signed-i386 "New upstream release; bump ABI to 25">
<correction newlib "Fix buffer overflow issue [CVE-2021-3420]">
<correction numpy "Conflict with python-numpy">
<correction openssl "New upstream stable release; fix denial of service issues [CVE-2024-2511 CVE-2024-4603]; fix use after free issue [CVE-2024-4741]">
<correction poe.app "Make comment cells editable; fix drawing when an NSActionCell in the preferences is acted on to change state">
<correction putty "Fix weak ECDSA nonce generation allowing secret key recovery [CVE-2024-31497]">
<correction qemu "New upstream stable release; fix denial of service issue [CVE-2024-4467]">
<correction riemann-c-client "Prevent malformed payload in GnuTLS send/receive operations">
<correction rustc-web "New upstream stable release, to support building new chromium and firefox-esr versions">
<correction shim "New upstream release">
<correction shim-helpers-amd64-signed "Rebuild against shim 15.8.1">
<correction shim-helpers-arm64-signed "Rebuild against shim 15.8.1">
<correction shim-helpers-i386-signed "Rebuild against shim 15.8.1">
<correction shim-signed "New upstream stable release">
<correction systemd "New upstream stable release; update hwdb">
<correction usb.ids "Update included data list">
<correction xmedcon "Fix buffer overflow issue [CVE-2024-29421]">
</table>


<h2>Security Updates</h2>


<p>This revision adds the following security updates to the stable release.
The Security Team has already released an advisory for each of these
updates:</p>

<table border=0>
<tr><th>Advisory ID</th>  <th>Package</th></tr>
<dsa 2024 5617 chromium>
<dsa 2024 5629 chromium>
<dsa 2024 5634 chromium>
<dsa 2024 5636 chromium>
<dsa 2024 5639 chromium>
<dsa 2024 5648 chromium>
<dsa 2024 5654 chromium>
<dsa 2024 5656 chromium>
<dsa 2024 5668 chromium>
<dsa 2024 5675 chromium>
<dsa 2024 5676 chromium>
<dsa 2024 5683 chromium>
<dsa 2024 5687 chromium>
<dsa 2024 5689 chromium>
<dsa 2024 5694 chromium>
<dsa 2024 5696 chromium>
<dsa 2024 5697 chromium>
<dsa 2024 5701 chromium>
<dsa 2024 5710 chromium>
<dsa 2024 5716 chromium>
<dsa 2024 5719 emacs>
<dsa 2024 5720 chromium>
<dsa 2024 5722 libvpx>
<dsa 2024 5723 plasma-workspace>
<dsa 2024 5724 openssh>
<dsa 2024 5725 znc>
<dsa 2024 5726 krb5>
<dsa 2024 5727 firefox-esr>
<dsa 2024 5728 exim4>
<dsa 2024 5729 apache2>
<dsa 2024 5731 linux-signed-amd64>
<dsa 2024 5731 linux-signed-arm64>
<dsa 2024 5731 linux-signed-i386>
<dsa 2024 5731 linux>
<dsa 2024 5732 chromium>
<dsa 2024 5734 bind9>
<dsa 2024 5735 chromium>
<dsa 2024 5737 libreoffice>
<dsa 2024 5738 openjdk-17>
<dsa 2024 5739 wpa>
<dsa 2024 5740 firefox-esr>
<dsa 2024 5741 chromium>
<dsa 2024 5743 roundcube>
<dsa 2024 5745 postgresql-15>
<dsa 2024 5748 ffmpeg>
<dsa 2024 5749 bubblewrap>
<dsa 2024 5749 flatpak>
<dsa 2024 5750 python-asyncssh>
<dsa 2024 5751 squid>
<dsa 2024 5752 dovecot>
<dsa 2024 5753 aom>
<dsa 2024 5754 cinder>
<dsa 2024 5755 glance>
<dsa 2024 5756 nova>
<dsa 2024 5757 chromium>
</table>


<h2>Removed packages</h2>

<p>The following packages were removed due to circumstances beyond our control:</p>

<table border=0>
<tr><th>Package</th>               <th>Reason</th></tr>
<correction bcachefs-tools "Buggy; obsolete">

</table>

<h2>Debian Installer</h2>
<p>The installer has been updated to include the fixes incorporated
into stable by the point release.</p>

<h2>URLs</h2>

<p>The complete lists of packages that have changed with this revision:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>The current stable distribution:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/stable/">
</div>

<p>Proposed updates to the stable distribution:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/proposed-updates">
</div>

<p>stable distribution information (release notes, errata etc.):</p>

<div class="center">
  <a
  href="$(HOME)/releases/stable/">https://www.debian.org/releases/stable/</a>
</div>

<p>Security announcements and information:</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>About Debian</h2>

<p>The Debian Project is an association of Free Software developers who
volunteer their time and effort in order to produce the completely
free operating system Debian.</p>

<h2>Contact Information</h2>

<p>For further information, please visit the Debian web pages at
<a href="$(HOME)/">https://www.debian.org/</a>, send mail to
&lt;press@debian.org&gt;, or contact the stable release team at
&lt;debian-release@lists.debian.org&gt;.</p>


