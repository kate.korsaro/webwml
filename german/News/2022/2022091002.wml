#use wml::debian::translation-check translation="4924e09e5cb1b4163d7ec7161488354e4167b24c" maintainer="Erik Pfannenstein"
<define-tag pagetitle>Debian 11 aktualisiert: 11.5 veröffentlicht</define-tag>
<define-tag release_date>2022-09-10</define-tag>
#use wml::debian::news
# $Id:

<define-tag release>11</define-tag>
<define-tag codename>Bullseye</define-tag>
<define-tag revision>11.5</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>
Das Debian-Projekt freut sich, die fünfte Aktualisierung seiner Stable-Distribution 
Debian <release> (Codename <q><codename></q>) ankündigen zu dürfen. Diese 
Zwischenveröffentlichung behebt hauptsächlich Sicherheitslücken der Stable-Veröffentlichung 
sowie einige ernste Probleme. Es sind bereits separate Sicherheitsankündigungen 
veröffentlicht worden, auf die, wenn möglich, verwiesen wird.
</p>

<p>
Bitte beachten Sie, dass diese Zwischenveröffentlichung keine neue Version von 
Debian <release> darstellt, sondern nur einige der enthaltenen Pakete auffrischt. 
Es gibt keinen Grund, <q><codename></q>-Medien zu entsorgen, da deren Pakete 
auch nach der Installation durch einen aktualisierten Debian-Spiegelserver auf 
den neuesten Stand gebracht werden können.
</p>

<p>Wer häufig Aktualisierungen von security.debian.org herunterlädt, wird nicht viele 
Pakete auf den neuesten Stand bringen müssen. Die meisten dieser Aktualisierungen sind 
in dieser Revision enthalten.</p>

<p>Neue Installationsabbilder können bald von den gewohnten Orten bezogen werden.</p>

<p>Vorhandene Installationen können auf diese Revision angehoben werden, indem 
das Paketverwaltungssystem auf einen der vielen HTTP-Spiegel von Debian verwiesen 
wird. Eine vollständige Liste der Spiegelserver ist verfügbar unter:</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>


<h2>Verschiedene Fehlerbehebungen</h2>

<p>Diese Stable-Aktualisierung fügt den folgenden Paketen einige wichtige Korrekturen hinzu:</p>

<table border=0>
<tr><th>Paket</th>               <th>Grund</th></tr>
<correction avahi "Anzeige von URLs, die '&amp;' enthalten, in avahi-discover behoben; bei Watch-Cleanup nicht den Timeout-Cleanup abschalten; Nullzeiger-Abstürze behoben, die auftreten, wenn versucht wird, schlecht formatierte Hostnamen aufzulösen [CVE-2021-3502]">
<correction base-files "Aktualisierung von /etc/debian_version auf die Version 11.5">
<correction cargo-mozilla "Neues Quellpaket, um das Kompilieren neuerer Versionen von firefox-esr und thunderbird zu unterstützen">
<correction clamav "Neue stabile Version der Originalautoren">
<correction commons-daemon "JVM-Erkennung überarbeitet">
<correction curl "Cookies mit <q>control bytes</q> abweisen [CVE-2022-35252]">
<correction dbus-broker "Assertions-Fehlschlag beim Trennen von Peer-Groups behoben; Speicherleck geflickt; Nullzeiger-Derefenzierung behoben [CVE-2022-31213]">
<correction debian-installer "Neukompilierung gegen proposed-updates; Linux-Kernel-ABI auf 5.10.0-18 angehoben">
<correction debian-installer-netboot-images "Neukompilierung gegen proposed-updates; Linux-Kernel-ABI auf 5.10.0-18 angehoben">
<correction debian-security-support "Unterstützungs-Status diverser Pakete aktualisiert">
<correction debootstrap "Sichergestellt, dass Chroots ohne zusammengeführtes usr auch weiterhin für ältere Veröffentlichungen und buildd-Chroots erzeugt werden können">
<correction dlt-daemon "Doppel-Free-Problem behoben [CVE-2022-31291]">
<correction dnsproxy "Statt auf der gegebenenfalls nicht verfügbaren Adresse 192.168.168.1 auf localhost auf Verbindungen warten">
<correction dovecot "Mögliche Sicherheitsprobleme behoben, die auftreten hätten können, wenn zwei passdb-Konfigurationen mit demselben Treiber und denselben eingestellten Argumenten vorhanden sind [CVE-2022-30550]">
<correction dpkg "Umgang mit bei Upgrades entfernten Konfigdateien überarbeitet, außerdem Speicherleck beim Umgang mit Entfernen-bei-Upgrade beseitigt; Dpkg::Shlibs::Objdump: apply_relocations überarbeitet, damit es mit versionierten Symbolen funktioniert; Unterstützung für ARCv2-CPUs hinzugefügt; verschiedene Aktualisierungen und Korrekturen bei dpkg-fsys-usrunmess">
<correction fig2dev "Doppel-Free-Problem behoben [CVE-2021-37529], genauso wie eine Dienstblockade [CVE-2021-37530]; falsche Platzierung von eingebetteten eps-Bildern abgestellt">
<correction foxtrotgps "Absturz durch Sicherstellen, dass Threads niemals referenziert sind, behoben">
<correction gif2apng "Heap-basierte Pufferüberläufe behoben [CVE-2021-45909 CVE-2021-45910 CVE-2021-45911]">
<correction glibc "Um-eins-daneben-Pufferüberlauf oder -Unterlauf in getcwd() behoben [CVE-2021-3999]; mehrere Überläufe in Wide-Character-Funktionen behoben; einige EVEX-optimierte Zeichenkettenfunktionen hinzugefügt, um ein Geschwindigkeitsproblem zu lösen (bis zu 40%) mit Skylake-X-Prozessoren; grantpt nach Multithread-Fork benutzbar machen; sicherstellen, dass libios vtable-Schutz angeschaltet ist">
<correction golang-github-pkg-term "Kompilierung auf neueren Linux-Kerneln überarbeitet">
<correction gri "Statt <q>convert</q> <q>ps2pdf</q> zum Konvertieren von PS nach PDF benutzen">
<correction grub-efi-amd64-signed "Neue Veröffentlichung der Originalautoren">
<correction grub-efi-arm64-signed "Neue Veröffentlichung der Originalautoren">
<correction grub-efi-ia32-signed "Neue Veröffentlichung der Originalautoren">
<correction grub2 "Neue Veröffentlichung der Originalautoren">
<correction http-parser "F_CHUNKED bei neuem Transfer-Encoding leeren, um potenzielle HTTP-Schmuggel-Probleme zu unterbinden [CVE-2020-8287]">
<correction ifenslave "Konfiguration von Bonded-Schnittstellen überarbeitet">
<correction inetutils "Probleme mit Pufferüberlauf [CVE-2019-0053], Stack-Exhaustion, Umgang mit FTP-PASV-Antworten behoben [CVE-2021-40491] sowie Dienstblockade [CVE-2022-39028]">
<correction knot "IXFR-auf-AXFR-Rückfall mit dnsmasq behoben">
<correction krb5 "SHA256 als Pkinit CMS Digest verwenden">
<correction libayatana-appindicator "Software, die auf libappindicator angewiesen ist, Kompatibilät anbieten">
<correction libdatetime-timezone-perl "Enthaltene Daten aktualisiert">
<correction libhttp-daemon-perl "Umgang mit Content-Length-Kopfzeile verbessert [CVE-2022-31081]">
<correction libreoffice "EUR in der .hr-Locale unterstützen; Calc und dem Euro-Assistenten den HRK&lt;-&gt;EUR-Umrechnungskurs hinzugefügt; Sicherheitskorrekturen [CVE-2021-25636 CVE-2022-26305 CVE-2022-26306 CVE-2022-26307]; Hänger beim Zugriff auf Evolution-Adressbücher behoben">
<correction linux "Neue stabile Version der Originalautoren">
<correction linux-signed-amd64 "Neue stabile Version der Originalautoren">
<correction linux-signed-arm64 "Neue stabile Version der Originalautoren">
<correction linux-signed-i386 "Neue stabile Version der Originalautoren">
<correction llvm-toolchain-13 "Neues Quellpaket, um die Kompilierung neuerer Versionen von firefox-esr und thunderbird zu unterstützen">
<correction lwip "Probleme mit Pufferüberlauf behoben [CVE-2020-22283 CVE-2020-22284]">
<correction mokutil "Neue Version der Originalautoren, um die SBAT-Verwaltung zu ermöglichen">
<correction node-log4js "Standardmäßig keine für die Welt lesbaren Dateien anlegen [CVE-2022-21704]">
<correction node-moment "Auf regulären Ausdrücken basierende Dienstblockade behoben [CVE-2022-31129]">
<correction nvidia-graphics-drivers "Neue Veröffentlichung der Originalautoren; Sicherheitskorrekturen [CVE-2022-31607 CVE-2022-31608 CVE-2022-31615]">
<correction nvidia-graphics-drivers-legacy-390xx "Neue Veröffentlichung der Originalautoren; Sicherheitskorrekturen [CVE-2022-31607 CVE-2022-31608 CVE-2022-31615]">
<correction nvidia-graphics-drivers-tesla-450 "Neue Veröffentlichung der Originalautoren; Sicherheitskorrekturen [CVE-2022-31607 CVE-2022-31608 CVE-2022-31615]">
<correction nvidia-graphics-drivers-tesla-470 "Neue Veröffentlichung der Originalautoren; Sicherheitskorrekturen [CVE-2022-31607 CVE-2022-31608 CVE-2022-31615]">
<correction nvidia-settings "Neue Veröffentlichung der Originalautoren; Kreuzkompilierung repariert">
<correction nvidia-settings-tesla-470 "Neue Veröffentlichung der Originalautoren; Kreuzkompilierung repariert">
<correction pcre2 "Lesezugriff außerhalb der Grenzen abgestellt [CVE-2022-1586 CVE-2022-1587]">
<correction postgresql-13 "Erweiterungsskripte keine Objekte ersetzen lassen, die nicht bereits zu der Erweiterung gehören [CVE-2022-2625]">
<correction publicsuffix "Enthaltene Daten aktualisiert">
<correction rocksdb "Illegale Instruktion auf arm64 korrigiert">
<correction sbuild "Buildd::Mail: MIME-kodierte Betreffs-Kopfzeile unterstützen und die Content-Type-Kopfzeile beim Weiterleiten von Mails kopieren">
<correction systemd "Mitgelieferte Kopie der linux/if_arp.h entfernt, um Kompilierungsfehlschläge bei neueren Kernel-Headern zu beheben; Erkennung von ARM64-Hyper-V-Gästen unterstützen; OpenStack-Instanz als KVM auf arm erkennen">
<correction twitter-bootstrap4 "CSS-Map-Dateien wirklich installieren">
<correction tzdata "Zeitzonendaten für Iran und Chile aktualisiert">
<correction xtables-addons "Sowohl alte als auch neue Version der security_skb_classify_flow() unterstützen">
</table>

<h2>Sicherheitsaktualisierungen</h2>

<p>Diese Revision fügt der Stable-Veröffentlichung die folgenden 
Sicherheitsaktualisierungen hinzu. Das Sicherheitsteam hat bereits für 
jede davon eine Ankündigung veröffentlicht:</p>

<table border=0>
<tr><th>Ankündigungs-ID</th>  <th>Paket</th></tr>
<dsa 2022 5175 thunderbird>
<dsa 2022 5176 blender>
<dsa 2022 5177 ldap-account-manager>
<dsa 2022 5178 intel-microcode>
<dsa 2022 5179 php7.4>
<dsa 2022 5180 chromium>
<dsa 2022 5181 request-tracker4>
<dsa 2022 5182 webkit2gtk>
<dsa 2022 5183 wpewebkit>
<dsa 2022 5184 xen>
<dsa 2022 5185 mat2>
<dsa 2022 5187 chromium>
<dsa 2022 5188 openjdk-11>
<dsa 2022 5189 gsasl>
<dsa 2022 5190 spip>
<dsa 2022 5191 linux-signed-amd64>
<dsa 2022 5191 linux-signed-arm64>
<dsa 2022 5191 linux-signed-i386>
<dsa 2022 5191 linux>
<dsa 2022 5192 openjdk-17>
<dsa 2022 5193 firefox-esr>
<dsa 2022 5194 booth>
<dsa 2022 5195 thunderbird>
<dsa 2022 5196 libpgjava>
<dsa 2022 5197 curl>
<dsa 2022 5198 jetty9>
<dsa 2022 5199 xorg-server>
<dsa 2022 5200 libtirpc>
<dsa 2022 5201 chromium>
<dsa 2022 5202 unzip>
<dsa 2022 5203 gnutls28>
<dsa 2022 5204 gst-plugins-good1.0>
<dsa 2022 5205 ldb>
<dsa 2022 5205 samba>
<dsa 2022 5206 trafficserver>
<dsa 2022 5207 linux-signed-amd64>
<dsa 2022 5207 linux-signed-arm64>
<dsa 2022 5207 linux-signed-i386>
<dsa 2022 5207 linux>
<dsa 2022 5208 epiphany-browser>
<dsa 2022 5209 net-snmp>
<dsa 2022 5210 webkit2gtk>
<dsa 2022 5211 wpewebkit>
<dsa 2022 5213 schroot>
<dsa 2022 5214 kicad>
<dsa 2022 5215 open-vm-tools>
<dsa 2022 5216 libxslt>
<dsa 2022 5217 firefox-esr>
<dsa 2022 5218 zlib>
<dsa 2022 5219 webkit2gtk>
<dsa 2022 5220 wpewebkit>
<dsa 2022 5221 thunderbird>
<dsa 2022 5222 dpdk>
</table>

<h2>Entfernte Pakete</h2>

<p>Die folgenden Pakete wurden wegen Umständen entfernt, die außerhalb 
unserer Kontrolle liegen::</p>

<table border=0>
<tr><th>Paket</th>               <th>Grund</th></tr>
<correction evenement "Unbetreut; wird nur für novim gebraucht, das bereits entfernt worden ist">
<correction php-cocur-slugify "Unbetreut; wird nur für novim gebraucht, das bereits entfernt worden ist">
<correction php-defuse-php-encryption "Unbetreut; wird nur für novim gebraucht, das bereits entfernt worden ist">
<correction php-dflydev-fig-cookies "Unbetreut; wird nur für novim gebraucht, das bereits entfernt worden ist">
<correction php-embed "Unbetreut; wird nur für novim gebraucht, das bereits entfernt worden ist">
<correction php-fabiang-sasl "Unbetreut; wird nur für novim gebraucht, das bereits entfernt worden ist">
<correction php-markdown "Unbetreut; wird nur für novim gebraucht, das bereits entfernt worden ist">
<correction php-raintpl "Unbetreut; wird nur für novim gebraucht, das bereits entfernt worden ist">
<correction php-react-child-process "Unbetreut; wird nur für novim gebraucht, das bereits entfernt worden ist">
<correction php-react-http "Unbetreut; wird nur für novim gebraucht, das bereits entfernt worden ist">
<correction php-respect-validation "Unbetreut; wird nur für novim gebraucht, das bereits entfernt worden ist">
<correction php-robmorgan-phinx "Unbetreut; wird nur für novim gebraucht, das bereits entfernt worden ist">
<correction ratchet-pawl "Unbetreut; wird nur für novim gebraucht, das bereits entfernt worden ist">
<correction ratchet-rfc6455 "Unbetreut; wird nur für novim gebraucht, das bereits entfernt worden ist">
<correction ratchetphp "Unbetreut; wird nur für novim gebraucht, das bereits entfernt worden ist">
<correction reactphp-cache "Unbetreut; wird nur für novim gebraucht, das bereits entfernt worden ist">
<correction reactphp-dns "Unbetreut; wird nur für novim gebraucht, das bereits entfernt worden ist">
<correction reactphp-event-loop "Unbetreut; wird nur für novim gebraucht, das bereits entfernt worden ist">
<correction reactphp-promise-stream "Unbetreut; wird nur für novim gebraucht, das bereits entfernt worden ist">
<correction reactphp-promise-timer "Unbetreut; wird nur für novim gebraucht, das bereits entfernt worden ist">
<correction reactphp-socket "Unbetreut; wird nur für novim gebraucht, das bereits entfernt worden ist">
<correction reactphp-stream "Unbetreut; wird nur für novim gebraucht, das bereits entfernt worden ist">
</table>

<h2>Debian-Installer</h2>
<p>Der Installer wurde aktualisiert, damit er die Korrekturen enthält, 
die mit dieser Zwischenveröffentlichung in Stable eingeflossen sind.</p>

<h2>URLs</h2>

<p>Die vollständige Liste von Paketen, die sich mit dieser Revision geändert haben:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>Die derzeitige Stable-Distribution:</p>


<div class="center">
  <url "https://deb.debian.org/debian/dists/stable/">
</div>

<p>Vorgeschlagene Aktualisierungen für die Stable-Distribution:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/proposed-updates">
</div>

<p>Vorgeschlagene Aktualisierungen für die Stable-Distribution:</p>

<div class="center">
  <a
  href="$(HOME)/releases/stable/">https://www.debian.org/releases/stable/</a>
</div>

<p>Sicherheitsankündigungen und -informationen:</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>


<h2>Über Debian</h2>

<p>Das Debian-Projekt ist ein Zusammenschluss von Entwicklern 
Freier Software, die ihre Zeit und Mühen einbringen, um das 
vollständig freie Betriebssystem Debian zu erschaffen.</p>

<h2>Kontaktinformationen</h2>

<p>Für weitere Informationen besuchen Sie bitte die Debian-Website unter 
<a href="$(HOME)/">https://www.debian.org/</a>, schicken Sie eine Mail 
(auf Englisch) an &lt;press@debian.org&gt; oder kontaktieren Sie das 
Stable-Veröffentlichungs-Team (auf Englisch) 
unter &lt;debian-release@lists.debian.org&gt;.</p>


